# Change Log\n\nAll notable changes to this project will be documented in this file. See [versionize](https://github.com/saintedlama/versionize) for commit guidelines.\n
<a name="0.4.0"></a>
## 0.4.0 (2021-2-25)

### Bug Fixes

* application hanging when mongodb connection cannot be established during startup (IndexerDistributedLockService.DisposeAsync won't throw an exception while disposing)
* Fixed `auction.highestBid`.
* serilog slack sink should now correctly send all the messages before the application shutdown
* update to new version of MongoDB.Driver (2.11.6) - should fix a CompositeServerSelector timeout issue
* **indexer:** fix indexing auction bids (was failing on setting bidCount)
* **indexer:** StartFromLevel configuration is now inclusive. The set block number is the first block to be indexed.

### Features

* Add mongoDbClient Timeout configuration setting + Improve mongoDb HealthCheck logging
* add new integration environment - autopublish docker images with 'integration' git tag
* Add remaining events (ClaimReverseRecord, UpdateReverseRecord, UpdateRecord, SetChildRecord,Commit)
* Added `id` GraphQL field on `bid` type.
* Added explicit `Exception` to `TezosClient` if the response isn't successful.
* always log version
* Changed `bidderBalances` GraphQL query to return empty result instead of null if corresponding database entry doesn't exist.
* improve mongoDb logging to allow multiple servers in connectionString
* Index auctions and provide them on API (GraphQL queries: auctions, auction, currentAuction)
* Index events (bid, settle, buy, renew, withdraw, outbid, auction-end) operations and provide them on API
* make mongoClient singleton
* remove purchases from indexer and api
* Renamed `block` history filter to `atBlock`.
* **api:** add additional sort (highest bid timestamp, number of bids) and filter (endsAt) for auctions graphQL
* **api:** Add addressReverseRecord, ownerReverseRecord to DomainGraphType
* **api:** add bidder balances to GraphQL Endpoint
* **api:** Add block graphType to Events
* **api:** Add BlockFilter to EventsFilter to be able to search by timestamp, level, hash
* **api:** Add Type to EventGraphType
* **api:** Changed unique params (id ->domainName + firstBidBlockLevel) for AuctionQuery, added FirstBidBlockLevel field to AuctionGraphType
* **api:** extend AuctionEndEvent with Participants, AuctionBidEvent with PreviousBidder and PreviousBidAmount
* **api:** extend block query to get latest index block (head)
* **api:** Extend reverseRecord graphType with domain graphType (resolved using name)
* **indexer:** Index user balances
* **indexer:** switch configuration to support edonet

<a name="0.3.2"></a>
## 0.3.2 (2020-11-28)

### Bug Fixes

* new buy entrypoint
* new buy entrypoints

<a name="0.3.1"></a>
## 0.3.1 (2020-11-25)

### Other

* fix Reorganization_ShouldRollbackAllOperations  integrationTest - increase wait period

<a name="0.3.0"></a>
## 0.3.0 (2020-11-25)

### Bug Fixes

* add Apollo-specific headers to CORS according to https://github.com/kamilkisiela/apollo-angular/issues/1591
* Dockerfile (after removal of TezosDomains.Indexer.Models project)
* **indexer**: index record/reverse_record data map as hexBytesString (do not decode values)

### Features

* **api**: Upgrade GraphQL packages to stable versions (GraphQL.Server 4.0.1, GraphQL 3.1.0)

<a name="0.2.1"></a>
## 0.2.1 (2020-10-27)

### Bug Fixes

* **api**: Added disclaimer that not all blocks are indexed but only from the configured one.
* **api**: Fixed `Id` of all `Node`-s to be non-nullable.
* **api**: Hardcoded secondary sort direction to be always ascending so that client app gets nice view of domains of ordered by expiresAt desc.
* **indexer**: Temporary quick fix for purchases TLD suffix (switched from 'tez' to 'delphi')
* **Indexer**: Improve rollback logging message

### Features

* Enhance Slack logging - send startup message
* **api**: Added `in` and `notIn` filter operators.
* **api**: Added composite filters `or`, `and` to main document filters.
* **api**: Added custom error if `block(...)` Graph query specifies `level` lower than the one from which we started indexing.
* **indexer**: add lock expiring warning
* **indexer**: Index data field on domains and reverse records
* **indexer**: switch contracts to delphinet
* **Indexer**: ReverseRecords are never removed

<a name="0.2.0"></a>
## 0.2.0 (2020-10-2)

### Bug Fixes

* **api**: Added disclaimer that not all blocks are indexed but only from the configured one.
* **api**: Fixed `Id` of all `Node`-s to be non-nullable.
* **api**: Hardcoded secondary sort direction to be always ascending so that client app gets nice view of domains of ordered by expiresAt desc.
* **indexer**: Improve rollback logging message

### Features

* **api**: Added `in` and `notIn` filter operators.
* **api**: Added composite filters `or`, `and` to main document filters.
* **api**: Added custom error if `block(...)` Graph query specifies `level` lower than the one from which we started indexing.
* **api**: Added `ReverseRecord` property on `Domain` in GraphQL
* **indexer**: Index data field on domains and reverse records
* **Indexer**: ReverseRecords are never removed
* Added Serilog slack integration to notify about production errors

<a name="0.1.0"></a>
## 0.1.0 (2020-9-9)

### Features

* Initial release of Indexer and GraphQL API

