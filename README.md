# Indexer & API (GraphQL)

## This project contains two parts:

- ### Tezos indexer

The indexer is indexing operations on [Tezos Domains contracts](https://gitlab.com/tezos-domains/contracts).

- ### API - GraphQL Endpoint

API Endpoint offers indexed data using GraphQL standard (schema is available as part of the [playground app](https://carthagenet-api.tezos.domains/playground)).

## Requirements:

- Tezos node - try with a free public node (e.g., https://giganode.io/)
- MongoDB instance - try with a free cloud instance (e.g., https://www.mongodb.com/cloud/atlas)

## Configuration:

Please update 'appsettings.json' with your tezos node and MongoDB instances.

```
{
  "TezosDomains": {
    "TezosNode": {
      "ConnectionString": "[tezos node url]"
    },
    "MongoDb": {
      "ConnectionString": "mongodb+srv://{Username}:{Password}@{Server}?{Database}retryWrites=true&w=majority"
    }
}
```

## How to run locally with .NET Core:

1. Configure
1. Start Indexer `dotnet run --project .\src\TezosDomains.Indexer\TezosDomains.Indexer.csproj`
   - Indexer exposes health page at
     - `http://localhost:5050/health`
     - `https://localhost:5051/health`
1. Start API `dotnet run --project .\src\TezosDomains.Api\TezosDomains.Api.csproj`
   - Access the GraphQL playground at
     - `http://localhost:5000/playground`
     - `https://localhost:5001/playground`
   - or use GraphQL endpoint at
     - `http://localhost:5000/graphql`
     - `https://localhost:5001/graphql`
1. (Optional) Run Tests `dotnet test`

## How to run locally with Docker:

1. Configure your ENV variables for MongoDB and Tezos node in `.env` file
1. Run `docker-compose up`
1. Access the GraphQL playground at `http://localhost:8080/playground` or use GraphQL endpoint at `http://localhost:8080/graphql`

## Release management:

This repository is using the ConventionalCommits (www.conventionalcommits.org) structure to generate a CHANGELOG file. Release build is triggered by pushing a version tag to origin. 

### Prerequisite:

* Install the dotnet tool [Versionize](https://github.com/saintedlama/versionize) (cmd: `dotnet tool install --global Versionize`)
* Create commit messages with the ConventionalCommits format (i.e. `feat(indexer): new feature added`)

### How to use:

1. Run `versionize` in the project directory (Command will increase version in csprojs, updated CHANGELOG.md and commit the changes with version tag `[major].[minor].[patch]`)
1. Push commit to origin (GitLab pipeline will automatically create release packages based on the tag version)
