using System;
using NUnit.Framework;

namespace TezosDomains.Data.Tests
{
    [TestFixture]
    public class DomainNameUtilsTests
    {
        [TestCase("tez", "tez", new string[0], null)]
        [TestCase("domains.tez", "domains", new string[] { "tez" }, "tez")]
        [TestCase("third.domains.tez", "third", new string[] { "tez", "domains.tez" }, "domains.tez")]
        public void Parse_IsSuccessful(string name, string label, string[] ancestors, string? parent)
        {
            var result = DomainNameUtils.Parse(name);

            Assert.That(result.Label, Is.EqualTo(label));
            Assert.That(result.Ancestors, Is.EqualTo(ancestors));
            Assert.That(result.Parent, Is.EqualTo(parent));
        }

        [TestCase("")]
        [TestCase(null)]
        [TestCase(".")]
        public void Parse_FailsWithInvalidArguments(string fullName)
        {
            Assert.Throws<ArgumentException>(() => DomainNameUtils.Parse(fullName));
        }

        [TestCase("tez", "tez")]
        [TestCase("domains.tez", "tez.domains")]
        [TestCase("first.domains.tez", "tez.domains.first")]
        public void Parse_NameReverse_IsSuccessful(string name, string nameReverse)
        {
            var result = DomainNameUtils.Parse(name);

            Assert.That(result.NameReverse, Is.EqualTo(nameReverse));
        }

        [TestCase("tez", 1)]
        [TestCase("domains.tez", 2)]
        [TestCase("first.domains.tez", 3)]
        public void Parse_Level_IsSuccessful(string name, int level)
        {
            var result = DomainNameUtils.Parse(name);

            Assert.That(result.Level, Is.EqualTo(level));
        }
    }
}