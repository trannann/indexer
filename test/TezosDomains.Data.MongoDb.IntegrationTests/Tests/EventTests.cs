﻿using FluentAssertions;
using MongoDB.Driver;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Data.MongoDb.IntegrationTests.TestData;
using TezosDomains.Data.MongoDb.Services;

namespace TezosDomains.Data.MongoDb.IntegrationTests.Tests
{
    [TestFixture]
    public class EventTests : TestBase
    {
        [Test]
        public async Task StoreAllEventTypes()
        {
            //arrange
            await UpdateService.StoreUpdatesAsync(
                BlockTestFactory.Create(),
                new BlockUpdates()
                {
                    AuctionBidUpdates =
                        { new AuctionBidUpdate("auction-settle.ddd", "bidderAddr", 1, DateTime.UtcNow.AddDays(-1), DateTime.UtcNow.AddYears(1)) }
                },
                null,
                CancellationToken.None
            );

            var block2 = BlockTestFactory.Create();
            var block = block2.ToSlim();

            #region Event setup

            var auctionWithdrawEvent = TestEvent.Auction.Withdraw("address2", "bbb", 10, block, "ogh2");
            var auctionSettleEvent = TestEvent.Auction.Settle(
                "address6",
                "auction-settle.ddd",
                "address7-own",
                "address8-fwd",
                new Dictionary<string, string> { { "k1", "v1" } },
                block,
                "ogh5"
            );
            var events = new Event[]
            {
                TestEvent.Auction.Bid("address1", "auction-bid.aaa", 1, block, "ogh1"),
                auctionWithdrawEvent,
                TestEvent.Auction.End("address3", 2, "address4", block),
                auctionSettleEvent,
                TestEvent.Domain.Buy(
                    "address9",
                    "domain-buy.eee",
                    4,
                    365,
                    "address10-own",
                    "address11-fwd",
                    new Dictionary<string, string> { { "k2", "v2" } },
                    block,
                    "ogh6"
                ),
                TestEvent.Domain.Renew(
                    "address10",
                    "domain-renew.fff",
                    5,
                    365 * 2,
                    block,
                    "ogh7"
                ),
                TestEvent.Domain.Commit(
                    "address11",
                    block,
                    "ogh8"
                ),
                TestEvent.Domain.Update(
                    "address12",
                    "domain-update.ggg",
                    "address12-own",
                    "address12-fwd",
                    new Dictionary<string, string> { { "k3", "v3" } },
                    block,
                    "ogh9"
                ),
                TestEvent.Domain.SetChildRecord(
                    "address13",
                    "domain-set-child-record.hhh",
                    "address13-own",
                    "address13-fwd",
                    new Dictionary<string, string> { { "k4", "v4" } },
                    block,
                    "ogh10"
                ),
                TestEvent.ReverseRecord.Claim(
                    "address14",
                    null,
                    "address14-own",
                    block,
                    "ogh11"
                ),
                TestEvent.ReverseRecord.Update(
                    "address15",
                    "reverse-record.iii",
                    "address15-own",
                    "address15-addr",
                    block,
                    "ogh12"
                ),
            };

            #endregion

            //act
            var updates = new BlockUpdates();
            updates.Events.AddRange(events);
            await UpdateService.StoreUpdatesAsync(
                block2,
                updates,
                null,
                CancellationToken.None
            );

            //assert
            updates.Events.Remove(auctionSettleEvent);
            updates.Events.Add(auctionSettleEvent.Clone(366, 1));
            var dbEvents = await Context.Get<Event>().Find(e => true).ToListAsync();
            dbEvents.Should().BeEquivalentTo(updates.Events, o => o.RespectingRuntimeTypes());

            var t = await new DocumentQueryService<Event>(Context).Collection.Find(FilterDefinition<Event>.Empty).ToListAsync();
            dbEvents.Should().BeEquivalentTo(t, o => o.RespectingRuntimeTypes().WithoutStrictOrdering());
        }

        [Test]
        public async Task UpdateAuctionBidEvent_When_BidsComeInSeparateBlocks()
        {
            //arrange
            var block1 = BlockTestFactory.Create();
            var block1Bid = TestEvent.Auction.Bid(
                sourceAddress: "address1",
                domainName: "auction-bid.aaa",
                bidAmount: 1,
                block: block1.ToSlim(),
                "ogh1"
            );
            var blockUpdates1 = new BlockUpdates()
            {
                Events = { block1Bid },
                AuctionBidUpdates =
                {
                    TestAuctionUpdate.GetBid(
                        domainName: "auction-bid.aaa",
                        bidder: "address1",
                        amount: 1,
                        auctionEndInUtc: block1.Timestamp.AddYears(value: 1),
                        ownedUntilUtc: block1.Timestamp.AddYears(value: 2)
                    )
                }
            };
            await UpdateService.StoreUpdatesAsync(block: block1, updates: blockUpdates1, previousBlock: null, cancellationToken: CancellationToken.None);

            //act
            var block2 = BlockTestFactory.Create();
            var block2Bid = TestEvent.Auction.Bid(
                sourceAddress: "address2",
                domainName: "auction-bid.aaa",
                bidAmount: 2,
                block: block2.ToSlim(),
                "ogh2"
            );
            var blockUpdates2 = new BlockUpdates { Events = { block2Bid } };
            await UpdateService.StoreUpdatesAsync(block: block2, updates: blockUpdates2, previousBlock: block1, cancellationToken: CancellationToken.None);

            //assert
            var dbEvents = await Context.Get<Event>().Find(filter: e => true).ToListAsync();
            dbEvents.Should()
                .BeEquivalentTo(
                    blockUpdates1.Events.Concat(new[] { block2Bid.Clone(block1Bid.SourceAddress, block1Bid.BidAmount) }),
                    o => o.RespectingRuntimeTypes()
                );
        }

        [Test]
        public async Task UpdateAuctionBidEvent_When_BidsComeInSameBlock()
        {
            //arrange
            var block1 = BlockTestFactory.Create();
            var secondBid = TestEvent.Auction.Bid(
                sourceAddress: "address2",
                domainName: "auction-bid.aaa",
                bidAmount: 2,
                block: block1.ToSlim(),
                "ogh1"
            );
            var firstBid = TestEvent.Auction.Bid(
                sourceAddress: "address1",
                domainName: "auction-bid.aaa",
                bidAmount: 1,
                block: block1.ToSlim(),
                "ogh2"
            );
            var blockUpdates1 = new BlockUpdates()
            {
                Events =
                {
                    secondBid,
                    firstBid
                }
            };

            //act
            await UpdateService.StoreUpdatesAsync(block: block1, updates: blockUpdates1, previousBlock: null, cancellationToken: CancellationToken.None);

            //assert
            var dbEvents = await Context.Get<Event>().Find(filter: e => true).ToListAsync();
            dbEvents.Should()
                .BeEquivalentTo(
                    new[] { firstBid, secondBid.Clone(firstBid.SourceAddress, firstBid.BidAmount) },
                    o => o.RespectingRuntimeTypes()
                );
        }

        [Test]
        public async Task GenerateAuctionEndEventAndStore()
        {
            var block1 = BlockTestFactory.Create();
            var blockUpdates1 = new BlockUpdates()
            {
                AuctionBidUpdates =
                {
                    TestAuctionUpdate.GetBid(
                        domainName: "auction-end.aaa",
                        bidder: "address1",
                        amount: 1,
                        auctionEndInUtc: block1.Timestamp.AddSeconds(1),
                        ownedUntilUtc: block1.Timestamp.AddYears(1)
                    )
                }
            };
            await UpdateService.StoreUpdatesAsync(block: block1, updates: blockUpdates1, previousBlock: null, cancellationToken: CancellationToken.None);

            //act
            var block2 = BlockTestFactory.Create();
            var blockUpdates2 = new BlockUpdates();
            await UpdateService.StoreUpdatesAsync(block: block2, updates: blockUpdates2, previousBlock: block1, cancellationToken: CancellationToken.None);

            //assert
            var dbEvents = await Context.Get<Event>().Find(filter: e => true).ToListAsync();
            var expectations = new[] { TestEvent.Auction.End("auction-end.aaa", 1, "address1", block2.ToSlim()) };
            dbEvents.Should().BeEquivalentTo(expectations, o => o.RespectingRuntimeTypes().Excluding(e => e.Id));
        }

        [Test]
        public async Task ProcessAuctionSettleEventAndStore()
        {
            var block1 = BlockTestFactory.Create();
            var blockUpdates1 = new BlockUpdates()
            {
                AuctionBidUpdates =
                {
                    TestAuctionUpdate.GetBid(
                        domainName: "auction-settle.aaa",
                        bidder: "address1",
                        amount: 10,
                        auctionEndInUtc: block1.Timestamp,
                        ownedUntilUtc: block1.Timestamp.AddYears(1)
                    )
                }
            };
            await UpdateService.StoreUpdatesAsync(block: block1, updates: blockUpdates1, previousBlock: null, cancellationToken: CancellationToken.None);

            //act
            var block2 = BlockTestFactory.Create();
            var auctionSettleEvent = TestEvent.Auction.Settle(
                "address1",
                "auction-settle.aaa",
                "address1",
                default,
                new Dictionary<string, string>(),
                block2.ToSlim(),
                "ogh1"
            );
            var blockUpdates2 = new BlockUpdates() { Events = { auctionSettleEvent } };
            await UpdateService.StoreUpdatesAsync(block: block2, updates: blockUpdates2, previousBlock: block1, cancellationToken: CancellationToken.None);
            var expectedEvent = auctionSettleEvent.Clone(365, 10);

            //assert
            var dbEvents = await Context.Get<Event>().Find(filter: e => true).ToListAsync();
            dbEvents.Should().BeEquivalentTo(new[] { expectedEvent }, o => o.RespectingRuntimeTypes());
        }

        [Test]
        public async Task ProcessAuctionWithdrawEventAndStore()
        {
            //arrange + act
            var block = BlockTestFactory.Create();
            var auctionWithdrawEvent1 = TestEvent.Auction.Withdraw(
                "address1",
                "a2",
                amount: 100,
                block: block.ToSlim(),
                operationGroupHash: "ogh1"
            );
            var auctionWithdrawEvent2 = TestEvent.Auction.Withdraw(
                "address1",
                "a3",
                amount: 1000,
                block: block.ToSlim(),
                operationGroupHash: "ogh1"
            );
            var blockUpdates2 = new BlockUpdates()
            {
                Events = { auctionWithdrawEvent1, auctionWithdrawEvent2 }
            };
            await UpdateService.StoreUpdatesAsync(block: block, updates: blockUpdates2, previousBlock: null, cancellationToken: CancellationToken.None);

            //assert
            var dbEvents = await Context.Get<Event>().Find(filter: e => true).ToListAsync();
            dbEvents.Should().BeEquivalentTo(blockUpdates2.Events, o => o.RespectingRuntimeTypes());
        }

        [Test]
        public async Task ProcessDomainSetChildRecordEventAndStore()
        {
            //arrange
            var block1 = BlockTestFactory.Create();
            await UpdateService.StoreUpdatesAsync(
                block: block1,
                updates: new BlockUpdates()
                {
                    DomainUpdates = { TestDomainUpdate.Get("existing.sub.domain", "owner") },
                },
                previousBlock: null,
                cancellationToken: CancellationToken.None
            );

            //act
            var block2 = BlockTestFactory.Create();
            var newSubdomainChange = TestEvent.Domain.SetChildRecord(
                "source1",
                "new.sub.domain",
                "owner",
                null,
                new Dictionary<string, string>(),
                block1.ToSlim(),
                "ogh1"
            );
            var existingSubdomainChange = TestEvent.Domain.SetChildRecord(
                "source1",
                "existing.sub.domain",
                "owner",
                null,
                new Dictionary<string, string>(),
                block1.ToSlim(),
                "ogh2"
            );
            var blockUpdates2 = new BlockUpdates() { Events = { newSubdomainChange, existingSubdomainChange, } };
            await UpdateService.StoreUpdatesAsync(block: block2, updates: blockUpdates2, previousBlock: block1, cancellationToken: CancellationToken.None);
            var expectedEvents = new[] { newSubdomainChange, existingSubdomainChange.Clone(false) };

            //assert
            var dbEvents = await Context.Get<Event>().Find(filter: e => true).ToListAsync();
            dbEvents.Should().BeEquivalentTo(expectedEvents, o => o.RespectingRuntimeTypes());
        }
    }
}