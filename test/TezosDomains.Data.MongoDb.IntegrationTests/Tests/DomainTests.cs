﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using MongoDB.Driver;
using NUnit.Framework;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Data.MongoDb.IntegrationTests.TestData;

namespace TezosDomains.Data.MongoDb.IntegrationTests.Tests
{
    public class DomainTests : TestBase
    {
        [Test]
        public async Task Create()
        {
            var block = BlockTestFactory.Create();
            var createUpdates = new[]
            {
                TestDomainUpdate.Get("domain-update.create-domain", "owner", validityKey: "domain-update.create-domain"),
                TestDomainUpdate.Get("address.domain-update.create-domain", "new-owner", "address", validityKey: "domain-update.create-domain"),
                TestDomainUpdate.Get(
                    "always-valid.domain-update.create-domain",
                    "new-owner",
                    "address",
                    data: new Dictionary<string, string>() { { "ttl", "343230" } }
                )
            };
            var data = new BlockUpdates();
            data.DomainUpdates.AddRange(createUpdates.ToList());

            await UpdateService.StoreUpdatesAsync(block, data, previousBlock: null, CancellationToken.None);

            var filterDefinition = Builders<Domain>.Filter.Eq(d => d.Ancestors[0], "create-domain");
            var result = await Context.Get<Domain>().Find(filterDefinition).ToListAsync();
            result.Should().BeEquivalentTo(createUpdates, "");

            var domains = result.ToDictionary(d => d.Name);
            var createUpdatePairs = CreateBlockUpdatePairs(block, null, createUpdates);
            await AssertHistoryAsync(domains["domain-update.create-domain"], createUpdatePairs[0]);
            await AssertHistoryAsync(domains["address.domain-update.create-domain"], createUpdatePairs[1]);
            await AssertHistoryAsync(domains["always-valid.domain-update.create-domain"], createUpdatePairs[2]);
        }

        [Test]
        public async Task Modify()
        {
            //arrange
            var filterDefinition = Builders<Domain>.Filter.Eq(d => d.Ancestors[0], "modify-domain");
            var createUpdates = new[]
            {
                TestDomainUpdate.Get("domain-update.modify-domain", "owner", validityKey: "domain-update.modify-domain"),
                TestDomainUpdate.Get("address.domain-update.modify-domain", "new-owner", "address", validityKey: "domain-update.modify-domain"),
                TestDomainUpdate.Get(
                    "always-valid.domain-update.modify-domain",
                    "new-owner",
                    "address",
                    data: new Dictionary<string, string>() { { "ttl", "343230" } }
                ),
            };
            var createBlock = BlockTestFactory.Create();
            var blockUpdates = new BlockUpdates();
            blockUpdates.DomainUpdates.AddRange(createUpdates);
            await UpdateService.StoreUpdatesAsync(createBlock, blockUpdates, previousBlock: null, CancellationToken.None);
            var currentData = await Context.Get<Domain>().Find(filterDefinition).ToListAsync();
            currentData.Should().BeEquivalentTo(createUpdates, "currentData were created in `Domain_IsCreated` test");
            var modifyUpdates = new[]
            {
                TestDomainUpdate.Get("domain-update.modify-domain", "owner2", "address1", validityKey: "domain-update.modify-domain"),
                TestDomainUpdate.Get("address.domain-update.modify-domain", "owner", "address", validityKey: "domain-update.modify-domain"),
                TestDomainUpdate.Get("created-during-update.domain-update.modify-domain", "update-owner"),
            };
            var updateBlock = BlockTestFactory.Create();
            var data = new BlockUpdates();
            data.DomainUpdates.AddRange(modifyUpdates);

            //act
            await UpdateService.StoreUpdatesAsync(updateBlock, data, previousBlock: null, CancellationToken.None);
            var newData = await Context.Get<Domain>().Find(filterDefinition).ToListAsync();

            //assert
            newData.Should()
                .BeEquivalentTo(
                    modifyUpdates.Concat(createUpdates.Skip(2)),
                    "First 2 domains were updated, `always-valid.domain-update.modify-domain` was not touched and new `created-during-update.domain-update.modify-domain` was added"
                );

            var domains = newData.ToDictionary(d => d.Name);
            var createPairs = CreateBlockUpdatePairs(createBlock, updateBlock, createUpdates);
            var modifyPairs = CreateBlockUpdatePairs(updateBlock, null, modifyUpdates);
            await AssertHistoryAsync(domains["domain-update.modify-domain"], createPairs[0], modifyPairs[0]);
            await AssertHistoryAsync(domains["address.domain-update.modify-domain"], createPairs[1], modifyPairs[1]);
            await AssertHistoryAsync(domains["always-valid.domain-update.modify-domain"], CreateBlockUpdatePairs(createBlock, null, createUpdates[2]));
            await AssertHistoryAsync(domains["created-during-update.domain-update.modify-domain"], CreateBlockUpdatePairs(updateBlock, null, modifyUpdates[2]));
        }
    }
}