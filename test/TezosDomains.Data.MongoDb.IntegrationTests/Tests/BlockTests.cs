﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using MongoDB.Driver;
using NUnit.Framework;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Updates;

namespace TezosDomains.Data.MongoDb.IntegrationTests.Tests
{
    [TestFixture]
    public class BlockTests : TestBase
    {
        [Test]
        public async Task StoreBlockInDb()
        {
            var block = BlockTestFactory.Create();
            await UpdateService.StoreUpdatesAsync(
                block,
                new BlockUpdates(),
                null,
                CancellationToken.None
            );
            var blocks = await Context.Blocks.Find(b => b.Hash == block.Hash).ToListAsync();
            blocks.Single().Should().BeEquivalentTo(block);
        }


        [Test]
        public async Task StoreBlockInDb_ShouldFail_WhenStoredBlockIsNotASuccessor()
        {
            //arrange
            var block = BlockTestFactory.Create();
            var blockWithUpdates = new BlockUpdates();
            await UpdateService.StoreUpdatesAsync(
                block,
                blockWithUpdates,
                null,
                CancellationToken.None
            );

            //act+assert
            //store same block twice
            Assert.ThrowsAsync<InvalidOperationException>(
                async () => await UpdateService.StoreUpdatesAsync(block, blockWithUpdates, previousBlock: null, CancellationToken.None)
            );
            //store with lower level

            var block2 = new Block(block.Level - 1, "invalid_block", DateTime.UtcNow.Date, "bla");
            var blockWithUpdates2 = new BlockUpdates();
            Assert.ThrowsAsync<InvalidOperationException>(
                async () => await UpdateService.StoreUpdatesAsync(block2, blockWithUpdates2, previousBlock: null, CancellationToken.None)
            );
        }
    }
}