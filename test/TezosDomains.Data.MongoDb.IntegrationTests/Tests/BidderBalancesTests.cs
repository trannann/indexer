﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using MongoDB.Driver;
using NUnit.Framework;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Data.MongoDb.IntegrationTests.TestData;

namespace TezosDomains.Data.MongoDb.IntegrationTests.Tests
{
    public class BidderBalancesTests : TestBase
    {
        [Test]
        public async Task CreateBidderBalances_WhenAddingFirstBidderBalancesUpdate()
        {
            var block = BlockTestFactory.Create();
            var update = TestBidderBalancesUpdate.Get("tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", "tld1", 1);

            await UpdateService.StoreUpdatesAsync(block, new BlockUpdates() { BidderBalancesUpdates = { update } }, null, CancellationToken.None);

            var user = await Context.Get<BidderBalances>().Find(FilterDefinition<BidderBalances>.Empty).SingleAsync();
            user.Address.Should().Be("tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n");
            user.Balances.Should().BeEquivalentTo(new Dictionary<string, decimal?> { { "tld1", 1 } });
            user.Block.Should().BeEquivalentTo(block.ToSlim());
            user.ValidUntilBlockLevel.Should().BeNull();
            user.ValidUntilTimestamp.Should().BeNull();
        }

        [Test]
        public async Task UpdateBidderBalances_WhenAddingUpdatingBalanceOnExistingTld()
        {
            //arrange
            var block1 = BlockTestFactory.Create();
            var update1 = TestBidderBalancesUpdate.Get("tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", "tld1", 1);
            var update2 = TestBidderBalancesUpdate.Get("tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", "tld2", 10);

            await UpdateService.StoreUpdatesAsync(block1, new BlockUpdates() { BidderBalancesUpdates = { update1, update2 } }, null, CancellationToken.None);
            var user1 = await Context.Get<BidderBalances>().Find(FilterDefinition<BidderBalances>.Empty).SingleAsync();

            //act
            var block2 = BlockTestFactory.Create();
            var update10 = TestBidderBalancesUpdate.Get("tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", "tld1", 50);

            await UpdateService.StoreUpdatesAsync(block2, new BlockUpdates() { BidderBalancesUpdates = { update10 } }, null, CancellationToken.None);

            //assert
            var user = await Context.Get<BidderBalances>().Find(FilterDefinition<BidderBalances>.Empty).SingleAsync();
            user.Address.Should().Be("tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n");
            user.Balances.Should().BeEquivalentTo(new Dictionary<string, decimal?> { { "tld1", 50 }, { "tld2", 10 } });
            user.Block.Should().BeEquivalentTo(block2.ToSlim());
            user.ValidUntilBlockLevel.Should().BeNull();
            user.ValidUntilTimestamp.Should().BeNull();
            var history = await Context.GetHistory<BidderBalances>().Find(_ => true).SingleAsync();
            history.History.Should().BeEquivalentTo(new[] { user1, user }, o => o.Excluding(e => e.ValidUntilBlockLevel).Excluding(e => e.ValidUntilTimestamp));
        }

        [Test]
        public async Task RemoveBidderBalances_WhenUpdateBalanceIsNull()
        {
            //arrange
            var block1 = BlockTestFactory.Create();
            var update1 = TestBidderBalancesUpdate.Get("tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", "tld1", 1);
            var update2 = TestBidderBalancesUpdate.Get("tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", "tld2", 10);

            await UpdateService.StoreUpdatesAsync(block1, new BlockUpdates() { BidderBalancesUpdates = { update1, update2 } }, null, CancellationToken.None);
            var user1 = await Context.Get<BidderBalances>().Find(FilterDefinition<BidderBalances>.Empty).SingleAsync();

            //act
            var block2 = BlockTestFactory.Create();
            var update10 = TestBidderBalancesUpdate.Get("tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", "tld1", null);

            await UpdateService.StoreUpdatesAsync(block2, new BlockUpdates() { BidderBalancesUpdates = { update10 } }, null, CancellationToken.None);

            //assert
            var user = await Context.Get<BidderBalances>().Find(FilterDefinition<BidderBalances>.Empty).SingleAsync();
            user.Address.Should().Be("tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n");
            user.Balances.Should().BeEquivalentTo(new Dictionary<string, decimal?> { { "tld2", 10 } });
            user.Block.Should().BeEquivalentTo(block2.ToSlim());
            user.ValidUntilBlockLevel.Should().BeNull();
            user.ValidUntilTimestamp.Should().BeNull();
            var history = await Context.GetHistory<BidderBalances>().Find(_ => true).SingleAsync();
            history.History.Should().BeEquivalentTo(new[] { user1, user }, o => o.Excluding(e => e.ValidUntilBlockLevel).Excluding(e => e.ValidUntilTimestamp));
        }
    }
}