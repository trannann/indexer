﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using MongoDB.Driver;
using NUnit.Framework;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Data.MongoDb.IntegrationTests.TestData;

namespace TezosDomains.Data.MongoDb.IntegrationTests.Tests
{
    public class AuctionTests : TestBase
    {
        [Test]
        public async Task CreateAuction_WhenAddingFirstBid()
        {
            var block = BlockTestFactory.Create();
            var today = DateTime.UtcNow.Date;
            var createUpdates = new[]
            {
                TestAuctionUpdate.GetBid("1.auction", "address1", 1, today.AddDays(1), today.AddDays(2)),
                TestAuctionUpdate.GetBid("2.auction", "address2", 1, today.AddDays(1), today.AddDays(2))
            };
            var data = new BlockUpdates();
            data.AuctionBidUpdates.AddRange(createUpdates.ToList());

            await UpdateService.StoreUpdatesAsync(block, data, previousBlock: null, CancellationToken.None);

            var filterDefinition = Builders<Auction>.Filter.Where(d => d.DomainName.EndsWith(".auction"));
            var result = await Context.Get<Auction>().Find(filterDefinition).ToListAsync();
            var auctionAsserts = MapUpdatesToAuctions(createUpdates, block.ToSlim());
            result.Should().BeEquivalentTo(auctionAsserts);
        }

        private static List<Auction> MapUpdatesToAuctions(
            AuctionBidUpdate[] createUpdates,
            BlockSlim block,
            (AuctionBidUpdate[] updates, BlockSlim Block)? modifyUpdates = null
        )
        {
            var modifyByDomain = (modifyUpdates?.updates ?? new AuctionBidUpdate[0]).ToDictionary(u => u.DomainName);
            return createUpdates.Select(
                    createUpdate =>
                    {
                        var lastUpdate = modifyByDomain.GetValueOrDefault(createUpdate.DomainName, createUpdate)!;
                        return new Auction(
                            id: Auction.GenerateId(lastUpdate!.DomainName, block.Level),
                            domainName: lastUpdate.DomainName,
                            bids: modifyUpdates.HasValue
                                ? new[] { Map(modifyUpdates.Value.Block, modifyByDomain[createUpdate.DomainName]), Map(block, createUpdate) }
                                : new[] { Map(block, createUpdate) },
                            endsAtUtc: lastUpdate.AuctionEndInUtc,
                            block: modifyUpdates?.Block ?? block,
                            validUntilBlockLevel: null,
                            validUntilTimestamp: null,
                            ownedUntilUtc: lastUpdate.OwnedUntilUtc,
                            isSettled: false,
                            firstBidAtUtc: block.Timestamp,
                            bidCount: modifyUpdates.HasValue ? 2 : 1
                        );
                    }
                )
                .ToList();
        }

        private static Bid Map(BlockSlim block, AuctionBidUpdate c) => new Bid(block, c.Bidder, c.Amount);

        [Test]
        public async Task UpdateAuction_WhenAddingSecondBid()
        {
            //arrange
            var block = BlockTestFactory.Create();
            var today = DateTime.UtcNow.Date;
            var createUpdates = new[]
            {
                TestAuctionUpdate.GetBid("3.auction", "address1", 1, today.AddDays(1), today.AddDays(3)),
                TestAuctionUpdate.GetBid("4.auction", "address2", 1, today.AddDays(1), today.AddDays(3))
            };
            var data = new BlockUpdates();
            data.AuctionBidUpdates.AddRange(createUpdates.ToList());

            await UpdateService.StoreUpdatesAsync(block, data, previousBlock: null, CancellationToken.None);

            var filterDefinition = Builders<Auction>.Filter.Where(d => d.DomainName.EndsWith(".auction"));
            var result = await Context.Get<Auction>().Find(filterDefinition).ToListAsync();
            var auctionAsserts = MapUpdatesToAuctions(createUpdates, block.ToSlim());
            result.Should().BeEquivalentTo(auctionAsserts);


            var updateBlock = BlockTestFactory.Create();
            var modifyUpdates = new[]
            {
                TestAuctionUpdate.GetBid("3.auction", "address3", 2, today.AddDays(1), today.AddDays(3)),
                TestAuctionUpdate.GetBid("4.auction", "address4", 10, today.AddDays(2), today.AddDays(3))
            };
            var updatedData = new BlockUpdates();
            updatedData.AuctionBidUpdates.AddRange(modifyUpdates.ToList());

            await UpdateService.StoreUpdatesAsync(updateBlock, updatedData, previousBlock: null, CancellationToken.None);
            var updatedResult = await Context.Get<Auction>().Find(filterDefinition).ToListAsync();
            var updatedAuctionAsserts = MapUpdatesToAuctions(createUpdates, block.ToSlim(), (modifyUpdates, updateBlock.ToSlim()));
            updatedResult.Should().BeEquivalentTo(updatedAuctionAsserts);
        }
    }
}