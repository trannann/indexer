﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using MongoDB.Driver;
using NUnit.Framework;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.IntegrationTests.TestData;

namespace TezosDomains.Data.MongoDb.IntegrationTests.Tests
{
    public class ValidityTests : TestBase
    {
        [Test]
        public async Task WithValidityUpdate_Create_DomainWithReverseRecord()
        {
            //arrange
            var expiresAtUtc = DateTime.Today.AddYears(1).ToUniversalTime();
            var block = BlockTestFactory.Create();
            var name = "domain.create-validity";
            var sub = "sub." + name;
            var address = "address.create-validity";
            var subAddress = "sub" + address;
            var updates = new BlockUpdates()
            {
                DomainUpdates = { TestDomainUpdate.Get(name, "owner1", validityKey: name), TestDomainUpdate.Get(sub, "owner11", validityKey: name) },
                ReverseRecordUpdates = { TestReverseRecordUpdate.Get(address, "owner2", name), TestReverseRecordUpdate.Get(subAddress, "owner22", sub) },
                ValidityUpdates = { new ValidityUpdate(name, expiresAtUtc) }
            };

            //act
            await UpdateService.StoreUpdatesAsync(block, updates, previousBlock: null, CancellationToken.None);

            //assert
            var domain = await Context.Get<Domain>().Find(d => d.Name == name).SingleAsync();
            domain.Should().BeEquivalentTo(updates.DomainUpdates[0]);
            domain.ExpiresAtUtc.Should().Be(expiresAtUtc);
            var domainHistory = await Context.GetHistory<Domain>().FindById(name).SingleAsync();
            domainHistory.History.Should().AllBeEquivalentTo(updates.DomainUpdates[0]);
            domainHistory.History.Single().Should().BeEquivalentTo(domain);

            var subdomain = await Context.Get<Domain>().FindById(sub).SingleAsync();
            subdomain.Should().BeEquivalentTo(updates.DomainUpdates[1]);
            subdomain.ExpiresAtUtc.Should().Be(expiresAtUtc);
            var subdomainHistory = await Context.GetHistory<Domain>().FindById(sub).SingleAsync();
            subdomainHistory.History.Should().AllBeEquivalentTo(updates.DomainUpdates[1]);
            subdomainHistory.History.Single().Should().BeEquivalentTo(subdomain);

            var record = await Context.Get<ReverseRecord>().FindById(address).SingleAsync();
            record.Should().BeEquivalentTo(updates.ReverseRecordUpdates[0]);
            record.ExpiresAtUtc.Should().Be(expiresAtUtc);
            var recordHistory = await Context.GetHistory<ReverseRecord>().FindById(address).SingleAsync();
            recordHistory.History.Single().Should().BeEquivalentTo(record);

            var subRecord = await Context.Get<ReverseRecord>().FindById(subAddress).SingleAsync();
            subRecord.Should().BeEquivalentTo(updates.ReverseRecordUpdates[1]);
            subRecord.ExpiresAtUtc.Should().Be(expiresAtUtc);
            var subRecordHistory = await Context.GetHistory<ReverseRecord>().FindById(subAddress).SingleAsync();
            subRecordHistory.History.Single().Should().BeEquivalentTo(subRecord);
        }

        [TestCase("domain.create-null-validity")]
        [TestCase(null)]
        public async Task NoValidityUpdate_Create_DomainWithReverseRecord_ShouldBeValidForever(string? validityKey)
        {
            //arrange
            var block = BlockTestFactory.Create();
            var name = $"domain{validityKey != null}.create-null-validity";
            var sub = "sub." + name;
            var address = $"address{validityKey != null}.create-null-validity";
            var subAddress = "sub" + address;
            var updates = new BlockUpdates()
            {
                DomainUpdates =
                {
                    TestDomainUpdate.Get(name, "owner1", validityKey: validityKey), TestDomainUpdate.Get(sub, "owner11", validityKey: validityKey)
                },
                ReverseRecordUpdates = { TestReverseRecordUpdate.Get(address, "owner2", name), TestReverseRecordUpdate.Get(subAddress, "owner22", sub) }
            };

            //act
            await UpdateService.StoreUpdatesAsync(block, updates, previousBlock: null, CancellationToken.None);

            //assert
            var domain = await Context.Get<Domain>().Find(d => d.Name == name).SingleAsync();
            domain.Should().BeEquivalentTo(updates.DomainUpdates[0]);
            domain.ExpiresAtUtc.Should().BeNull();
            var domainHistory = await Context.GetHistory<Domain>().FindById(name).SingleAsync();
            domainHistory.History.Should().AllBeEquivalentTo(updates.DomainUpdates[0]);
            domainHistory.History.Single().Should().BeEquivalentTo(domain);

            var subdomain = await Context.Get<Domain>().Find(d => d.Name == sub).SingleAsync();
            subdomain.Should().BeEquivalentTo(updates.DomainUpdates[1]);
            subdomain.ExpiresAtUtc.Should().BeNull();
            var subdomainHistory = await Context.GetHistory<Domain>().FindById(sub).SingleAsync();
            subdomainHistory.History.Should().AllBeEquivalentTo(updates.DomainUpdates[1]);
            subdomainHistory.History.Single().Should().BeEquivalentTo(subdomain);

            var record = await Context.Get<ReverseRecord>().FindById(address).SingleAsync();
            record.Should().BeEquivalentTo(updates.ReverseRecordUpdates[0]);
            record.ExpiresAtUtc.Should().BeNull();
            var recordHistory = await Context.GetHistory<ReverseRecord>().FindById(address).SingleAsync();
            recordHistory.History.Single().Should().BeEquivalentTo(record);

            var subRecord = await Context.Get<ReverseRecord>().FindById(subAddress).SingleAsync();
            subRecord.Should().BeEquivalentTo(updates.ReverseRecordUpdates[1]);
            subRecord.ExpiresAtUtc.Should().BeNull();
            var subRecordHistory = await Context.GetHistory<ReverseRecord>().FindById(subAddress).SingleAsync();
            subRecordHistory.History.Single().Should().BeEquivalentTo(subRecord);
        }

        [TestCase(null)]
        [TestCase(10)]
        public async Task WithValidityUpdate_Update_ExistingDomainAndReverseRecord(int? extendedValidityInYears)
        {
            //arrange
            var expiresAtUtc = DateTime.Today.ToUniversalTime();

            var name = $"domain{extendedValidityInYears}.update-validity";
            var sub = $"sub.{name}";
            var address = $"address{extendedValidityInYears}.update-validity";
            var subAddress = "sub" + address;
            var updates = new BlockUpdates()
            {
                DomainUpdates = { TestDomainUpdate.Get(name, "owner1", validityKey: name), TestDomainUpdate.Get(sub, "owner11", "addr11", name) },
                ReverseRecordUpdates = { TestReverseRecordUpdate.Get(address, "owner2", name), TestReverseRecordUpdate.Get(subAddress, "owner22", sub) },
                ValidityUpdates = { new ValidityUpdate(name, expiresAtUtc) }
            };

            await UpdateService.StoreUpdatesAsync(BlockTestFactory.Create(), updates, previousBlock: null, CancellationToken.None);


            //act
            var newValidity = extendedValidityInYears.HasValue ? expiresAtUtc.AddYears(extendedValidityInYears.Value) : (DateTime?) null;
            await UpdateService.StoreUpdatesAsync(
                BlockTestFactory.Create(),
                new BlockUpdates { ValidityUpdates = { new ValidityUpdate(name, newValidity) } },
                null,
                CancellationToken.None
            );

            //assert
            var domain = await Context.Get<Domain>().Find(d => d.Name == name).SingleAsync();
            domain.Should().BeEquivalentTo(updates.DomainUpdates[0]);
            domain.ExpiresAtUtc.Should().Be(newValidity);
            var domainHistory = await Context.GetHistory<Domain>().FindById(name).SingleAsync();
            AssertHistory(domainHistory.History, updates.DomainUpdates[0], expiresAtUtc, newValidity);

            var subdomain = await Context.Get<Domain>().Find(d => d.Name == sub).SingleAsync();
            subdomain.Should().BeEquivalentTo(updates.DomainUpdates[1]);
            subdomain.ExpiresAtUtc.Should().Be(newValidity);
            var subdomainHistory = await Context.GetHistory<Domain>().FindById(sub).SingleAsync();
            AssertHistory(subdomainHistory.History, updates.DomainUpdates[1], expiresAtUtc, newValidity);

            var record = await Context.Get<ReverseRecord>().Find(d => d.Address == address).SingleAsync();
            record.Should().BeEquivalentTo(updates.ReverseRecordUpdates[0]);
            record.ExpiresAtUtc.Should().Be(newValidity);
            var recordHistory = await Context.GetHistory<ReverseRecord>().FindById(address).SingleAsync();
            AssertHistory(recordHistory.History, updates.ReverseRecordUpdates[0], expiresAtUtc, newValidity);

            var subRecord = await Context.Get<ReverseRecord>().Find(d => d.Address == subAddress).SingleAsync();
            subRecord.Should().BeEquivalentTo(updates.ReverseRecordUpdates[1]);
            subRecord.ExpiresAtUtc.Should().Be(newValidity);
            var subRecordHistory = await Context.GetHistory<ReverseRecord>().FindById(subAddress).SingleAsync();
            AssertHistory(subRecordHistory.History, updates.ReverseRecordUpdates[1], expiresAtUtc, newValidity);
        }

        private static void AssertHistory<T, TUpdate>(IReadOnlyList<T> historyItems, TUpdate update, params DateTime?[] equivalentHistoryExpiresAtUtcProperties)
        {
            historyItems.Should().BeEquivalentTo(equivalentHistoryExpiresAtUtcProperties.Select(d => new { ExpiresAtUtc = d }));
            historyItems.Should().AllBeEquivalentTo(update);
        }

        [TestCase(true)]
        [TestCase(false)]
        public async Task AddNewDomainAndReverseRecord_When_ValidityIsAlreadyPresent(bool isValidForever)
        {
            //arrange
            var expiresAtUtc = isValidForever ? (DateTime?) null : DateTime.Today.ToUniversalTime();

            var name = $"domain{isValidForever}.add-new-after-validity";
            var address = $"address{isValidForever}.add-new-after-validity";
            var current = new BlockUpdates()
            {
                DomainUpdates = { TestDomainUpdate.Get(name, "owner1", validityKey: name) },
                ReverseRecordUpdates = { TestReverseRecordUpdate.Get(address, "owner2", name) },
                ValidityUpdates = { new ValidityUpdate(name, expiresAtUtc) }
            };

            await UpdateService.StoreUpdatesAsync(BlockTestFactory.Create(), current, previousBlock: null, CancellationToken.None);


            //act
            var sub = $"sub.{name}";
            var subAddress = "sub" + address;
            var updates = new BlockUpdates()
            {
                DomainUpdates = { TestDomainUpdate.Get(name, "owner11", validityKey: name), TestDomainUpdate.Get(sub, "owner12", "addr12", name) },
                ReverseRecordUpdates = { TestReverseRecordUpdate.Get(address, "owner21", name), TestReverseRecordUpdate.Get(subAddress, "owner22", sub) },
            };
            await UpdateService.StoreUpdatesAsync(BlockTestFactory.Create(), updates, previousBlock: null, CancellationToken.None);


            //assert
            var domain = await Context.Get<Domain>().Find(d => d.Name == name).SingleAsync();
            domain.Should().BeEquivalentTo(updates.DomainUpdates[0]);
            domain.ExpiresAtUtc.Should().Be(expiresAtUtc);
            var domainHistory = await Context.GetHistory<Domain>().FindById(name).SingleAsync();
            AssertHistory(domainHistory.History, expiresAtUtc, current.DomainUpdates[0], updates.DomainUpdates[0]);

            var subdomain = await Context.Get<Domain>().Find(d => d.Name == sub).SingleAsync();
            subdomain.Should().BeEquivalentTo(updates.DomainUpdates[1]);
            subdomain.ExpiresAtUtc.Should().Be(expiresAtUtc);
            var subdomainHistory = await Context.GetHistory<Domain>().FindById(sub).SingleAsync();
            AssertHistory(subdomainHistory.History, expiresAtUtc, updates.DomainUpdates[1]);

            var record = await Context.Get<ReverseRecord>().Find(d => d.Address == address).SingleAsync();
            record.Should().BeEquivalentTo(updates.ReverseRecordUpdates[0]);
            record.ExpiresAtUtc.Should().Be(expiresAtUtc);
            var recordHistory = await Context.GetHistory<ReverseRecord>().FindById(address).SingleAsync();
            AssertHistory(recordHistory.History, expiresAtUtc, current.ReverseRecordUpdates[0], updates.ReverseRecordUpdates[0]);

            var subRecord = await Context.Get<ReverseRecord>().Find(d => d.Address == subAddress).SingleAsync();
            subRecord.Should().BeEquivalentTo(updates.ReverseRecordUpdates[1]);
            subRecord.ExpiresAtUtc.Should().Be(expiresAtUtc);
            var subRecordHistory = await Context.GetHistory<ReverseRecord>().FindById(subAddress).SingleAsync();
            AssertHistory(subRecordHistory.History, expiresAtUtc, updates.ReverseRecordUpdates[1]);
        }

        private static void AssertHistory<T, TUpdate>(IReadOnlyList<T> historyItems, DateTime? expiresAtUtc, params TUpdate[] historyUpdates)
        {
            historyItems.Should().BeEquivalentTo(historyUpdates);
            historyItems.Should().AllBeEquivalentTo(new { ExpiresAtUtc = expiresAtUtc });
        }
    }
}