﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using MongoDB.Driver;
using NUnit.Framework;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Data.MongoDb.IntegrationTests.TestData;

namespace TezosDomains.Data.MongoDb.IntegrationTests.Tests
{
    public class ReverseRecordTests : TestBase
    {
        [Test]
        public async Task Create_Should_CreateReverseRecords()
        {
            var block = BlockTestFactory.Create();
            var createUpdates = new BlockUpdates()
            {
                ReverseRecordUpdates =
                {
                    TestReverseRecordUpdate.Get(address: "deleted.created-rr", owner: "addr"),
                    TestReverseRecordUpdate.Get(
                        "addr.created-rr",
                        owner: "addr",
                        name: "created.rr"
                    ),
                }
            };
            await UpdateService.StoreUpdatesAsync(block, createUpdates, previousBlock: null, CancellationToken.None);

            Expression<Func<ReverseRecord, bool>> filterDefinition = rr => rr.Address.EndsWith("created-rr");
            var result = await Context.Get<ReverseRecord>()
                .Find(filterDefinition)
                .ToListAsync();
            result.Should().BeEquivalentTo(createUpdates.ReverseRecordUpdates);

            var recordsByName = result.ToDictionary(d => d.Address);
            var createPairs = CreateBlockUpdatePairs(block, null, createUpdates.ReverseRecordUpdates.ToArray());
            await AssertHistoryAsync(recordsByName["deleted.created-rr"], createPairs[0]);
            await AssertHistoryAsync(recordsByName["addr.created-rr"], createPairs[1]);
        }

        [Test]
        public async Task Modify_Should_UpdateReverseRecords()
        {
            Expression<Func<ReverseRecord, bool>> filterDefinition = rr => rr.Address.EndsWith("modify-rr");
            var createBlock = BlockTestFactory.Create();
            var createUpdates = new BlockUpdates
            {
                ReverseRecordUpdates =
                {
                    TestReverseRecordUpdate.Get(address: "deleted.modify-rr", owner: "addr"),
                    TestReverseRecordUpdate.Get(
                        address: "addr.modify-rr",
                        owner: "addr",
                        name: "modify.rr"
                    )
                }
            };
            await UpdateService.StoreUpdatesAsync(createBlock, createUpdates, previousBlock: null, CancellationToken.None);
            var currentData = await Context.Get<ReverseRecord>().Find(filterDefinition).ToListAsync();
            currentData.Should().BeEquivalentTo(createUpdates.ReverseRecordUpdates, "currentData were created in `ReverseRecord_IsCreated` test");
            var modifyUpdates = new BlockUpdates
            {
                ReverseRecordUpdates =
                {
                    TestReverseRecordUpdate.Get("deleted.modify-rr", owner: "addr", name: "1.modify.rr"),
                    TestReverseRecordUpdate.Get("new-addr.modify-rr", owner: "addr-3", name: "3.modify.rr"),
                }
            };
            var updateBlock = BlockTestFactory.Create();

            await UpdateService.StoreUpdatesAsync(updateBlock, modifyUpdates, previousBlock: null, CancellationToken.None);
            var newData = await Context.Get<ReverseRecord>().Find(filterDefinition).ToListAsync();

            newData.Should()
                .BeEquivalentTo(
                    modifyUpdates.ReverseRecordUpdates.Concat(createUpdates.ReverseRecordUpdates.Skip(1)),
                    "First reverse record was updated, `addr.modify-rr` was not touched and new `new-addr.modify-rr` was added"
                );

            var recordsByName = newData.ToDictionary(d => d.Address);
            await AssertHistoryAsync(
                recordsByName["deleted.modify-rr"],
                CreateBlockUpdatePairs(createBlock, updateBlock, createUpdates.ReverseRecordUpdates[0])[0],
                CreateBlockUpdatePairs(updateBlock, null, modifyUpdates.ReverseRecordUpdates[0])[0]
            );
            await AssertHistoryAsync(recordsByName["addr.modify-rr"], CreateBlockUpdatePairs(createBlock, null, createUpdates.ReverseRecordUpdates[1]));
            await AssertHistoryAsync(recordsByName["new-addr.modify-rr"], CreateBlockUpdatePairs(updateBlock, null, modifyUpdates.ReverseRecordUpdates[1]));
        }

        [Test]
        public async Task UpdateNameToNull_Should_SetValidityPropertiesToNull()
        {
            //arrange
            var address = "addr.remove-rr";
            var domainName = "remove.rr";
            var createBlock = BlockTestFactory.Create();
            var expiresAtUtc = DateTime.UtcNow.Date.AddYears(1);
            var blockUpdates = new BlockUpdates
            {
                DomainUpdates = { TestDomainUpdate.Get(domainName, address, address, domainName) },
                ReverseRecordUpdates = { TestReverseRecordUpdate.Get(address, "owner", domainName) },
                ValidityUpdates = { new ValidityUpdate(domainName, expiresAtUtc) }
            };

            await UpdateService.StoreUpdatesAsync(
                createBlock,
                blockUpdates,
                null,
                CancellationToken.None
            );
            var record = await Context.Get<ReverseRecord>().Find(r => r.Address == address).SingleAsync();
            record.Should().BeEquivalentTo(blockUpdates.ReverseRecordUpdates[0]);
            record.ExpiresAtUtc.Should().Be(expiresAtUtc);
            record.ValidityKey.Should().Be(domainName);

            //act
            var removeBlock = BlockTestFactory.Create();
            var removeUpdates = new BlockUpdates { ReverseRecordUpdates = { TestReverseRecordUpdate.Get(address, "owner") } };
            await UpdateService.StoreUpdatesAsync(removeBlock, removeUpdates, previousBlock: null, CancellationToken.None);

            //assert
            record = await Context.Get<ReverseRecord>().Find(r => r.Address == address).SingleAsync();
            record.Should().BeEquivalentTo(removeUpdates.ReverseRecordUpdates[0]);
            record.ExpiresAtUtc.Should().BeNull();
            record.ValidityKey.Should().BeNull();
            await AssertHistoryAsync(
                record,
                CreateBlockUpdatePairs(createBlock, removeBlock, blockUpdates.ReverseRecordUpdates[0])[0],
                CreateBlockUpdatePairs(removeBlock, null, removeUpdates.ReverseRecordUpdates[0])[0]
            );
        }
    }
}