﻿using TezosDomains.Data.Models.Updates;

namespace TezosDomains.Data.MongoDb.IntegrationTests.TestData
{
    public static class TestBidderBalancesUpdate
    {
        public static BidderBalanceUpdate Get(string address, string tldName, decimal? balance)
            => new BidderBalanceUpdate(address, tldName, balance);
    }
}