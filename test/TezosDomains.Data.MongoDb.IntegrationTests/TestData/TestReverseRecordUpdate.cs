﻿using TezosDomains.Data.Models.Updates;

namespace TezosDomains.Data.MongoDb.IntegrationTests.TestData
{
    public static class TestReverseRecordUpdate
    {
        public static ReverseRecordUpdate Get(
            string address,
            string owner,
            string? name = null
        )
            => new ReverseRecordUpdate(address, owner, name);
    }
}