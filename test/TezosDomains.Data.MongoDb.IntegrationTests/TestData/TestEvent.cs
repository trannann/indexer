﻿using System;
using System.Collections.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;

namespace TezosDomains.Data.MongoDb.IntegrationTests.TestData
{
    public static class TestEvent
    {
        public static class Auction
        {
            public static AuctionBidEvent Bid(
                string sourceAddress,
                string domainName,
                decimal bidAmount,
                BlockSlim block,
                string operationGroupHash,
                string? previousBidderAddress = default,
                decimal? previousBidAmount = default
            )
                => new AuctionBidEvent(
                    sourceAddress,
                    domainName,
                    bidAmount,
                    bidAmount,
                    block,
                    operationGroupHash,
                    Event.GenerateId(),
                    previousBidderAddress,
                    previousBidAmount
                );

            public static AuctionEndEvent End(string domainName, decimal highestBid, string highestBidderAddress, BlockSlim block)
                => new AuctionEndEvent(
                    domainName,
                    highestBidderAddress,
                    highestBid,
                    new[] { highestBidderAddress },
                    block,
                    Event.GenerateId()
                );

            public static AuctionSettleEvent Settle(
                string sourceAddress,
                string domainName,
                string domainOwnerAddress,
                string? domainForwardRecordAddress,
                Dictionary<string, string> data,
                BlockSlim block,
                string operationGroupHash
            )
                => new AuctionSettleEvent(
                    sourceAddress,
                    domainName,
                    domainOwnerAddress,
                    domainForwardRecordAddress,
                    data,
                    block,
                    operationGroupHash,
                    Event.GenerateId(),
                    registrationDurationInDays: 123,
                    winningBid: 100
                );

            public static AuctionWithdrawEvent Withdraw(
                string sourceAddress,
                string tldName,
                decimal amount,
                BlockSlim block,
                string operationGroupHash
            )
                => new AuctionWithdrawEvent(sourceAddress, tldName, amount, block, operationGroupHash, Event.GenerateId());
        }

        public static class Domain
        {
            public static DomainBuyEvent Buy(
                string sourceAddress,
                string domainName,
                decimal price,
                int durationInDays,
                string domainOwnerAddress,
                string? domainForwardRecordAddress,
                Dictionary<string, string> data,
                BlockSlim block,
                string operationGroupHash
            )
                => new DomainBuyEvent(
                    sourceAddress,
                    domainName,
                    price,
                    durationInDays,
                    domainOwnerAddress,
                    domainForwardRecordAddress,
                    data,
                    block,
                    operationGroupHash,
                    Event.GenerateId()
                );

            public static DomainRenewEvent Renew(
                string sourceAddress,
                string domainName,
                decimal price,
                int durationInDays,
                BlockSlim block,
                string operationGroupHash
            )
                => new DomainRenewEvent(sourceAddress, domainName, price, durationInDays, block, operationGroupHash, Event.GenerateId());

            public static DomainCommitEvent Commit(
                string sourceAddress,
                BlockSlim block,
                string operationGroupHash
            )
                => new DomainCommitEvent(sourceAddress, block, operationGroupHash, Event.GenerateId(), Guid.NewGuid().ToString("N"));

            public static DomainUpdateEvent Update(
                string sourceAddress,
                string domainName,
                string domainOwnerAddress,
                string? domainForwardRecordAddress,
                Dictionary<string, string> data,
                BlockSlim block,
                string operationGroupHash
            )
                => new DomainUpdateEvent(
                    sourceAddress,
                    domainName,
                    domainOwnerAddress,
                    domainForwardRecordAddress,
                    data,
                    block,
                    operationGroupHash,
                    Event.GenerateId()
                );

            public static DomainSetChildRecordEvent SetChildRecord(
                string sourceAddress,
                string domainName,
                string domainOwnerAddress,
                string? domainForwardRecordAddress,
                Dictionary<string, string> data,
                BlockSlim block,
                string operationGroupHash
            )
                => new DomainSetChildRecordEvent(
                    sourceAddress,
                    domainName,
                    domainOwnerAddress,
                    domainForwardRecordAddress,
                    data,
                    block,
                    operationGroupHash,
                    Event.GenerateId(),
                    true
                );
        }

        public static class ReverseRecord
        {
            public static ReverseRecordUpdateEvent Update(
                string sourceAddress,
                string? domainName,
                string ownerAddress,
                string reverseRecordAddress,
                BlockSlim block,
                string operationGroupHash
            )
                => new ReverseRecordUpdateEvent(
                    sourceAddress,
                    domainName,
                    ownerAddress,
                    reverseRecordAddress,
                    block,
                    operationGroupHash,
                    Event.GenerateId()
                );

            public static ReverseRecordClaimEvent Claim(
                string sourceAddress,
                string? domainName,
                string ownerAddress,
                BlockSlim block,
                string operationGroupHash
            )
                => new ReverseRecordClaimEvent(
                    sourceAddress,
                    domainName,
                    ownerAddress,
                    block,
                    operationGroupHash,
                    Event.GenerateId()
                );
        }
    }
}