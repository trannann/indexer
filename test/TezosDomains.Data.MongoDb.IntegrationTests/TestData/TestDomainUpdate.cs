﻿using System.Collections.Generic;
using TezosDomains.Data.Models.Updates;

namespace TezosDomains.Data.MongoDb.IntegrationTests.TestData
{
    public static class TestDomainUpdate
    {
        public static DomainUpdate Get(
            string name,
            string owner,
            string? address = null,
            string? validityKey = null,
            IReadOnlyDictionary<string, string>? data = null
        )
            => new DomainUpdate(name, address, owner, validityKey, data ?? new Dictionary<string, string>());
    }
}