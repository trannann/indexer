﻿using System;
using TezosDomains.Data.Models.Updates;

namespace TezosDomains.Data.MongoDb.IntegrationTests.TestData
{
    public static class TestAuctionUpdate
    {
        public static AuctionBidUpdate GetBid(string domainName, string bidder, decimal amount, DateTime auctionEndInUtc, DateTime ownedUntilUtc)
            => new AuctionBidUpdate(domainName, bidder, amount, auctionEndInUtc, ownedUntilUtc);
    }
}