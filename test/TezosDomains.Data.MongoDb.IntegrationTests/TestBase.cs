﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using NUnit.Framework;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Configurations;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.IntegrationTests.Tests;
using TezosDomains.Data.MongoDb.Services;
using TezosDomains.Data.MongoDb.Services.Update;

namespace TezosDomains.Data.MongoDb.IntegrationTests
{
    public class TestBase
    {
        private MongoDbConfiguration _config = null!;
        public MongoDbContext Context { get; private set; } = null!;
        public UpdateService UpdateService { get; private set; } = null!;
        public MongoClient Client { get; private set; } = null!;

        [SetUp]
        public void OneTimeSetUp()
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("testsettings.json")
                .AddUserSecrets<BlockTests>(optional: true)
                .AddEnvironmentVariables()
                .Build();


            _config = configuration.GetSection("TezosDomains:MongoDb").Get<MongoDbConfiguration>();
            _config.OverrideDatabaseName(_ => $"t_{Guid.NewGuid()}");
            MongoDbSetup.InitializeMappingStructures();

            Client = MongoDbClientFactory.Create(_config, TestLogger.Create<MongoClient>());
            Context = new MongoDbContext(Client, _config, TestLogger.Create<MongoDbContext>());
            UpdateService = new UpdateService(
                Context,
                new DomainUpdateService(Context, TestLogger.Create<DomainUpdateService>()),
                new ReverseRecordUpdateService(Context, TestLogger.Create<ReverseRecordUpdateService>()),
                new AuctionUpdateService(Context, TestLogger.Create<AuctionUpdateService>()),
                new BidderBalancesUpdateService(Context, TestLogger.Create<BidderBalancesUpdateService>()),
                new EventProcessor(
                    new AuctionQueryService(Context),
                    new DomainQueryService(Context),
                    TestLogger.Create<EventProcessor>()
                )
            );

            new MongoDbInitializer(TestLogger.Create<MongoDbInitializer>(), Context).Initialize();
        }

        [TearDown]
        public async Task TearDown()
        {
            await Client.DropDatabaseAsync(_config.Database);
        }

        protected (BlockSlim Block, RecordValidUntil ValidUntil, object Update)[] CreateBlockUpdatePairs<TUpdate>(
            Block oldBlock,
            Block? newBlock,
            params TUpdate[] updates
        )
            where TUpdate : class
        {
            var blockWithValidity = new BlockSlim(
                oldBlock.Level,
                oldBlock.Hash,
                oldBlock.Timestamp
            );

            var validUntil = new RecordValidUntil(newBlock?.Timestamp, newBlock?.Level);

            return updates.Select(u => (blockWithValidity, validUntil, (object) u)).ToArray();
        }

        protected async Task AssertHistoryAsync<T>(
            T item,
            params (BlockSlim Block, RecordValidUntil ValidUntil, object Update)[] equivalentHistoryObjects
        )
            where T : IDocumentWithHistory
        {
            var historyItem = await Context.GetHistory<T>().FindById(item.GetId()).SingleOrDefaultAsync();
            historyItem.History.Should().BeEquivalentTo(equivalentHistoryObjects.Select(o => o.Update));
            historyItem.History.Should().BeEquivalentTo(equivalentHistoryObjects.Select(o => o.ValidUntil));
            historyItem.History.Should().BeEquivalentTo(equivalentHistoryObjects.Select(o => new { o.Block }));
        }

        public class RecordValidUntil
        {
            public RecordValidUntil(DateTime? validUntilTimestamp, int? validUntilBlockLevel)
            {
                ValidUntilTimestamp = validUntilTimestamp;
                ValidUntilBlockLevel = validUntilBlockLevel;
            }

            public DateTime? ValidUntilTimestamp { get; }
            public int? ValidUntilBlockLevel { get; }
        }
    }
}