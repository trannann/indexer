﻿using System;
using System.Threading;
using TezosDomains.Data.Models;

namespace TezosDomains.Data.MongoDb.IntegrationTests
{
    public class BlockTestFactory
    {
        private static int _level = 0;

        public static Block Create()
        {
            var level = Interlocked.Add(ref _level, 1);

            var hash = Guid.NewGuid().ToString();
            var predecessor = Guid.NewGuid().ToString();
            return new Block(level, hash, DateTime.UtcNow.Date.AddSeconds(level), predecessor);
        }
    }
}