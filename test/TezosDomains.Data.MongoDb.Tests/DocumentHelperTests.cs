﻿using FluentAssertions;
using NUnit.Framework;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb.Helpers;

namespace TezosDomains.Data.MongoDb.Tests
{
    [TestFixture]
    public class DocumentHelperTests
    {
        [Test]
        public void GetFieldName_ShouldWorkForArrayAccessorWithProperty()
        {
            var fieldName = DocumentHelper.GetFieldName<Auction, decimal>(a => a.Bids[0].Amount);
            fieldName.Should().Be("Bids.0.Amount");
        }
    }
}