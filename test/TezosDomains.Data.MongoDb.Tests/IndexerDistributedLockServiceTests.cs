using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using NSubstitute;
using NUnit.Framework;
using TezosDomains.Data.MongoDb.DistributedLock;
using TezosDomains.Data.MongoDb.Services;

namespace TezosDomains.Data.MongoDb.Tests
{
    public class IndexerDistributedLockServiceTests
    {
        private IMongoCollection<Lock> _collectionMock = null!;
        private readonly TimeSpan _distributedLockLifetime = TimeSpan.FromSeconds(1);
        private IndexerDistributedLockService _service = null!;
        private CancellationTokenSource _cancellationTokenSource = null!;

        [SetUp]
        public void Setup()
        {
            _cancellationTokenSource = new CancellationTokenSource();
            _collectionMock = Substitute.For<IMongoCollection<Lock>>();
            _service = new IndexerDistributedLockService(
                _collectionMock,
                Substitute.For<ILogger<IndexerDistributedLockService>>(),
                _distributedLockLifetime
            );
        }


        [Test]
        public async Task AcquireAsync_Succeed_AndHeartbeatIsPeriodicallyCalled()
        {
            Lock CreateLock() => new Lock("indexer", DateTime.UtcNow.Add(_distributedLockLifetime), "m1", 1);

            //arrange
            _collectionMock.FindOneAndUpdateAsync(
                    Arg.Any<FilterDefinition<Lock>>(),
                    Arg.Any<UpdateDefinition<Lock>>(),
                    Arg.Any<FindOneAndUpdateOptions<Lock, Lock>>(),
                    Arg.Any<CancellationToken>()
                )
                .Returns((Lock) null!, CreateLock(), CreateLock());

            //act
            await _service.AcquireAsync(_cancellationTokenSource);
            await Task.Delay(_distributedLockLifetime / 2, _cancellationTokenSource.Token);

            //assert
            //FindOneAndUpdateAsync should be called once during acquire and 2 more in heartbeat
            //heartbeat calls after 1/5th of lock lifetime => test waited for 1/2 of lifetime, so the number of call should be 5/2 => 2
            var calls = _collectionMock.ReceivedCalls();
            calls.Where(c => c.GetMethodInfo().Name == nameof(_collectionMock.FindOneAndUpdateAsync))
                .Should()
                .HaveCount(3);
            Assert.DoesNotThrow(() => _service.EnsureLockActive());
        }

        [Test]
        public async Task AcquireAsync_Succeed_AndHeartbeatFails_ResultsInLockExpiration()
        {
            //arrange
            _collectionMock.FindOneAndUpdateAsync<Lock>(default(FilterDefinition<Lock>), default, default, default)
                .Returns(o => (Lock) null!, o => throw new Exception("break heartbeat"));

            //act
            await _service.AcquireAsync(_cancellationTokenSource);


            //assert
            Assert.DoesNotThrow(() => _service.EnsureLockActive());
            try
            {
                await Task.Delay(_distributedLockLifetime, _cancellationTokenSource.Token);
            }
            catch (OperationCanceledException)
            {
            }

            Assert.Throws<MongoDistributedLockException>(() => _service.EnsureLockActive());
            Assert.IsTrue(_cancellationTokenSource.IsCancellationRequested);
        }

        [Test]
        public void AcquireAsync_MultipleCalls_ShouldThrowException()
        {
            //act
            _ = _service.AcquireAsync(_cancellationTokenSource);

            //assert
            Assert.ThrowsAsync<InvalidOperationException>(async () => await _service.AcquireAsync(_cancellationTokenSource));
            _cancellationTokenSource.Cancel();
        }
    }
}