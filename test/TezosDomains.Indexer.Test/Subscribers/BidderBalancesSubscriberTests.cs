﻿using NSubstitute;
using NUnit.Framework;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Indexer.Processor.Subscribers;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Test.Subscribers
{
    public class BidderBalancesSubscriberTests : BigMapDiffSubscriberTestBase<BidderBalancesSubscriber>
    {
        [Test]
        public void OnBigMapDiff_WithBigMapValue_ShouldCreateUpdateWithBalance()
        {
            //arrange
            var storagePropertiesMock = new PropertiesBuilder()
                .Setup("tld", v => v.GetValueAsString(), "tez")
                .Build();
            var keyProperty = Substitute.For<IMichelsonValue>();
            keyProperty.GetValueAsAddress().Returns(Address1);
            var valuePropertiesMock = new PropertiesBuilder()
                .Setup("__value__", v => v.GetValueAsMutezDecimal(), 10)
                .Build();

            //act
            Target.OnBigMapDiff(
                RandomTezosBlock,
                "ogh1",
                "bidder_balances",
                keyProperty,
                valuePropertiesMock.Properties,
                storagePropertiesMock.Properties
            );

            //assert
            keyProperty.Received(1).GetValueAsAddress();
            valuePropertiesMock.AssertAction();
            storagePropertiesMock.AssertAction();
            AssertUpdateInCollector(Target, u => u.BidderBalancesUpdates, new BidderBalanceUpdate(Address1, "tez", 10));
        }

        [Test]
        public void OnBigMapDiff_WithoutBigMapValue_ShouldCreateUpdateWithoutBalance()
        {
            //arrange
            var storagePropertiesMock = new PropertiesBuilder()
                .Setup("tld", v => v.GetValueAsString(), "tez")
                .Build();
            var keyProperty = Substitute.For<IMichelsonValue>();
            keyProperty.GetValueAsAddress().Returns(Address1);

            //act
            Target.OnBigMapDiff(
                RandomTezosBlock,
                "ogh1",
                "bidder_balances",
                keyProperty,
                EmptyProperties,
                storagePropertiesMock.Properties
            );

            //assert
            keyProperty.Received(1).GetValueAsAddress();
            storagePropertiesMock.AssertAction();
            AssertUpdateInCollector(Target, u => u.BidderBalancesUpdates, new BidderBalanceUpdate(Address1, "tez", null));
        }
    }
}