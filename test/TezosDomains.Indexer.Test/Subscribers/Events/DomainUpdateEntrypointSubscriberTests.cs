﻿using NUnit.Framework;
using System.Collections.Generic;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Processor.Subscribers.Events;

namespace TezosDomains.Indexer.Test.Subscribers.Events
{
    public class DomainUpdateEntrypointSubscriberTests : EntrypointEventSubscriberTestsBase<DomainUpdateEntrypointSubscriber, DomainUpdateEvent>
    {
        [Test]
        public void OnEntrypoint_ShouldParseDataSuccessfully()
        {
            //arrange
            var propertiesMock = new PropertiesBuilder()
                .Setup("name", v => v.GetValueAsString(), "used-shoe.tez")
                .Setup("owner", v => v.GetValueAsAddress(), Address1)
                .Setup("address", v => v.GetValueAsNullableAddress(), Address2)
                .Setup("data", v => v.GetValueAsMap(), new Dictionary<string, string>())
                .Build();
            var block = GenerateTezosBlock();

            //act
            Target.OnEntrypoint(
                block.TezosBlock,
                "ogh1",
                "update_record",
                SourceAddress,
                10,
                propertiesMock.Properties,
                EmptyProperties
            );

            //assert
            propertiesMock.AssertAction();
            AssertEventInUpdatesCollector(
                Target,
                new DomainUpdateEvent(
                    SourceAddress,
                    "used-shoe.tez",
                    Address1,
                    Address2,
                    new Dictionary<string, string>(),
                    block.BlockSlim,
                    "ogh1",
                    "id"
                )
            );
        }
    }
}