﻿using NUnit.Framework;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Processor;
using TezosDomains.Indexer.Processor.Subscribers.Events;

namespace TezosDomains.Indexer.Test.Subscribers.Events
{
    public abstract class EntrypointEventSubscriberTestsBase<TSubscriber, TEvent> : SubscriberTestsBase<TSubscriber>
        where TSubscriber : EventEntrypointSubscriber<TEvent>, new()
        where TEvent : Event
    {
        [Test]
        public void OnEntrypoint_ShouldNotDoAnything_WhenEntrypointNameMismatch()
        {
            Test_OnEntrypoint_ShouldNotDoAnything();
        }

        [Test]
        public void OnBigMapDiff_ShouldNotDoAnything()
        {
            Test_OnBigMapDiff_BigMap1_ShouldNotDoAnything();
        }

        [Test]
        public void OnOutgoingTransaction_ShouldNotDoAnything()
        {
            Test_OnOutgoingTransaction_ShouldNotDoAnything();
        }

        protected static void AssertEventInUpdatesCollector(IBlockUpdatesCollector updatesCollector, TEvent @event)
        {
            AssertUpdateInCollector(updatesCollector, u => u.Events, @event, o => o.Excluding(a => a.Id));
        }
    }
}