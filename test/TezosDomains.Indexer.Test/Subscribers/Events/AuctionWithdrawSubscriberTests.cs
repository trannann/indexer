﻿using NUnit.Framework;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Processor.Subscribers.Events;

namespace TezosDomains.Indexer.Test.Subscribers.Events
{
    public class AuctionWithdrawSubscriberTests : SubscriberTestsBase<AuctionWithdrawSubscriber>
    {
        [Test]
        public void OnOutgoingTransaction_ShouldParseDataSuccessfully()
        {
            //arrange
            var storagePropertiesMock = new PropertiesBuilder()
                .Setup("tld", v => v.GetValueAsString(), "tez")
                .Build();
            var block = GenerateTezosBlock();

            //act
            Target.OnOutgoingTransaction(
                block.TezosBlock,
                "ogh1",
                SourceAddress,
                10,
                storagePropertiesMock.Properties
            );

            //assert
            storagePropertiesMock.AssertAction();
            AssertUpdateInCollector(
                Target,
                u => u.Events,
                new AuctionWithdrawEvent(
                    SourceAddress,
                    "tez",
                    10,
                    block.BlockSlim,
                    "ogh1",
                    "id"
                ),
                o => o.Excluding(e => e.Id)
            );
        }

        [Test]
        public void OnBigMapDiff_ShouldNotDoAnything()
        {
            Test_OnBigMapDiff_BigMap1_ShouldNotDoAnything();
        }

        [Test]
        public void OnEntrypoint_ShouldNotDoAnything()
        {
            Test_OnEntrypoint_ShouldNotDoAnything();
        }
    }
}