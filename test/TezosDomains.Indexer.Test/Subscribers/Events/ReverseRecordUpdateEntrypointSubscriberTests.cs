﻿using NUnit.Framework;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Processor.Subscribers.Events;

namespace TezosDomains.Indexer.Test.Subscribers.Events
{
    public class ReverseRecordUpdateEntrypointSubscriberTests : EntrypointEventSubscriberTestsBase<ReverseRecordUpdateEntrypointSubscriber,
        ReverseRecordUpdateEvent>
    {
        [Test]
        public void OnEntrypoint_ShouldParseDataSuccessfully()
        {
            //arrange
            var propertiesMock = new PropertiesBuilder()
                .Setup("name", v => v.GetValueAsNullableString(), "used-shoe.tez")
                .Setup("owner", v => v.GetValueAsAddress(), Address1)
                .Setup("address", v => v.GetValueAsAddress(), Address2)
                .Build();
            var block = GenerateTezosBlock();

            //act
            Target.OnEntrypoint(
                block.TezosBlock,
                "ogh1",
                "update_reverse_record",
                SourceAddress,
                10,
                propertiesMock.Properties,
                EmptyProperties
            );

            //assert
            propertiesMock.AssertAction();
            AssertEventInUpdatesCollector(
                Target,
                new ReverseRecordUpdateEvent(
                    SourceAddress,
                    "used-shoe.tez",
                    Address1,
                    Address2,
                    block.BlockSlim,
                    "ogh1",
                    "id"
                )
            );
        }
    }
}