﻿using NUnit.Framework;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Processor.Subscribers.Events;

namespace TezosDomains.Indexer.Test.Subscribers.Events
{
    public class AuctionBidEntrypointSubscriberTests : EntrypointEventSubscriberTestsBase<AuctionBidEntrypointSubscriber, AuctionBidEvent>
    {
        [Test]
        public void OnEntrypoint_ShouldParseDataSuccessfully()
        {
            //arrange
            var storageMock = new PropertiesBuilder()
                .Setup("tld", v => v.GetValueAsString(), "tez")
                .Build();
            var propertiesMock = new PropertiesBuilder()
                .Setup("label", v => v.GetValueAsString(), "used-shoe")
                .Setup("bid", v => v.GetValueAsMutezDecimal(), 10)
                .Build();
            var block = GenerateTezosBlock();

            //act
            Target.OnEntrypoint(
                block.TezosBlock,
                "ogh1",
                "bid",
                "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                123,
                propertiesMock.Properties,
                storageMock.Properties
            );

            //assert
            storageMock.AssertAction();
            propertiesMock.AssertAction();
            AssertEventInUpdatesCollector(
                Target,
                new AuctionBidEvent(
                    "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    "used-shoe.tez",
                    10,
                    123,
                    block.BlockSlim,
                    "ogh1",
                    "id",
                    default,
                    default
                )
            );
        }
    }
}