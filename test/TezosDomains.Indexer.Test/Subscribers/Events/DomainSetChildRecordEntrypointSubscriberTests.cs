﻿using NUnit.Framework;
using System.Collections.Generic;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Processor.Subscribers.Events;

namespace TezosDomains.Indexer.Test.Subscribers.Events
{
    public class DomainSetChildRecordEntrypointSubscriberTests : EntrypointEventSubscriberTestsBase<DomainSetChildRecordEntrypointSubscriber,
        DomainSetChildRecordEvent>
    {
        [Test]
        public void OnEntrypoint_ShouldParseDataSuccessfully()
        {
            //arrange
            var propertiesMock = new PropertiesBuilder()
                .Setup("label", v => v.GetValueAsString(), "used-shoe")
                .Setup("parent", v => v.GetValueAsString(), "tez")
                .Setup("owner", v => v.GetValueAsAddress(), Address1)
                .Setup("address", v => v.GetValueAsNullableAddress(), Address2)
                .Setup("data", v => v.GetValueAsMap(), new Dictionary<string, string>())
                .Build();
            var block = GenerateTezosBlock();

            //act
            Target.OnEntrypoint(
                block.TezosBlock,
                "ogh1",
                "set_child_record",
                SourceAddress,
                10,
                propertiesMock.Properties,
                EmptyProperties
            );

            //assert
            propertiesMock.AssertAction();
            AssertEventInUpdatesCollector(
                Target,
                new DomainSetChildRecordEvent(
                    SourceAddress,
                    "used-shoe.tez",
                    Address1,
                    Address2,
                    new Dictionary<string, string>(),
                    block.BlockSlim,
                    "ogh1",
                    "id",
                    true
                )
            );
        }
    }
}