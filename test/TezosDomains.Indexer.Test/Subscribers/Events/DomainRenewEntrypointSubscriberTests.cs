﻿using NUnit.Framework;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Processor.Subscribers.Events;

namespace TezosDomains.Indexer.Test.Subscribers.Events
{
    public class DomainRenewEntrypointSubscriberTests : EntrypointEventSubscriberTestsBase<DomainRenewEntrypointSubscriber, DomainRenewEvent>
    {
        [Test]
        public void OnEntrypoint_ShouldParseDataSuccessfully()
        {
            //arrange
            var storageMock = new PropertiesBuilder()
                .Setup("tld", v => v.GetValueAsString(), "tez")
                .Build();
            var propertiesMock = new PropertiesBuilder()
                .Setup("label", v => v.GetValueAsString(), "used-shoe")
                .Setup("duration", v => v.GetValueAsInt(), 365)
                .Build();
            var block = GenerateTezosBlock();

            //act
            Target.OnEntrypoint(
                block.TezosBlock,
                "ogh1",
                "renew",
                SourceAddress,
                10,
                propertiesMock.Properties,
                storageMock.Properties
            );

            //assert
            storageMock.AssertAction();
            propertiesMock.AssertAction();
            AssertEventInUpdatesCollector(
                Target,
                new DomainRenewEvent(
                    SourceAddress,
                    "used-shoe.tez",
                    10,
                    365,
                    block.BlockSlim,
                    "ogh1",
                    "id"
                )
            );
        }
    }
}