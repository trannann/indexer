﻿using NUnit.Framework;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Processor.Subscribers.Events;

namespace TezosDomains.Indexer.Test.Subscribers.Events
{
    public class DomainCommitEntrypointSubscriberTests : EntrypointEventSubscriberTestsBase<DomainCommitEntrypointSubscriber, DomainCommitEvent>
    {
        [Test]
        public void OnEntrypoint_ShouldParseDataSuccessfully()
        {
            //arrange
            var propertiesMock = new PropertiesBuilder()
                .Setup("commit", v => v.GetValueAsHexBytesString(), "commit-hash")
                .Build();
            var block = GenerateTezosBlock();

            //act
            Target.OnEntrypoint(
                block.TezosBlock,
                "ogh1",
                "commit",
                SourceAddress,
                10,
                propertiesMock.Properties,
                EmptyProperties
            );

            //assert
            propertiesMock.AssertAction();
            AssertEventInUpdatesCollector(
                Target,
                new DomainCommitEvent(
                    SourceAddress,
                    block.BlockSlim,
                    "ogh1",
                    "id",
                    "commit-hash"
                )
            );
        }
    }
}