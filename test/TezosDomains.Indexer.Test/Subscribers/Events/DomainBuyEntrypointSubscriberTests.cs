﻿using NUnit.Framework;
using System.Collections.Generic;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Processor.Subscribers.Events;

namespace TezosDomains.Indexer.Test.Subscribers.Events
{
    public class DomainBuyEntrypointSubscriberTests : EntrypointEventSubscriberTestsBase<DomainBuyEntrypointSubscriber, DomainBuyEvent>
    {
        [Test]
        public void OnEntrypoint_ShouldParseDataSuccessfully()
        {
            //arrange
            var storageMock = new PropertiesBuilder()
                .Setup("tld", v => v.GetValueAsString(), "tez")
                .Build();
            var propertiesMock = new PropertiesBuilder()
                .Setup("label", v => v.GetValueAsString(), "used-shoe")
                .Setup("owner", v => v.GetValueAsAddress(), Address1)
                .Setup("address", v => v.GetValueAsNullableAddress(), Address2)
                .Setup("data", v => v.GetValueAsMap(), new Dictionary<string, string>())
                .Setup("duration", v => v.GetValueAsInt(), 365)
                .Build();
            var block = GenerateTezosBlock();

            //act
            Target.OnEntrypoint(
                block.TezosBlock,
                "ogh1",
                "buy",
                SourceAddress,
                10,
                propertiesMock.Properties,
                storageMock.Properties
            );

            //assert
            storageMock.AssertAction();
            propertiesMock.AssertAction();
            AssertEventInUpdatesCollector(
                Target,
                new DomainBuyEvent(
                    SourceAddress,
                    "used-shoe.tez",
                    10,
                    365,
                    Address1,
                    Address2,
                    new Dictionary<string, string>(),
                    block.BlockSlim,
                    "ogh1",
                    "id"
                )
            );
        }
    }
}