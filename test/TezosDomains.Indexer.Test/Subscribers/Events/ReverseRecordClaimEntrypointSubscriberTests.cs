﻿using NUnit.Framework;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Processor.Subscribers.Events;

namespace TezosDomains.Indexer.Test.Subscribers.Events
{
    public class ReverseRecordClaimEntrypointSubscriberTests : EntrypointEventSubscriberTestsBase<ReverseRecordClaimEntrypointSubscriber,
        ReverseRecordClaimEvent>
    {
        [Test]
        public void OnEntrypoint_ShouldParseDataSuccessfully()
        {
            //arrange
            var propertiesMock = new PropertiesBuilder()
                .Setup("name", v => v.GetValueAsNullableString(), "used-shoe.tez")
                .Setup("owner", v => v.GetValueAsAddress(), Address1)
                .Build();
            var block = GenerateTezosBlock();

            //act
            Target.OnEntrypoint(
                block.TezosBlock,
                "ogh1",
                "claim_reverse_record",
                SourceAddress,
                10,
                propertiesMock.Properties,
                EmptyProperties
            );

            //assert
            propertiesMock.AssertAction();
            AssertEventInUpdatesCollector(
                Target,
                new ReverseRecordClaimEvent(
                    SourceAddress,
                    "used-shoe.tez",
                    Address1,
                    block.BlockSlim,
                    "ogh1",
                    "id"
                )
            );
        }
    }
}