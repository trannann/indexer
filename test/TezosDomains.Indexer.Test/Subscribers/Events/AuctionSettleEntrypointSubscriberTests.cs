﻿using NUnit.Framework;
using System.Collections.Generic;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Processor.Subscribers.Events;

namespace TezosDomains.Indexer.Test.Subscribers.Events
{
    public class AuctionSettleEntrypointSubscriberTests : EntrypointEventSubscriberTestsBase<AuctionSettleEntrypointSubscriber, AuctionSettleEvent>
    {
        [Test]
        public void OnEntrypoint_ShouldParseDataSuccessfully()
        {
            //arrange
            var storageMock = new PropertiesBuilder()
                .Setup("tld", v => v.GetValueAsString(), "tez")
                .Build();
            var propertiesMock = new PropertiesBuilder()
                .Setup("label", v => v.GetValueAsString(), "used-shoe")
                .Setup("owner", v => v.GetValueAsAddress(), Address1)
                .Setup("address", v => v.GetValueAsNullableAddress(), Address2)
                .Setup("data", v => v.GetValueAsMap(), new Dictionary<string, string>())
                .Build();
            var block = GenerateTezosBlock();

            //act
            Target.OnEntrypoint(
                block.TezosBlock,
                "ogh1",
                "settle",
                SourceAddress,
                0,
                propertiesMock.Properties,
                storageMock.Properties
            );

            //assert
            storageMock.AssertAction();
            propertiesMock.AssertAction();
            AssertEventInUpdatesCollector(
                Target,
                new AuctionSettleEvent(
                    SourceAddress,
                    "used-shoe.tez",
                    Address1,
                    Address2,
                    new Dictionary<string, string>(),
                    block.BlockSlim,
                    "ogh1",
                    "id",
                    0,
                    0m
                )
            );
        }
    }
}