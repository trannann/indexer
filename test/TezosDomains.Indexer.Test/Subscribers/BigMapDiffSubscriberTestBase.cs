﻿using NUnit.Framework;
using TezosDomains.Indexer.Processor.Subscribers;

namespace TezosDomains.Indexer.Test.Subscribers
{
    public abstract class BigMapDiffSubscriberTestBase<TSubscriber> : SubscriberTestsBase<TSubscriber>
        where TSubscriber : BaseSubscriber, new()
    {
        [Test]
        public void OnBigMapDiff_DifferentName_ShouldNotDoAnything()
        {
            Test_OnBigMapDiff_BigMap1_ShouldNotDoAnything();
        }

        [Test]
        public void OnEntrypoint_ShouldNotDoAnything()
        {
            Test_OnEntrypoint_ShouldNotDoAnything();
        }

        [Test]
        public void OnOutgoingTransaction_ShouldNotDoAnything()
        {
            Test_OnOutgoingTransaction_ShouldNotDoAnything();
        }
    }
}