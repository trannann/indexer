﻿using NSubstitute;
using NUnit.Framework;
using System;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Indexer.Processor.Subscribers;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Test.Subscribers
{
    public class AuctionSubscriberTests : BigMapDiffSubscriberTestBase<AuctionSubscriber>
    {
        [Test]
        public void OnBigMapDiff_WithBigMapValue_ShouldCreateAuctionBidUpdate()
        {
            //arrange
            var storagePropertiesMock = new PropertiesBuilder()
                .Setup("tld", v => v.GetValueAsString(), "tez")
                .Build();
            var keyProperty = Substitute.For<IMichelsonValue>();
            keyProperty.GetValueAsString().Returns("used-shoe");
            var now = DateTime.UtcNow;
            var valuePropertiesMock = new PropertiesBuilder()
                .Setup("last_bidder", v => v.GetValueAsAddress(), Address1)
                .Setup("last_bid", v => v.GetValueAsMutezDecimal(), 10)
                .Setup("ownership_period", v => v.GetValueAsInt(), 365)
                .Setup("ends_at", v => v.GetValueAsDateTime(), now)
                .Build();

            //act
            Target.OnBigMapDiff(
                RandomTezosBlock,
                "ogh1",
                "auctions",
                keyProperty,
                valuePropertiesMock.Properties,
                storagePropertiesMock.Properties
            );

            //assert
            keyProperty.Received(1).GetValueAsString();
            valuePropertiesMock.AssertAction();
            storagePropertiesMock.AssertAction();
            AssertUpdateInCollector(Target, u => u.BidUpdates, new AuctionBidUpdate("used-shoe.tez", Address1, 10, now, now.AddDays(365)));
        }

        [Test]
        public void OnBigMapDiff_WithoutBigMapValue_ShouldCreateAuctionSettleUpdate()
        {
            //arrange
            var storagePropertiesMock = new PropertiesBuilder()
                .Setup("tld", v => v.GetValueAsString(), "tez")
                .Build();
            var keyProperty = Substitute.For<IMichelsonValue>();
            keyProperty.GetValueAsString().Returns("used-shoe");

            //act
            Target.OnBigMapDiff(
                RandomTezosBlock,
                "ogh1",
                "auctions",
                keyProperty,
                EmptyProperties,
                storagePropertiesMock.Properties
            );

            //assert
            keyProperty.Received(1).GetValueAsString();
            storagePropertiesMock.AssertAction();
            AssertUpdateInCollector(Target, u => u.AuctionSettledUpdates, "used-shoe.tez");
        }
    }
}