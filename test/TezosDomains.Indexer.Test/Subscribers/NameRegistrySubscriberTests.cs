﻿using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Indexer.Processor.Subscribers;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Test.Subscribers
{
    public class NameRegistrySubscriberTests : BigMapDiffSubscriberTestBase<NameRegistrySubscriber>
    {
        [Test]
        public void OnBigMapDiff_Records_ShouldCreateUpdate()
        {
            //arrange
            var keyProperty = Substitute.For<IMichelsonValue>();
            keyProperty.GetValueAsString().Returns("first.used-shoe.tez");
            var valuePropertiesMock = new PropertiesBuilder()
                .Setup("owner", v => v.GetValueAsAddress(), Address1)
                .Setup("address", v => v.GetValueAsNullableAddress(), Address2)
                .Setup("data", v => v.GetValueAsMap(), new Dictionary<string, string>())
                .Setup("expiry_key", v => v.GetValueAsNullableString(), "used-shoe.tez")
                .Build();

            //act
            Target.OnBigMapDiff(
                RandomTezosBlock,
                "ogh1",
                "records",
                keyProperty,
                valuePropertiesMock.Properties,
                EmptyProperties
            );

            //assert
            keyProperty.Received(1).GetValueAsString();
            valuePropertiesMock.AssertAction();
            AssertUpdateInCollector(
                Target,
                u => u.DomainUpdates,
                new DomainUpdate(
                    "first.used-shoe.tez",
                    Address2,
                    Address1,
                    "used-shoe.tez",
                    new Dictionary<string, string>()
                )
            );
        }


        [Test]
        public void OnBigMapDiff_ReverseRecords_ShouldCreateUpdate()
        {
            //arrange
            var keyProperty = Substitute.For<IMichelsonValue>();
            keyProperty.GetValueAsAddress().Returns(Address1);
            var valuePropertiesMock = new PropertiesBuilder()
                .Setup("owner", v => v.GetValueAsAddress(), Address2)
                .Setup("name", v => v.GetValueAsNullableString(), "used-shoe.tez")
                .Build();

            //act
            Target.OnBigMapDiff(
                RandomTezosBlock,
                "ogh1",
                "reverse_records",
                keyProperty,
                valuePropertiesMock.Properties,
                EmptyProperties
            );

            //assert
            keyProperty.Received(1).GetValueAsAddress();
            valuePropertiesMock.AssertAction();
            AssertUpdateInCollector(
                Target,
                u => u.ReverseRecordUpdates,
                new ReverseRecordUpdate(
                    Address1,
                    Address2,
                    "used-shoe.tez"
                )
            );
        }


        [Test]
        public void OnBigMapDiff_ExpiryMap_ShouldCreateUpdate([Values] bool valuePresent)
        {
            //arrange
            var keyProperty = Substitute.For<IMichelsonValue>();
            keyProperty.GetValueAsString().Returns("used-shoe.tez");
            var valuePropertiesBuilder = new PropertiesBuilder();
            var now = DateTime.UtcNow;
            if (valuePresent)
                valuePropertiesBuilder.Setup("__value__", v => v.GetValueAsDateTime(), now);

            var valuePropertiesMock = valuePropertiesBuilder.Build();

            //act
            Target.OnBigMapDiff(
                RandomTezosBlock,
                "ogh1",
                "expiry_map",
                keyProperty,
                valuePropertiesMock.Properties,
                EmptyProperties
            );

            //assert
            keyProperty.Received(1).GetValueAsString();
            valuePropertiesMock.AssertAction();
            AssertUpdateInCollector(
                Target,
                u => u.ValidityUpdates,
                new ValidityUpdate(
                    "used-shoe.tez",
                    valuePresent ? now : null
                )
            );
        }
    }
}