﻿using FluentAssertions;
using FluentAssertions.Equivalency;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Indexer.Mappers;
using TezosDomains.Indexer.Processor;
using TezosDomains.Indexer.Processor.Subscribers;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Test.Subscribers
{
    public abstract class SubscriberTestsBase<TSubscriber>
        where TSubscriber : BaseSubscriber, new()
    {
        protected const string SourceAddress = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n";
        protected const string Address1 = "tz1a6Sxwj5wJKa1oXDGDVBjH2ZLVXk8zVcsb";
        protected const string Address2 = "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL";
        protected TezosBlock RandomTezosBlock => GenerateTezosBlock().TezosBlock;
        private readonly IMichelsonValue _emptyMichelsonValue = Substitute.For<IMichelsonValue>();
        protected IReadOnlyDictionary<string, IMichelsonValue> EmptyProperties => new Dictionary<string, IMichelsonValue>();
        protected TSubscriber Target { get; private set; } = null!;

        [SetUp]
        public void SetUp()
        {
            Target = new TSubscriber();
        }

        protected void Test_OnEntrypoint_ShouldNotDoAnything()
        {
            //act
            Target.OnEntrypoint(RandomTezosBlock, "ogh", "different-entrypoint-name", "source", 1, EmptyProperties, EmptyProperties);

            //assert
            AssertNoUpdatesInCollector(Target);
        }

        protected void Test_OnBigMapDiff_BigMap1_ShouldNotDoAnything()
        {
            //act
            Target.OnBigMapDiff(RandomTezosBlock, "ogh", "bigmap1", _emptyMichelsonValue, EmptyProperties, EmptyProperties);

            //assert
            AssertNoUpdatesInCollector(Target);
        }

        protected void Test_OnOutgoingTransaction_ShouldNotDoAnything()
        {
            //act
            Target.OnOutgoingTransaction(RandomTezosBlock, "ogh", "dest", 1, EmptyProperties);

            //assert
            AssertNoUpdatesInCollector(Target);
        }

        protected static void AssertUpdateInCollector<TUpdate, TContainerUpdate>(
            IBlockUpdatesCollector updatesCollector,
            Func<IBlockUpdates, IReadOnlyList<TContainerUpdate>> containerSelector,
            TUpdate update,
            Func<EquivalencyAssertionOptions<TUpdate>, EquivalencyAssertionOptions<TUpdate>>? options = null
        )
            where TUpdate : TContainerUpdate
        {
            EquivalencyAssertionOptions<TUpdate> DefaultConfiguration(EquivalencyAssertionOptions<TUpdate> opt) => opt;
            containerSelector(updatesCollector.Updates).Should().BeEquivalentTo(new[] { update }, options ?? DefaultConfiguration);
        }

        private static void AssertNoUpdatesInCollector(IBlockUpdatesCollector updatesCollector)
        {
            updatesCollector.Updates.Events.Should().HaveCount(0);
            updatesCollector.Updates.AuctionSettledUpdates.Should().HaveCount(0);
            updatesCollector.Updates.BidUpdates.Should().HaveCount(0);
            updatesCollector.Updates.BidderBalancesUpdates.Should().HaveCount(0);
            updatesCollector.Updates.DomainUpdates.Should().HaveCount(0);
            updatesCollector.Updates.ReverseRecordUpdates.Should().HaveCount(0);
            updatesCollector.Updates.ValidityUpdates.Should().HaveCount(0);
        }

        protected static (TezosBlock TezosBlock, BlockSlim BlockSlim) GenerateTezosBlock()
        {
            var tezosBlock = new TezosBlock()
            {
                Hash = Guid.NewGuid().ToString(),
                Header = new TezosBlockHeader()
                {
                    Level = new Random().Next(), Timestamp = DateTime.UtcNow
                }
            };
            return (tezosBlock, BlockMapper.MapSlim(tezosBlock));
        }
    }
}