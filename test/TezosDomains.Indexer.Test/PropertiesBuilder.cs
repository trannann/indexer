﻿using NSubstitute;
using System;
using System.Collections.Generic;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Test
{
    public class PropertiesBuilder
    {
        private readonly Dictionary<string, IMichelsonValue> _properties;
        private readonly List<Action> _assertions;

        public PropertiesBuilder()
        {
            _properties = new Dictionary<string, IMichelsonValue>();
            _assertions = new List<Action>();
        }

        public PropertiesBuilder Setup<TValue>(
            string key,
            Func<IMichelsonValue, TValue> valueFunc,
            TValue value
        )
        {
            var michelsonValue = Substitute.For<IMichelsonValue>();
            valueFunc(michelsonValue).Returns(value);
            _assertions.Add(() => valueFunc(michelsonValue.Received(1)));

            _properties[key] = michelsonValue;

            return this;
        }

        private void Assert()
        {
            foreach (var assertion in _assertions)
                assertion();
        }

        public (IReadOnlyDictionary<string, IMichelsonValue> Properties, Action AssertAction) Build() => (_properties, Assert);
    }
}