﻿using NSubstitute;
using System.IO;
using System.Text.Json;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using TezosDomains.Indexer.Tezos.Clients;
using TezosDomains.Indexer.Tezos.Models;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Indexer.Tezos.Serializers;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.IntegrationTests
{
    public class TezosClientStub : ITezosClient
    {
        private readonly Channel<TezosBlock> _pendingBlocks;

        public TezosClientStub()
        {
            _pendingBlocks = Channel.CreateUnbounded<TezosBlock>();
        }


        public Task<TezosBlockHeader> GetHeadAsync(CancellationToken none)
        {
            return Task.FromResult(new TezosBlockHeader() { Level = int.MaxValue });
        }

        public async Task<ContractScript> GetContractScriptAsync(string contract, CancellationToken cancellationToken)
        {
            var path = GetTestDataFilePath($"{contract}_contract.json");

            await using var file = File.OpenRead(path);
            var result = await TezosNodeDeserializer.DeserializeAsync<JsonElement>(file!, cancellationToken);
            var scriptElement = result.GetProperty("script");
            return new ContractScript(contract, scriptElement.GetProperty("code"), scriptElement.GetProperty("storage"));
        }

        public Task<NodeVersion> GetNodeVersionAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(Substitute.For<NodeVersion>());
        }

        public Task<TezosBlock> GetBlockAsync(int level, CancellationToken cancellationToken)
        {
            return _pendingBlocks.Reader.ReadAsync(cancellationToken).AsTask();
        }

        public Task PublishBlockAsync(params string[] filePathSegments)
        {
            return _pendingBlocks.Writer.WriteAsync(ReadBlockFromFile(filePathSegments)).AsTask();
        }

        private static TezosBlock ReadBlockFromFile(params string[] filePathSegments)
        {
            var path = GetTestDataFilePath(filePathSegments);
            var content = File.ReadAllText(path, System.Text.Encoding.UTF8);
            return TezosNodeDeserializer.Deserialize<TezosBlock>(content).GuardNotNull();
        }

        private static string GetTestDataFilePath(params string[] filePathSegments)
        {
            var directory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)!;
            var path = Path.Combine(directory!, "TestData", Path.Combine(filePathSegments));
            return path;
        }
    }
}