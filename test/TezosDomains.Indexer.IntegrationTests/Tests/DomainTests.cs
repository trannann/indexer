﻿using System.Threading.Tasks;
using FluentAssertions;
using MongoDB.Driver;
using NUnit.Framework;
using TezosDomains.Data.Models;

namespace TezosDomains.Indexer.IntegrationTests.Tests
{
    public class DomainTests : TestBase
    {
        [Test]
        public async Task Add2ndLevelDomain_WithNullAddress()
        {
            //act
            await TezosClientStub.PublishBlockAsync("546155-add-alice_tez.json");

            //arrange
            var block = await WaitForBlockAsync(546155);

            var domain = await Context.Get<Domain>().Find(d => d.Name == "alice.tez").SingleAsync();
            block.Should().BeEquivalentTo(domain.Block);
            domain.Should()
                .BeEquivalentTo(
                    new
                    {
                        Address = (string?) null,
                        Owner = "tz1Q4vimV3wsfp21o7Annt64X7Hs6MXg9Wix",
                        ValidityKey = "alice.tez",
                        Name = "alice.tez"
                    }
                );
        }
    }
}