﻿using System.Threading.Tasks;
using FluentAssertions;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using NUnit.Framework;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;

namespace TezosDomains.Indexer.IntegrationTests.Tests
{
    public class BlockReorganizationTests : TestBase
    {
        [Test]
        public async Task Reorganization_ShouldRollbackAllOperations()
        {
            //arrange
            await TezosClientStub.PublishBlockAsync("BlockReorganizationTest/82664.json");
            await TezosClientStub.PublishBlockAsync("BlockReorganizationTest/82665.json");
            await TezosClientStub.PublishBlockAsync("BlockReorganizationTest/82666.json");
            var block = await WaitForBlockAsync(82666);
            var domain = await Context.Get<Domain>().Find(r => r.Name == "expired.test").SingleAsync();
            domain.Block.Should().BeEquivalentTo(block, b => b.Excluding(o => o.Predecessor));

            //act + assert
            await TezosClientStub.PublishBlockAsync("BlockReorganizationTest/82667-wrongPredecessor.json");
            WaitForBlockDeletion(block.Hash);
            var id = BsonClassMap.LookupClassMap(typeof(Domain)).IdMemberMap.ElementName;
            var domainHistory = await Context.GetHistory<Domain>().Find(Builders<HistoryOf<Domain>>.Filter.Eq(id, "expired.test")).SingleAsync();
            domainHistory.History.Should().HaveCount(1);

            await TezosClientStub.PublishBlockAsync("BlockReorganizationTest/82666-wrongPredecessor.json");
            WaitForBlockDeletion("BMXKma1fjMuUZ235ELtHMnon3sLuVJV8U61VEEUA3dotL5MQTDS");
            var count = await Context.Get<ReverseRecord>().Find(r => r.Address == "tz1ZJY5PSH6kTMvjmfbXB2DwYcaU8X8rJtKx").CountDocumentsAsync();
            count.Should().Be(0, "rollback should remove the reverse record");

            await TezosClientStub.PublishBlockAsync("BlockReorganizationTest/82665-wrongPredecessor.json");
            WaitForBlockDeletion("BLZZmpb5cJsxsZde5rZXU7RV8QsE1Z7dyhkcP2yn3PcFwHE2yvD");
            count = await Context.Get<Domain>().Find(r => r.Name == "expired.test").CountDocumentsAsync();
            count.Should().Be(0, "rollback should remove the domain record");

            await AssetCountEqualsZeroAsync(Context.Get<ReverseRecord>());
            await AssetCountEqualsZeroAsync(Context.Get<Domain>());
            await AssetCountEqualsZeroAsync(Context.Blocks);
            await AssetCountEqualsZeroAsync(Context.Get<Event>()); //todo Add Events to arrange section
        }

        private async Task AssetCountEqualsZeroAsync<TDocument>(IMongoCollection<TDocument> collection)
        {
            var count = await collection.CountDocumentsAsync(FilterDefinition<TDocument>.Empty);
            count.Should().Be(0);
        }
    }
}