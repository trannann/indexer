﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using FluentAssertions;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using NUnit.Framework;
using TezosDomains.Data.Models;

namespace TezosDomains.Indexer.IntegrationTests.Tests
{
    public class ReverseRecordTests : TestBase
    {
        [Test]
        public async Task CreateReverseRecordAsync()
        {
            //act
            await TezosClientStub.PublishBlockAsync("ReverseRecordTest/82658.json");
            await TezosClientStub.PublishBlockAsync("ReverseRecordTest/82659.json");

            //assert
            var block = await WaitForBlockAsync(82659);

            var domain = await Context.Get<Domain>().Find(d => d.Name == "ok.test").SingleAsync();
            var expiresAtUtc = DateTime.Parse("2100-01-31 23:00:00", styles: DateTimeStyles.AssumeUniversal).ToUniversalTime();
            var expectation = new
            {
                Address = "tz1goJEvA7TNvFUcHUtnWdfM72QmuwbLsuxM",
                Owner = "tz1goJEvA7TNvFUcHUtnWdfM72QmuwbLsuxM",
                ValidityKey = "ok.test",
                Name = "ok.test",
                ExpiresAtUtc = expiresAtUtc
            };
            domain.Should().BeEquivalentTo(expectation);
            var id = BsonClassMap.LookupClassMap(typeof(Domain)).IdMemberMap.ElementName;
            var domainHistory = await Context.GetHistory<Domain>().Find(Builders<HistoryOf<Domain>>.Filter.Eq(id, "ok.test")).SingleAsync();
            domainHistory.History.Should().BeEquivalentTo(new { Block = new { Level = 82658 } });

            var reverseRecord = await Context.Get<ReverseRecord>().Find(r => r.Address == "tz1goJEvA7TNvFUcHUtnWdfM72QmuwbLsuxM").SingleAsync();
            reverseRecord.Should().BeEquivalentTo(expectation);
            reverseRecord.Block.Should().BeEquivalentTo(block, b => b.Excluding(o => o.Predecessor));
            var reverseRecordHistory = await Context.GetHistory<ReverseRecord>()
                .Find(Builders<HistoryOf<ReverseRecord>>.Filter.Eq(id, "tz1goJEvA7TNvFUcHUtnWdfM72QmuwbLsuxM"))
                .SingleAsync();
            reverseRecordHistory.History.Should().BeEquivalentTo(new { Block = new { Level = 82659 } });
        }
    }
}