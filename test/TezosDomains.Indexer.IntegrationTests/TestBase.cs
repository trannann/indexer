﻿using FluentAssertions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using NUnit.Framework;
using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Configurations;
using TezosDomains.Indexer.Tezos.Clients;

namespace TezosDomains.Indexer.IntegrationTests
{
    public class TestBase
    {
        private static readonly string AppName = Assembly.GetAssembly(typeof(TestBase))!.GetName().Name!;
        private IHost _host = null!;

        [SetUp]
        public async Task SetUpBase()
        {
            // start our host
            _host = Program.CreateHostBuilder()
                .ConfigureAppConfiguration(
                    (ctx, builder) =>
                    {
                        ctx.HostingEnvironment.ApplicationName = AppName;

                        // force testsettings.json and custom user secrets to avoid loading any of the original settings for the project
                        builder.Sources.Clear();
                        builder
                            .AddJsonFile("testsettings.json")
                            .AddUserSecrets<Program>(optional: true)
                            .AddEnvironmentVariables();
                    }
                )
                .ConfigureServices(
                    serviceCollection =>
                    {
                        serviceCollection.RemoveAll(typeof(IMongoDbConfiguration));
                        serviceCollection.AddSingleton<IMongoDbConfiguration>(
                            sp =>
                            {
                                var configuration = sp.GetRequiredService<IConfiguration>();
                                var mongoDbConfiguration = configuration.GetSection("TezosDomains:MongoDb").Get<MongoDbConfiguration>();
                                mongoDbConfiguration.OverrideDatabaseName(_ => $"t_{Guid.NewGuid()}");
                                return mongoDbConfiguration;
                            }
                        );

                        serviceCollection.RemoveAll(typeof(ITezosClient));
                        serviceCollection.AddSingleton<TezosClientStub>();
                        serviceCollection.AddSingleton<ITezosClient>(sp => sp.GetRequiredService<TezosClientStub>());

                        serviceCollection.RemoveAll(typeof(ILogger<>));
                        serviceCollection.AddSingleton(typeof(ILogger<>), typeof(TestLogger.NUnitLogger<>));
                    }
                )
                .Build();
            await _host.StartAsync();

            Context = _host.Services.GetRequiredService<MongoDbContext>();
        }

        protected TezosClientStub TezosClientStub => _host.Services.GetRequiredService<TezosClientStub>();
        protected MongoDbContext Context { get; private set; } = null!;

        [TearDown]
        public async Task TearDownBase()
        {
            await DropDatabaseAsync();
            await _host.StopAsync();
            _host.Dispose();
        }

        private async Task DropDatabaseAsync()
        {
            var config = _host.Services.GetRequiredService<IMongoDbConfiguration>();
            var client = _host.Services.GetRequiredService<MongoClient>();
            await client.DropDatabaseAsync(config.Database);
        }

        protected Task<Block> WaitForBlockAsync(int level)
        {
            var cancellationToken = new CancellationTokenSource(TimeSpan.FromSeconds(30)).Token;
            var watchDefinition = new EmptyPipelineDefinition<ChangeStreamDocument<Block>>()
                .Match(x => x.FullDocument.Level == level && x.OperationType == ChangeStreamOperationType.Insert);
            var cursor = Context.Blocks.Watch(watchDefinition, cancellationToken: cancellationToken);

            try
            {
                return Task.Run(
                    () =>
                    {
                        foreach (var change in cursor.ToEnumerable(cancellationToken))
                        {
                            cursor.Dispose();
                            return change.FullDocument;
                        }

                        throw new InvalidOperationException($"Block with level: {level} was not found");
                    },
                    cancellationToken
                );
            }
            catch (OperationCanceledException)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    Assert.Fail($"Block {level} has not appeared in time");
                }

                throw;
            }
        }

        protected void WaitForBlockDeletion(string blockHash)
        {
            var cancellationToken = new CancellationTokenSource(TimeSpan.FromSeconds(10)).Token;

            var watchDefinition = new EmptyPipelineDefinition<ChangeStreamDocument<Block>>()
                .Match(x => x.OperationType == ChangeStreamOperationType.Delete);
            try
            {
                using var cursor = Context.Blocks.Watch(watchDefinition, cancellationToken: cancellationToken);
                foreach (var change in cursor.ToEnumerable(cancellationToken))
                {
                    change.DocumentKey["_id"].AsString.Should().Be(blockHash);
                    return;
                }
            }
            catch (OperationCanceledException)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    Assert.Fail($"Block {blockHash} has not been deleted in time");
                }

                throw;
            }
        }
    }
}