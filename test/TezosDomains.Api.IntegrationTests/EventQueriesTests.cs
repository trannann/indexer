﻿using FluentAssertions;
using FluentAssertions.Equivalency;
using GraphQL.Types.Relay.DataObjects;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Api.IntegrationTests.Dto;
using TezosDomains.Api.IntegrationTests.TestData;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Utils;

namespace TezosDomains.Api.IntegrationTests
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public sealed class
        EventQueriesTests : TestBase
    {
        private Event[] _events = new Event[0];
        private ReverseRecord[] _reverseRecords = new ReverseRecord[0];
        private Domain[] _domains = new Domain[0];

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            var today = DateTime.UtcNow.Date;
            _events = new Event[]
            {
                TestEvent.Auction.Bid("tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", "auction-bid.aaa", 1, new BlockSlim(1, "h", today), "ogh1"),
                TestEvent.Auction.Withdraw("tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7", "bbb", 1000, new BlockSlim(2, "h", today.AddMinutes(1)), "ogh2"),
                TestEvent.Auction.End(
                    "auction-end.ccc",
                    2,
                    "tz1NRTQeqcuwybgrZfJavBY3of83u8uLpFBj",
                    new BlockSlim(3, "h", today.AddMinutes(2)),
                    previousParticipants: "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL"
                ),
                TestEvent.Auction.Settle(
                    "tz1RomaiWJV3NFDZWTMVR2aEeHknsn3iF5Gi",
                    "auction-settle.ddd",
                    "tz1RomaiWJV3NFDZWTMVR2aEeHknsn3iF5Gi",
                    "tz1RomaiWJV3NFDZWTMVR2aEeHknsn3iF5Gi",
                    new Dictionary<string, string> { { "k1", "v1" } },
                    new BlockSlim(5, "h", today.AddMinutes(4)),
                    "ogh5"
                ),
                TestEvent.Domain.Buy(
                    "tz1f8Ybid6x2pMvJGauz3Zi8enkABSSkGPn8",
                    "domain-buy.eee",
                    4,
                    365,
                    "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    null,
                    new Dictionary<string, string> { { "k2", "v2" } },
                    new BlockSlim(6, "h", today.AddMinutes(5)),
                    "ogh5"
                ),
                TestEvent.Domain.Renew(
                    "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL",
                    "domain-renew.fff",
                    5,
                    365 * 2,
                    new BlockSlim(7, "h", today.AddMinutes(6)),
                    "ogh6"
                ),
                TestEvent.Domain.Commit(
                    "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    new BlockSlim(8, "h", today.AddMinutes(7)),
                    "ogh8"
                ),
                TestEvent.Domain.Update(
                    "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    "domain-update.ggg",
                    "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    domainForwardRecordAddress: null,
                    new Dictionary<string, string> { { "k3", "v3" } },
                    new BlockSlim(9, "h", today.AddMinutes(8)),
                    "ogh9"
                ),
                TestEvent.Domain.SetChildRecord(
                    "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    "domain-set-child-record.hhh",
                    "tz1XyaS3pWSQHrKxvnJ9Kmpk9PKgWWunYXzU",
                    "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    new Dictionary<string, string> { { "k4", "v4" } },
                    new BlockSlim(10, "h", today.AddMinutes(9)),
                    "ogh10"
                ),
                TestEvent.ReverseRecord.Claim(
                    "tz1XyaS3pWSQHrKxvnJ9Kmpk9PKgWWunYXzU",
                    null,
                    "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    new BlockSlim(11, "h", today.AddMinutes(10)),
                    "ogh11"
                ),
                TestEvent.ReverseRecord.Update(
                    "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    "reverse-record.iii",
                    "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    "tz1XyaS3pWSQHrKxvnJ9Kmpk9PKgWWunYXzU",
                    new BlockSlim(12, "h", today.AddMinutes(11)),
                    "ogh12"
                ),
                TestEvent.Auction.Bid(
                    "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7",
                    "auction-bid.aaa",
                    2,
                    new BlockSlim(13, "h", today.AddMinutes(12)),
                    "ogh1",
                    previousBidderAddress: "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL",
                    previousBidAmount: 1
                ),
            };
            _events = _events.OrderByDescending(e => e.Block.Timestamp).ToArray(); // Put in the same order as query output.

            Insert(_events);

            _domains = new[]
            {
                TestDomain.Get("valid.domain", "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", expiresAtUtc: today.AddYears(1)),
                TestDomain.Get("expired.domain", "tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7", expiresAtUtc: today.AddDays(-1))
            };
            Insert(_domains);

            _reverseRecords = new[]
            {
                TestReverseRecord.Get("tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", "valid.domain", expiresAtUtc: today.AddYears(1)), //valid rr with valid domain
                TestReverseRecord.Get("tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7", "expired.domain", expiresAtUtc: today.AddDays(-1)), //invalid rr
                TestReverseRecord.Get("tz1XyaS3pWSQHrKxvnJ9Kmpk9PKgWWunYXzU", name: null, expiresAtUtc: null) //valid rr without domain
            };
            Insert(_reverseRecords);
        }

        [Test]
        public async Task ShouldLoadAllFields()
        {
            var expectedNotNullReverseRecords = new[]
            {
                new { SourceAddress = _reverseRecords[0].Address, SourceAddressReverseRecord = ReverseRecordDto.Parse(_reverseRecords[0], _domains[0]) },
                new { SourceAddress = _reverseRecords[2].Address, SourceAddressReverseRecord = ReverseRecordDto.Parse(_reverseRecords[2]) },
            }.ToDictionary(r => r.SourceAddress);

            var data = await GetAllAsync("where:{}, first: 20");

            //assert all properties except Id, Data, AuctionEndEvent.Participants
            data.Items.Should()
                .BeEquivalentTo(
                    _events,
                    o => o
                        .Excluding(f => f.Id)
                        .Excluding((IMemberInfo mi) => mi.SelectedMemberInfo.Name == nameof(EventDto.Data))
                        .Excluding((IMemberInfo mi) => mi.SelectedMemberInfo.Name == nameof(EventDto.Participants))
                        .ExcludingMissingMembers()
                        .RespectingRuntimeTypes()
                        .WithStrictOrdering()
                );
            //assert Id
            data.Items.Select(i => i.Id).Should().BeEquivalentTo(_events.Select(e => $"{e.GetType().Name}:{e.Id}"));
            //assert Data
            data.Items.Select(i => new { Data = i.Data?.ToDictionary(d => d.Key.GuardNotNull(), d => d.RawValue) })
                .Should()
                .BeEquivalentTo(
                    _events,
                    o => o.ExcludingMissingMembers().RespectingRuntimeTypes().WithStrictOrdering()
                );
            //assert AuctionEndEvent.Participants
            data.Items.Select(i => new { Participants = i.Participants?.Select(p => p.Address).ToArray() })
                .Should()
                .BeEquivalentTo(
                    _events,
                    o => o.ExcludingMissingMembers().RespectingRuntimeTypes().WithStrictOrdering()
                );

            //assert fetched valid reverse records with their domain
            foreach (var expectedNotNullReverseRecord in expectedNotNullReverseRecords.Values)
            {
                data.Items.Where(i => i.SourceAddress == expectedNotNullReverseRecord.SourceAddress)
                    .Should()
                    .AllBeEquivalentTo(expectedNotNullReverseRecord, o => o.ExcludingMissingMembers());
            }

            //sourceAddress without reverse record or with invalid has SourceAddressReverseRecord NULL
            data.Items.Where(i => i.SourceAddress == null || !expectedNotNullReverseRecords.ContainsKey(i.SourceAddress))
                .Select(i => i.SourceAddressReverseRecord)
                .Should()
                .AllBeEquivalentTo((ReverseRecordDto?) null);
        }

        [TestCase("startsWith: TZ2")]
        [TestCase(@"in: [""tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL""]")]
        [TestCase(@"notIn: [""tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n""]")]
        public async Task FilterByAddress_ShouldReturnAuctionEndEventThroughParticipants(string filter)
        {
            var data = await GetAllAsync($"where:{{address:{{ {filter} }}type:{{in: AUCTION_END_EVENT }} }}");

            //assert all properties except Id, Data
            data.Items.Single()
                .Should()
                .BeEquivalentTo(
                    _events.OfType<AuctionEndEvent>().Single(),
                    o => o
                        .Excluding(f => f.Id)
                        .Excluding(f => f.Participants)
                        .ExcludingMissingMembers()
                        .RespectingRuntimeTypes()
                );
        }

        [Test]
        public Task ShouldOrderByTimestampDescending()
            => RunTest(
                "where: {}",
                expectedTypes: new[]
                {
                    "AUCTION_BID_EVENT", "REVERSE_RECORD_UPDATE_EVENT", "REVERSE_RECORD_CLAIM_EVENT", "DOMAIN_SET_CHILD_RECORD_EVENT", "DOMAIN_UPDATE_EVENT",
                    "DOMAIN_COMMIT_EVENT", "DOMAIN_RENEW_EVENT", "DOMAIN_BUY_EVENT", "AUCTION_SETTLE_EVENT", "AUCTION_END_EVENT"
                }
            );

        [TestCase("domainName", "auction-settle.ddd", new[] { "AUCTION_SETTLE_EVENT" })]
        [TestCase(
            "address",
            "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL",
            new[] { "AUCTION_BID_EVENT", "DOMAIN_RENEW_EVENT", "AUCTION_END_EVENT" },
            Description = "Filter by SourceAddress, AuctionBidEvent.PreviousBidderAddress, AuctionEndEvent.Participants"
        )]
        public Task ShouldFilterByField(string field, string value, string[] expectedTypes)
            => RunTest($@"where: {{ {field}: {{ equalTo: ""{value}"" }} }}", expectedTypes);

        [TestCase("AUCTION_BID_EVENT, DOMAIN_BUY_EVENT", "AUCTION_BID_EVENT", "DOMAIN_BUY_EVENT", "AUCTION_BID_EVENT")]
        [TestCase("AUCTION_END_EVENT", "AUCTION_END_EVENT")]
        public Task ShouldFilterByType(string types, params string[] expectedTypes)
            => RunTest(@$"where: {{ type: {{ in: [{types}] }} }}", expectedTypes);


        [TestCase(null, 1, "AUCTION_BID_EVENT")]
        [TestCase(2, 5, "DOMAIN_BUY_EVENT", "AUCTION_SETTLE_EVENT")]
        [TestCase(8, null, "AUCTION_BID_EVENT", "REVERSE_RECORD_UPDATE_EVENT", "REVERSE_RECORD_CLAIM_EVENT")]
        public Task ShouldFilterByTimestamp(int? fromEventIndex, int? toEventIndex, params string[] expectedTypes)
        {
            var eventsInInitialOrder = _events.Reverse().ToArray();
            var from = fromEventIndex.HasValue ? eventsInInitialOrder[fromEventIndex.Value].Block.Timestamp.ToString("O") : null;
            var to = toEventIndex.HasValue ? eventsInInitialOrder[toEventIndex.Value].Block.Timestamp.ToString("O") : null;

            return (from, to) switch
            {
                (_, null) => RunTest(@$"where: {{ block: {{ timestamp: {{ greaterThan: ""{from}"" }} }} }}", expectedTypes),
                (null, _) => RunTest(@$"where: {{ block: {{ timestamp: {{ lessThan: ""{to}"" }} }} }}", expectedTypes),
                (_, _) => RunTest(
                    @$"where: {{ block: {{ timestamp: {{ lessThan: ""{to}"" }} }}, and: {{ block: {{ timestamp: {{ greaterThan: ""{from}"" }} }} }} }}",
                    expectedTypes
                )
            };
        }

        private async Task RunTest(string queryArgs, string[] expectedTypes)
        {
            var entities = await GetAllAsync(queryArgs);

            entities.Items.Select(p => p.Type).Should().Equal(expectedTypes);
        }

        private Task<Connection<EventDto>> GetAllAsync(string queryArgs)
            => SendToGraphQLAsync<Connection<EventDto>>($"events({queryArgs}) {Edges(EventDto.FieldsQuery)}");
    }
}