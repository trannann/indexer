using FluentAssertions;
using GraphQL.Types.Relay.DataObjects;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Api.IntegrationTests.Dto;
using TezosDomains.Api.IntegrationTests.Dto.Auctions;
using TezosDomains.Api.IntegrationTests.TestData;
using TezosDomains.Data.Models;

namespace TezosDomains.Api.IntegrationTests
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class AuctionQueriesTests : TestBase
    {
        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            Context.Blocks.InsertOne(new Block(0, Guid.NewGuid().ToString(), DateTime.UtcNow.AddDays(-2), Guid.NewGuid().ToString()));
            Context.Blocks.InsertOne(new Block(1, Guid.NewGuid().ToString(), DateTime.UtcNow.AddDays(-1), Guid.NewGuid().ToString()));
            Context.Blocks.InsertOne(new Block(2, Guid.NewGuid().ToString(), DateTime.UtcNow, Guid.NewGuid().ToString()));

            #region Insert Auctions

            InsertAuction(
                "default.auction:65431",
                firstBidAtUtc: GetTime(2000, 2, 3),
                endsAtUtc: GetTime(2150, 2, 4),
                ownedUntilUtc: GetTime(2200, 2, 3),
                isSettled: false,
                bids: new[]
                {
                    (bidder: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", amount: 2m),
                    (bidder: "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm", amount: 1m)
                }
            );
            InsertAuction(
                "can-be-settled.auction:0",
                firstBidAtUtc: GetTime(2000, 2, 3),
                endsAtUtc: GetTime(2000, 2, 3),
                ownedUntilUtc: GetTime(2200, 2, 3),
                isSettled: false,
                bids: (bidder: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", amount: 10m)
            );
            InsertAuction(
                "settled.auction:0",
                firstBidAtUtc: GetTime(2000, 2, 2),
                endsAtUtc: GetTime(2000, 2, 2),
                ownedUntilUtc: GetTime(2200, 2, 3),
                isSettled: true,
                bids: new[]
                {
                    (bidder: "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm", amount: 1000m),
                    (bidder: "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm", amount: 2m),
                    (bidder: "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm", amount: 1m)
                }
            );
            InsertAuction(
                "settlement-expired.auction:0",
                firstBidAtUtc: GetTime(2000, 2, 1),
                endsAtUtc: GetTime(2000, 2, 1),
                ownedUntilUtc: GetTime(2000, 2, 3),
                isSettled: false,
                bids: (bidder: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", amount: 100m)
            );

            #endregion

            Insert(TestDomain.Get("bidder.tez", address: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n"));
            Insert(TestReverseRecord.Get("tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", "bidder.tez"));
        }

        private void InsertAuction(
            string id,
            DateTime? firstBidAtUtc = null,
            DateTime? endsAtUtc = null,
            DateTime? ownedUntilUtc = null,
            bool isSettled = false,
            params (string bidder, decimal amount)[] bids
        )
            => InsertWithHistory(
                document: TestAuction.Get(id, endsAtUtc, ownedUntilUtc, firstBidAtUtc, isSettled, bids: bids),
                history: new[]
                {
                    TestAuction.Get(
                        id,
                        endsAtUtc,
                        ownedUntilUtc,
                        firstBidAtUtc,
                        isSettled,
                        bids: bids
                    ),
                    TestAuction.Get(
                        id,
                        endsAtUtc,
                        ownedUntilUtc,
                        firstBidAtUtc,
                        bids: (bidder: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", amount: 66m),
                        block: (Level: 1, DaysOffset: -1),
                        validUntil: (GetTime(2000, 2, 3), Level: 2)
                    ),
                }
            );

        #region Auction

        [Test]
        public async Task Auction_ShouldLoadAllFields()
        {
            var auction = await GetAuctionAsync(@"domainName: ""default.auction"", startedAtLevel: 65431");

            VerifyTestAuction(auction!);
        }

        [Test]
        public async Task Auction_ShouldLoadNull_IfNotExist()
        {
            var auction = await GetAuctionAsync(@"domainName: ""nonexisting.auction"", startedAtLevel: 0");

            auction.Should().BeNull();
        }

        [TestCase("default.auction", "IN_PROGRESS")]
        [TestCase("can-be-settled.auction", "CAN_BE_SETTLED")]
        [TestCase("settled.auction", "SETTLED")]
        [TestCase("settlement-expired.auction", "SETTLEMENT_EXPIRED")]
        public async Task Auction_ShouldFilterByState(string domainName, string state)
        {
            var auction = await GetAuctionsAsync($@"where: {{domainName: {{ equalTo: ""{domainName}"" }}, state: {{ in: {state} }} }}");

            (auction.Items.SingleOrDefault()?.DomainName).Should().Be(domainName);
        }

        [TestCase(0, null)]
        [TestCase(1, "CAN_BE_SETTLED")]
        [TestCase(2, "SETTLED")]
        public async Task Auction_HistoricQuery_ShouldShowCorrectState(int blockLevel, string? stateOrNotFound)
        {
            var auctions = await GetAuctionsAsync(
                $@"where: {{ domainName: {{ equalTo: ""settled.auction"" }} }},
                atBlock: {{ level: {blockLevel} }}"
            );
            var auction = await GetAuctionAsync($@"domainName: ""settled.auction"", startedAtLevel: 0, atBlock: {{ level: {blockLevel} }}");

            if (stateOrNotFound == null)
            {
                auctions.Items.Should().BeEmpty();
                auction.Should().BeNull();
            }
            else
            {
                auctions.Items.Should().HaveCount(1);
                auctions.Items.First().State.Should().Be(stateOrNotFound);
                auction.Should().NotBeNull();
                auction!.State.Should().Be(stateOrNotFound);
            }
        }

        #endregion

        #region CurrentAuction

        [Test]
        public async Task CurrentAuction_ShouldShowOnlyRunningAuctions()
        {
            var currentAuction = await SendToGraphQLAsync<AuctionDto?>(
                $@"currentAuction( domainName: ""default.auction"" ) {AuctionDto.FieldsQuery}"
            );

            VerifyTestAuction(currentAuction);
        }

        #endregion

        #region Auctions

        [Test]
        public async Task Auctions_ShouldLoadAllFields()
        {
            var auctions = await GetAuctionsAsync(@"where: { domainName: { equalTo: ""default.auction"" }}");

            auctions.Items.Should().HaveCount(1);
            VerifyTestAuction(auctions.Items.First());
        }

        [Test]
        public Task Auctions_ShouldOrderByEndsAtDescByDefault()
            => RunAuctionsTest(
                "where: {}",
                expectedNames: new[] { "default.auction", "can-be-settled.auction", "settled.auction", "settlement-expired.auction" }
            );

        [TestCase("field: ID direction: ASC", "can-be-settled.auction", "default.auction", "settled.auction", "settlement-expired.auction")]
        [TestCase("field: DOMAIN_NAME direction: DESC", "settlement-expired.auction", "settled.auction", "default.auction", "can-be-settled.auction")]
        [TestCase("field: ENDS_AT direction: ASC", "settlement-expired.auction", "settled.auction", "can-be-settled.auction", "default.auction")]
        [TestCase("field: HIGHEST_BID_AMOUNT direction: DESC", "settled.auction", "settlement-expired.auction", "can-be-settled.auction", "default.auction")]
        [TestCase("field: HIGHEST_BID_TIMESTAMP direction: DESC", "settlement-expired.auction", "settled.auction", "can-be-settled.auction", "default.auction")]
        [TestCase("field: BID_COUNT direction: DESC", "settled.auction", "default.auction", "can-be-settled.auction", "settlement-expired.auction")]
        public Task Auctions_ShouldOrderAccordingToQuery(string order, params string[] expectedNames)
            => RunAuctionsTest($"order: {{ {order} }}", expectedNames);

        [TestCase("domainName", "equalTo", "\"default.auction\"", "default.auction")] // Filter should be case-insensitive
        [TestCase("state", "in", @"IN_PROGRESS", "default.auction")]
        [TestCase("highestBidder", "equalTo", "\"tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm\"", "settled.auction")]
        [TestCase("bidders", "include", "\"tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm\"", "default.auction", "settled.auction")]
        [TestCase("endsAtUtc", "greaterThan", "\"2150-02-03T00:00:00Z\"", "default.auction")]
        [TestCase("bidCount", "greaterThan", "2", "settled.auction")]
        public Task Auctions_ShouldFilterByField(string field, string filterType, string value, params string[] expectedNames)
            => RunAuctionsTest(@$"where: {{ {field}: {{ {filterType}: {value} }}}}", expectedNames);

        #endregion

        private async Task RunAuctionsTest(string queryArgs, string[] expectedNames)
        {
            var auctions = await GetAuctionsAsync(queryArgs);

            auctions.Items.Select(d => d.DomainName).Should().Equal(expectedNames);
        }

        private static void VerifyTestAuction(AuctionDto? auction)
        {
            auction.Should().NotBeNull();
            var expected = new AuctionDto
            {
                Id = "Auction:default.auction:65431",
                TypeName = "Auction",
                DomainName = "default.auction",
                EndsAtUtc = GetTime(2150, 2, 4),
                OwnedUntilUtc = GetTime(2200, 2, 3),
                State = "IN_PROGRESS",
                BidCount = 2,
                Bids = new[]
                {
                    new BidDto()
                    {
                        Id = "Bid:default.auction:65431:2",
                        TypeName = "Bid",
                        Bidder = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                        Amount = 2,
                        BidderReverseRecord = new ReverseRecordDto()
                        {
                            TypeName = "ReverseRecord",
                            Id = "ReverseRecord:tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                            Name = "bidder.tez",
                            Address = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                            Owner = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                            Domain = new DomainDto() { Address = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", Name = "bidder.tez" },
                            OwnerReverseRecord = new ReverseRecordDto() { Address = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n" }
                        }
                    },
                    new BidDto()
                    {
                        Id = "Bid:default.auction:65431:1",
                        TypeName = "Bid",
                        Bidder = "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm",
                        Amount = 1
                    }
                },
                HighestBid = new BidDto()
                {
                    Id = "Bid:default.auction:65431:2",
                    TypeName = "Bid",
                    Bidder = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                    Amount = 2,
                    BidderReverseRecord = new ReverseRecordDto()
                    {
                        TypeName = "ReverseRecord",
                        Id = "ReverseRecord:tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                        Name = "bidder.tez",
                        Address = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                        Owner = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                        Domain = new DomainDto() { Address = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", Name = "bidder.tez" },
                        OwnerReverseRecord = new ReverseRecordDto() { Address = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n" }
                    }
                },
                IsSettled = false,
                StartedAtLevel = 65431
            };
            auction.Should().BeEquivalentTo(expected);
        }

        private Task<AuctionDto?> GetAuctionAsync(string queryArgs)
            => SendToGraphQLAsync<AuctionDto?>($@"auction({queryArgs}) {AuctionDto.FieldsQuery}");

        private Task<Connection<AuctionDto>> GetAuctionsAsync(string queryArgs)
            => SendToGraphQLAsync<Connection<AuctionDto>>($"auctions({queryArgs}) {Edges(AuctionDto.FieldsQuery)}");
    }
}