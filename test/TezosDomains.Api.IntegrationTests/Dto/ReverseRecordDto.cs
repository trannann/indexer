﻿using System;
using TezosDomains.Data.Models;

namespace TezosDomains.Api.IntegrationTests.Dto
{
    public sealed class ReverseRecordDto : NodeDto
    {
        public static readonly string FieldsQuery = @$"{{
            {NodeFieldsQuery}
            address
            name
            owner
            ownerReverseRecord {{ address }}
            expiresAtUtc
            domain {{ name address }} 
        }}";

        public string? Address { get; set; }
        public string? Name { get; set; }
        public string? Owner { get; set; }
        public ReverseRecordDto? OwnerReverseRecord { get; set; }
        public DateTime? ExpiresAtUtc { get; set; }
        public DomainDto? Domain { get; set; }

        public static ReverseRecordDto? Parse(ReverseRecord? rr, Domain? d = null)
        {
            if (rr == null)
                return null;

            return new ReverseRecordDto()
            {
                Name = rr.Name,
                Address = rr.Address,
                Domain = d != null
                    ? new DomainDto()
                    {
                        Address = d.Address,
                        Name = d.Name
                    }
                    : null,
                OwnerReverseRecord = new ReverseRecordDto() { Address = rr.Owner },
                ExpiresAtUtc = rr.ExpiresAtUtc,
                Id = $"ReverseRecord:{rr.Address}",
                Owner = rr.Owner,
                TypeName = nameof(ReverseRecord)
            };
        }
    }
}