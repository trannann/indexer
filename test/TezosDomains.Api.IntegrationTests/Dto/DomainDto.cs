﻿using GraphQL.Types.Relay.DataObjects;
using System;

namespace TezosDomains.Api.IntegrationTests.Dto
{
    public sealed class DomainDto : NodeDto
    {
        public static readonly string FieldsQuery = $@"{{
            {NodeFieldsQuery}
            name
            address
            owner
            expiresAtUtc
            level
            data {DataItemDto.FieldsQuery}
            subdomains {TestBase.Edges("{ name }")}
            reverseRecord {ReverseRecordDto.FieldsQuery}

            addressReverseRecord {ReverseRecordDto.FieldsQuery}
            ownerReverseRecord {ReverseRecordDto.FieldsQuery}
        }}";

        public string? Name { get; set; }
        public string? Address { get; set; }
        public string? Owner { get; set; }
        public DateTime? ExpiresAtUtc { get; set; }
        public int Level { get; set; }
        public DataItemDto[]? Data { get; set; }
        public Connection<DomainDto> Subdomains { get; set; } = new Connection<DomainDto>();
        public ReverseRecordDto? ReverseRecord { get; set; }
        public ReverseRecordDto? AddressReverseRecord { get; set; }
        public ReverseRecordDto? OwnerReverseRecord { get; set; }
    }
}