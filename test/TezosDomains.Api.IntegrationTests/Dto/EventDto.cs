﻿namespace TezosDomains.Api.IntegrationTests.Dto
{
    public sealed class EventDto : NodeDto
    {
        public static readonly string FieldsQuery = @$"{{
        type
        id
        block {BlockDto.FieldsQuery}
        sourceAddress
        sourceAddressReverseRecord {ReverseRecordDto.FieldsQuery}
        
        ... on AuctionBidEvent {{
            operationGroupHash
            bidAmount
            transactionAmount
            domainName
            previousBidAmount
            previousBidderAddress
            previousBidderAddressReverseRecord {ReverseRecordDto.FieldsQuery}
          }}

          ... on AuctionEndEvent {{
            winningBid
            domainName
            participants {AuctionParticipantDto.FieldsQuery}
          }}

          ... on AuctionWithdrawEvent {{
            operationGroupHash
            tldName
            withdrawnAmount
          }}

          ... on AuctionSettleEvent {{
            operationGroupHash
            domainName
            domainOwnerAddress
            domainForwardRecordAddress
            data {DataItemDto.FieldsQuery}
            registrationDurationInDays
            winningBid
          }}

          ... on DomainBuyEvent {{
            operationGroupHash
            domainName
            price
            durationInDays
            domainOwnerAddress
            domainForwardRecordAddress
            data {DataItemDto.FieldsQuery}
          }}

          ... on DomainRenewEvent {{
            operationGroupHash
            domainName
            durationInDays
            price
          }}

          ... on DomainSetChildRecordEvent {{
            operationGroupHash
            domainName
            domainOwnerAddress
            domainForwardRecordAddress
            isNewRecord
            data {DataItemDto.FieldsQuery}
          }}

          ... on DomainUpdateEvent {{
            operationGroupHash
            domainName
            domainOwnerAddress
            domainForwardRecordAddress
            data {DataItemDto.FieldsQuery}
          }}

          ... on DomainCommitEvent {{
            operationGroupHash
            commitmentHash
          }}

          ... on ReverseRecordUpdateEvent {{
            operationGroupHash
            name
            reverseRecordOwnerAddress
            reverseRecordAddress
          }}

          ... on ReverseRecordClaimEvent {{
            operationGroupHash
            name
            reverseRecordOwnerAddress
          }}
        }}";

        public string? Type { get; set; }
        public decimal? WithdrawnAmount { get; set; }
        public decimal? WinningBid { get; set; }
        public decimal? BidAmount { get; set; }
        public decimal? TransactionAmount { get; set; }
        public decimal? PreviousBidAmount { get; set; }
        public decimal? Price { get; set; }
        public int? DurationInDays { get; set; }
        public string? DomainOwnerAddress { get; set; }
        public string? DomainForwardRecordAddress { get; set; }
        public string? TldName { get; set; }
        public string? SourceAddress { get; set; }
        public string? DomainName { get; set; }
        public string? Name { get; set; }
        public DataItemDto[]? Data { get; set; }
        public BlockDto? Block { get; set; }
        public string? CommitmentHash { get; set; }
        public string? OperationGroupHash { get; set; }
        public int? RegistrationDurationInDays { get; set; }
        public ReverseRecordDto? SourceAddressReverseRecord { get; set; }
        public bool? IsNewRecord { get; set; }
        public AuctionParticipantDto[]? Participants { get; set; }
    }
}