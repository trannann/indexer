﻿namespace TezosDomains.Api.IntegrationTests.Dto
{
    public sealed class DataItemDto
    {
        public const string FieldsQuery = "{ key rawValue }";

        public string? Key { get; set; }
        public string? RawValue { get; set; }
    }
}