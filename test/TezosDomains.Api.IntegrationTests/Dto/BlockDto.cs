﻿using System;

namespace TezosDomains.Api.IntegrationTests.Dto
{
    public class BlockDto
    {
        public const string FieldsQuery = @"{
            level
            hash
            timestamp
        }";

        public int Level { get; set; }
        public string? Hash { get; set; }
        public DateTime Timestamp { get; set; }
    }
}