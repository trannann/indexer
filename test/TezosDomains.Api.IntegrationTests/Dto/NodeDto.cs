﻿using Newtonsoft.Json;

namespace TezosDomains.Api.IntegrationTests.Dto
{
    public abstract class NodeDto
    {
        private const string TypeNameField = "__typename";
        public const string NodeFieldsQuery = "id " + TypeNameField;

        public string? Id { get; set; }

        [JsonProperty(PropertyName = TypeNameField)]
        public string? TypeName { get; set; }
    }
}