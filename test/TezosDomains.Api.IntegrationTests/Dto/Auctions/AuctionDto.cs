﻿using System;
using System.Collections.Generic;

namespace TezosDomains.Api.IntegrationTests.Dto.Auctions
{
    public class AuctionDto : NodeDto
    {
        public static readonly string FieldsQuery = $@"{{
            {NodeFieldsQuery}
            domainName
            bids {BidDto.FieldsQuery}
            highestBid {BidDto.FieldsQuery}
            endsAtUtc
            ownedUntilUtc
            state
            bidCount
            startedAtLevel
        }}";

        public string? DomainName { get; set; }
        public IReadOnlyList<BidDto>? Bids { get; set; }
        public BidDto? HighestBid { get; set; }
        public DateTime EndsAtUtc { get; set; }
        public DateTime OwnedUntilUtc { get; set; }

        public int? ValidUntilBlockLevel { get; set; }
        public DateTime? ValidUntilTimestamp { get; set; }
        public bool IsSettled { get; set; }
        public string? State { get; set; }
        public int? BidCount { get; set; }
        public int? StartedAtLevel { get; set; }
    }
}