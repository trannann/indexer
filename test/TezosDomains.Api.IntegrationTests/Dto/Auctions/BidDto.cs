﻿namespace TezosDomains.Api.IntegrationTests.Dto.Auctions
{
    public class BidDto : NodeDto
    {
        public static readonly string FieldsQuery = $@"{{
            {NodeFieldsQuery}
            id
            bidder
            amount
            bidderReverseRecord {ReverseRecordDto.FieldsQuery}
        }}";

        public string? Bidder { get; set; }
        public decimal Amount { get; set; }
        public ReverseRecordDto? BidderReverseRecord { get; set; }
    }
}