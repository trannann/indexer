﻿namespace TezosDomains.Api.IntegrationTests.Dto.Auctions
{
    public sealed class BalanceDto
    {
        public const string FieldsQuery = "{ tldName balance }";

        public string? TldName { get; set; }
        public decimal? Balance { get; set; }
    }
}