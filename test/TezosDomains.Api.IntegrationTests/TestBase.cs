﻿using FluentAssertions;
using GraphQL;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using MongoDB.Driver;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Configurations;
using TezosDomains.Data.MongoDb.Helpers;

namespace TezosDomains.Api.IntegrationTests
{
    public class TestBase
    {
        private static readonly string AppName = Assembly.GetAssembly(typeof(TestBase))!.GetName().Name!;
        private static int _portCounter = 5000;

        private string _hostUrl = null!;
        private IHost _host = null!;
        protected MongoDbContext Context = null!;
        protected readonly DateTime Now = DateTime.UtcNow;

        [OneTimeSetUp]
        public async Task OneTimeSetupBase()
        {
            // start our host
            _hostUrl = "http://localhost:" + _portCounter++;
            _host = Program.CreateHostBuilder()
                .ConfigureAppConfiguration(
                    (ctx, builder) =>
                    {
                        ctx.HostingEnvironment.ApplicationName = AppName;

                        // force testsettings.json and custom user secrets to avoid loading any of the original settings for the project
                        builder.Sources.Clear();
                        builder
                            .AddJsonFile("testsettings.json")
                            .AddUserSecrets<Program>(optional: true)
                            .AddEnvironmentVariables()
                            .AddInMemoryCollection(
                                new Dictionary<string, string>
                                {
                                    { "Urls", _hostUrl }
                                }
                            )
                            ;
                    }
                )
                .ConfigureServices(
                    serviceCollection =>
                    {
                        serviceCollection.RemoveAll(typeof(IMongoDbConfiguration));
                        serviceCollection.AddSingleton<IMongoDbConfiguration>(
                            sp =>
                            {
                                var configuration = sp.GetRequiredService<IConfiguration>();
                                var mongoDbConfiguration = configuration.GetSection("TezosDomains:MongoDb").Get<MongoDbConfiguration>();
                                mongoDbConfiguration.OverrideDatabaseName(_ => $"t_{Guid.NewGuid()}");
                                return mongoDbConfiguration;
                            }
                        );
                    }
                )
                .Build();
            await _host.StartAsync();

            Context = _host.Services.GetRequiredService<MongoDbContext>();
        }

        [OneTimeTearDown]
        public async Task OneTimeTeardown()
        {
            await DropDatabaseAsync();
            await _host.StopAsync();
            _host.Dispose();
        }

        private async Task DropDatabaseAsync()
        {
            var config = _host.Services.GetRequiredService<IMongoDbConfiguration>();
            var client = _host.Services.GetRequiredService<MongoClient>();
            await client.DropDatabaseAsync(config.Database);
        }

        public sealed class ResponseDto<TData>
        {
            public TData Data { get; set; } = default!;
        }

        protected async Task<TData> SendToGraphQLAsync<TData>(string query, string? expectedError = null)
        {
            Console.WriteLine($"Executing Graph query: {query}");
            using var client = new GraphQLHttpClient(_hostUrl + "/graphql", new NewtonsoftJsonSerializer());
            var request = new GraphQLRequest { Query = $"query Test {{ data: {query} }}" };
            var response = await client.SendQueryAsync<ResponseDto<TData>>(request);

            var errors = string.Join(", ", response.Errors?.Select(e => e.Message) ?? new string[0]);
            if (expectedError != null)
                errors.Should().Contain(expectedError);
            else
                errors.Should().BeEmpty();

            return response.Data.Data;
        }

        protected static DateTime GetTime(int year, int month, int day)
            => new DateTime(year, month, day, 0, 0, 0, 0, DateTimeKind.Utc);

        protected void Insert<TDocument>(params TDocument[] documents)
            where TDocument : IDocument
        {
            Context.Get<TDocument>().InsertMany(documents);
        }

        protected TDocumentWithHistory InsertWithHistory<TDocumentWithHistory>(TDocumentWithHistory document, TDocumentWithHistory[] history)
            where TDocumentWithHistory : IDocumentWithHistory
        {
            Insert(document);
            Context.GetHistory<TDocumentWithHistory>()
                .UpdateOne(
                    Builders<HistoryOf<TDocumentWithHistory>>.Filter.Eq("_id", document.GetId()),
                    Builders<HistoryOf<TDocumentWithHistory>>.Update.Set(d => d.History, history)
                );

            return document;
        }

        public static string Edges(string fields)
            => $@"{{
                edges {{
                    node {fields}
                    cursor
                }}
                totalCount
                pageInfo {{
                    hasNextPage
                    hasPreviousPage
                    startCursor
                    endCursor
                }}
            }}";

        protected static string AtBlockFilter(int? level, int? daysOffset)
        {
            var timestampStr = daysOffset != null ? @$"""{DateTime.UtcNow.AddSeconds(1).AddDays(daysOffset.Value):u}""" : "null";
            var levelStr = level?.ToString() ?? "null";

            return $"atBlock: {{ timestamp: {timestampStr}, level: {levelStr} }}";
        }
    }
}