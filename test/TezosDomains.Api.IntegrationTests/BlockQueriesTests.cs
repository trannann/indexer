﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using TezosDomains.Api.IntegrationTests.Dto;
using TezosDomains.Data.Models;

namespace TezosDomains.Api.IntegrationTests
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public sealed class BlockQueriesTests : TestBase
    {
        [OneTimeSetUp]
        public async Task OneTimeSetup()
        {
            await Context.Blocks.InsertOneAsync(
                new Block(
                    level: 66,
                    hash: "xyz",
                    timestamp: GetTime(2001, 2, 3),
                    predecessor: "abc"
                )
            );
        }

        [TestCase("")] //Should load latest block for when filter applied
        [TestCase("(level: 66)")]
        [TestCase(@"(hash: ""xyz"")")]
        [TestCase(@"(hash: ""xyz"", level: 66)")]
        public async Task ShouldLoadBlock(string filter)
        {
            var block = await GetBlockAsync(filter);

            block.Should()
                .BeEquivalentTo(
                    new BlockDto
                    {
                        Level = 66,
                        Hash = "xyz",
                        Timestamp = GetTime(2001, 2, 3),
                    }
                );
            block!.Timestamp.Kind.Should().Be(DateTimeKind.Utc);
        }

        [TestCase("(level: 77)")]
        [TestCase(@"(hash: ""tuv"")")]
        [TestCase(@"(hash: ""tuv"", level: 77)")]
        [TestCase(@"(hash: ""xyz"", level: 77)")] // Hash matches but level doesn't
        public async Task ShouldLoadNull_IfBlockNotExist(string filter)
        {
            var block = await GetBlockAsync(filter);

            block.Should().BeNull();
        }

        [TestCase("(level: 65)", "Argument 'level' must be greater than or equal to 66 because older blocks are not indexed but value 65 was specified.")]
        public async Task ShouldFail_IfInvalidFilter(string filter, string expectedError)
        {
            var block = await GetBlockAsync(filter, expectedError);
            block.Should().BeNull();
        }

        private Task<BlockDto?> GetBlockAsync(string queryArgs, string? expectedError = null)
            => SendToGraphQLAsync<BlockDto?>($"block{queryArgs} {BlockDto.FieldsQuery}", expectedError);
    }
}