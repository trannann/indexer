﻿using System;
using System.Linq;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;

namespace TezosDomains.Api.IntegrationTests.TestData
{
    public static class TestUser
    {
        public static BidderBalances Get(
            string address,
            (int? Level, int DaysOffset) block = default,
            (DateTime? Timestamp, int? Level) validUntil = default,
            params (string tldName, decimal balance)[] balances
        )
        {
            var blockBase = new BlockSlim(block.Level ?? 2, hash: Guid.NewGuid().ToString(), timestamp: DateTime.UtcNow.AddDays(block.DaysOffset));
            return new BidderBalances(
                address,
                balances.ToDictionary(b => b.tldName, b => b.balance),
                blockBase,
                validUntil.Level,
                validUntil.Timestamp
            );
        }
    }
}