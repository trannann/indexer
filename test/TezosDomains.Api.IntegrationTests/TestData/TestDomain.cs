﻿using System;
using System.Linq;
using TezosDomains.Data;
using TezosDomains.Data.Models;
using TezosDomains.Utils;

namespace TezosDomains.Api.IntegrationTests.TestData
{
    public static class TestDomain
    {
        public static Domain Get(
            string name,
            string? address = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
            string owner = "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL",
            DateTime? expiresAtUtc = null,
            (string Key, string Value)[]? data = null,
            int? level = null,
            (int? Level, int DaysOffset) block = default,
            (DateTime? Timestamp, int? Level) validUntil = default
        )
        {
            var (label, parent, ancestors, nameReverse, domainLevel) = DomainNameUtils.Parse(name);
            return new Domain(
                name,
                address,
                owner,
                data.NullToEmpty().ToDictionary(d => d.Key, d => d.Value),
                label,
                ancestors,
                parent,
                expiresAtUtc,
                new BlockSlim(block.Level ?? 2, hash: Guid.NewGuid().ToString(), timestamp: DateTime.UtcNow.AddDays(block.DaysOffset)),
                validityKey: ancestors.Length >= 2 ? ancestors[1] : name,
                level ?? domainLevel,
                nameReverse,
                validUntil.Level,
                validUntil.Timestamp
            );
        }
    }
}