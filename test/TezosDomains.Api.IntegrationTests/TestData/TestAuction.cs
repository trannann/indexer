﻿using System;
using System.Linq;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;

namespace TezosDomains.Api.IntegrationTests.TestData
{
    public static class TestAuction
    {
        public static Auction Get(
            string id,
            DateTime? endsAt = default,
            DateTime? ownedUntil = default,
            DateTime? firstBidAt = default,
            bool isSettled = default,
            (int? Level, int DaysOffset) block = default,
            (DateTime? Timestamp, int? Level) validUntil = default,
            params (string bidder, decimal amount)[] bids
        )
        {
            var blockBase = new BlockSlim(block.Level ?? 2, hash: Guid.NewGuid().ToString(), timestamp: DateTime.UtcNow.AddDays(block.DaysOffset));
            var todayUtc = DateTime.UtcNow.Date;
            var name = id.Split(":")[0];
            return new Auction(
                id,
                name,
                bids.Select(b => new Bid(blockBase, b.bidder, b.amount)).ToArray(),
                endsAt ?? todayUtc.AddDays(1),
                blockBase,
                validUntil.Level,
                validUntil.Timestamp,
                ownedUntil ?? todayUtc.AddDays(2),
                isSettled,
                firstBidAt ?? todayUtc.AddDays(-1),
                bids.Length
            );
        }
    }
}