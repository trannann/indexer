﻿using System;
using TezosDomains.Data;
using TezosDomains.Data.Models;

namespace TezosDomains.Api.IntegrationTests.TestData
{
    public static class TestReverseRecord
    {
        public static ReverseRecord Get(
            string address,
            string? name = null,
            string owner = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
            DateTime? expiresAtUtc = null,
            (int? Level, int DaysOffset) block = default,
            (DateTime? Timestamp, int? Level) validUntil = default
        )
        {
            var ancestors = name != null ? DomainNameUtils.Parse(name).Ancestors : null;
            return new ReverseRecord(
                address,
                new BlockSlim(block.Level ?? 2, hash: Guid.NewGuid().ToString(), timestamp: DateTime.UtcNow.AddDays(block.DaysOffset)),
                name,
                owner,
                expiresAtUtc,
                validityKey: ancestors?.Length >= 2 ? ancestors[1] : name,
                validUntil.Level,
                validUntil.Timestamp
            );
        }
    }
}