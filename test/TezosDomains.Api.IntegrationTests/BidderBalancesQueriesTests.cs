using FluentAssertions;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Api.IntegrationTests.Dto.Auctions;
using TezosDomains.Api.IntegrationTests.TestData;
using TezosDomains.Data.Models;

namespace TezosDomains.Api.IntegrationTests
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class BidderBalancesQueriesTests : TestBase
    {
        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            Context.Blocks.InsertOne(new Block(0, Guid.NewGuid().ToString(), DateTime.UtcNow.AddDays(-2), Guid.NewGuid().ToString()));
            Context.Blocks.InsertOne(new Block(1, Guid.NewGuid().ToString(), DateTime.UtcNow.AddDays(-1), Guid.NewGuid().ToString()));
            var block = new Block(2, Guid.NewGuid().ToString(), DateTime.UtcNow, Guid.NewGuid().ToString());
            Context.Blocks.InsertOne(block);

            var bidderBalances = TestUser.Get("tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", block: default, validUntil: default, ("a1", 10), ("a2", 100));
            InsertWithHistory(
                bidderBalances,
                new[]
                {
                    bidderBalances,
                    TestUser.Get("tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", block: (1, -1), validUntil: (block.Timestamp, 2), ("a1", 10))
                }
            );
        }

        [Test]
        public Task ShouldLoadAllFields()
            => RunFieldsTest(
                address: "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                expectedBalances: new[] { new BalanceDto { TldName = "a1", Balance = 10 }, new BalanceDto { TldName = "a2", Balance = 100 } }
            );

        [Test]
        public Task ShouldLoadEmpty_IfNoEntryInDatabase()
            => RunFieldsTest(
                address: "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL",
                expectedBalances: Array.Empty<BalanceDto>()
            );

        private async Task RunFieldsTest(string address, BalanceDto[] expectedBalances)
        {
            var item = await GetAsync(@$"address: ""{address}""");

            var expected = new BidderBalancesDto
            {
                Address = address,
                Balances = expectedBalances,
                TypeName = "BidderBalances",
                Id = $"BidderBalances:{address}"
            };
            item.Should().BeEquivalentTo(expected);
        }

        [TestCase(0, new int[0])]
        [TestCase(1, new[] { 10 })]
        [TestCase(2, new[] { 10, 100 })]
        public async Task HistoricQuery_ShouldShowCorrectBalances(int blockLevel, int[] balances)
        {
            var item = await GetAsync($@"address: ""tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n"", atBlock: {{ level: {blockLevel} }}");

            (item?.Balances?.Select(b => b.Balance)).Should().BeEquivalentTo(balances);
        }

        private Task<BidderBalancesDto?> GetAsync(string queryArgs)
            => SendToGraphQLAsync<BidderBalancesDto?>($@"bidderBalances({queryArgs}) {BidderBalancesDto.FieldsQuery}");
    }
}