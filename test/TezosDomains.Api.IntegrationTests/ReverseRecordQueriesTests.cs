using FluentAssertions;
using GraphQL.Types.Relay.DataObjects;
using NUnit.Framework;
using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Api.IntegrationTests.Dto;
using TezosDomains.Api.IntegrationTests.TestData;

namespace TezosDomains.Api.IntegrationTests
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class ReverseRecordQueriesTests : TestBase
    {
        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            InsertRecord("tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL", "2.test.tez");
            InsertRecord("KT1RKZcsTS7kLuRd6JygiVkC1RJ4pfwznwkf", name: null, owner: "tz1a6Sxwj5wJKa1oXDGDVBjH2ZLVXk8zVcsb");
            InsertRecord("tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm", "3.other.tez", expiresAtUtc: GetTime(2150, 2, 3));
            InsertRecord("tz1eLWfccL46VAUjtyz9kEKgzuKnwyZH4rTA", "1.test.tez", historicalOwner: "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL");
            InsertRecord("KT1HEUqdorP87Fbtcr8k5SB7TKHjCzCW38hD", "expired.test.tez", expiresAtUtc: GetTime(2001, 2, 3));

            Insert(TestDomain.Get("3.other.tez", "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm"));
        }

        private void InsertRecord(
            string address,
            string? name,
            string owner = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
            string historicalOwner = "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm",
            DateTime? expiresAtUtc = null
        )
            => InsertWithHistory(
                document: TestReverseRecord.Get(address, name, owner, expiresAtUtc),
                history: new[]
                {
                    TestReverseRecord.Get(address, name + "2", historicalOwner, expiresAtUtc),
                    TestReverseRecord.Get(address, name + "1", owner, expiresAtUtc, block: (Level: 1, DaysOffset: -1), validUntil: (DateTime.UtcNow, Level: 2)),
                }
            );

        [Test]
        public async Task ReverseRecord_ShouldLoadAllFields()
        {
            var record = await GetReverseRecordAsync(@"address: ""tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm""");

            VerifyTestRecord(record);
        }

        [TestCase("tz1YNNsGoz1rekPfoU8WzigR4GhyFskoHwr7")] // Not exist
        [TestCase("tz1XyaS3pWSQHrKxvnJ9Kmpk9PKgWWunYXzU")] // Deleted
        public async Task ReverseRecord_ShouldLoadNull_IfNotExistOrDelete(string address)
        {
            var record = await GetReverseRecordAsync($@"address: ""{address}""");

            record.Should().BeNull();
        }

        [TestCase("tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm", "VALID", true)]
        [TestCase("tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm", "ALL", true)]
        [TestCase("tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm", "EXPIRED", false)]
        [TestCase("tz1eLWfccL46VAUjtyz9kEKgzuKnwyZH4rTA", "VALID", true)] // Doesn't expire
        [TestCase("KT1HEUqdorP87Fbtcr8k5SB7TKHjCzCW38hD", "VALID", false)]
        [TestCase("KT1HEUqdorP87Fbtcr8k5SB7TKHjCzCW38hD", "ALL", true)]
        [TestCase("KT1HEUqdorP87Fbtcr8k5SB7TKHjCzCW38hD", "EXPIRED", true)]
        [TestCase("tz1XyaS3pWSQHrKxvnJ9Kmpk9PKgWWunYXzU", "ALL", false)] // Deleted
        public async Task ReverseRecord_ShouldFilterByValidity(string address, string validity, bool expectedSuccess)
        {
            var record = await GetReverseRecordAsync(@$"address: ""{address}"", validity: {validity}");

            if (expectedSuccess)
            {
                record.Should().NotBeNull();
                record!.Address.Should().Be(expectedSuccess ? address : null);
            }
            else
            {
                record.Should().BeNull();
            }
        }

        public static readonly IEnumerable HistoryTestCases = new[]
        {
            new object?[] { 0, null, null },
            new object?[] { 1, null, "1.test.tez1" },
            new object?[] { 2, null, "1.test.tez2" },
            new object?[] { 3, null, "1.test.tez2" },
            new object?[] { null, -2, null },
            new object?[] { null, -1, "1.test.tez1" },
            new object?[] { null, 0, "1.test.tez2" },
            new object?[] { null, 1, "1.test.tez2" },
        };

        [TestCaseSource(nameof(HistoryTestCases))]
        public async Task ReverseRecord_ShouldFilterByHistory(int? level, int? daysToNow, string? expectedName)
        {
            var record = await GetReverseRecordAsync(
                @"address: ""tz1eLWfccL46VAUjtyz9kEKgzuKnwyZH4rTA"","
                + AtBlockFilter(level, daysToNow)
            );

            (record?.Name).Should().Be(expectedName);
        }

        [TestCaseSource(nameof(HistoryTestCases))]
        public Task ReverseRecords_ShouldFilterByHistory(int? level, int? daysToNow, string? expectedName)
            => RunRecordsTest(
                @"where: { address: { equalTo: ""tz1eLWfccL46VAUjtyz9kEKgzuKnwyZH4rTA"" } },"
                + AtBlockFilter(level, daysToNow),
                expectedName != null ? new[] { expectedName } : new string?[0]
            );

        [Test]
        public async Task ReverseRecords_ShouldCountHistoricalEntriesCorrectly()
        {
            var records = await GetReverseRecordsAsync(
                @"where: { owner: { startsWith: TZ2 } },
                atBlock: { level: 2 }"
            );

            records.TotalCount.Should().Be(1);
        }

        [Test]
        public async Task ReverseRecords_ShouldLoadAllFields()
        {
            var records = await GetReverseRecordsAsync(@"where: { address: { equalTo: ""tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm"" }}");

            VerifyTestRecord(records.Items.Single());
        }

        private static void VerifyTestRecord(ReverseRecordDto? record)
        {
            var expected = new ReverseRecordDto
            {
                Id = "ReverseRecord:tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm",
                TypeName = "ReverseRecord",
                Address = "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm",
                Name = "3.other.tez",
                Owner = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                ExpiresAtUtc = GetTime(2150, 2, 3),
                Domain = new DomainDto
                {
                    Name = "3.other.tez",
                    Address = "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm"
                }
            };
            record.Should().BeEquivalentTo(expected);
        }

        [Test]
        public Task ReverseRecords_ShouldOrderByNameByDefault()
            => RunRecordsTest("where: {}", expectedNames: new[] { null, "1.test.tez", "2.test.tez", "3.other.tez" });

        [Test]
        public Task ReverseRecords_ShouldOrderAccordingToQuery()
            => RunRecordsTest("order: { field: NAME direction: DESC }", expectedNames: new[] { "3.other.tez", "2.test.tez", "1.test.tez", null });

        [TestCase("address", @"""tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm""", new[] { "3.other.tez" })]
        [TestCase("name", @"""1.TEST.tez""", new[] { "1.test.tez" })] // Filter should be case-insensitive
        [TestCase("owner", @"""tz1a6Sxwj5wJKa1oXDGDVBjH2ZLVXk8zVcsb""", new string?[] { null })]
        [TestCase("expiresAtUtc", @"""2150-02-03T00:00:00.000+00:00""", new[] { "3.other.tez" })]
        public Task ReverseRecords_ShouldFilterByField(string field, string value, string?[] expectedNames)
            => RunRecordsTest($"where: {{ {field}: {{ equalTo: {value} }}}}", expectedNames);

        [TestCase("VALID", new[] { null, "1.test.tez", "2.test.tez", "3.other.tez" })]
        [TestCase("EXPIRED", new[] { "expired.test.tez" })]
        [TestCase("ALL", new[] { null, "1.test.tez", "2.test.tez", "3.other.tez", "expired.test.tez" })]
        public Task ShouldFilterByValidity(string validity, string[] expectedNames)
            => RunRecordsTest($"where: {{ validity: {validity} }}", expectedNames);


        [TestCase("tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm", "3.other.tez")]
        [TestCase("KT1RKZcsTS7kLuRd6JygiVkC1RJ4pfwznwkf", null)] // ReverseRecord has no name
        public async Task ShouldLoadRelatedDomainRecord_IfNameAndAddressMatched(string address, string? expectedNameOrNull)
        {
            var reverseRecord = await GetReverseRecordAsync($@"address: ""{address}""");

            reverseRecord.Should().NotBeNull();
            if (expectedNameOrNull == null)
            {
                reverseRecord!.Domain.Should().BeNull();
            }
            else
            {
                reverseRecord!.Domain.Should().NotBeNull();
                reverseRecord!.Domain!.Name.Should().Be(expectedNameOrNull);
                reverseRecord.Domain.Address.Should().Be(address);
            }
        }

        private async Task RunRecordsTest(string queryArgs, string?[] expectedNames)
        {
            var records = await GetReverseRecordsAsync(queryArgs);

            records.Items.Select(d => d.Name).Should().Equal(expectedNames);
        }

        private Task<ReverseRecordDto?> GetReverseRecordAsync(string queryArgs)
            => SendToGraphQLAsync<ReverseRecordDto?>($@"reverseRecord({queryArgs}) {ReverseRecordDto.FieldsQuery}");

        private Task<Connection<ReverseRecordDto>> GetReverseRecordsAsync(string queryArgs)
            => SendToGraphQLAsync<Connection<ReverseRecordDto>>($"reverseRecords({queryArgs}) {Edges(ReverseRecordDto.FieldsQuery)}");
    }
}