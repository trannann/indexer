using FluentAssertions;
using GraphQL.Types.Relay.DataObjects;
using NUnit.Framework;
using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Api.IntegrationTests.Dto;
using TezosDomains.Api.IntegrationTests.TestData;
using TezosDomains.Data.Models;

namespace TezosDomains.Api.IntegrationTests
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class DomainQueriesTests : TestBase
    {
        private Domain _domainTest = null!;
        private ReverseRecord _reverseRecordTz1 = null!;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            InsertDomain("2.test.tez", address: "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm");
            _domainTest = InsertDomain("test.tez", expiresAtUtc: GetTime(2150, 2, 3), data: new[] { ("foo", "1"), ("bar", "2") });
            InsertDomain("3.other.tez", address: null, owner: "KT1RKZcsTS7kLuRd6JygiVkC1RJ4pfwznwkf");
            InsertDomain("1.test.tez", address: null, historicalAddress: "tz1eLWfccL46VAUjtyz9kEKgzuKnwyZH4rTA");
            InsertDomain("expired.test.tez", expiresAtUtc: GetTime(2001, 2, 3));

            _reverseRecordTz1 = TestReverseRecord.Get("tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", "test.tez");
            Insert(_reverseRecordTz1);
            Insert(TestReverseRecord.Get("tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm", "3.test.tez")); // Doesn't correspond with both address and name
        }

        private Domain InsertDomain(
            string name,
            string? address = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
            string? historicalAddress = null,
            string owner = "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL",
            DateTime? expiresAtUtc = null,
            (string Key, string Value)[]? data = null
        )
            => InsertWithHistory(
                document: TestDomain.Get(name, address, owner, expiresAtUtc, data),
                history: new[]
                {
                    TestDomain.Get(name, historicalAddress, owner, expiresAtUtc, level: 2),
                    TestDomain.Get(name, address, owner, expiresAtUtc, level: 1, block: (Level: 1, DaysOffset: -1), validUntil: (DateTime.UtcNow, Level: 2)),
                }
            );

        [Test]
        public async Task Domain_ShouldLoadAllFields()
        {
            var domain = await GetDomainAsync(@"name: ""test.tez""");

            VerifyTestDomain(domain!);
        }

        [Test]
        public async Task Domain_ShouldLoadNull_IfNotExist()
        {
            var domain = await GetDomainAsync(@"name: ""omg.tez""");

            domain.Should().BeNull();
        }

        [TestCase("test.tez", "VALID", true)]
        [TestCase("test.tez", "ALL", true)]
        [TestCase("test.tez", "EXPIRED", false)]
        [TestCase("1.test.tez", "VALID", true)] // Has ExpiresAtUtc == null -> doesn't expire
        [TestCase("expired.test.tez", "VALID", false)]
        [TestCase("expired.test.tez", "ALL", true)]
        [TestCase("expired.test.tez", "EXPIRED", true)]
        public async Task Domain_ShouldFilterByValidity(string name, string validity, bool expectedSuccess)
        {
            var domain = await GetDomainAsync($@"name: ""{name}"", validity: {validity}");

            (domain?.Name).Should().Be(expectedSuccess ? name : null);
        }

        public static readonly IEnumerable HistoryTestCases = new[]
        {
            new object?[] { 0, null, null },
            new object?[] { 1, null, 1 },
            new object?[] { 2, null, 2 },
            new object?[] { 3, null, 2 },
            new object?[] { null, -2, null },
            new object?[] { null, -1, 1 },
            new object?[] { null, 0, 2 },
            new object?[] { null, 1, 2 },
        };

        [TestCaseSource(nameof(HistoryTestCases))]
        public async Task Domain_ShouldFilterByHistory(int? blockLevel, int? blockDaysOffset, int? expectedLevel)
        {
            var domain = await GetDomainAsync(
                @"name: ""test.tez"","
                + AtBlockFilter(blockLevel, blockDaysOffset)
            );

            (domain?.Level).Should().Be(expectedLevel);
        }

        [TestCaseSource(nameof(HistoryTestCases))]
        public async Task Domains_ShouldFilterByHistory(int? blockLevel, int? blockDaysOffset, int? expectedLevel)
        {
            var domains = await GetDomainsAsync(
                @"where: { name: { equalTo: ""test.tez"" } },"
                + AtBlockFilter(blockLevel, blockDaysOffset)
            );

            (domains.Items.SingleOrDefault()?.Level).Should().Be(expectedLevel);
        }

        [Test]
        public async Task Domains_ShouldCountHistoricalEntriesCorrectly()
        {
            var domains = await GetDomainsAsync(
                @"where: { address: { startsWith: TZ1 } },
                atBlock: { level: 2 }"
            );

            domains.TotalCount.Should().Be(1);
        }

        [Test]
        public async Task Domains_ShouldLoadAllFields()
        {
            var domains = await GetDomainsAsync(@"where: { name: { equalTo: ""test.tez"" }}");

            VerifyTestDomain(domains.Items.Single());
        }

        private void VerifyTestDomain(DomainDto domain)
        {
            var expected = new DomainDto
            {
                Id = "Domain:test.tez",
                TypeName = "Domain",
                Name = "test.tez",
                Address = "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
                Owner = "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL",
                ExpiresAtUtc = GetTime(2150, 2, 3),
                Level = 2,
                Data = new[]
                {
                    new DataItemDto { Key = "foo", RawValue = "1" },
                    new DataItemDto { Key = "bar", RawValue = "2" },
                },
                AddressReverseRecord = ReverseRecordDto.Parse(_reverseRecordTz1, _domainTest),
                OwnerReverseRecord = null,
            };
            domain.Should().BeEquivalentTo(expected, c => c.Excluding(d => d.Subdomains).Excluding(d => d.ReverseRecord));
            domain.ExpiresAtUtc!.Value.Kind.Should().Be(DateTimeKind.Utc);
            domain.Subdomains.Items.Select(d => d.Name).Should().Equal("1.test.tez", "2.test.tez");
        }

        [Test]
        public Task Domains_ShouldOrderByReverseNameByDefault()
            => RunDomainsTest("where: {}", expectedNames: new[] { "3.other.tez", "test.tez", "1.test.tez", "2.test.tez" });

        [Test]
        public Task Domains_ShouldOrderAccordingToQuery()
            => RunDomainsTest("order: { field: NAME direction: DESC }", expectedNames: new[] { "test.tez", "3.other.tez", "2.test.tez", "1.test.tez" });

        [TestCase("name", @"""1.TEST.tez""", new[] { "1.test.tez" })] // Filter should be case-insensitive
        [TestCase("address", @"""tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm""", new[] { "2.test.tez" })]
        [TestCase("owner", @"""KT1RKZcsTS7kLuRd6JygiVkC1RJ4pfwznwkf""", new[] { "3.other.tez" })]
        [TestCase("level", "2", new[] { "test.tez" })]
        public Task Domains_ShouldFilterByField(string field, string value, string[] expectedNames)
            => RunDomainsTest(@$"where: {{ {field}: {{ equalTo: {value} }}}}", expectedNames);

        [Test]
        public Task Domains_ShouldFilterByAncestorField() // Filter should be case-insensitive
            => RunDomainsTest(@"where: { ancestors: { include: ""other.TEZ"" }}", expectedNames: new[] { "3.other.tez" });


        [TestCase("VALID", new[] { "3.other.tez", "test.tez", "1.test.tez", "2.test.tez" })]
        [TestCase("EXPIRED", new[] { "expired.test.tez" })]
        [TestCase("ALL", new[] { "3.other.tez", "test.tez", "1.test.tez", "2.test.tez", "expired.test.tez" })]
        public Task Domains_ShouldFilterByValidityField(string validity, string[] expectedNames)
            => RunDomainsTest($"where: {{ validity: {validity} }}", expectedNames);

        private async Task RunDomainsTest(string queryArgs, string[] expectedNames)
        {
            var domains = await GetDomainsAsync(queryArgs);

            domains.Items.Select(d => d.Name).Should().Equal(expectedNames);
        }

        [TestCase("test.tez", "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n")]
        [TestCase("1.test.tez", null)] // Domain has no address
        [TestCase("2.test.tez", null)] // Domain address corresponding to reverse record with different name
        public async Task ShouldLoadRelatedReverseRecord_IfNameAndAddressMatched(string name, string? expectedAddress)
        {
            var domain = await GetDomainAsync($@"name: ""{name}""");

            (domain!.ReverseRecord?.Address).Should().Be(expectedAddress);
            (domain.ReverseRecord?.Name).Should().Be(expectedAddress != null ? name : null);
        }

        private Task<DomainDto?> GetDomainAsync(string queryArgs)
            => SendToGraphQLAsync<DomainDto?>($@"domain({queryArgs}) {DomainDto.FieldsQuery}");

        private Task<Connection<DomainDto>> GetDomainsAsync(string queryArgs)
            => SendToGraphQLAsync<Connection<DomainDto>>($"domains({queryArgs}) {Edges(DomainDto.FieldsQuery)}");
    }
}