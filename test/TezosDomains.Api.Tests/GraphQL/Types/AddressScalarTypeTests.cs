﻿using FluentAssertions;
using NUnit.Framework;
using System;
using TezosDomains.Api.GraphQL.Scalars;
using TezosDomains.Indexer.Tezos.Encoding;

namespace TezosDomains.Api.Tests.GraphQL.Types
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class AddressScalarTypeTests
    {
        [TestCase("tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n")]
        [TestCase("KT1Tcaz8RGvAA5rvjFEubQnjLTc62cjuUEyk")]
        public void ShouldReturnSame_IfValidAddress(string value)
            => AddressScalarType.ValidateAddress(value).Should().BeSameAs(value);

        [TestCase(null)]
        [TestCase(123)]
        [TestCase("")]
        [TestCase("wtf")]
        [TestCase("tz1VxthisisinvalidudmADssPp6FPDGRsvJ")]
        [TestCase("tz1L9r8mWmRpndRhuvMCWESLGSVeFzQ9NAWx")] // Invalid checksum
        public void ShouldThrow_IfInvalidAddress(object value)
            => new Action(() => AddressScalarType.ValidateAddress(value))
                .Should()
                .Throw<Exception>()
                .Which.Message.Should()
                .ContainAll($"'{value}'", TezosEncoding.Address.Description);
    }
}