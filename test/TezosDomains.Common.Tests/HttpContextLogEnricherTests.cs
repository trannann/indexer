﻿using FluentAssertions;
using Microsoft.AspNetCore.Http;
using NSubstitute;
using NUnit.Framework;
using Serilog.Core;
using Serilog.Events;
using Serilog.Parsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace TezosDomains.Common.Tests
{
    [TestFixture]
    public class HttpContextLogEnricherTests
    {
        private ILogEventEnricher _target = null!;
        private IHttpContextAccessor _httpContextAccessor = null!;

        private LogEvent _logEvent = null!;
        private ILogEventPropertyFactory _propertyFactory = null!;
        private LogEventProperty _testProperty = null!;

        [SetUp]
        public void Setup()
        {
            _httpContextAccessor = Substitute.For<IHttpContextAccessor>();
            _target = new HttpContextLogEnricher(_httpContextAccessor);

            _logEvent = new LogEvent(default, default, null, new MessageTemplate(Array.Empty<MessageTemplateToken>()), new LogEventProperty[0]);
            _propertyFactory = Substitute.For<ILogEventPropertyFactory>();
            _testProperty = new LogEventProperty("Foo", new ScalarValue("Bar"));

            _httpContextAccessor.HttpContext = new DefaultHttpContext
            {
                Request =
                {
                    Method = "POST",
                    Scheme = "https",
                    Host = new HostString("tezos.io", 8080),
                    PathBase = "/app",
                    Path = "/request",
                    QueryString = new QueryString("?q=1"),
                },
                Connection =
                {
                    RemoteIpAddress = IPAddress.Parse("6.6.6.6"),
                },
            };
            _propertyFactory.CreateProperty("Http", Arg.Any<object>()).Returns(_testProperty);
        }

        private void Act() => _target.Enrich(_logEvent, _propertyFactory);

        [Test]
        public void ShouldAddHttpContextProperties()
        {
            Act();

            _logEvent.Properties.Should().HaveCount(1).And.Contain(p => p.Key == _testProperty.Name && p.Value == _testProperty.Value);

            var expectedValues = new Dictionary<string, string>
            {
                { "Method", "POST" },
                { "Scheme", "https" },
                { "Host", "tezos.io:8080" },
                { "Path", "/app/request" },
                { "Query", "?q=1" },
                { "ClientIp", "6.6.6.6" },
            };
            GetReceivedPropertyValues().Should().Equal(expectedValues);
        }

        [TestCase("1.1.1.1", "1.1.1.1")]
        [TestCase("1.1.1.1, 2.2.2.2", "1.1.1.1")]
        [TestCase("1.1.1.1   ,2.2.2.2, 3.3.3.3", "1.1.1.1")]
        [TestCase(", 1.1.1.1", "1.1.1.1")]
        [TestCase(",", "6.6.6.6")]
        [TestCase("  ", "6.6.6.6")]
        public void ShouldParseClientIpFromXForwardedForHeader(string headerValue, string expected)
        {
            _httpContextAccessor.HttpContext!.Request.Headers.Add("X-Forwarded-For", headerValue);

            Act();

            GetReceivedPropertyValues().Should().Contain("ClientIp", expected);
        }

        [Test]
        public void ShouldNotAddAnything_IfNoHttpContext()
        {
            _httpContextAccessor.HttpContext = null;

            Act();

            _logEvent.Properties.Should().BeEmpty();
        }

        private Dictionary<string, string> GetReceivedPropertyValues()
            => (Dictionary<string, string>) _propertyFactory.ReceivedCalls().Single().GetArguments()[1];
    }
}