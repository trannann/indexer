﻿using System.Collections.Generic;
using System.Text.Json;
using FluentAssertions;
using NUnit.Framework;

namespace TezosDomains.Indexer.Tezos.Tests.Layout
{
    [TestFixture]
    public class LayoutTests
    {
        #region DefinitionElement

        private const string Definition = @"
                                          {
                                            ""prim"": ""pair"",
                                            ""args"": [
                                              {
                                                ""prim"": ""pair"",
                                                ""args"": [
                                                  {
                                                    ""prim"": ""pair"",
                                                    ""args"": [
                                                      {
                                                        ""prim"": ""option"",
                                                        ""args"": [{ ""prim"": ""address"" }],
                                                        ""annots"": [""%address""]
                                                      },
                                                      {
                                                        ""prim"": ""map"",
                                                        ""args"": [{ ""prim"": ""string"" }, { ""prim"": ""bytes"" }],
                                                        ""annots"": [""%data""]
                                                      }
                                                    ]
                                                  },
                                                  {
                                                    ""prim"": ""pair"",
                                                    ""args"": [
                                                      {
                                                        ""prim"": ""option"",
                                                        ""args"": [{ ""prim"": ""bytes"" }],
                                                        ""annots"": [""%expiry_key""]
                                                      },
                                                      {
                                                        ""prim"": ""map"",
                                                        ""args"": [{ ""prim"": ""string"" }, { ""prim"": ""bytes"" }],
                                                        ""annots"": [""%internal_data""]
                                                      }
                                                    ]
                                                  }
                                                ]
                                              },
                                              {
                                                ""prim"": ""pair"",
                                                ""args"": [
                                                  {
                                                    ""prim"": ""pair"",
                                                    ""args"": [
                                                      { ""prim"": ""nat"", ""annots"": [""%level""] },
                                                      { ""prim"": ""address"", ""annots"": [""%owner""] }
                                                    ]
                                                  },
                                                  {
                                                    ""prim"": ""option"",
                                                    ""args"": [{ ""prim"": ""nat"" }],
                                                    ""annots"": [""%validator""]
                                                  }
                                                ]
                                              }
                                            ]
                                          }";

        #endregion

        #region ValueElement

        private const string Value = @"
                        {
                          ""prim"": ""Pair"",
                          ""args"": [
                            {
                              ""prim"": ""Pair"",
                              ""args"": [
                                {
                                  ""prim"": ""Pair"",
                                  ""args"": [
                                    {
                                      ""prim"": ""Some"",
                                      ""args"": [ { ""bytes"": ""0000e81b5d37fc4c2635d9b1c6d657eb570b86d80096"" } ]
                                    },
                                    [
                                      {
                                        ""prim"": ""Elt"",
                                        ""args"": [
                                          { ""string"": ""td:ttl"" },
                                          { ""bytes"": ""343230"" }
                                        ]
                                      }
                                    ]
                                  ]
                                },
                                {
                                  ""prim"": ""Pair"",
                                  ""args"": [
                                    {
                                      ""prim"": ""Some"",
                                      ""args"": [ { ""bytes"": ""6f6b2e74657374"" } ]
                                    },
                                    []
                                  ]
                                }
                              ]
                            },
                            {
                              ""prim"": ""Pair"",
                              ""args"": [
                                {
                                  ""prim"": ""Pair"",
                                  ""args"": [
                                    { ""int"": ""2"" },
                                    { ""bytes"": ""0000e81b5d37fc4c2635d9b1c6d657eb570b86d80096"" }
                                  ]
                                },
                                {
                                  ""prim"": ""Some"",
                                  ""args"": [ { ""int"": ""1"" } ]
                                }
                              ]
                            }
                          ]
                        }";

        #endregion

        [Test]
        public void Apply_ShouldExtractValuesUsingTheirDefinition()
        {
            var layout = new Layouts.RecordLayout(JsonDocument.Parse(Definition).RootElement);

            var parsedValues = layout.Apply(JsonDocument.Parse(Value).RootElement);

            parsedValues["address"].GetValueAsNullableAddress().Should().Be("tz1goJEvA7TNvFUcHUtnWdfM72QmuwbLsuxM");
            parsedValues["owner"].GetValueAsAddress().Should().Be("tz1goJEvA7TNvFUcHUtnWdfM72QmuwbLsuxM");
            parsedValues["level"].GetValueAsInt().Should().Be(2);
            parsedValues["expiry_key"].GetValueAsNullableString().Should().Be("ok.test");
            parsedValues["data"].GetValueAsMap().Should().BeEquivalentTo(new Dictionary<string, string> { { "td:ttl", "343230" } });
            parsedValues["internal_data"].GetValueAsMap().Should().BeEmpty();
        }
    }
}