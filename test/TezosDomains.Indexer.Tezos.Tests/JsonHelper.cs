﻿using System.IO;
using System.Linq;

namespace TezosDomains.Indexer.Tezos.Tests
{
    internal static class JsonHelper
    {
        public static string ReadJsonFile(params string[] filePathSegments)
        {
            var path = GetFilePath(filePathSegments);
            return File.ReadAllText(path, System.Text.Encoding.UTF8);
        }

        public static string GetFilePath(params string[] filePathSegments)
        {
            var projectFolder = Path.GetFullPath($"..{Path.DirectorySeparatorChar}..{Path.DirectorySeparatorChar}..{Path.DirectorySeparatorChar}");
            var path = Path.Combine(new[] { projectFolder }.Concat(filePathSegments).ToArray());
            return path;
        }
    }
}