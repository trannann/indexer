﻿using Microsoft.Extensions.Logging;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Indexer.Tezos.Clients;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.Indexer.Tezos.Implementations;
using TezosDomains.Indexer.Tezos.Layouts;
using TezosDomains.Indexer.Tezos.Models;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Indexer.Tezos.Models.Block.Internals;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Tezos.Tests.BlockTraverser
{
    public class BlockTraverserTests
    {
        private static readonly JsonElement EmptyJsonDocument = JsonDocument.Parse("{}").RootElement;

        [TestCase(true)]
        [TestCase(false)]
        public async Task InitializeAsync_ShouldSubscribeToProxyOrDestinationContract_BasedOn_ResolveThroughProxy(bool resolveThroughProxy)
        {
            //arrange
            var clientMock = Substitute.For<ITezosClient>();
            var recordLayoutFactoryMock = Substitute.For<ILayoutFactory>();

            var subscriptionConfigurations = new Dictionary<string, ContractSubscriptionConfiguration[]>
            {
                { "tz1proxy", new[] { new ContractSubscriptionConfiguration { ResolveThroughProxy = resolveThroughProxy } } }
            };
            var (contractScriptProxy, contractScriptDestination) = SetUpMocksForProxyAndDestinationContracts("tz1", clientMock, recordLayoutFactoryMock);
            var instance = new TezosBlockTraverser(clientMock, Substitute.For<ILogger<TezosBlockTraverser>>(), recordLayoutFactoryMock);


            //act
            await instance.InitializeAsync(subscriptionConfigurations, CancellationToken.None);

            //assert
            await clientMock.Received(1).GetContractScriptAsync("tz1proxy", Arg.Any<CancellationToken>());
            await clientMock.Received(1).GetContractScriptAsync("tz1destination", Arg.Any<CancellationToken>());
            recordLayoutFactoryMock.Received(1).CreateStorageLayout(contractScriptProxy);
            recordLayoutFactoryMock.Received(1).CreateStorageLayout(contractScriptDestination);
            if (resolveThroughProxy)
            {
                //bigmap and entrypoint layouts are only created for destination contract
                recordLayoutFactoryMock.Received(1).CreateBigMapLayouts(contractScriptDestination);
                recordLayoutFactoryMock.Received(1).CreateEntrypointLayouts("tz1destination", contractScriptDestination);
            }
            else
            {
                //bigmap and entrypoint layouts are only created for proxy contract
                recordLayoutFactoryMock.Received(1).CreateBigMapLayouts(contractScriptProxy);
                recordLayoutFactoryMock.Received(1).CreateEntrypointLayouts("tz1proxy", contractScriptProxy);
            }
        }


        [Test]
        public async Task TraverseAndNotifySubscribers_ShouldNotify_ContractSubscriber_WhenSubscribedDestinationAddressInTransactionAsync()
        {
            //arrange

            #region Block setup

            var block = new TezosBlock()
            {
                OperationGroups = new[]
                {
                    new OperationGroup { Hash = "h1", Operations = new[] { new Operation() { Kind = "wrong_kind" } } },
                    new OperationGroup
                    {
                        Hash = "h2_tz1proxy_invalid_operation_status", Operations = new[]
                        {
                            new Operation()
                            {
                                Kind = Michelson.Const.Operation.Kind.Transaction,
                                Destination = "tz1proxy",
                                Metadata = new OperationMetadata() { OperationResult = new Result() { Status = "wrong_status" } }
                            }
                        }
                    },
                    new OperationGroup
                    {
                        Hash = "h3_tz1proxy_valid_entrypoint", Operations = new[]
                        {
                            new Operation()
                            {
                                Kind = Michelson.Const.Operation.Kind.Transaction,
                                Destination = "tz1proxy",
                                Source = "h3",
                                Metadata =
                                    new OperationMetadata() { OperationResult = new Result() { Status = Michelson.Const.OperationResultStatus.Applied } },
                                Parameters = new OperationParameters() { Entrypoint = "tz1proxy.entrypoint" },
                                Amount = "123",
                            }
                        }
                    },
                    new OperationGroup
                    {
                        Hash = "h4_tz2destination_valid_bigmap_update",
                        Operations = new[]
                        {
                            new Operation()
                            {
                                Kind = Michelson.Const.Operation.Kind.Transaction,
                                Destination = "tz2destination",
                                Source = "h4",
                                Parameters = new OperationParameters() { Entrypoint = "tz2destination.entrypoint" },
                                Amount = "123",
                                Metadata = new OperationMetadata()
                                {
                                    OperationResult = new Result()
                                    {
                                        Status = Michelson.Const.OperationResultStatus.Applied,
                                        BigMapDiffs = new[]
                                        {
                                            new OperationResultBigMapDiff()
                                                { Action = Michelson.Const.BigMapDiffAction.Update, BigMap = "tz2destination.bigmap" }
                                        }
                                    }
                                },
                            },
                        },
                    },
                    new OperationGroup
                    {
                        Hash = "h5_tz2destination_wrong_bigmap_action",
                        Operations = new[]
                        {
                            new Operation()
                            {
                                Kind = Michelson.Const.Operation.Kind.Transaction,
                                Destination = "tz2destination",
                                Parameters = new OperationParameters() { Entrypoint = "tz2destination.entrypoint" },
                                Amount = "123",
                                Source = "h5",
                                Metadata = new OperationMetadata()
                                {
                                    OperationResult = new Result()
                                    {
                                        Status = Michelson.Const.OperationResultStatus.Applied,
                                        BigMapDiffs = new[]
                                        {
                                            new OperationResultBigMapDiff()
                                                { Action = "wrong_action", BigMap = "tz2destination.bigmap" }
                                        }
                                    }
                                },
                            },
                        },
                    },
                    new OperationGroup
                    {
                        Hash = "h6_tz1proxy_valid_entrypoint_internalOperation",
                        Operations = new[]
                        {
                            new Operation()
                            {
                                Kind = Michelson.Const.Operation.Kind.Transaction,
                                Source = "h6",
                                Destination = "tz1noSubscription",
                                Metadata = new OperationMetadata()
                                {
                                    OperationResult = new Result() { Status = Michelson.Const.OperationResultStatus.Applied },
                                    InternalOperations = new[]
                                    {
                                        new InternalOperation()
                                        {
                                            Kind = Michelson.Const.Operation.Kind.Transaction,
                                            Destination = "tz1proxy",
                                            Parameters = new OperationParameters() { Entrypoint = "tz1proxy.entrypoint" },
                                            Amount = "123",
                                            Source = "h6.internal",
                                            Result = new Result()
                                            {
                                                Status = Michelson.Const.OperationResultStatus.Applied,
                                            }
                                        }
                                    }
                                },
                            }
                        }
                    },
                    new OperationGroup
                    {
                        Hash = "h7_tz2destination_valid_bigmap_update_internalOperation",
                        Operations = new[]
                        {
                            new Operation()
                            {
                                Kind = Michelson.Const.Operation.Kind.Transaction,
                                Source = "h7",
                                Destination = "tz1noSubscription",
                                Metadata = new OperationMetadata()
                                {
                                    OperationResult = new Result()
                                    {
                                        Status = Michelson.Const.OperationResultStatus.Applied,
                                    },
                                    InternalOperations = new[]
                                    {
                                        new InternalOperation
                                        {
                                            Result = new Result
                                            {
                                                Status = Michelson.Const.OperationResultStatus.Applied,
                                                BigMapDiffs = new[]
                                                {
                                                    new OperationResultBigMapDiff
                                                        { Action = Michelson.Const.BigMapDiffAction.Update, BigMap = "tz2destination.bigmap" }
                                                }
                                            },
                                            Kind = Michelson.Const.Operation.Kind.Transaction,
                                            Parameters = new OperationParameters() { Entrypoint = "tz2destination.entrypoint" },
                                            Amount = "123",
                                            Source = "h7.internal",
                                            Destination = "tz2destination",
                                        }
                                    }
                                },
                            },
                        },
                    },
                    new OperationGroup
                    {
                        Hash = "h8_no_subscription_on_tz2proxy_address",
                        Operations = new[]
                        {
                            new Operation()
                            {
                                Kind = Michelson.Const.Operation.Kind.Transaction,
                                Destination = "tz2proxy",
                                Source = "h8",
                                Metadata = new OperationMetadata() { OperationResult = new Result() { Status = Michelson.Const.OperationResultStatus.Applied } }
                            }
                        }
                    },
                    new OperationGroup
                    {
                        Hash = "h9_no_subscription_on_tz1destination_address",
                        Operations = new[]
                        {
                            new Operation()
                            {
                                Kind = Michelson.Const.Operation.Kind.Transaction,
                                Destination = "tz1destination",
                                Source = "h9",
                                Metadata = new OperationMetadata() { OperationResult = new Result() { Status = Michelson.Const.OperationResultStatus.Applied } }
                            }
                        }
                    },
                    new OperationGroup
                    {
                        Hash = "h10_tz2destination_outgoingtransaction_internalOperation",
                        Operations = new[]
                        {
                            new Operation()
                            {
                                Kind = Michelson.Const.Operation.Kind.Transaction,
                                Source = "h10",
                                Destination = "tz1noSubscription",
                                Metadata = new OperationMetadata()
                                {
                                    OperationResult = new Result()
                                    {
                                        Status = Michelson.Const.OperationResultStatus.Applied,
                                    },
                                    InternalOperations = new[]
                                    {
                                        new InternalOperation
                                        {
                                            Result = new Result
                                            {
                                                Status = Michelson.Const.OperationResultStatus.Applied,
                                            },
                                            Kind = Michelson.Const.Operation.Kind.Transaction,
                                            Amount = "123",
                                            Source = "tz2destination",
                                            Destination = "h10.internal",
                                        }
                                    }
                                },
                            },
                        },
                    },
                }
            };

            #endregion

            var clientMock = Substitute.For<ITezosClient>();
            var recordLayoutFactoryMock = Substitute.For<ILayoutFactory>();

            var (csProxy1, csDestination1) = SetUpMocksForProxyAndDestinationContracts("tz1", clientMock, recordLayoutFactoryMock);
            SetUpLayoutMocks(recordLayoutFactoryMock, csProxy1);
            SetUpLayoutMocks(recordLayoutFactoryMock, csDestination1);
            var (csProxy2, csDestination2) = SetUpMocksForProxyAndDestinationContracts("tz2", clientMock, recordLayoutFactoryMock);
            SetUpLayoutMocks(recordLayoutFactoryMock, csProxy2);
            SetUpLayoutMocks(recordLayoutFactoryMock, csDestination2);

            var subscriptionConfigurations = new Dictionary<string, ContractSubscriptionConfiguration[]>
            {
                { "tz1proxy", new[] { new ContractSubscriptionConfiguration { ResolveThroughProxy = false } } },
                { "tz2proxy", new[] { new ContractSubscriptionConfiguration { ResolveThroughProxy = true } } }
            };

            var instance = new TezosBlockTraverser(clientMock, Substitute.For<ILogger<TezosBlockTraverser>>(), recordLayoutFactoryMock);
            await instance.InitializeAsync(subscriptionConfigurations, CancellationToken.None);

            //act
            var contractSubscriberProxy1Mock = Substitute.For<IContractSubscriber>();
            var contractSubscriberDestination2Mock = Substitute.For<IContractSubscriber>();
            instance.TraverseAndNotifySubscribers(
                block,
                new Dictionary<string, IContractSubscriber[]>
                {
                    { "tz1proxy", new[] { contractSubscriberProxy1Mock } },
                    { "tz2proxy", new[] { contractSubscriberDestination2Mock } },
                }
            );

            //assert
            contractSubscriberProxy1Mock.Received(1)
                .OnEntrypoint(
                    block,
                    "h3_tz1proxy_valid_entrypoint",
                    "tz1proxy.entrypoint",
                    "h3",
                    123,
                    Arg.Any<IReadOnlyDictionary<string, IMichelsonValue>>(),
                    Arg.Any<IReadOnlyDictionary<string, IMichelsonValue>>()
                );
            contractSubscriberProxy1Mock.Received(1)
                .OnEntrypoint(
                    block,
                    "h6_tz1proxy_valid_entrypoint_internalOperation",
                    "tz1proxy.entrypoint",
                    "h6.internal",
                    123,
                    Arg.Any<IReadOnlyDictionary<string, IMichelsonValue>>(),
                    Arg.Any<IReadOnlyDictionary<string, IMichelsonValue>>()
                );
            //only previous two call were done, no other call onEntrypoint nor any call for OnBigMap
            contractSubscriberProxy1Mock.ReceivedWithAnyArgs(2);

            contractSubscriberDestination2Mock.Received(1)
                .OnBigMapDiff(
                    block,
                    "h4_tz2destination_valid_bigmap_update",
                    "bigmap",
                    Arg.Any<MichelsonValue>(),
                    Arg.Any<IReadOnlyDictionary<string, IMichelsonValue>>(),
                    Arg.Any<IReadOnlyDictionary<string, IMichelsonValue>>()
                );
            contractSubscriberDestination2Mock.Received(1)
                .OnBigMapDiff(
                    block,
                    "h7_tz2destination_valid_bigmap_update_internalOperation",
                    "bigmap",
                    Arg.Any<MichelsonValue>(),
                    Arg.Any<IReadOnlyDictionary<string, IMichelsonValue>>(),
                    Arg.Any<IReadOnlyDictionary<string, IMichelsonValue>>()
                );
            contractSubscriberDestination2Mock.Received(1)
                .OnOutgoingTransaction(
                    block,
                    "h10_tz2destination_outgoingtransaction_internalOperation",
                    "h10.internal",
                    123,
                    Arg.Any<IReadOnlyDictionary<string, IMichelsonValue>>()
                );
            //two onEntrypointCalls, two onBigMapCalls, one OnOutgoingTransaction
            contractSubscriberDestination2Mock.ReceivedWithAnyArgs(5);
        }

        private static (ContractScript Proxy, ContractScript Destination) SetUpMocksForProxyAndDestinationContracts(
            string addressPrefix,
            ITezosClient clientMock,
            ILayoutFactory layoutFactoryMock
        )
        {
            //create contract scripts
            var destination = addressPrefix + "destination";
            var proxyStorage = JsonDocument.Parse(@"{""string"":""" + destination + @"""}").RootElement;
            var contractScriptProxy = new ContractScript(addressPrefix + "proxy", new JsonElement(), proxyStorage);
            var contractScriptDestination = new ContractScript(destination, new JsonElement(), EmptyJsonDocument);

            //setup tezos node client mock
            clientMock.GetContractScriptAsync(contractScriptProxy.Address, Arg.Any<CancellationToken>())
                .Returns(contractScriptProxy);
            clientMock.GetContractScriptAsync(contractScriptDestination.Address, Arg.Any<CancellationToken>())
                .Returns(contractScriptDestination);

            //setup record layout factory mock with proxy storage pointing to destination contact
            var storageDefinition = JsonDocument.Parse(
                    $"{{\"prim\":\"{Michelson.Const.Prim.Address}\", \"{Michelson.Const.Annotation}\":[\"%{Michelson.Const.Proxy.DestinationContractAddress}\"]}}"
                )
                .RootElement;
            layoutFactoryMock.CreateStorageLayout(contractScriptProxy).Returns(new RecordLayout(storageDefinition));
            layoutFactoryMock.CreateStorageLayout(contractScriptDestination).Returns(new RecordLayout(EmptyJsonDocument));

            return (contractScriptProxy, contractScriptDestination);
        }

        private static void SetUpLayoutMocks(ILayoutFactory layoutFactoryMock, ContractScript contractScriptProxy)
        {
            layoutFactoryMock.CreateEntrypointLayouts(contractScriptProxy.Address, contractScriptProxy)
                .Returns(
                    new Dictionary<string, Layouts.RecordLayout>
                        { { contractScriptProxy.Address + ".entrypoint", new Layouts.RecordLayout(EmptyJsonDocument) } }
                );
            layoutFactoryMock.CreateBigMapLayouts(contractScriptProxy)
                .Returns(
                    new Dictionary<string, Layouts.BigMapLayout>
                    {
                        {
                            contractScriptProxy.Address + ".bigmap",
                            new BigMapLayout("bigmap", EmptyJsonDocument, new Layouts.RecordLayout(EmptyJsonDocument))
                        }
                    }
                );
        }
    }
}