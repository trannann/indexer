﻿using System.Collections;
using NUnit.Framework;
using TezosDomains.Indexer.Tezos.Encoding;

namespace TezosDomains.Indexer.Tezos.Tests.Encoding
{
    [TestFixture]
    public class TezosEncodingTests
    {
        public static readonly IEnumerable AddressTestCases = new[]
        {
            new object[] { "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n", "00007128c922351e2a0b591f36ce638880052891b9f6" },
            new object[] { "tz1g1mFqhoTk2CHMonQX7FQ2wvnwGPUozZJj", "0000df7ec819ea15ee62ed936143273547e2c662599f" },
            new object[] { "tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL", "00013e2d7004b58772f841e8065a2a7504bfd0138d7a" },
            new object[] { "tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm", "00024442bb78323de8d682548dbf9902b48eb437c7c6" },
            new object[] { "KT1RKZcsTS7kLuRd6JygiVkC1RJ4pfwznwkf", "01b799ed2ec74368e18789be5b2e1e9fe2fb83543f00" },
            new object[] { "KT1BDEn6wobs7tDReKkGheXAhoq278TGaNn5", "011cd5f135e80fd8ebb6e43335b24ca6116edeba6900" },
        };

        [TestCaseSource(nameof(AddressTestCases))]
        public void ConvertValidAddress_FromHexStringToBase58_ShouldSucceed(string base58Address, string hexAddress)
        {
            Assert.That(TezosEncoding.Address.ConvertFromHex(hexAddress), Is.EqualTo(base58Address));
        }

        [TestCaseSource(nameof(AddressTestCases))]
        public void ConvertValidAddress_FromBase58ToHexString_ShouldSucceed(string base58Address, string hexAddress)
        {
            Assert.That(TezosEncoding.Address.ConvertFromBase58(base58Address), Is.EqualTo(hexAddress));
        }


        [TestCase("706c61792e6d696b652e74657a", "play.mike.tez")]
        public void ConvertValidName_FromHexStringToUtf8_ShouldSucceed(string hexString, string utf8String)
        {
            Assert.That(TezosEncoding.String.ConvertFromHex(hexString), Is.EqualTo(utf8String));
        }
    }
}