﻿using NUnit.Framework;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Indexer.Tezos.Serializers;

namespace TezosDomains.Indexer.Tezos.Tests.Serializer
{
    [TestFixture]
    public class TezosNodeDeserializerTest
    {
        [Test]
        public void Deserialize_ShouldNotFail_WithMaximumDepthOf64()
        {
            TezosNodeDeserializer.Deserialize<TezosBlock>(JsonHelper.ReadJsonFile("Serializer", "583943.json"));
        }
    }
}