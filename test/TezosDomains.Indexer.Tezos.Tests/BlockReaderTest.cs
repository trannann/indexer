﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NUnit.Framework;
using TezosDomains.Indexer.Tezos.Clients;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.Indexer.Tezos.Implementations;
using TezosDomains.Indexer.Tezos.Models.Block;

namespace TezosDomains.Indexer.Tezos.Tests
{
    [TestFixture]
    public class BlockReaderTest
    {
        private IBlockMonitor _monitor = null!;
        private ITezosClient _tezosClient = null!;
        private ILogger<BlockReader> _logger = null!;
        private BlockReader _reader = null!;

        [SetUp]
        public void SetUp()
        {
            _monitor = Substitute.For<IBlockMonitor>();
            _tezosClient = Substitute.For<ITezosClient>();
            _logger = Substitute.For<ILogger<BlockReader>>();
            _reader = new BlockReader(_monitor, _tezosClient, _logger, new TezosContractConfiguration() { StartFromLevel = 1 });
            _reader.Reset(5);
        }

        private void SetupGetBlockReturnValue(int level, string hash)
        {
            var block = new TezosBlock { Hash = hash, Header = new TezosBlockHeader() { Level = level } };
            _tezosClient.GetBlockAsync(level, Arg.Any<CancellationToken>()).Returns(block);
        }

        [Test]
        public async Task NextBlock_OnMonitorOnly()
        {
            // Arrange
            _tezosClient.GetHeadAsync(CancellationToken.None).Returns(new TezosBlockHeader { Level = 5 });
            _monitor.IsOpen.Returns(false);
            _monitor.WaitForNextAsync(CancellationToken.None).Returns(new TezosBlockHeader { Level = 6 });
            SetupGetBlockReturnValue(6, "aaa");

            // Act
            var block = await _reader.NextBlockAsync(CancellationToken.None);

            // Assert
            Assert.That(block.Hash, Is.EqualTo("aaa"));
            await _tezosClient.Received(1).GetHeadAsync(CancellationToken.None);
            await _tezosClient.Received(1).GetBlockAsync(6, CancellationToken.None);
            await _monitor.Received(1).WaitForNextAsync(CancellationToken.None);
        }

        [Test]
        public async Task NextBlock_OnMonitorOverlap()
        {
            // Arrange
            _tezosClient.GetHeadAsync(CancellationToken.None).Returns(new TezosBlockHeader { Level = 5 });
            _monitor.IsOpen.Returns(false);
            _monitor.WaitForNextAsync(CancellationToken.None).Returns(new TezosBlockHeader { Level = 5 }, new TezosBlockHeader { Level = 6 });
            SetupGetBlockReturnValue(6, "aaa");

            // Act
            var block = await _reader.NextBlockAsync(CancellationToken.None);

            // Assert
            Assert.That(block.Hash, Is.EqualTo("aaa"));
            await _tezosClient.Received(1).GetHeadAsync(CancellationToken.None);
            await _tezosClient.Received(1).GetBlockAsync(6, CancellationToken.None);
            await _tezosClient.DidNotReceive().GetBlockAsync(Arg.Is<int>(a => a != 6), CancellationToken.None);
            await _monitor.Received(2).WaitForNextAsync(CancellationToken.None);
        }

        [Test]
        public async Task NextBlock_OnExistingSequence()
        {
            // block 6
            _monitor.IsOpen.Returns(false);
            _tezosClient.GetHeadAsync(CancellationToken.None).Returns(new TezosBlockHeader { Level = 7 });
            SetupGetBlockReturnValue(6, "666");
            var block6 = await _reader.NextBlockAsync(CancellationToken.None);
            Assert.That(block6.Hash, Is.EqualTo("666"));

            // block 7
            SetupGetBlockReturnValue(7, "777");
            var block7 = await _reader.NextBlockAsync(CancellationToken.None);
            Assert.That(block7.Hash, Is.EqualTo("777"));

            // block 8
            _tezosClient.GetHeadAsync(CancellationToken.None).Returns(new TezosBlockHeader { Level = 8 });
            SetupGetBlockReturnValue(8, "888");

            var block8 = await _reader.NextBlockAsync(CancellationToken.None);
            Assert.That(block8.Hash, Is.EqualTo("888"));

            // block 9
            _monitor.WaitForNextAsync(CancellationToken.None).Returns(new TezosBlockHeader { Level = 9 });
            SetupGetBlockReturnValue(9, "999");

            var block9 = await _reader.NextBlockAsync(CancellationToken.None);
            Assert.That(block9.Hash, Is.EqualTo("999"));

            // block 10
            _monitor.IsOpen.Returns(true);
            _monitor.WaitForNextAsync(CancellationToken.None).Returns(new TezosBlockHeader { Level = 10 });
            SetupGetBlockReturnValue(10, "000");

            var block10 = await _reader.NextBlockAsync(CancellationToken.None);
            Assert.That(block10.Hash, Is.EqualTo("000"));
        }
    }
}