using System;
using System.Collections.Generic;

namespace TezosDomains.Utils
{
    public static class EnumerableExtensions
    {
        public static int RequiredIndexOf<T>(this IEnumerable<T> items, T itemToFind)
        {
            var index = items.IndexOf(itemToFind);
            return index >= 0
                ? index
                : throw new IndexOutOfRangeException($"Item {itemToFind} wasn't found in a sequence of {typeof(T)} items.");
        }

        public static int IndexOf<T>(this IEnumerable<T> items, T itemToFind)
        {
            var index = 0;
            foreach (var item in items)
            {
                if (Equals(item, itemToFind))
                    return index;

                index++;
            }

            return -1;
        }

        public static IReadOnlyList<T> NullToEmpty<T>(this IReadOnlyList<T>? enumerable)
            => enumerable ?? Array.Empty<T>();

        public static IEnumerable<T> NullToEmpty<T>(this IEnumerable<T>? enumerable)
            => enumerable ?? Array.Empty<T>();
    }
}