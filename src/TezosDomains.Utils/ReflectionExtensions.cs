﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace TezosDomains.Utils
{
    public static class ReflectionExtensions
    {
        public static string GetDescription<TEnum>(this TEnum thisEnum)
            where TEnum : Enum
        {
            var field = typeof(TEnum).GetField(thisEnum.ToString()).GuardNotNull();
            var attr = field.GetCustomAttribute<DescriptionAttribute>(inherit: true);

            return attr?.Description.WhiteSpaceToNull()
                   ?? throw new Exception($"Missing {nameof(DescriptionAttribute)} on {thisEnum} of {typeof(TEnum)}.");
        }
    }
}