﻿using System;

namespace TezosDomains.Utils
{
    public static class NullableExtensions
    {
        public static T GuardNotNull<T>(this T? value, string message = "The value cannost be null. Check this stack trace to find more details.")
            => value ?? throw new ArgumentNullException(message, innerException: null);
    }
}