﻿using System;
using TezosDomains.Data.Models;

namespace TezosDomains.Data.MongoDb.DistributedLock
{
    /// <summary>
    /// Document used for holding a distributed lock in mongo.
    /// </summary>
    public class Lock : IDocument
    {
        public Lock(string resource, DateTime expireAtUtc, string machineName, int processId)
        {
            Resource = resource;
            ExpireAtUtc = expireAtUtc;
            MachineName = machineName;
            ProcessId = processId;
        }

        /// <summary>
        /// The name of the resource being held.
        /// </summary>
        public string Resource { get; }

        /// <summary>
        /// The timestamp for when the lock expires.
        /// This is used if the lock is not maintained or 
        /// cleaned up by the owner (e.g. process was shut down).
        /// </summary>
        public DateTime ExpireAtUtc { get; }

        /// <summary>
        /// The name of machine which acquired the lock.
        /// </summary>
        public string MachineName { get; }

        /// <summary>
        /// The process id of the application which acquired the lock.
        /// </summary>
        public int ProcessId { get; }
    }
}