﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Configurations;
using TezosDomains.Data.MongoDb.DistributedLock;

namespace TezosDomains.Data.MongoDb
{
    public class MongoDbContext
    {
        public IMongoDatabase Database { get; }

        public MongoDbContext(MongoClient client, IMongoDbConfiguration configuration, ILogger<MongoDbContext> logger)
        {
            if (logger == null)
                throw new ArgumentNullException(nameof(logger));

            Database = client.GetDatabase(configuration.Database);
        }

        public IMongoCollection<Block> Blocks => Database.GetCollection<Block>("blocks");

        private static readonly IReadOnlyDictionary<Type, string> CollectionNames = new Dictionary<Type, string>
        {
            { typeof(Domain), "domains" },
            { typeof(ReverseRecord), "reverse_records" },
            { typeof(Auction), "auctions" },
            { typeof(Lock), "locks" },
            { typeof(Event), "events" },
            { typeof(BidderBalances), "bidder_balances" }
        };

        public IMongoCollection<TRecord> Get<TRecord>()
            where TRecord : IDocument
            => Database.GetCollection<TRecord>(CollectionNames[typeof(TRecord)]);

        public IMongoCollection<HistoryOf<TRecord>> GetHistory<TRecord>()
            where TRecord : IDocumentWithHistory
            => Database.GetCollection<HistoryOf<TRecord>>(CollectionNames[typeof(TRecord)]);

        public IMongoCollection<THistory> GetHistory<TRecord, THistory>()
            where THistory : HistoryOf<TRecord>
            where TRecord : IDocumentWithHistory
            => Database.GetCollection<THistory>(CollectionNames[typeof(TRecord)]);
    }
}