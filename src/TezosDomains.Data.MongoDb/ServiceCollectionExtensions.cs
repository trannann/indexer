﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using System;
using TezosDomains.Data.Command;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Configurations;
using TezosDomains.Data.MongoDb.HealthCheck;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Ordering;
using TezosDomains.Data.MongoDb.Services;
using TezosDomains.Data.MongoDb.Services.Update;

namespace TezosDomains.Data.MongoDb
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddMongoDb(this IServiceCollection services, IHostEnvironment environment, MongoDbConfiguration configuration)
        {
            if (environment.IsDevelopment())
            {
                configuration.OverrideDatabaseName(name => $"{name}_{Environment.MachineName}");
            }

            services
                .AddHealthChecks()
                .AddCheck<MongoDbHealthCheck>("MongoDb");

            services
                .AddSingleton<IMongoDbConfiguration>(configuration)
                .AddSingleton<MongoDbInitializer>()
                .AddSingleton<MongoDbContext>()
                .AddTransient<MongoDbHealthCheck>()
                .AddTransient<IUpdateService, UpdateService>()
                .AddTransient<DomainUpdateService>()
                .AddTransient<ReverseRecordUpdateService>()
                .AddTransient<AuctionUpdateService>()
                .AddTransient<BidderBalancesUpdateService>()
                .AddTransient<IBlockQueryService, BlockQueryService>()
                .AddSingleton<IndexerDistributedLockService>()
                .AddTransient<IQueryService<Domain>, DomainQueryService>()
                .AddTransient<IDomainQueryService, DomainQueryService>()
                .AddTransient<IQueryService<ReverseRecord>, DocumentWithHistoryQueryService<ReverseRecord>>()
                .AddTransient<IQueryService<BidderBalances>, DocumentWithHistoryQueryService<BidderBalances>>()
                .AddTransient<IQueryService<Auction>, AuctionQueryService>()
                .AddTransient<IAuctionQueryService, AuctionQueryService>()
                .AddTransient<EventProcessor>()
                .AddTransient<IQueryService<Event>>(
                    sp => new DocumentQueryService<Event>(sp.GetService<MongoDbContext>(), skipUnusedProperties: false)
                ) //all properties needs to be retrieve for inheritance to work
                .AddSingleton(sp => MongoDbClientFactory.Create(sp.GetService<IMongoDbConfiguration>(), sp.GetService<ILogger<MongoClient>>()));

            // Default Document Orders
            services.AddSingleton(DomainsOrder.Default);
            services.AddSingleton(ReverseRecordsOrder.Default);

            MongoDbSetup.InitializeMappingStructures();

            return services;
        }
    }
}