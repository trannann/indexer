﻿using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Core.Events;
using System;
using TezosDomains.Data.MongoDb.Configurations;

namespace TezosDomains.Data.MongoDb
{
    internal static class MongoDbClientFactory
    {
        internal static MongoClient Create(IMongoDbConfiguration configuration, ILogger logger)
        {
            if (string.IsNullOrWhiteSpace(configuration.Database))
                throw new ArgumentException("Mongo database cannot be null or empty", nameof(configuration.Database));

            var settings = MongoClientSettings.FromConnectionString(configuration.ConnectionString);

            settings.SdamLogFilename = configuration.SdamLogFilename;

            if (configuration.Timeout != default)
                settings.ConnectTimeout = settings.ServerSelectionTimeout = settings.WaitQueueTimeout = configuration.Timeout;

            settings.ClusterConfigurator = cb =>
            {
                if (logger.IsEnabled(LogLevel.Trace))
                {
                    cb.Subscribe<CommandStartedEvent>(
                        e => { logger.LogTrace("MongoDb command '{eCommandName}' started with request: {eCommand}", e.CommandName, e.Command.ToJson()); }
                    );
                    cb.Subscribe<CommandSucceededEvent>(
                        e =>
                        {
                            logger.LogTrace(
                                "MongoDb command '{eCommandName}' finished after {eDuration} with response: {eReply}",
                                e.CommandName,
                                e.Duration,
                                e.Reply.ToJson()
                            );
                        }
                    );
                }
            };

            logger.LogInformation(
                "Creating MongoClient with servers: '{servers}', user: '{user}' and database: '{database}' ",
                configuration.Servers,
                configuration.Username,
                configuration.Database
            );
            return new MongoClient(settings);
        }
    }
}