﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Data.MongoDb.Query.Ordering
{
    public static class EventsOrder
    {
        private static readonly Expression<Func<Event, object>> UniqueExpression = p => p.Id;

        public static readonly IReadOnlyList<DocumentOrderField<Event>> Fields = new[]
        {
            DocumentOrderField<Event>.Create(
                displayName: "TIMESTAMP",
                description: "Order events by timestamp of associated block.",
                primaryExpression: p => p.Block.Timestamp,
                secondaryUniqueExpression: UniqueExpression
            )
        };

        public static readonly DocumentOrder<Event> Default = new DocumentOrder<Event>(Fields[0], OrderDirection.Desc);
    }
}