﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Data.MongoDb.Query.Ordering
{
    public static class DomainsOrder
    {
        private static readonly Expression<Func<Domain, object>> UniqueExpression = d => d.NameReverse;

        public static readonly IReadOnlyList<DocumentOrderField<Domain>> Fields = new[]
        {
            DocumentOrderField<Domain>.Create(
                displayName: "DOMAIN",
                description: "Order domains by name and level.",
                uniqueExpression: UniqueExpression
            ),
            DocumentOrderField<Domain>.Create(
                displayName: "NAME",
                description: "Order domains by name.",
                uniqueExpression: d => d.Name
            ),
            DocumentOrderField<Domain>.CreateNullable(
                displayName: "ADDRESS",
                description: "Order domains by address.",
                primaryExpression: d => d.Address,
                secondaryUniqueExpression: UniqueExpression
            ),
            DocumentOrderField<Domain>.Create(
                displayName: "OWNER",
                description: "Order domains by owner.",
                primaryExpression: d => d.Owner,
                secondaryUniqueExpression: UniqueExpression
            ),
            DocumentOrderField<Domain>.CreateNullable(
                displayName: "EXPIRES_AT",
                description: "Order domains by validity.",
                primaryExpression: d => d.ExpiresAtUtc,
                secondaryUniqueExpression: UniqueExpression
            ),
            DocumentOrderField<Domain>.Create(
                displayName: "LEVEL",
                description: "Order domains by level.",
                primaryExpression: d => d.Level,
                secondaryUniqueExpression: UniqueExpression
            ),
        };

        public static readonly DocumentOrder<Domain> Default = new DocumentOrder<Domain>(Fields[0], OrderDirection.Asc);
    }
}