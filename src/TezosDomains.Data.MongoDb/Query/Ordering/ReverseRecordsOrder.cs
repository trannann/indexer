﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Data.MongoDb.Query.Ordering
{
    public static class ReverseRecordsOrder
    {
        private static readonly Expression<Func<ReverseRecord, object>> UniqueExpression = r => r.Address;

        public static readonly IReadOnlyList<DocumentOrderField<ReverseRecord>> Fields = new[]
        {
            DocumentOrderField<ReverseRecord>.CreateNullable(
                displayName: "NAME",
                description: "Order reverse records by domain name.",
                primaryExpression: r => r.Name,
                secondaryUniqueExpression: UniqueExpression
            ),
            DocumentOrderField<ReverseRecord>.Create(
                displayName: "ADDRESS",
                description: "Order reverse records by address.",
                uniqueExpression: UniqueExpression
            ),
            DocumentOrderField<ReverseRecord>.Create(
                displayName: "OWNER",
                description: "Order reverse records by owner.",
                primaryExpression: r => r.Owner,
                secondaryUniqueExpression: UniqueExpression
            ),
            DocumentOrderField<ReverseRecord>.CreateNullable(
                displayName: "EXPIRES_AT",
                description: "Order reverse records by validity.",
                primaryExpression: r => r.ExpiresAtUtc,
                secondaryUniqueExpression: UniqueExpression
            ),
        };

        public static readonly DocumentOrder<ReverseRecord> Default = new DocumentOrder<ReverseRecord>(Fields[0], OrderDirection.Asc);
    }
}