﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Data.MongoDb.Query.Ordering
{
    public static class AuctionsOrder
    {
        private static readonly Expression<Func<Auction, object>> UniqueExpression = d => d.Id;

        public static readonly IReadOnlyList<DocumentOrderField<Auction>> Fields = new[]
        {
            DocumentOrderField<Auction>.Create(
                displayName: "ID",
                description: "Order auctions by id.",
                uniqueExpression: UniqueExpression
            ),
            DocumentOrderField<Auction>.Create(
                displayName: "DOMAIN_NAME",
                description: "Order auctions by domain name.",
                primaryExpression: d => d.DomainName,
                secondaryUniqueExpression: UniqueExpression
            ),
            DocumentOrderField<Auction>.Create(
                displayName: "ENDS_AT",
                description: "Order auctions by auction end datetime.",
                primaryExpression: d => d.EndsAtUtc,
                secondaryUniqueExpression: UniqueExpression
            ),
            DocumentOrderField<Auction>.Create(
                displayName: "HIGHEST_BID_AMOUNT",
                description: "Order auctions by currently highest bid amount.",
                primaryExpression: d => d.Bids[0].Amount,
                secondaryUniqueExpression: UniqueExpression
            ),
            DocumentOrderField<Auction>.Create(
                displayName: "HIGHEST_BID_TIMESTAMP",
                description: "Order auctions by currently highest bid timestamp.",
                primaryExpression: d => d.Bids[0].Block.Timestamp,
                secondaryUniqueExpression: UniqueExpression
            ),
            DocumentOrderField<Auction>.Create(
                displayName: "BID_COUNT",
                description: "Order auctions by total number of bids.",
                primaryExpression: d => d.BidCount,
                secondaryUniqueExpression: UniqueExpression
            )
        };

        public static readonly DocumentOrder<Auction> Default = new DocumentOrder<Auction>(Fields[2], OrderDirection.Desc);
    }
}