﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Data.MongoDb.Query.Ordering
{
    public static class BidderBalancesOrder
    {
        private static readonly Expression<Func<BidderBalances, object>> UniqueExpression = p => p.Address;

        public static readonly IReadOnlyList<DocumentOrderField<BidderBalances>> Fields = new[]
        {
            DocumentOrderField<BidderBalances>.Create(
                displayName: "TIMESTAMP",
                description: "Order Users by timestamp of associated block.",
                primaryExpression: p => p.Block.Timestamp,
                secondaryUniqueExpression: UniqueExpression
            )
        };

        public static readonly DocumentOrder<BidderBalances> Default = new DocumentOrder<BidderBalances>(Fields[0], OrderDirection.Desc);
    }
}