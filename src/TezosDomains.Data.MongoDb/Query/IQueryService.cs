﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Data.MongoDb.Query
{
    public interface IQueryService<TDocument>
        where TDocument : class, IDocument
    {
        Task<TDocument?> FindSingleAsync(IDocumentFilter<TDocument> filter, CancellationToken cancellationToken);
        Task<List<TDocument>> FindAllAsync(DocumentQuery<TDocument> query, CancellationToken cancellationToken);
        Task<long> CountAsync(IDocumentFilter<TDocument> filter, CancellationToken cancellationToken);
    }
}