﻿using MongoDB.Driver;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Events
{
    public sealed class EventsFilter : NestedFilter<Event, EventsFilter>
    {
        public StringFilter? Address { get; set; }
        public StringFilter? DomainName { get; set; }
        public EnumAsStringFilter<EventType>? Type { get; set; }
        public AssociatedBlockFilter? Block { get; set; }

        protected override FilterDefinition<Event>? ToDbFilter(FilterDefinitionBuilder<Event> builder)
        {
            return builder.AndNullable(
                base.ToDbFilter(builder),
                AddressToDbFilter(builder),
                DomainName?.ToDbFilter(builder, nameof(DomainBuyEvent.DomainName)),
                Type?.ToDbFilter(builder, "_t"),
                Block?.ToDbFilter(builder)
            );
        }

        private FilterDefinition<Event>? AddressToDbFilter(FilterDefinitionBuilder<Event> builder)
        {
            if (Address == null)
                return null;

            return builder.Or(
                Address.ToDbFilter(builder, nameof(DomainBuyEvent.SourceAddress)),
                Address.ToDbFilter(builder, nameof(AuctionBidEvent.PreviousBidderAddress)),
                Address.ToDbFilter(builder, nameof(AuctionEndEvent.Participants))
            );
        }
    }
}