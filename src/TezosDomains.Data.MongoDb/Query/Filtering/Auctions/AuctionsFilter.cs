﻿using MongoDB.Driver;
using System;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Auctions
{
    public sealed class AuctionsFilter : DocumentWithHistoryFilter<Auction, AuctionsFilter>
    {
        public StringFilter? DomainName { get; set; }
        public AuctionStateFilter? State { get; set; }
        public StringFilter? HighestBidder { get; set; }
        public StringArrayFilter? Bidders { get; set; }
        public ComparableFilter<DateTime>? EndsAtUtc { get; set; }
        public ComparableFilter<int>? BidCount { get; set; }

        protected override FilterDefinition<Auction>? ToDbFilter(FilterDefinitionBuilder<Auction> builder)
            => builder.AndNullable(
                base.ToDbFilter(builder),
                DomainName?.ToLowerInvariant().ToDbFilter(builder, d => d.DomainName),
                HighestBidder?.ToDbFilter(builder, a => a.Bids[0].Bidder),
                Bidders?.ToDbFilter(builder, a => a.Bids, b => b.Bidder),
                State?.ToDbFilter(builder, AtBlock),
                EndsAtUtc?.ToDbFilter(builder, d => d.EndsAtUtc),
                BidCount?.ToDbFilter(builder, d => d.BidCount)
            );
    }
}