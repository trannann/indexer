﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Auctions
{
    public sealed class AuctionStateFilter : IEnumFilter<AuctionState>
    {
        public FilterDefinition<Auction>? ToDbFilter(FilterDefinitionBuilder<Auction> builder, BlockHistoryFilter? block)
        {
            if (In == null)
                return null;

            var atUtc = block?.Timestamp ?? DateTime.UtcNow;
            return builder.Or(In.Select(s => Build(s, atUtc)));
        }

        private static FilterDefinition<Auction> Build(AuctionState state, DateTime atUtc)
        {
            var builder = Builders<Auction>.Filter;
            return state switch
            {
                AuctionState.InProgress => builder.And(builder.Lte(a => a.FirstBidAtUtc, atUtc), builder.Gt(a => a.EndsAtUtc, atUtc)),
                AuctionState.CanBeSettled => builder.And(
                    builder.Lte(a => a.EndsAtUtc, atUtc),
                    builder.Gt(a => a.OwnedUntilUtc, atUtc),
                    builder.Eq(a => a.IsSettled, false)
                ),
                AuctionState.SettlementExpired => builder.And(builder.Lte(a => a.OwnedUntilUtc, atUtc), builder.Eq(a => a.IsSettled, false)),
                AuctionState.Settled => builder.Eq(a => a.IsSettled, true),
                _ => throw new ArgumentOutOfRangeException(nameof(state), state, null)
            };
        }

        public List<AuctionState>? In { get; set; }
    }
}