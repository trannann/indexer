﻿using MongoDB.Driver;
using System;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Auctions
{
    public class CurrentAuctionFilter : DocumentWithHistoryFilter<Auction, CurrentAuctionFilter>
    {
        public CurrentAuctionFilter(string domainName, BlockHistoryFilter? atBlock)
        {
            DomainName = domainName;
            AtBlock = atBlock;
        }

        public string DomainName { get; }

        protected override FilterDefinition<Auction>? ToDbFilter(FilterDefinitionBuilder<Auction> builder)
        {
            var atUtc = AtBlock?.Timestamp ?? DateTime.UtcNow;
            return builder.AndNullable(
                base.ToDbFilter(builder),
                builder.Eq(a => a.DomainName, DomainName),
                builder.Gt(a => a.OwnedUntilUtc, atUtc),
                builder.Lte(a => a.FirstBidAtUtc, atUtc)
            );
        }
    }
}