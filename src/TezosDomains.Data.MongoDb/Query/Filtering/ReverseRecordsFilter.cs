﻿using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Data.MongoDb.Query.Filtering
{
    public sealed class ReverseRecordsFilter : RecordFilter<ReverseRecord, ReverseRecordsFilter>
    {
        public StringFilter? Address { get; set; }
        public StringFilter? Name { get; set; }
        public StringFilter? Owner { get; set; }

        protected override FilterDefinition<ReverseRecord>? ToDbFilter(FilterDefinitionBuilder<ReverseRecord> builder)
            => builder.AndNullable(
                base.ToDbFilter(builder),
                Address?.ToDbFilter(builder, r => r.Address),
                Name?.ToLowerInvariant().ToDbFilter(builder, r => r.Name),
                Owner?.ToDbFilter(builder, r => r.Owner)
            );
    }
}