﻿using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Data.MongoDb.Query.Filtering
{
    public sealed class DomainsFilter : RecordFilter<Domain, DomainsFilter>
    {
        public StringFilter? Name { get; set; }
        public StringFilter? Address { get; set; }
        public StringFilter? Owner { get; set; }
        public ComparableFilter<int>? Level { get; set; }
        public StringArrayFilter? Ancestors { get; set; }

        protected override FilterDefinition<Domain>? ToDbFilter(FilterDefinitionBuilder<Domain> builder)
            => builder.AndNullable(
                base.ToDbFilter(builder),
                Name?.ToLowerInvariant().ToDbFilter(builder, d => d.Name),
                Address?.ToDbFilter(builder, d => d.Address),
                Owner?.ToDbFilter(builder, d => d.Owner),
                Level?.ToDbFilter(builder, d => d.Level),
                Ancestors?.ToLowerInvariant().ToDbFilter(builder, d => d.Ancestors)
            );
    }
}