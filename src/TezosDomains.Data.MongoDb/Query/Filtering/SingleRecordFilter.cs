﻿using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Data.MongoDb.Query.Filtering
{
    public sealed class SingleRecordFilter<TRecord> : RecordFilter<TRecord, SingleRecordFilter<TRecord>>
        where TRecord : class, IRecord
    {
        public SingleRecordFilter(string id, RecordValidity validity, BlockHistoryFilter? atBlock)
        {
            Id = id;
            Validity = validity;
            AtBlock = atBlock;
        }

        public string Id { get; set; }

        protected override FilterDefinition<TRecord>? ToDbFilter(FilterDefinitionBuilder<TRecord> builder)
            => builder.AndNullable(
                base.ToDbFilter(builder),
                builder.Eq(DocumentHelper<TRecord>.IdExpression, Id)
            );
    }
}