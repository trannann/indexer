﻿using System;
using System.ComponentModel;
using MongoDB.Driver;
using TezosDomains.Data.Models;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    [Description("Validity of the record based on it's validity date-time. Default is `VALID`.")]
    public enum RecordValidity
    {
        [Description("Validity date-time is in the future or null (domain doesn't expire at all).")]
        Valid,

        [Description("Validity date-time is in the past.")]
        Expired,

        [Description("All record regardless of their validity date-time.")]
        All,
    }

    public static class RecordValidityExtensions
    {
        public static FilterDefinition<TRecord>? ToDbFilter<TRecord>(this RecordValidity validity, FilterDefinitionBuilder<TRecord> builder)
            where TRecord : IRecord
            => validity switch
            {
                RecordValidity.Valid => builder.Or(
                    builder.Gte(r => r.ExpiresAtUtc, DateTime.UtcNow),
                    builder.Eq(r => r.ExpiresAtUtc, null)
                ),
                RecordValidity.Expired => builder.Lt(r => r.ExpiresAtUtc, DateTime.UtcNow),
                _ => null,
            };
    }
}