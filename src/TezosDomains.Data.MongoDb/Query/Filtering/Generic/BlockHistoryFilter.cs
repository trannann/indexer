﻿using System;
using System.Linq.Expressions;
using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    public class BlockHistoryFilter
    {
        public int? Level { get; set; }
        public DateTime? Timestamp { get; set; }
        public bool IsHistoricQuery => Timestamp != null || Level != null;

        public FilterDefinition<TDocumentWithHistory>? ToDbFilter<TDocumentWithHistory>(FilterDefinitionBuilder<TDocumentWithHistory> builder)
            where TDocumentWithHistory : IDocumentWithHistory
        {
            return builder.AndNullable(
                Between(Level, fromField: b => b.Block.Level, toField: b => b.ValidUntilBlockLevel),
                Between(Timestamp, fromField: b => b.Block.Timestamp, toField: b => b.ValidUntilTimestamp)
            );

            FilterDefinition<TDocumentWithHistory>? Between<T>(
                T? value,
                Expression<Func<TDocumentWithHistory, T?>> fromField,
                Expression<Func<TDocumentWithHistory, T?>> toField
            )
                where T : struct
                => value != null
                    ? builder.And(
                        builder.Lte(fromField, value),
                        builder.Or(
                            builder.Gt(toField, value),
                            builder.Eq(toField, null)
                        )
                    )
                    : null;
        }
    }
}