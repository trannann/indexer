﻿using System;
using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    public sealed class AssociatedBlockFilter
    {
        public StringFilter? Hash { get; set; }
        public ComparableFilter<int>? Level { get; set; }
        public ComparableFilter<DateTime>? Timestamp { get; set; }

        public FilterDefinition<TDocument>? ToDbFilter<TDocument>(FilterDefinitionBuilder<TDocument> builder)
            where TDocument : IDocumentWithBlock
            => builder.AndNullable(
                Hash?.ToDbFilter(builder, d => d.Block.Hash),
                Level?.ToDbFilter(builder, d => d.Block.Level),
                Timestamp?.ToDbFilter(builder, d => d.Block.Timestamp)
            );
    }
}