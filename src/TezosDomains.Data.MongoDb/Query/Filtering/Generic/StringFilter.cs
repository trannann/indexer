﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using TezosDomains.Data.MongoDb.Helpers;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    public sealed class StringFilter : EqualityFilter<string?>
    {
        public string? StartsWith { get; set; }
        public string? EndsWith { get; set; }
        public string? Like { get; set; }

        public override FilterDefinition<TDocument>? ToDbFilter<TDocument>(
            FilterDefinitionBuilder<TDocument> builder,
            Expression<Func<TDocument, string?>> field
        )
            => ToDbFilter(builder, new ExpressionFieldDefinition<TDocument, string?>(field));

        public override FilterDefinition<TDocument>? ToDbFilter<TDocument>(
            FilterDefinitionBuilder<TDocument> builder,
            FieldDefinition<TDocument, string?> field
        )
        {
            return builder.AndNullable(
                base.ToDbFilter(builder, field),
                RegexFilter(StartsWith, x => "^" + x),
                RegexFilter(EndsWith, x => x + "$"),
                RegexFilter(Like, x => x)
            );

            FilterDefinition<TDocument>? RegexFilter(string? value, Func<string, string> getPattern)
                => value != null
                    ? builder.Regex(
                        field,
                        getPattern(Regex.Escape(value))
                    )
                    : null;
        }

        private static readonly IReadOnlyList<PropertyInfo> Properties = typeof(StringFilter).GetProperties();

        public StringFilter ToLowerInvariant() // Lowers inherited properties too
        {
            var clone = new StringFilter();
            foreach (var property in Properties)
            {
                var value = property.GetValue(this);
                property.SetValue(clone, value is string str ? str.ToLowerInvariant() : value);
            }

            return clone;
        }
    }
}