﻿using MongoDB.Driver;
using System;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    public interface IRecordFilter<TRecord> : IDocumentWithHistoryFilter<TRecord>
        where TRecord : class, IRecord
    {
        RecordValidity Validity { get; }
        ComparableFilter<DateTime>? ExpiresAtUtc { get; }
    }

    public abstract class RecordFilter<TRecord, TFilter> : DocumentWithHistoryFilter<TRecord, TFilter>, IRecordFilter<TRecord>
        where TRecord : class, IRecord
        where TFilter : RecordFilter<TRecord, TFilter>
    {
        public RecordValidity Validity { get; set; }
        public ComparableFilter<DateTime>? ExpiresAtUtc { get; set; }

        protected override FilterDefinition<TRecord>? ToDbFilter(FilterDefinitionBuilder<TRecord> builder)
            => builder.AndNullable(
                base.ToDbFilter(builder),
                Validity.ToDbFilter(builder),
                ExpiresAtUtc?.ToDbFilter(builder, d => d.ExpiresAtUtc)
            );
    }
}