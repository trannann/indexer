﻿using System;
using System.Linq.Expressions;
using MongoDB.Driver;
using TezosDomains.Data.MongoDb.Helpers;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    public class ComparableFilter<T> : EqualityFilter<T?>
        where T : struct
    {
        public T? LessThan { get; set; }
        public T? LessThanOrEqualTo { get; set; }
        public T? GreaterThan { get; set; }
        public T? GreaterThanOrEqualTo { get; set; }

        public override FilterDefinition<TDocument>? ToDbFilter<TDocument>(
            FilterDefinitionBuilder<TDocument> builder,
            Expression<Func<TDocument, T?>> field
        )
            => builder.AndNullable(
                base.ToDbFilter(builder, field),
                Filter(field, builder.Lt, LessThan),
                Filter(field, builder.Lte, LessThanOrEqualTo),
                Filter(field, builder.Gt, GreaterThan),
                Filter(field, builder.Gte, GreaterThanOrEqualTo)
            );
    }
}