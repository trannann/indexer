﻿using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    public sealed class SingleDocumentWithHistoryFilter<TDocument> : DocumentWithHistoryFilter<TDocument, SingleDocumentWithHistoryFilter<TDocument>>
        where TDocument : class, IDocumentWithHistory
    {
        public SingleDocumentWithHistoryFilter(string id, BlockHistoryFilter? atBlock)
        {
            Id = id;
            AtBlock = atBlock;
        }

        public string Id { get; set; }

        protected override FilterDefinition<TDocument>? ToDbFilter(FilterDefinitionBuilder<TDocument> builder)
            => builder.AndNullable(
                base.ToDbFilter(builder),
                builder.Eq(DocumentHelper<TDocument>.IdExpression, Id)
            );
    }
}