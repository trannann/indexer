﻿using System;
using System.Collections.Generic;
using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    public interface INestedFilter<TFilter>
    {
        List<TFilter>? Or { get; }
        List<TFilter>? And { get; }
    }

    public abstract class NestedFilter<TDocument, TFilter> : IDocumentFilter<TDocument>, INestedFilter<TFilter>
        where TDocument : class, IDocument
        where TFilter : NestedFilter<TDocument, TFilter>
    {
        public List<TFilter>? Or { get; set; }
        public List<TFilter>? And { get; set; }

        public FilterDefinition<TDocument> ToDbFilter()
        {
            var builder = Builders<TDocument>.Filter;
            return ToDbFilter(builder) ?? builder.Empty;
        }

        protected virtual FilterDefinition<TDocument>? ToDbFilter(FilterDefinitionBuilder<TDocument> builder)
            => builder.AndNullable(
                Filter(Or, builder.Or),
                Filter(And, builder.And)
            );

        private static FilterDefinition<TDocument>? Filter(
            List<TFilter>? children,
            Func<IEnumerable<FilterDefinition<TDocument>>, FilterDefinition<TDocument>> join
        )
            => children?.Count > 0 ? join(children.ConvertAll(f => f.ToDbFilter())) : null;
    }
}