﻿using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    public interface IDocumentWithHistoryFilter<TDocumentWithHistory> : IDocumentFilter<TDocumentWithHistory>
        where TDocumentWithHistory : class, IDocumentWithHistory
    {
        BlockHistoryFilter? AtBlock { get; set; }
    }

    public abstract class DocumentWithHistoryFilter<TDocumentWithHistory, TFilter> : NestedFilter<TDocumentWithHistory, TFilter>,
        IDocumentWithHistoryFilter<TDocumentWithHistory>
        where TDocumentWithHistory : class, IDocumentWithHistory
        where TFilter : DocumentWithHistoryFilter<TDocumentWithHistory, TFilter>
    {
        public BlockHistoryFilter? AtBlock { get; set; }

        protected override FilterDefinition<TDocumentWithHistory>? ToDbFilter(FilterDefinitionBuilder<TDocumentWithHistory> builder)
            => builder.AndNullable(
                base.ToDbFilter(builder),
                AtBlock?.ToDbFilter(builder)
            );
    }
}