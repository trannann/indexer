using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MongoDB.Driver;
using TezosDomains.Data.MongoDb.Helpers;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    public abstract class EqualityFilter<TField>
    {
        public bool? IsNull { get; set; }
        public TField EqualTo { get; set; } = default!;
        public TField NotEqualTo { get; set; } = default!;
        public List<TField>? In { get; set; }
        public List<TField>? NotIn { get; set; }

        public virtual FilterDefinition<TDocument>? ToDbFilter<TDocument>(
            FilterDefinitionBuilder<TDocument> builder,
            Expression<Func<TDocument, TField>> field
        )
            => ToDbFilter(builder, new ExpressionFieldDefinition<TDocument, TField>(field));

        public virtual FilterDefinition<TDocument>? ToDbFilter<TDocument>(
            FilterDefinitionBuilder<TDocument> builder,
            FieldDefinition<TDocument, TField> field
        )
            => builder.AndNullable(
                IsNull switch
                {
                    null => null,
                    true => builder.Eq(field, default!),
                    false => builder.Ne(field, default!),
                },
                Filter(field, builder.Eq, EqualTo),
                Filter(field, builder.Ne, NotEqualTo),
                Filter(field, builder.In, In),
                Filter(field, builder.Nin, NotIn)
            );


        protected static FilterDefinition<TDocument>? Filter<TDocument, TValue>(
            Expression<Func<TDocument, TField>> field,
            Func<FieldDefinition<TDocument, TField>, TValue, FilterDefinition<TDocument>> @operator,
            TValue value
        )
            => Filter(new ExpressionFieldDefinition<TDocument, TField>(field), @operator, value);

        private static FilterDefinition<TDocument>? Filter<TDocument, TValue>(
            FieldDefinition<TDocument, TField> field,
            Func<FieldDefinition<TDocument, TField>, TValue, FilterDefinition<TDocument>> @operator,
            TValue value
        )
            => value != null ? @operator(field, value) : null;
    }
}