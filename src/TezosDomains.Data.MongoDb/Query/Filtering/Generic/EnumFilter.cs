﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MongoDB.Driver;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    public interface IEnumFilter<TEnum>
        where TEnum : Enum
    {
        public List<TEnum>? In { get; set; }
    }

    public sealed class EnumFilter<TEnum> : IEnumFilter<TEnum>
        where TEnum : Enum
    {
        public FilterDefinition<TDocument>? ToDbFilter<TDocument>(
            FilterDefinitionBuilder<TDocument> builder,
            Expression<Func<TDocument, TEnum>> field
        )
            => In != null ? builder.In(field, In) : null;

        public List<TEnum>? In { get; set; }
    }

    public sealed class EnumAsStringFilter<TEnum> : IEnumFilter<TEnum>
        where TEnum : Enum
    {
        public FilterDefinition<TDocument>? ToDbFilter<TDocument>(
            FilterDefinitionBuilder<TDocument> builder,
            FieldDefinition<TDocument, string> field
        )
            => In != null
                ? builder.In(field, In.Select(i => i.ToString()))
                : null; //hack: List<Enum> is not serialized as string https://github.com/mongodb/mongo-csharp-driver/pull/305#issuecomment-731475503

        public List<TEnum>? In { get; set; }
    }
}