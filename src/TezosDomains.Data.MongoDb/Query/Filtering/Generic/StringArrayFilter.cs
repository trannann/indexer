﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MongoDB.Driver;

namespace TezosDomains.Data.MongoDb.Query.Filtering.Generic
{
    public sealed class StringArrayFilter
    {
        public string? Include { get; set; }

        public FilterDefinition<TDocument>? ToDbFilter<TDocument>(
            FilterDefinitionBuilder<TDocument> builder,
            Expression<Func<TDocument, IEnumerable<string>>> field
        )
            => Include != null ? builder.AnyEq(field, Include) : null;

        public FilterDefinition<TDocument>? ToDbFilter<TDocument, TArrayElement>(
            FilterDefinitionBuilder<TDocument> builder,
            Expression<Func<TDocument, IEnumerable<TArrayElement>>> array,
            Expression<Func<TArrayElement, string>> field
        )
            => Include != null ? builder.ElemMatch(array, Builders<TArrayElement>.Filter.Eq(field, Include)) : null;

        public StringArrayFilter ToLowerInvariant()
            => new StringArrayFilter { Include = Include?.ToLowerInvariant() };
    }
}