﻿using System;
using System.Linq.Expressions;

namespace TezosDomains.Data.MongoDb.Query.Abstract
{
    public sealed class DocumentOrderField<TDocument>
    {
        public static DocumentOrderField<TDocument> Create(
            string displayName,
            string description,
            Expression<Func<TDocument, object>> uniqueExpression
        )
            => new DocumentOrderField<TDocument>(displayName, description, uniqueExpression!);

        public static DocumentOrderField<TDocument> Create(
            string displayName,
            string description,
            Expression<Func<TDocument, object>> primaryExpression,
            Expression<Func<TDocument, object>> secondaryUniqueExpression
        )
            => new DocumentOrderField<TDocument>(displayName, description, primaryExpression!, secondaryUniqueExpression!);

        public static DocumentOrderField<TDocument> CreateNullable(
            string displayName,
            string description,
            Expression<Func<TDocument, object?>> primaryExpression,
            Expression<Func<TDocument, object>> secondaryUniqueExpression
        )
            => new DocumentOrderField<TDocument>(displayName, description, primaryExpression!, secondaryUniqueExpression!, isPrimaryNullable: true);

        private DocumentOrderField(
            string displayName,
            string description,
            Expression<Func<TDocument, object?>> primaryExpression,
            Expression<Func<TDocument, object?>>? secondaryExpression = null,
            bool isPrimaryNullable = false
        )
        {
            DisplayName = displayName;
            Description = description;
            PrimaryExpression = primaryExpression;
            PrimaryAccessor = primaryExpression.Compile();
            SecondaryExpression = secondaryExpression;
            SecondaryAccessor = secondaryExpression?.Compile();
            IsPrimaryNullable = isPrimaryNullable;
        }

        public string DisplayName { get; }
        public string Description { get; }
        public Expression<Func<TDocument, object?>> PrimaryExpression { get; }
        public Func<TDocument, object?> PrimaryAccessor { get; }
        public bool IsPrimaryNullable { get; }
        public Expression<Func<TDocument, object?>>? SecondaryExpression { get; }
        public Func<TDocument, object?>? SecondaryAccessor { get; }
    }
}