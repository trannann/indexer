﻿using TezosDomains.Data.Models;

namespace TezosDomains.Data.MongoDb.Query.Abstract
{
    public sealed class DocumentQuery<TDocument>
        where TDocument : class, IDocument
    {
        public DocumentQuery(IDocumentFilter<TDocument> filter, DocumentOrder<TDocument> order, PagingIndex? afterIndex, int takeCount)
        {
            Filter = filter;
            Order = order;
            AfterIndex = afterIndex;
            TakeCount = takeCount;
        }

        public IDocumentFilter<TDocument> Filter { get; }
        public DocumentOrder<TDocument> Order { get; }
        public PagingIndex? AfterIndex { get; }
        public int TakeCount { get; }
    }
}