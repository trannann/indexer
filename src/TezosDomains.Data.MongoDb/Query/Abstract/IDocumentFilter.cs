﻿using MongoDB.Driver;
using TezosDomains.Data.Models;

namespace TezosDomains.Data.MongoDb.Query.Abstract
{
    public interface IDocumentFilter<TDocument>
        where TDocument : class, IDocument
    {
        FilterDefinition<TDocument> ToDbFilter();
    }
}