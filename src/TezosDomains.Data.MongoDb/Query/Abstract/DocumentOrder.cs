﻿using MongoDB.Driver;
using System;
using System.Linq.Expressions;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;

namespace TezosDomains.Data.MongoDb.Query.Abstract
{
    public sealed class DocumentOrder<TDocument>
    {
        /// <summary>Overload needed for GraphQL to be able to bind it.</summary>
        public DocumentOrder(DocumentOrderField<TDocument> field)
            : this(field, default)
        {
        }

        public DocumentOrder(DocumentOrderField<TDocument> field, OrderDirection direction)
        {
            Field = field ?? throw new ArgumentNullException(nameof(field));
            Direction = direction;
        }

        public OrderDirection Direction { get; }
        public DocumentOrderField<TDocument> Field { get; }

        public SortDefinition<TSource> ToDbSort<TSource>()
        {
            var builder = Builders<TSource>.Sort;
            var dbSort = BuildSort(Field.PrimaryExpression, Direction);

            if (Field.SecondaryExpression == null)
                return dbSort;

            var secondarySort = BuildSort(Field.SecondaryExpression, OrderDirection.Asc);
            return builder.Combine(dbSort, secondarySort);

            SortDefinition<TSource> BuildSort(Expression<Func<TDocument, object?>> fieldExpression, OrderDirection direction)
            {
                var field = new StringFieldDefinition<TSource, object?>(fieldExpression.GetFieldName());
                return direction switch
                {
                    OrderDirection.Asc => builder.Ascending(field),
                    OrderDirection.Desc => builder.Descending(field),
                    _ => throw direction.CreateInvalidError(),
                };
            }
        }
    }
}