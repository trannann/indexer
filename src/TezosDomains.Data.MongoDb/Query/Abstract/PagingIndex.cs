﻿using System;
using System.Linq.Expressions;
using MongoDB.Driver;
using TezosDomains.Data.Models;

namespace TezosDomains.Data.MongoDb.Query.Abstract
{
    public sealed class PagingIndex
    {
        public PagingIndex(string orderFieldName, object? orderFieldValue, object? secondaryValue)
        {
            OrderFieldName = orderFieldName;
            OrderFieldValue = orderFieldValue;
            SecondaryValue = secondaryValue;
        }

        public string OrderFieldName { get; }
        public object? OrderFieldValue { get; }
        public object? SecondaryValue { get; }

        public FilterDefinition<TDocument>? ToDbFilter<TDocument>(DocumentOrder<TDocument> order)
            where TDocument : class, IDocument
        {
            var builder = Builders<TDocument>.Filter;
            var nextOrders = GetNext(order.Field.PrimaryExpression, OrderFieldValue);

            // Simple case -> just get next by unique field
            if (order.Field.SecondaryExpression == null)
                return nextOrders;

            var sameOrderNextSecondaries = builder.And(
                builder.Eq(order.Field.PrimaryExpression, OrderFieldValue),
                GetNext(order.Field.SecondaryExpression, SecondaryValue)
            );

            // ID, Name - sorted ASC by Name + ID
            // 1, a
            // 3, a
            // 2, b
            // after (1, b) => (Name == a && ID > 1) || Name > a
            if (!order.Field.IsPrimaryNullable)
                return builder.Or(sameOrderNextSecondaries, nextOrders);

            return (order.Direction, OrderFieldValue) switch
            {
                // ID, Name - sorted ASC by Name + ID
                // 1, null
                // 5, null
                // 3, a
                // 4, a
                // 2, b

                // after (1, null) => (Name == null && ID > 1) || Name != a
                (OrderDirection.Asc, null) => builder.Or(sameOrderNextSecondaries, builder.Ne(order.Field.PrimaryExpression, null)),

                // after (3, a) => (Name == a && ID > 3) || Name > a
                (OrderDirection.Asc, _) => builder.Or(sameOrderNextSecondaries, nextOrders),


                // ID, Name - sorted DESC by Name + ID
                // 4, b
                // 2, b
                // 3, a
                // 5, null
                // 1, null

                // after (5, null) => Name == null && ID < 5
                (OrderDirection.Desc, null) => sameOrderNextSecondaries,

                // after (4, b) => (Name == b && ID < 4) || Name < b || Name == null
                (OrderDirection.Desc, _) => builder.Or(
                    sameOrderNextSecondaries,
                    nextOrders,
                    builder.Eq(order.Field.PrimaryExpression, null)
                ),

                _ => throw order.Direction.CreateInvalidError(),
            };

            FilterDefinition<TDocument> GetNext(Expression<Func<TDocument, object?>> field, object? value)
                => order.Direction switch
                {
                    OrderDirection.Asc => builder.Gt(field, value),
                    OrderDirection.Desc => builder.Lt(field, value),
                    _ => throw order.Direction.CreateInvalidError(),
                };
        }
    }
}