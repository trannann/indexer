﻿using System.ComponentModel;
using TezosDomains.Data.Models;

namespace TezosDomains.Data.MongoDb.Query.Abstract
{
    [Description("Possible directions in which to order a list of items when provided an `orderBy` argument.")]
    public enum OrderDirection
    {
        [Description("Specifies an ascending order for a given `orderBy` argument.")]
        Asc,

        [Description("Specifies a descending order for a given `orderBy` argument.")]
        Desc,
    }

    public static class OrderDirectionExtensions
    {
        public static OrderDirection Reverse(this OrderDirection direction)
            => direction switch
            {
                OrderDirection.Asc => OrderDirection.Desc,
                OrderDirection.Desc => OrderDirection.Asc,
                _ => throw direction.CreateInvalidError(),
            };
    }
}