﻿using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;

namespace TezosDomains.Data.MongoDb.Query
{
    public interface IBlockQueryService
    {
        Task<int?> GetStartLevelOrNullAsync(CancellationToken cancellationToken);
        Task<int?> GetLatestLevelOrNullAsync(CancellationToken cancellationToken);
        Task<Block?> FindAsync(string hash, CancellationToken cancellationToken);
        Task<Block?> FindAsync(int level, CancellationToken cancellationToken);
        Task<Block?> GetLatestOrNullAsync(CancellationToken cancellationToken);
    }
}