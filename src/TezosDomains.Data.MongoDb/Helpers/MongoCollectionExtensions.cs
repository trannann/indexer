﻿using System.Linq;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using TezosDomains.Data.Models;

namespace TezosDomains.Data.MongoDb.Helpers
{
    public static class MongoCollectionExtensions
    {
        public static IFindFluent<TDocument, TDocument> FindById<TDocument>(this IMongoCollection<TDocument> collection, string id)
        {
            var idElementName = BsonClassMap.LookupClassMap(typeof(TDocument)).IdMemberMap.ElementName;
            return collection.Find(Builders<TDocument>.Filter.Eq(idElementName, id));
        }

        public static IFindFluent<HistoryOf<TDocument>, HistoryOf<TDocument>> FindById<TDocument>(
            this IMongoCollection<HistoryOf<TDocument>> collection,
            string id
        )
        {
            var idElementName = BsonClassMap.LookupClassMap(typeof(TDocument)).IdMemberMap.ElementName;
            return collection.Find(Builders<HistoryOf<TDocument>>.Filter.Eq(idElementName, id));
        }

        public static IFindFluent<HistoryOf<TDocument>, HistoryOf<TDocument>> FindInHistory<TDocument>(
            this IMongoCollection<HistoryOf<TDocument>> collection,
            FilterDefinition<TDocument> filter
        ) =>
            collection.Find(Builders<HistoryOf<TDocument>>.Filter.ElemMatch(w => w.History, filter));

        public static IFindFluent<HistoryOf<TDocument>, HistoryOf<TDocument>> Project<TDocument>(
            this IFindFluent<HistoryOf<TDocument>, HistoryOf<TDocument>> find,
            FilterDefinition<TDocument> filter
        )
        {
            var id = BsonClassMap.LookupClassMap(typeof(TDocument)).IdMemberMap.ElementName;
            return find.Project(
                (ProjectionDefinition<HistoryOf<TDocument>, HistoryOf<TDocument>>)
                Builders<HistoryOf<TDocument>>.Projection.ElemMatch(w => w.History, filter).Exclude(id)
            );
        }

        public static IFindFluent<TDocument, TDocument> Project<TDocument>(this IFindFluent<TDocument, TDocument> find, bool stripUnusedProperties = true)
        {
            if (!stripUnusedProperties)
                return find;

            ProjectionDefinition<TDocument, TDocument> projections = Builders<TDocument>.Projection.Combine(
                BsonClassMap.LookupClassMap(typeof(TDocument))
                    .AllMemberMaps.Select(map => Builders<TDocument>.Projection.Include(new StringFieldDefinition<TDocument>(map.ElementName)))
            );

            return find.Project(projections);
        }
    }
}