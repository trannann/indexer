﻿using MongoDB.Bson.Serialization;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using TezosDomains.Data.Models;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Helpers
{
    public static class DocumentHelper
    {
        public static string GetId<TDocument>(this TDocument record)
            where TDocument : IDocument
            => (string) GetIdMap<TDocument>().Getter(record);

        public static string GetIdDbPropertyName<TDocument>()
            where TDocument : IDocument
            => GetIdMap<TDocument>().ElementName;

        public static Expression<Func<TDocument, string>> GetIdExpression<TDocument>()
            where TDocument : IDocument
        {
            var idMember = GetIdMap<TDocument>().MemberInfo;
            var param = Expression.Parameter(typeof(TDocument));
            return Expression.Lambda<Func<TDocument, string>>(Expression.MakeMemberAccess(param, idMember), param);
        }

        private static BsonMemberMap GetIdMap<TDocument>()
            => BsonClassMap.LookupClassMap(typeof(TDocument)).IdMemberMap;

        public static string GetFieldName<TDocument, TField>(this Expression<Func<TDocument, TField>> fieldExpression)
        {
            var memberExpression =
                fieldExpression.Body as MemberExpression
                ?? (fieldExpression.Body as UnaryExpression)?.Operand as MemberExpression // Value is upcasted
                ?? throw new InvalidOperationException($"Expression must be simple property retrieval but it's {fieldExpression}");

            return string.Join(".", GetFieldNames(memberExpression));

            static IEnumerable<string> GetFieldNames(MemberExpression memberExpr)
            {
                if (memberExpr.Expression is MemberExpression me)
                    foreach (var t in GetFieldNames(me))
                        yield return t;

                //array accessor a=>a[0].b  ==> a.0.b
                if (memberExpr.Expression is MethodCallExpression mce
                    && mce.Method.Name == "get_Item"
                    && mce.Arguments.Count == 1
                    && mce.Arguments[0] is ConstantExpression idx
                    && mce.Object is MemberExpression ome)
                {
                    foreach (var fieldName in GetFieldNames(ome))
                        yield return fieldName;

                    yield return (idx.Value?.ToString().WhiteSpaceToNull()).GuardNotNull("Null or empty string can't be used as an indexer key.");
                }

                yield return memberExpr.Member.Name;
            }
        }
    }

    public static class DocumentHelper<TDocument>
        where TDocument : IDocument
    {
        public static readonly Expression<Func<TDocument, string>> IdExpression = DocumentHelper.GetIdExpression<TDocument>();
        public static readonly Func<TDocument, string> GetId = IdExpression.Compile();
    }
}