﻿using System.Linq;
using MongoDB.Driver;

namespace TezosDomains.Data.MongoDb.Helpers
{
    public static class FilterDefinitionBuilderExtensions
    {
        public static FilterDefinition<TDocument>? AndNullable<TDocument>(
            this FilterDefinitionBuilder<TDocument> builder,
            params FilterDefinition<TDocument>?[] filters
        )
        {
            var nonNullFilters = filters.Where(f => f != null).ToList();
            return nonNullFilters.Count switch
            {
                0 => null,
                1 => nonNullFilters[0],
                _ => builder.And(nonNullFilters),
            };
        }
    }
}