﻿using MongoDB.Driver;
using System;
using System.Linq;
using TezosDomains.Utils;

namespace TezosDomains.Data.MongoDb.Configurations
{
    public class MongoDbConfiguration : IMongoDbConfiguration
    {
        public TimeSpan DistributedLockLifetime { get; set; }

        public string? ConnectionString { get; set; }

        public TimeSpan Timeout { get; set; }

        public string? SdamLogFilename { get; set; }

        string IMongoDbConfiguration.ConnectionString => ConnectionString.GuardNotNull();


        public string Servers => string.Join(",", MongoUrl.Create(ConnectionString.GuardNotNull()).Servers.Select(s => s.ToString()));
        public string Username => MongoUrl.Create(ConnectionString.GuardNotNull()).Username;


        private string? _databaseNameOverride;
        public string Database => _databaseNameOverride ?? MongoUrl.Create(ConnectionString.GuardNotNull()).DatabaseName;

        public void OverrideDatabaseName(Func<string, string> databaseNameModifier)
        {
            _databaseNameOverride = databaseNameModifier(MongoUrl.Create(ConnectionString.GuardNotNull()).DatabaseName);
        }
    }
}