﻿using System;

namespace TezosDomains.Data.MongoDb.Configurations
{
    public interface IMongoDbConfiguration
    {
        string Database { get; }
        string ConnectionString { get; }
        TimeSpan DistributedLockLifetime { get; set; }
        string Servers { get; }
        string Username { get; }
        TimeSpan Timeout { get; }
        string? SdamLogFilename { get; }
    }
}