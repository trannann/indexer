﻿using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.MongoDb.Configurations;
using TezosDomains.Data.MongoDb.DistributedLock;

namespace TezosDomains.Data.MongoDb.Services
{
    public class IndexerDistributedLockService : IAsyncDisposable
    {
        private static readonly TimeSpan LOCK_EXPIRING_THRESHOLD = TimeSpan.FromMilliseconds(500);

        private readonly IMongoCollection<Lock> _locks;
        private readonly ILogger _logger;
        private readonly TimeSpan _distributedLockLifetime;
        private const string Resource = "Indexer";

        private bool _disposed;
        private bool _acquiringStarted;
        private readonly object _lockObject = new object();
        private readonly int _currentProcessId;
        private long _lockExpirationTicks;
        private CancellationTokenSource? _cancellationTokenSource;

        public IndexerDistributedLockService(MongoDbContext dbContext, ILogger<IndexerDistributedLockService> logger, IMongoDbConfiguration configuration)
            : this(dbContext.Get<Lock>(), logger, configuration.DistributedLockLifetime)
        {
        }

        public IndexerDistributedLockService(IMongoCollection<Lock> locks, ILogger<IndexerDistributedLockService> logger, TimeSpan distributedLockLifetime)
        {
            _locks = locks;
            _logger = logger;
            _distributedLockLifetime = distributedLockLifetime;


            _disposed = false;
            _acquiringStarted = false;
            using var currentProcess = Process.GetCurrentProcess();
            _currentProcessId = currentProcess.Id;
        }

        public async Task AcquireAsync(CancellationTokenSource cancellationTokenSource)
        {
            _logger.LogInformation("Acquiring exclusive write access with indefinite timeout");

            lock (_lockObject)
            {
                if (_acquiringStarted)
                    throw new InvalidOperationException($"{nameof(AcquireAsync)} was already called. It is possible to call it only once.");

                _acquiringStarted = true;
            }

            _cancellationTokenSource = cancellationTokenSource;
            var cancellationToken = cancellationTokenSource.Token;

            // If result is null, then it means we acquired the lock
            var isLockAcquired = false;
            while (!isLockAcquired)
            {
                cancellationToken.ThrowIfCancellationRequested();
                await CleanupAsync(cancellationToken);

                // Acquire the lock if it does not exist - Notice: ReturnDocument.Before
                var filter = Builders<Lock>.Filter.Eq(_ => _.Resource, Resource);
                var lockExpirationUtc = DateTime.UtcNow.Add(_distributedLockLifetime);
                var update = Builders<Lock>.Update
                    .SetOnInsert(_ => _.ExpireAtUtc, lockExpirationUtc)
                    .SetOnInsert(_ => _.MachineName, Environment.MachineName)
                    .SetOnInsert(_ => _.ProcessId, _currentProcessId);
                var options = new FindOneAndUpdateOptions<Lock>
                {
                    IsUpsert = true,
                    ReturnDocument = ReturnDocument.Before
                };

                try
                {
                    var result = await _locks.FindOneAndUpdateAsync(filter, update, options, cancellationToken);

                    // If result is null, it means we acquired the lock
                    if (result == null)
                    {
                        _logger.LogInformation("Exclusive write access acquired.");
                        isLockAcquired = true;
                        Interlocked.Exchange(ref _lockExpirationTicks, lockExpirationUtc.Ticks);
                    }
                }
                catch (MongoCommandException ex)
                {
                    // this can occur if two processes attempt to acquire a lock on the same resource simultaneously.
                    // unfortunately there doesn't appear to be a more specific exception type to catch.
                    _logger.LogWarning(ex, "Error while acquiring an exclusive write access");
                }

                if (!isLockAcquired)
                    await Task.Delay(_distributedLockLifetime / 4, cancellationToken);
            }

            _ = Task.Run(() => StartHeartBeat(cancellationToken), cancellationToken);
        }

        public bool IsLockExpiring => Interlocked.Read(ref _lockExpirationTicks) < DateTime.UtcNow.Add(LOCK_EXPIRING_THRESHOLD).Ticks;

        public void EnsureLockActive()
        {
            if (IsLockExpiring)
                throw new MongoDistributedLockException("Distributed lock expired");
        }


        private async Task ReleaseAsync()
        {
            _logger.LogInformation("Release exclusive write access (if acquired)");

            // Remove resource lock
            await _locks.DeleteOneAsync(l => l.Resource == Resource && l.ProcessId == _currentProcessId && l.MachineName == Environment.MachineName);
        }


        private async Task CleanupAsync(CancellationToken cancellationToken = default)
        {
            try
            {
                // Delete expired locks
                await _locks.DeleteOneAsync(
                    Builders<Lock>.Filter.Eq(_ => _.Resource, Resource)
                    & Builders<Lock>.Filter.Lt(_ => _.ExpireAtUtc, DateTime.UtcNow),
                    cancellationToken
                );
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to clean up an expired exclusive lock.", Resource);
            }
        }

        /// <summary>
        /// Starts database heartbeat
        /// </summary>
        /// <param name="cancellationToken"></param>
        private async Task StartHeartBeat(CancellationToken cancellationToken)
        {
            var timerInterval = _distributedLockLifetime / 5;
            while (!cancellationToken.IsCancellationRequested)
            {
                await Task.Delay(timerInterval, cancellationToken);

                if (IsLockExpiring)
                {
                    _logger.LogWarning("Unable to extend lock in time for heartbeat {timerInterval}. Cancelling");
                    _cancellationTokenSource?.Cancel();
                }

                if (!cancellationToken.IsCancellationRequested)
                {
                    try
                    {
                        var filterBuilder = Builders<Lock>.Filter;
                        var filter = filterBuilder.And(
                            filterBuilder.Eq(_ => _.Resource, Resource),
                            filterBuilder.Eq(_ => _.MachineName, Environment.MachineName),
                            filterBuilder.Eq(_ => _.ProcessId, _currentProcessId)
                        );
                        var lockExpirationUtc = DateTime.UtcNow.Add(_distributedLockLifetime);
                        var update = Builders<Lock>.Update.Set(_ => _.ExpireAtUtc, lockExpirationUtc);
                        var lockData = await _locks.FindOneAndUpdateAsync(filter, update, cancellationToken: cancellationToken);
                        if (lockData != null)
                        {
                            Interlocked.Exchange(ref _lockExpirationTicks, lockExpirationUtc.Ticks);
                            _logger.LogDebug(
                                "Exclusive write access - Heartbeat extended lock on {resource} to {ExpireAtUtc} ({@lock})",
                                Resource,
                                lockExpirationUtc,
                                lockData
                            );
                        }
                        else
                        {
                            _logger.LogError("Exclusive write access - Unable to update heartbeat. No update happened.");
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, "Exclusive write access - Unable to update heartbeat.", Resource);
                    }
                }
            }
        }

        public async ValueTask DisposeAsync()
        {
            _logger.LogDebug("Dispose started");

            if (_disposed)
            {
                _logger.LogDebug("Dispose finished... Already disposed");
                return;
            }

            _disposed = true;

            try
            {
                await ReleaseAsync();
                _logger.LogDebug("Dispose finished... Lock released");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exclusive write access could not be released.");
            }
        }
    }
}