﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Data.MongoDb.Services
{
    public class DocumentQueryService<TDocument> : IQueryService<TDocument>
        where TDocument : class, IDocument
    {
        private readonly bool _skipUnusedProperties = true;
        public IMongoCollection<TDocument> Collection { get; }

        public DocumentQueryService(MongoDbContext dbContext)
        {
            Collection = dbContext.Get<TDocument>();
        }

        public DocumentQueryService(MongoDbContext dbContext, bool skipUnusedProperties) : this(dbContext)
        {
            _skipUnusedProperties = skipUnusedProperties;
        }

        public virtual Task<TDocument?> FindSingleAsync(IDocumentFilter<TDocument> documentFilter, CancellationToken cancellationToken)
        {
            var dbFilter = documentFilter.ToDbFilter();
            return Collection
                .Find(dbFilter)
                .Project(_skipUnusedProperties)
                .SingleOrDefaultAsync(cancellationToken)
                .AsNullableResult();
        }

        public virtual Task<List<TDocument>> FindAllAsync(DocumentQuery<TDocument> query, CancellationToken cancellationToken)
        {
            var dbFilter = BuildFilter(query);
            var dbSort = query.Order.ToDbSort<TDocument>();
            return Collection
                .Find(dbFilter)
                .Project(_skipUnusedProperties)
                .Sort(dbSort)
                .Limit(query.TakeCount)
                .ToListAsync(cancellationToken);
        }

        public virtual Task<long> CountAsync(IDocumentFilter<TDocument> documentFilter, CancellationToken cancellationToken)
        {
            var dbFilter = documentFilter.ToDbFilter();
            return Collection
                .Find(dbFilter)
                .CountDocumentsAsync(cancellationToken);
        }

        protected FilterDefinition<TDocument> BuildFilter(DocumentQuery<TDocument> query)
        {
            var dbFilter = query.Filter.ToDbFilter();
            var pagingFilter = query.AfterIndex?.ToDbFilter(query.Order);
            return pagingFilter != null ? Builders<TDocument>.Filter.And(dbFilter, pagingFilter) : dbFilter;
        }
    }
}