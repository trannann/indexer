﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Data.MongoDb.Services
{
    public class DocumentWithHistoryQueryService<TRecord> : DocumentQueryService<TRecord>
        where TRecord : class, IDocumentWithHistory
    {
        private readonly IMongoCollection<HistoryOf<TRecord>> _collectionWithHistory;

        public DocumentWithHistoryQueryService(MongoDbContext dbContext) : base(dbContext)
        {
            _collectionWithHistory = dbContext.GetHistory<TRecord>();
        }

        public override async Task<TRecord?> FindSingleAsync(IDocumentFilter<TRecord> documentFilter, CancellationToken cancellationToken)
        {
            if (!IsHistoricQuery(documentFilter))
                return await base.FindSingleAsync(documentFilter, cancellationToken);

            var dbFilter = documentFilter.ToDbFilter();
            var data = await _collectionWithHistory
                .FindInHistory(dbFilter)
                .Project(dbFilter)
                .FirstOrDefaultAsync(cancellationToken);

            return data?.History.FirstOrDefault();
        }

        public override async Task<List<TRecord>> FindAllAsync(DocumentQuery<TRecord> query, CancellationToken cancellationToken)
        {
            if (!IsHistoricQuery(query.Filter))
                return await base.FindAllAsync(query, cancellationToken);

            var dbFilter = BuildFilter(query);
            var dbSort = query.Order.ToDbSort<HistoryOf<TRecord>>();
            var data = await _collectionWithHistory
                .FindInHistory(dbFilter)
                .Project(dbFilter)
                .Sort(dbSort)
                .Limit(query.TakeCount)
                .ToListAsync(cancellationToken);

            return data.SelectMany(d => d.History).ToList();
        }

        public override Task<long> CountAsync(IDocumentFilter<TRecord> documentFilter, CancellationToken cancellationToken)
        {
            if (!IsHistoricQuery(documentFilter))
                return base.CountAsync(documentFilter, cancellationToken);

            var dbFilter = documentFilter.ToDbFilter();
            return _collectionWithHistory
                .FindInHistory(dbFilter)
                .CountDocumentsAsync(cancellationToken);
        }

        private static bool IsHistoricQuery(IDocumentFilter<TRecord> documentFilter)
            => (documentFilter as IDocumentWithHistoryFilter<TRecord>)?.AtBlock?.IsHistoricQuery == true;
    }
}