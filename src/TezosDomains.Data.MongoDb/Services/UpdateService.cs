﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver;
using TezosDomains.Data.Command;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Data.MongoDb.Services.Update;

namespace TezosDomains.Data.MongoDb.Services
{
    public class UpdateService : IUpdateService
    {
        private readonly MongoDbContext _dbContext;
        private readonly DomainUpdateService _domainService;
        private readonly ReverseRecordUpdateService _reverseRecordService;
        private readonly AuctionUpdateService _auctionService;
        private readonly BidderBalancesUpdateService _bidderBalancesService;
        private readonly EventProcessor _eventProcessor;

        public UpdateService(
            MongoDbContext dbContext,
            DomainUpdateService domainService,
            ReverseRecordUpdateService reverseRecordService,
            AuctionUpdateService auctionService,
            BidderBalancesUpdateService bidderBalancesService,
            EventProcessor eventProcessor
        )
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _domainService = domainService;
            _reverseRecordService = reverseRecordService;
            _auctionService = auctionService;
            _bidderBalancesService = bidderBalancesService;
            _eventProcessor = eventProcessor;
        }

        private async Task ExecuteInTransaction(Func<IClientSessionHandle, Task> actions, CancellationToken cancellationToken)
        {
            using var session = await _dbContext.Database.Client.StartSessionAsync(null, cancellationToken);
            session.StartTransaction(
                new TransactionOptions(
                    readPreference: ReadPreference.Primary,
                    readConcern: ReadConcern.Local,
                    writeConcern: WriteConcern.WMajority
                )
            );

            await actions(session);

            await session.CommitTransactionAsync(cancellationToken);
        }

        public async Task StoreUpdatesAsync(Block block, IBlockUpdates updates, Block? previousBlock, CancellationToken cancellationToken)
        {
            var blockSlim = block.ToSlim();
            await ValidateLatestStoredBlockAsync(block.Level - 1, cancellationToken);
            var processedEvents = await _eventProcessor.GeneratedAndUpdateEventsAsync(blockSlim, updates, previousBlock?.ToSlim(), cancellationToken);

            await ExecuteInTransaction(
                async session =>
                {
                    await _domainService.StoreUpdatesAsync(blockSlim, updates, session, cancellationToken);

                    await _reverseRecordService.StoreUpdatesAsync(blockSlim, updates, session, cancellationToken);

                    await _auctionService.StoreUpdatesAsync(blockSlim, updates, session, cancellationToken);

                    await _bidderBalancesService.StoreUpdatesAsync(blockSlim, updates, session, cancellationToken);

                    await InsertEventsAsync(processedEvents, cancellationToken, session);

                    await InsertBlockAsync(block, session, cancellationToken);
                },
                cancellationToken
            );
        }

        private async Task InsertEventsAsync(
            IReadOnlyList<Event> events,
            CancellationToken cancellationToken,
            IClientSessionHandle session
        )
        {
            if (events.Count > 0)
            {
                var insertModels = events.Select(e => new InsertOneModel<Event>(e));
                await _dbContext.Get<Event>().BulkWriteAsync(session, insertModels, new BulkWriteOptions() { IsOrdered = false }, cancellationToken);
            }
        }

        public async Task RollbackAsync(int level, CancellationToken cancellationToken)
        {
            await ValidateLatestStoredBlockAsync(level, cancellationToken);

            await ExecuteInTransaction(
                async session =>
                {
                    await _domainService.RollbackAsync(level, session, cancellationToken);
                    await _reverseRecordService.RollbackAsync(level, session, cancellationToken);
                    await _auctionService.RollbackAsync(level, session, cancellationToken);
                    await _dbContext.Get<Event>()
                        .DeleteManyAsync(session, Builders<Event>.Filter.Eq(b => b.Block.Level, level), options: null, cancellationToken);
                    await _dbContext.Blocks.DeleteOneAsync(session, Builders<Block>.Filter.Eq(b => b.Level, level), options: null, cancellationToken);
                },
                cancellationToken
            );
        }

        private Task InsertBlockAsync(Block block, IClientSessionHandle session, CancellationToken cancellationToken)
        {
            return _dbContext.Blocks.InsertOneAsync(session, block, options: null, cancellationToken);
        }

        private async Task ValidateLatestStoredBlockAsync(int blockLevel, CancellationToken cancellationToken)
        {
            var latestLevelOrNull = await _dbContext.Blocks.Find(FilterDefinition<Block>.Empty)
                .Sort(Builders<Block>.Sort.Descending(b => b.Level))
                .Project(b => (int?) b.Level)
                .Limit(1)
                .FirstOrDefaultAsync(cancellationToken);

            if (latestLevelOrNull.HasValue && latestLevelOrNull != blockLevel)
                throw new InvalidOperationException(
                    $"ValidationError: Unable to store update. Block level:{blockLevel} is not a direct successor to last stored block (level: {latestLevelOrNull})."
                );
        }
    }
}