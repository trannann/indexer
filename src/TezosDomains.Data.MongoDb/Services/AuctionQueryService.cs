﻿using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models.Auctions;

namespace TezosDomains.Data.MongoDb.Services
{
    public interface IAuctionQueryService
    {
        Task<IReadOnlyList<(string DomainName, IReadOnlyList<Bid> Bids)>> GetEndedAsync(
            DateTime? from,
            DateTime to,
            CancellationToken cancellationToken
        );

        Task<IReadOnlyList<(string DomainName, string HighestBidder, decimal HighestBid)>> GetAllInProgressAsync(
            IReadOnlyCollection<string> domainNames,
            DateTime atUtc,
            CancellationToken cancellationToken
        );

        Task<IReadOnlyList<(string DomainName, int RegistrationDurationInDays, decimal WinningPrice)>> GetCanBeSettledAsync(
            IReadOnlyCollection<string> domainNames,
            DateTime atUtc,
            CancellationToken cancellationToken
        );
    }

    public class AuctionQueryService : DocumentWithHistoryQueryService<Auction>, IAuctionQueryService
    {
        public AuctionQueryService(MongoDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<IReadOnlyList<(string DomainName, IReadOnlyList<Bid> Bids)>> GetEndedAsync(
            DateTime? from,
            DateTime to,
            CancellationToken cancellationToken
        )
        {
            if (from == null)
            {
                //there is are no data in db yet (no previous block present in db), therefore we can return empty array
                return new (string DomainName, IReadOnlyList<Bid> Bids)[0];
            }

            var endedAuctions = await Collection.AsQueryable()
                .Where(a => a.EndsAtUtc <= to && a.EndsAtUtc > from.Value)
                .Select(a => new { a.DomainName, a.Bids })
                .ToListAsync(cancellationToken);

            return endedAuctions
                .Select(a => (a.DomainName, a.Bids))
                .ToList();
        }

        public async Task<IReadOnlyList<(string DomainName, int RegistrationDurationInDays, decimal WinningPrice)>> GetCanBeSettledAsync(
            IReadOnlyCollection<string> domainNames,
            DateTime atUtc,
            CancellationToken cancellationToken
        )
        {
            var endedAuctions = await Collection.AsQueryable()
                .Where(a => domainNames.Contains(a.DomainName) && a.EndsAtUtc <= atUtc && a.OwnedUntilUtc > atUtc)
                .Select(a => new { a.DomainName, a.OwnedUntilUtc, a.EndsAtUtc, a.Bids[0].Amount })
                .ToListAsync(cancellationToken);

            return endedAuctions
                .Select(a => (a.DomainName, (a.OwnedUntilUtc - a.EndsAtUtc).Days, a.Amount))
                .ToList();
        }

        public async Task<IReadOnlyList<(string DomainName, string HighestBidder, decimal HighestBid)>> GetAllInProgressAsync(
            IReadOnlyCollection<string> domainNames,
            DateTime atUtc,
            CancellationToken cancellationToken
        )
        {
            if (domainNames.Count == 0)
                return new (string DomainName, string HighestBidder, decimal HighestBid)[0];

            var auctions = await Collection.AsQueryable()
                .Where(a => domainNames.Contains(a.DomainName) && a.EndsAtUtc > atUtc)
                .Select(a => new { a.DomainName, a.Bids[0].Bidder, a.Bids[0].Amount })
                .ToListAsync(cancellationToken);

            return auctions.Select(a => (a.DomainName, HighestBidder: a.Bidder, HighestBid: a.Amount)).ToList();
        }
    }
}