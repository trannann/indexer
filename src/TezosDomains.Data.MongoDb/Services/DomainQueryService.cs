﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver;
using TezosDomains.Data.Models;

namespace TezosDomains.Data.MongoDb.Services
{
    public interface IDomainQueryService
    {
        Task<IReadOnlyList<string>> GetExistingNamesAsync(IReadOnlyCollection<string> domainNames, CancellationToken cancellationToken);
    }

    public class DomainQueryService : DocumentWithHistoryQueryService<Domain>, IDomainQueryService
    {
        public DomainQueryService(MongoDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<IReadOnlyList<string>> GetExistingNamesAsync(IReadOnlyCollection<string> domainNames, CancellationToken cancellationToken)
        {
            var existingDomains = await Collection.Find(Builders<Domain>.Filter.In(d => d.Name, domainNames))
                .Project(d => d.Name)
                .ToListAsync(cancellationToken);
            return existingDomains;
        }
    }
}