﻿using System;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query;

namespace TezosDomains.Data.MongoDb.Services
{
    public class BlockQueryService : IBlockQueryService
    {
        private readonly MongoDbContext _dbContext;

        public BlockQueryService(MongoDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public Task<int?> GetStartLevelOrNullAsync(CancellationToken cancellationToken)
            => GetFirstLevelAsync(Builders<Block>.Sort.Ascending(b => b.Level), Builders<Block>.Projection.Expression(b => (int?) b.Level), cancellationToken);

        public Task<int?> GetLatestLevelOrNullAsync(CancellationToken cancellationToken)
            => GetFirstLevelAsync(Builders<Block>.Sort.Descending(b => b.Level), Builders<Block>.Projection.Expression(b => (int?) b.Level), cancellationToken);

        public Task<Block?> GetLatestOrNullAsync(CancellationToken cancellationToken)
            => GetFirstLevelAsync(Builders<Block>.Sort.Descending(b => b.Level), Builders<Block>.Projection.Expression(b => (Block?) b), cancellationToken);


        private Task<TResult> GetFirstLevelAsync<TResult>(
            SortDefinition<Block> sort,
            ProjectionDefinition<Block, TResult> projection,
            CancellationToken cancellationToken
        )
            => _dbContext.Blocks
                .Find(Builders<Block>.Filter.Empty)
                .Sort(sort)
                .Project(projection)
                .FirstOrDefaultAsync(cancellationToken);

        public Task<Block?> FindAsync(string hash, CancellationToken cancellationToken)
            => FindAsync(b => b.Hash, hash, cancellationToken);

        public Task<Block?> FindAsync(int level, CancellationToken cancellationToken)
            => FindAsync(b => b.Level, level, cancellationToken);

        private Task<Block?> FindAsync<TField>(Expression<Func<Block, TField>> field, TField uniqueValue, CancellationToken cancellationToken)
            => _dbContext.Blocks
                .Find(Builders<Block>.Filter.Eq(field, uniqueValue))
                .SingleOrDefaultAsync(cancellationToken)
                .AsNullableResult();
    }
}