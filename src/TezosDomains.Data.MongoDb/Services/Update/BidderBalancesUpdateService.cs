﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Data.MongoDb.Services.Update.Generic;

namespace TezosDomains.Data.MongoDb.Services.Update
{
    public class BidderBalancesUpdateService : HistorizationService<BidderBalances>
    {
        public BidderBalancesUpdateService(MongoDbContext dbContext, ILogger<BidderBalancesUpdateService> logger) : base(dbContext, logger)
        {
        }

        protected override UpdateDefinition<BidderBalances> CreateDocumentUpdate(BidderBalances update)
        {
            return new UpdateDefinitionBuilder<BidderBalances>()
                    .Set(a => a.Balances, update.Balances)
                ;
        }


        public override async Task StoreUpdatesAsync(BlockSlim block, IBlockUpdates updates, IClientSessionHandle session, CancellationToken cancellationToken)
        {
            if (updates.BidderBalancesUpdates.Count == 0)
                return;

            var processedUpdates = EnsureSingleUpdatePerAddress(block, updates);

            var updateModels = processedUpdates.Select(
                u => new UpdateOneModel<BidderBalances>(
                        Builders<BidderBalances>.Filter.Eq(uu => uu.Address, u.Address),
                        UpdateBalance(u)
                            .Set(a => a.Block, block)
                    )
                    { IsUpsert = true }
            );

            await DbContext.Get<BidderBalances>().BulkWriteAsync(session, updateModels, new BulkWriteOptions() { IsOrdered = false }, cancellationToken);

            await HistorizeDocumentsAsync(processedUpdates.Select(a => a.Address).Distinct().ToArray(), block, session, cancellationToken);
        }

        private static UpdateDefinition<BidderBalances> UpdateBalance(BidderBalanceUpdate update)
        {
            var builder = Builders<BidderBalances>.Update;

            return update.Balance.HasValue
                ? builder.Set(f => f.Balances[update.TldName], update.Balance)
                : builder.Unset(f => f.Balances[update.TldName]);
        }

        private IReadOnlyList<BidderBalanceUpdate> EnsureSingleUpdatePerAddress(BlockSlim block, IBlockUpdates updates)
        {
            var updatesPerAddress = updates.BidderBalancesUpdates.Select((u, i) => (Index: i, Update: u))
                .GroupBy(u => (u.Update.Address, u.Update.TldName), (_, changes) => changes.OrderByDescending(c => c.Index).First().Update)
                .ToArray();

            if (updates.BidderBalancesUpdates.Count != updatesPerAddress.Length)
                Logger.LogWarning("Block {level} {hash} has multiple 'bidder_balances' updates for same address", block.Level, block.Hash);

            return updatesPerAddress;
        }
    }
}