﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Data.MongoDb.Services.Update.Generic;

namespace TezosDomains.Data.MongoDb.Services.Update
{
    public class AuctionUpdateService : HistorizationService<Auction>
    {
        public AuctionUpdateService(MongoDbContext dbContext, ILogger<AuctionUpdateService> logger) : base(dbContext, logger)
        {
        }

        protected override UpdateDefinition<Auction> CreateDocumentUpdate(Auction update)
        {
            return new UpdateDefinitionBuilder<Auction>()
                    .Set(a => a.DomainName, update.DomainName)
                    .Set(a => a.Bids, update.Bids)
                    .Set(a => a.FirstBidAtUtc, update.FirstBidAtUtc)
                    .Set(a => a.EndsAtUtc, update.EndsAtUtc)
                    .Set(a => a.OwnedUntilUtc, update.OwnedUntilUtc)
                    .Set(a => a.BidCount, update.BidCount)
                    .Set(a => a.IsSettled, update.IsSettled)
                ;
        }


        public override async Task StoreUpdatesAsync(BlockSlim block, IBlockUpdates updates, IClientSessionHandle session, CancellationToken cancellationToken)
        {
            if (updates.BidUpdates.Count == 0 && updates.AuctionSettledUpdates.Count == 0)
                return;

            var storedAuctions = await GetActiveAuctions(block, updates, session, cancellationToken);

            //settle updates
            var settleUpdates = GenerateUpdates(
                block,
                storedAuctions,
                updates.AuctionSettledUpdates,
                domainName => domainName,
                (domainName, builder) =>
                    builder.Set(a => a.DomainName, domainName)
                        .Set(a => a.IsSettled, true)
            );

            //bid updates
            var bidUpdates = GenerateUpdates(
                block,
                storedAuctions,
                updates.BidUpdates,
                bid => bid.DomainName,
                (bid, builder) =>
                    builder
                        .SetOnInsert(a => a.DomainName, bid.DomainName)
                        .SetOnInsert(a => a.FirstBidAtUtc, block.Timestamp)
                        .Set(a => a.EndsAtUtc, bid.AuctionEndInUtc)
                        .Set(a => a.OwnedUntilUtc, bid.OwnedUntilUtc)
                        .Inc(a => a.BidCount, 1)
                        .Set(a => a.IsSettled, false)
                        .PushEach(a => a.Bids, new[] { new Bid(block, bid.Bidder, bid.Amount) }, position: 0)
            );

            await DbContext.Get<Auction>()
                .BulkWriteAsync(session, bidUpdates.Values.Concat(settleUpdates.Values), new BulkWriteOptions() { IsOrdered = false }, cancellationToken);

            await HistorizeDocumentsAsync(bidUpdates.Keys.Concat(settleUpdates.Keys).ToArray(), block, session, cancellationToken);
        }

        private static Dictionary<string, UpdateOneModel<Auction>> GenerateUpdates<TUpdate>(
            BlockSlim block,
            Dictionary<string, string> storedAuctions,
            IReadOnlyList<TUpdate> updates,
            Func<TUpdate, string> domainNameAccessor,
            Func<TUpdate, UpdateDefinition<Auction>,
                UpdateDefinition<Auction>> updateBuilder
        ) =>
            updates.Select(
                    update =>
                    {
                        var domainName = domainNameAccessor(update);
                        var id = storedAuctions.GetValueOrDefault(domainName, Auction.GenerateId(domainName, block.Level));
                        var upsert = new UpdateOneModel<Auction>(
                                Builders<Auction>.Filter.Eq(a => a.Id, id),
                                updateBuilder(update, Builders<Auction>.Update.Set(a => a.Block, block))
                            )
                            { IsUpsert = true };
                        return (Id: id, Upsert: upsert);
                    }
                )
                .ToDictionary(a => a.Id, a => a.Upsert);

        private async Task<Dictionary<string, string>> GetActiveAuctions(
            BlockSlim block,
            IBlockUpdates updates,
            IClientSessionHandle session,
            CancellationToken cancellationToken
        )
        {
            var bidDomains = updates.BidUpdates.Select(b => b.DomainName).Distinct().ToArray();
            var storedAuctions = (await DbContext.Get<Auction>()
                    .Find(
                        session,
                        Builders<Auction>.Filter.Or(
                            Builders<Auction>.Filter.And(
                                Builders<Auction>.Filter.In(a => a.DomainName, bidDomains),
                                Builders<Auction>.Filter.Gte(a => a.EndsAtUtc, block.Timestamp)
                            ),
                            Builders<Auction>.Filter.And(
                                Builders<Auction>.Filter.In(a => a.DomainName, updates.AuctionSettledUpdates),
                                Builders<Auction>.Filter.Gte(a => a.OwnedUntilUtc, block.Timestamp)
                            )
                        )
                    )
                    .Project(a => new { a.DomainName, a.Id })
                    .ToListAsync(cancellationToken))
                .ToDictionary(a => a.DomainName, a => a.Id);
            return storedAuctions;
        }
    }
}