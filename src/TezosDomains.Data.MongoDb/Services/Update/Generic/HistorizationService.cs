﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Data.MongoDb.Helpers;

namespace TezosDomains.Data.MongoDb.Services.Update.Generic
{
    public abstract class HistorizationService<TRecord>
        where TRecord : class, IDocumentWithHistory
    {
        protected readonly ILogger Logger;
        protected readonly MongoDbContext DbContext;

        protected HistorizationService(MongoDbContext dbContext, ILogger logger)
        {
            Logger = logger;
            DbContext = dbContext;
        }

        public abstract Task StoreUpdatesAsync(BlockSlim block, IBlockUpdates updates, IClientSessionHandle session, CancellationToken cancellationToken);

        protected abstract UpdateDefinition<TRecord> CreateDocumentUpdate(TRecord update);


        protected async Task HistorizeDocumentsAsync(
            IReadOnlyCollection<string> names,
            BlockSlim block,
            IClientSessionHandle session,
            CancellationToken cancellationToken
        )
        {
            if (names.Count == 0)
                return;

            var idMap = BsonClassMap.LookupClassMap(typeof(TRecord)).IdMemberMap;

            var historizeItems = await DbContext.Get<TRecord>()
                .Find(session, Builders<TRecord>.Filter.In(idMap.ElementName, names))
                .Project()
                .ToListAsync(cancellationToken);


            var updateModels = new List<WriteModel<HistoryOf<TRecord>>>
            {
                new UpdateManyModel<HistoryOf<TRecord>>(
                    Builders<HistoryOf<TRecord>>.Filter.And(
                        Builders<HistoryOf<TRecord>>.Filter.In(idMap.ElementName, names),
                        Builders<HistoryOf<TRecord>>.Filter.SizeGte(d => d.History, 1)
                    ),
                    Builders<HistoryOf<TRecord>>.Update
                        .Set(d => d.History[0].ValidUntilBlockLevel, block.Level)
                        .Set(d => d.History[0].ValidUntilTimestamp, block.Timestamp)
                )
            };
            updateModels.AddRange(
                historizeItems.Select(
                    h => new UpdateOneModel<HistoryOf<TRecord>>(
                        Builders<HistoryOf<TRecord>>.Filter.Eq(idMap.ElementName, idMap.Getter(h)),
                        Builders<HistoryOf<TRecord>>.Update
                            .PushEach(d => d.History, new[] { h }, position: 0)
                    )
                )
            );

            await DbContext.GetHistory<TRecord>().BulkWriteAsync(session, updateModels, new BulkWriteOptions { IsOrdered = false }, cancellationToken);
        }

        public async Task RollbackAsync(int levelToRollback, IClientSessionHandle session, CancellationToken cancellationToken)
        {
            Expression<Func<TRecord, int>> levelAccessor = r => r.Block.Level;
            var previousVersions = await DbContext.GetHistory<TRecord, HistoryWithId>()
                .Aggregate(session) //only aggregate framework support projection with $arrayElemAt
                .Match(Builders<HistoryWithId>.Filter.Eq(levelAccessor.GetFieldName(), levelToRollback))
                .Project(h => new PreviousRecord { Id = h.Id, PreviousVersion = h.History[1] }) // h.History[1] returns null on db if not exists
                .ToListAsync(cancellationToken);

            if (previousVersions.Count == 0)
                return;

            var updateModels = previousVersions
                .Select(
                    o =>
                    {
                        var findById = Builders<TRecord>.Filter.Eq<string?>(DocumentHelper.GetIdDbPropertyName<TRecord>(), o.Id);
                        return o.PreviousVersion == null
                            ? (WriteModel<TRecord>) new DeleteOneModel<TRecord>(findById)
                            : new UpdateOneModel<TRecord>(
                                findById,
                                CreateDocumentUpdate(o.PreviousVersion)
                                    .Set(a => a.Block, o.PreviousVersion.Block)
                                    //set validity to null to set indefinite validity
                                    .Set(a => a.ValidUntilTimestamp, null)
                                    .Set(a => a.ValidUntilBlockLevel, null)
                            );
                    }
                )
                .ToList();

            await DbContext.Get<TRecord>().BulkWriteAsync(session, updateModels, new BulkWriteOptions() { IsOrdered = false }, cancellationToken);

            var updateHistoryModels = previousVersions
                .Where(o => o.PreviousVersion != null)
                .SelectMany(
                    o => new[]
                    {
                        //History removal and changes in history must be done separately
                        new UpdateOneModel<HistoryOf<TRecord>>(
                            Builders<HistoryOf<TRecord>>.Filter.Eq<string?>(DocumentHelper.GetIdDbPropertyName<TRecord>(), o.Id),
                            new UpdateDefinitionBuilder<HistoryOf<TRecord>>()
                                .PopFirst(h => h.History)
                        ),
                        new UpdateOneModel<HistoryOf<TRecord>>(
                            Builders<HistoryOf<TRecord>>.Filter.Eq<string?>(DocumentHelper.GetIdDbPropertyName<TRecord>(), o.Id),
                            new UpdateDefinitionBuilder<HistoryOf<TRecord>>()
                                .Set(d => d.History[0].ValidUntilBlockLevel, null)
                                .Set(d => d.History[0].ValidUntilTimestamp, null)
                        )
                    }
                )
                .ToList();

            if (updateHistoryModels.Count > 0)
                await DbContext.GetHistory<TRecord>()
                    .BulkWriteAsync(session, updateHistoryModels, new BulkWriteOptions() { IsOrdered = true }, cancellationToken);
        }


        private sealed class HistoryWithId : HistoryOf<TRecord>
        {
            public HistoryWithId(IReadOnlyList<TRecord> history, string id) : base(history)
            {
                Id = id;
            }

            public string Id { get; }
        }

        private sealed class PreviousRecord
        {
            public string? Id { get; set; }

            public TRecord? PreviousVersion { get; set; }
        }
    }
}