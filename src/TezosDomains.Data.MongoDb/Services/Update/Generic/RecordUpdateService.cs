﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Data.MongoDb.Helpers;

namespace TezosDomains.Data.MongoDb.Services.Update.Generic
{
    public abstract class RecordUpdateService<TRecord, TUpdate> : HistorizationService<TRecord>
        where TRecord : class, IRecord
    {
        private readonly Expression<Func<TRecord, string>> _idGetterExpression;

        protected RecordUpdateService(MongoDbContext dbContext, ILogger logger) : base(dbContext, logger)
        {
            _idGetterExpression = DocumentHelper.GetIdExpression<TRecord>();
        }

        public override async Task StoreUpdatesAsync(BlockSlim block, IBlockUpdates updates, IClientSessionHandle session, CancellationToken cancellationToken)
        {
            var changedNames = await StoreDocumentUpdatesAsync(ExtractUpdates(updates), block, session, cancellationToken);
            changedNames.UnionWith(await StoreValidityUpdatesAsync(updates.ValidityUpdates, block, session, cancellationToken));
            await HistorizeDocumentsAsync(changedNames, block, session, cancellationToken);
        }

        protected abstract IReadOnlyList<TUpdate> ExtractUpdates(IBlockUpdates updates);

        protected abstract TRecord Map(TUpdate update, string? validityKey, DateTime? expiresAtUtc, BlockSlim block);

        private async Task<IReadOnlyDictionary<string, DateTime?>> GetValidityMappingAsync(
            IEnumerable<string?> validityKeys,
            IClientSessionHandle session,
            CancellationToken cancellationToken
        )
        {
            var keyList = validityKeys.Where(k => k != null).ToHashSet();
            if (keyList.Count == 0)
                return new Dictionary<string, DateTime?>();

            var mappingList = await DbContext.Get<Domain>()
                .Find(session, Builders<Domain>.Filter.In(d => d.Name, keyList))
                .Project(d => new { d.Name, d.ExpiresAtUtc })
                .ToListAsync(cancellationToken);

            return mappingList.ToDictionary(x => x.Name, x => x.ExpiresAtUtc);
        }

        private static DateTime? CalculateExpiresAtUtc(string? validityKey, IReadOnlyDictionary<string, DateTime?> validityMapping)
            => validityKey != null ? validityMapping.GetValueOrDefault(validityKey) : null;

        protected abstract Task<IReadOnlyDictionary<TUpdate, string?>> GetValidityKeysAsync(
            IReadOnlyList<TUpdate> updates,
            IClientSessionHandle session,
            CancellationToken cancellationToken
        );

        private async Task<HashSet<string>> StoreDocumentUpdatesAsync(
            IReadOnlyList<TUpdate> updates,
            BlockSlim block,
            IClientSessionHandle session,
            CancellationToken cancellationToken
        )
        {
            var ids = new HashSet<string>();
            if (updates.Count == 0)
                return ids;

            var validityKeys = await GetValidityKeysAsync(updates, session, cancellationToken);
            var validityMapping = await GetValidityMappingAsync(validityKeys.Values, session, cancellationToken);
            var bulkRequests = updates.Select(
                u =>
                {
                    var expiresAtUtc = CalculateExpiresAtUtc(validityKeys[u], validityMapping);
                    var record = Map(u, validityKeys[u], expiresAtUtc, block);

                    var id = record.GetId();
                    var filter = Builders<TRecord>.Filter.Eq(DocumentHelper.GetIdDbPropertyName<TRecord>(), id);
                    var update = CreateDocumentUpdate(record)
                        .Set(b => b.Block, record.Block)
                        .Set(DocumentHelper.GetIdDbPropertyName<TRecord>(), id);

                    ids.Add(id);
                    return new UpdateOneModel<TRecord>(filter, update) { IsUpsert = true };
                }
            );

            await DbContext.Get<TRecord>().BulkWriteAsync(session, bulkRequests, new BulkWriteOptions { IsOrdered = false }, cancellationToken);

            return ids;
        }

        private async Task<IReadOnlyList<string>> StoreValidityUpdatesAsync(
            IReadOnlyList<ValidityUpdate> updates,
            BlockSlim block,
            IClientSessionHandle session,
            CancellationToken cancellationToken
        )
        {
            if (updates.Count == 0)
                return Array.Empty<string>();

            var validityKeys = new List<string>();
            var bulkRequests = updates.Select(
                u =>
                {
                    validityKeys.Add(u.ValidityKey);

                    return new UpdateManyModel<TRecord>(
                        Builders<TRecord>.Filter.Eq(d => d.ValidityKey, u.ValidityKey),
                        Builders<TRecord>.Update
                            .Set(d => d.ExpiresAtUtc, u.ExpiresAtUtc)
                            .Set(d => d.Block, block)
                    );
                }
            );

            await DbContext.Get<TRecord>().BulkWriteAsync(session, bulkRequests, new BulkWriteOptions { IsOrdered = false }, cancellationToken);
            return await DbContext.Get<TRecord>()
                .Find(session, Builders<TRecord>.Filter.In(d => d.ValidityKey, validityKeys))
                .Project(_idGetterExpression)
                .ToListAsync(cancellationToken);
        }
    }
}