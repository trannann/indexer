﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Data.MongoDb.Services.Update.Generic;

namespace TezosDomains.Data.MongoDb.Services.Update
{
    public class DomainUpdateService : RecordUpdateService<Domain, DomainUpdate>
    {
        public DomainUpdateService(MongoDbContext dbContext, ILogger<DomainUpdateService> logger) : base(dbContext, logger)
        {
        }

        protected override IReadOnlyList<DomainUpdate> ExtractUpdates(IBlockUpdates updates) => updates.DomainUpdates;

        protected override Domain Map(DomainUpdate update, string? validityKey, DateTime? expiresAtUtc, BlockSlim block)
        {
            var calculated = DomainNameUtils.Parse(update.Name);
            return new Domain(
                update.Name,
                update.Address,
                update.Owner,
                update.Data,
                calculated.Label,
                calculated.Ancestors,
                calculated.Parent,
                expiresAtUtc,
                block,
                validityKey,
                calculated.Level,
                calculated.NameReverse
            );
        }

        protected override UpdateDefinition<Domain> CreateDocumentUpdate(Domain update)
        {
            return new UpdateDefinitionBuilder<Domain>()
                    .Set(d => d.Address, update.Address)
                    .Set(d => d.Owner, update.Owner)
                    .Set(d => d.Data, update.Data)
                    .Set(d => d.Ancestors, update.Ancestors)
                    .Set(d => d.Parent, update.Parent)
                    .Set(d => d.Label, update.Label)
                    .Set(d => d.NameReverse, update.NameReverse)
                    .Set(d => d.Level, update.Level)
                    .Set(d => d.ValidityKey, update.ValidityKey)
                    .Set(d => d.ExpiresAtUtc, update.ExpiresAtUtc)
                ;
        }

        protected override Task<IReadOnlyDictionary<DomainUpdate, string?>> GetValidityKeysAsync(
            IReadOnlyList<DomainUpdate> updates,
            IClientSessionHandle session,
            CancellationToken cancellationToken
        )
        {
            return Task.FromResult<IReadOnlyDictionary<DomainUpdate, string?>>(updates.ToDictionary(u => u, u => u.ValidityKey));
        }
    }
}