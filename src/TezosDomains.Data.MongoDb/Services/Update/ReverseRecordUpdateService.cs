﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Data.MongoDb.Services.Update.Generic;

namespace TezosDomains.Data.MongoDb.Services.Update
{
    public class ReverseRecordUpdateService : RecordUpdateService<ReverseRecord, ReverseRecordUpdate>
    {
        public ReverseRecordUpdateService(MongoDbContext dbContext, ILogger<ReverseRecordUpdateService> logger) : base(dbContext, logger)
        {
        }

        protected override IReadOnlyList<ReverseRecordUpdate> ExtractUpdates(IBlockUpdates updates) => updates.ReverseRecordUpdates;

        protected override ReverseRecord Map(ReverseRecordUpdate update, string? validityKey, DateTime? expiresAtUtc, BlockSlim block)
        {
            return new ReverseRecord(update.Address, block, update.Name, update.Owner, expiresAtUtc, validityKey);
        }

        protected override UpdateDefinition<ReverseRecord> CreateDocumentUpdate(ReverseRecord update)
        {
            var builder = new UpdateDefinitionBuilder<ReverseRecord>()
                .Set(d => d.Name, update.Name)
                .Set(d => d.Owner, update.Owner)
                .Set(d => d.ValidityKey, update.ValidityKey)
                .Set(d => d.ExpiresAtUtc, update.ExpiresAtUtc);
            return builder;
        }

        protected override async Task<IReadOnlyDictionary<ReverseRecordUpdate, string?>> GetValidityKeysAsync(
            IReadOnlyList<ReverseRecordUpdate> updates,
            IClientSessionHandle session,
            CancellationToken cancellationToken
        )
        {
            var names = updates.Where(u => u.Name != null).Select(u => u.Name).Distinct();
            var data = await DbContext.Get<Domain>()
                .Find(session, Builders<Domain>.Filter.In(d => d.Name, names))
                .Project(d => new { d.Name, d.ValidityKey })
                .ToListAsync(cancellationToken);
            var validityKeysByName = data.ToDictionary(d => d.Name, d => d.ValidityKey);

            return updates.ToDictionary(u => u, u => u.Name != null ? validityKeysByName.GetValueOrDefault(u.Name) : null);
        }
    }
}