﻿using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.Models.Updates;

namespace TezosDomains.Data.MongoDb.Services
{
    public class EventProcessor
    {
        private readonly IAuctionQueryService _auctionQueryService;
        private readonly IDomainQueryService _domainQueryService;
        private readonly ILogger<EventProcessor> _logger;

        public EventProcessor(
            IAuctionQueryService auctionQueryService,
            IDomainQueryService domainQueryService,
            ILogger<EventProcessor> logger
        )
        {
            _auctionQueryService = auctionQueryService;
            _domainQueryService = domainQueryService;
            _logger = logger;
        }


        public async Task<IReadOnlyList<Event>> GeneratedAndUpdateEventsAsync(
            BlockSlim block,
            IBlockUpdates blockUpdates,
            BlockSlim? previousBlock,
            CancellationToken cancellationToken
        )
        {
            _logger.LogDebug("Event processing started.");

            var unprocessedAuctionSettleEvents = blockUpdates.Events.OfType<AuctionSettleEvent>().ToArray();
            var unprocessedDomainSetChildRecordEvents = blockUpdates.Events.OfType<DomainSetChildRecordEvent>().ToArray();
            var unprocessedAuctionBidEvents = blockUpdates.Events.OfType<AuctionBidEvent>().ToArray();
            var otherEvents = blockUpdates.Events
                .Except(unprocessedAuctionSettleEvents)
                .Except(unprocessedDomainSetChildRecordEvents)
                .Except(unprocessedAuctionBidEvents);

            var auctionSettleEvents = await UpdateAuctionSettleEvents(block, unprocessedAuctionSettleEvents, cancellationToken);
            var domainSetChildRecordEvents = await UpdateDomainSetChildRecordEvents(unprocessedDomainSetChildRecordEvents, cancellationToken);
            var auctionBidEvents = await UpdateAuctionBidsAsync(block, unprocessedAuctionBidEvents, cancellationToken);
            var auctionEndEvents = await GenerateAuctionEndEventsAsync(previousBlock, block, cancellationToken);

            _logger.LogDebug("Event processing finished.");

            return otherEvents
                .Concat(auctionSettleEvents)
                .Concat(domainSetChildRecordEvents)
                .Concat(auctionBidEvents)
                .Concat(auctionEndEvents)
                .ToArray();
        }

        private async Task<IEnumerable<DomainSetChildRecordEvent>> UpdateDomainSetChildRecordEvents(
            IEnumerable<DomainSetChildRecordEvent> domainSetChildRecordEvents,
            CancellationToken cancellationToken
        )
        {
            var events = domainSetChildRecordEvents.ToDictionary(e => e.DomainName);
            if (events.Count == 0)
                return new DomainSetChildRecordEvent[0];

            var existingDomains = new HashSet<string>(await _domainQueryService.GetExistingNamesAsync(events.Keys, cancellationToken));

            return events.Values.Select(e => e.Clone(isNewRecord: !existingDomains.Contains(e.DomainName)));
        }

        private async Task<IEnumerable<AuctionSettleEvent>> UpdateAuctionSettleEvents(
            BlockSlim block,
            IEnumerable<AuctionSettleEvent> auctionSettleEvents,
            CancellationToken cancellationToken
        )
        {
            var events = auctionSettleEvents.ToDictionary(e => e.DomainName);
            if (events.Count == 0)
                return new AuctionSettleEvent[0];

            var auctions = await _auctionQueryService.GetCanBeSettledAsync(events.Keys, block.Timestamp, cancellationToken);

            if (auctions.Count != events.Count)
                throw new InvalidDataException(
                    $"AuctionSettleEvents parsed from block {block.Level} ({block.Hash}), but no corresponding auctions in CanBeSettled state exists in db."
                );

            return auctions.Select(auction => events[auction.DomainName].Clone(auction.RegistrationDurationInDays, auction.WinningPrice));
        }

        private async Task<IEnumerable<Event>> GenerateAuctionEndEventsAsync(
            BlockSlim? previousBlock,
            BlockSlim block,
            CancellationToken cancellationToken
        )
        {
            var endedAuctions = await _auctionQueryService.GetEndedAsync(
                from: previousBlock?.Timestamp,
                to: block.Timestamp,
                cancellationToken: cancellationToken
            );

            return endedAuctions.Select(
                a => new AuctionEndEvent(
                    a.DomainName,
                    a.Bids[0].Bidder,
                    a.Bids[0].Amount,
                    a.Bids.Select(b => b.Bidder).ToArray(),
                    block,
                    Event.GenerateId()
                )
            );
        }

        private async Task<IReadOnlyList<Event>> UpdateAuctionBidsAsync(
            BlockSlim block,
            IReadOnlyList<AuctionBidEvent> bidEvents,
            CancellationToken cancellationToken
        )
        {
            if (bidEvents.Count == 0)
                return new Event[0];

            var domainGroups = bidEvents.GroupBy(b => b.DomainName, (_, bids) => bids.OrderBy(b => b.BidAmount));
            var bidLookupInDb = new List<AuctionBidEvent>();
            var processedEvents = new List<Event>();

            foreach (var group in domainGroups)
            {
                AuctionBidEvent? previousBid = null;
                foreach (var bid in group)
                {
                    if (previousBid == null)
                    {
                        // This part generates outbids to bids from previous blocks (bids that are stored in the DB)
                        bidLookupInDb.Add(bid);
                    }
                    else
                    {
                        // This part generates outbids to other bids in this block
                        processedEvents.Add(bid.Clone(previousBid.SourceAddress, previousBid.BidAmount));
                    }

                    previousBid = bid;
                }
            }

            var auctions = await _auctionQueryService.GetAllInProgressAsync(
                bidLookupInDb.Select(b => b.DomainName).ToArray(),
                block.Timestamp,
                cancellationToken
            );
            var auctionPerDomain = auctions.ToDictionary(a => a.DomainName);
            processedEvents.AddRange(
                bidLookupInDb.Select(
                    bid => auctionPerDomain.TryGetValue(bid.DomainName, out var auction)
                        ? bid.Clone(auction.HighestBidder, auction.HighestBid)
                        : bid
                )
            );

            return processedEvents;
        }
    }
}