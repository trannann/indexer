﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using TezosDomains.Data.MongoDb.Configurations;

namespace TezosDomains.Data.MongoDb.HealthCheck
{
    public class MongoDbHealthCheck : IHealthCheck
    {
        private readonly ILogger<MongoDbHealthCheck> _logger;

        private readonly MongoClient _mongoClient;
        private readonly string _specifiedDatabase;

        public MongoDbHealthCheck(MongoClient mongoClient, IMongoDbConfiguration configuration, ILogger<MongoDbHealthCheck> logger)
        {
            _mongoClient = mongoClient;
            _logger = logger;
            _specifiedDatabase = configuration.Database;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            try
            {
                using var cursor = await _mongoClient
                    .GetDatabase(_specifiedDatabase)
                    .ListCollectionNamesAsync(cancellationToken: cancellationToken);
                await cursor.FirstAsync(cancellationToken);

                return HealthCheckResult.Healthy();
            }
            catch (Exception ex)
            {
                _logger.LogWarning("MongoDb health check failed", ex);
                return new HealthCheckResult(context.Registration.FailureStatus, exception: ex);
            }
        }
    }
}