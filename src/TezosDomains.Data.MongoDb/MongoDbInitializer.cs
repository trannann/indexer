﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.Models.Events;

namespace TezosDomains.Data.MongoDb
{
    public class MongoDbInitializer
    {
        private readonly ILogger _logger;
        private readonly MongoDbContext _dbContext;
        private bool _initialized;

        public MongoDbInitializer(ILogger<MongoDbInitializer> logger, MongoDbContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
            _initialized = false;
        }

        public void Initialize()
        {
            if (_initialized)
                return;

            _initialized = true;

            CreateDatabaseAndIndexesIfMissing(_dbContext);
        }

        private CreateIndexModel<ReverseRecord>[] CreateReverseRecordsIndexDefinitions(
            IndexKeysDefinitionBuilder<ReverseRecord> blockBuilder
        )
            => new[]
            {
                new CreateIndexModel<ReverseRecord>(blockBuilder.Ascending(o => o.Owner)),
                new CreateIndexModel<ReverseRecord>(blockBuilder.Ascending(o => o.Name)),
                new CreateIndexModel<ReverseRecord>(blockBuilder.Ascending(o => o.ValidityKey)),
            };

        private CreateIndexModel<Block>[] CreateBlocksIndexDefinitions(IndexKeysDefinitionBuilder<Block> blockBuilder)
            => new[]
            {
                new CreateIndexModel<Block>(blockBuilder.Descending(o => o.Level)),
            };

        private CreateIndexModel<Domain>[] CreateDomainsIndexDefinitions(
            IndexKeysDefinitionBuilder<Domain> domainBuilder
        )
            => new[]
            {
                new CreateIndexModel<Domain>(domainBuilder.Ascending(d => d.NameReverse), new CreateIndexOptions { Unique = true }),
                new CreateIndexModel<Domain>(domainBuilder.Ascending(d => d.Ancestors)),
                new CreateIndexModel<Domain>(domainBuilder.Ascending(d => d.Ancestors[1])),
                new CreateIndexModel<Domain>(domainBuilder.Ascending(d => d.Label)),
                new CreateIndexModel<Domain>(domainBuilder.Ascending(d => d.Owner)),
                new CreateIndexModel<Domain>(domainBuilder.Ascending(d => d.Address)),
                new CreateIndexModel<Domain>(domainBuilder.Ascending(d => d.ExpiresAtUtc)),
            };

        private CreateIndexModel<Event>[] CreateEventsIndexDefinitions(IndexKeysDefinitionBuilder<Event> builder)
            => new[]
            {
                new CreateIndexModel<Event>(builder.Ascending(nameof(DomainBuyEvent.SourceAddress))),
                new CreateIndexModel<Event>(builder.Ascending(e => e.Block.Level))
            };

        private CreateIndexModel<Auction>[] CreateAuctionsIndexDefinitions(IndexKeysDefinitionBuilder<Auction> domainBuilder)
            => new[]
            {
                new CreateIndexModel<Auction>(domainBuilder.Ascending(d => d.DomainName)),
                new CreateIndexModel<Auction>(domainBuilder.Ascending(d => d.Bids[0])),
                new CreateIndexModel<Auction>(domainBuilder.Descending(d => d.EndsAtUtc)),
                new CreateIndexModel<Auction>(domainBuilder.Descending(d => d.BidCount)),
            };

        /// <summary>
        /// Index creation in mongodb is idempotent,
        /// therefore we can try to create the indexes as many time as we want
        /// </summary>
        /// <param name="dbContext"></param>
        private void CreateDatabaseAndIndexesIfMissing(MongoDbContext dbContext)
        {
            try
            {
                _logger.LogInformation("Create or update database indexes STARTED.");

                var createdIndexes = new List<string>();
                createdIndexes.AddRange(CreateIndexes(dbContext.Get<Domain>(), CreateDomainsIndexDefinitions));
                createdIndexes.AddRange(CreateIndexes(dbContext.Blocks, CreateBlocksIndexDefinitions));
                createdIndexes.AddRange(CreateIndexes(dbContext.Get<ReverseRecord>(), CreateReverseRecordsIndexDefinitions));
                createdIndexes.AddRange(CreateIndexes(dbContext.Get<Auction>(), CreateAuctionsIndexDefinitions));
                createdIndexes.AddRange(CreateIndexes(dbContext.Get<Event>(), CreateEventsIndexDefinitions));
                createdIndexes.AddRange(CreateIndexes(dbContext.Get<BidderBalances>(), CreateBidderBalancesIndexDefinitions));

                _logger.LogInformation(
                    "Create or update database indexes FINISHED. Following indexes created (or were already existing): {createdIndexes}.",
                    createdIndexes
                );
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Unable to create mongoDb indexes");
            }
        }

        private static CreateIndexModel<BidderBalances>[] CreateBidderBalancesIndexDefinitions(IndexKeysDefinitionBuilder<BidderBalances> builder) =>
            new[] { new CreateIndexModel<BidderBalances>(builder.Ascending(d => d.Address)) };


        private IEnumerable<string> CreateIndexes<TDocument>(
            IMongoCollection<TDocument> collection,
            Func<IndexKeysDefinitionBuilder<TDocument>, CreateIndexModel<TDocument>[]> createIndexDefinitions
        )
        {
            var builder = Builders<TDocument>.IndexKeys;
            return collection.Indexes.CreateMany(createIndexDefinitions(builder));
        }
    }
}