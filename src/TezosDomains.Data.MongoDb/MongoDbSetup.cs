﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.Serializers;
using System;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.DistributedLock;

namespace TezosDomains.Data.MongoDb
{
    internal static class MongoDbSetup
    {
        private static bool _initialized = false;
        private static readonly object Lock = new object();

        internal static void InitializeMappingStructures()
        {
            lock (Lock)
            {
                if (_initialized)
                    return;

                var pack = new ConventionPack
                {
                    new IgnoreExtraElementsConvention(true),
                    new ImmutableTypeClassMapConvention(),
                    new EnumRepresentationConvention(BsonType.String),
                };
                ConventionRegistry.Register("Conventions", pack, t => true);

                BsonSerializer.RegisterSerializer(typeof(decimal), new DecimalSerializer(BsonType.Decimal128));
                BsonSerializer.RegisterSerializer(typeof(decimal?), new NullableSerializer<decimal>(new DecimalSerializer(BsonType.Decimal128)));

                RegisterClassMap<Block>(m => m.MapIdProperty(p => p.Hash));
                RegisterClassMap<BlockSlim>(m => m.MapIdProperty(p => p.Hash));

                RegisterClassWithHistoryMap<Domain>(m => m.MapIdProperty(p => p.Name));

                RegisterClassWithHistoryMap<ReverseRecord>(m => m.MapIdProperty(p => p.Address));

                RegisterClassWithHistoryMap<Auction>(m => m.MapIdProperty(p => p.Id).SetIdGenerator(new AuctionIdGenerator()));

                RegisterClassWithHistoryMap<BidderBalances>(m => m.MapIdProperty(p => p.Address));

                RegisterClassMap<Lock>(m => m.MapIdProperty(p => p.Resource));

                RegisterClassMap<TezosDocument>(
                    m => { m.MapIdProperty(p => p.Id); }
                );

                BsonClassMap.RegisterClassMap<Event>();
                RegisterClassMap<AuctionBidEvent>(m => { m.SetDiscriminator(EventType.AuctionBidEvent.ToString()); });
                RegisterClassMap<AuctionSettleEvent>(m => { m.SetDiscriminator(EventType.AuctionSettleEvent.ToString()); });
                RegisterClassMap<AuctionWithdrawEvent>(m => { m.SetDiscriminator(EventType.AuctionWithdrawEvent.ToString()); });
                RegisterClassMap<AuctionEndEvent>(m => { m.SetDiscriminator(EventType.AuctionEndEvent.ToString()); });
                RegisterClassMap<DomainBuyEvent>(m => { m.SetDiscriminator(EventType.DomainBuyEvent.ToString()); });
                RegisterClassMap<DomainRenewEvent>(m => { m.SetDiscriminator(EventType.DomainRenewEvent.ToString()); });
                RegisterClassMap<DomainUpdateEvent>(m => { m.SetDiscriminator(EventType.DomainUpdateEvent.ToString()); });
                RegisterClassMap<DomainSetChildRecordEvent>(m => { m.SetDiscriminator(EventType.DomainSetChildRecordEvent.ToString()); });
                RegisterClassMap<DomainCommitEvent>(m => { m.SetDiscriminator(EventType.DomainCommitEvent.ToString()); });
                RegisterClassMap<ReverseRecordUpdateEvent>(m => { m.SetDiscriminator(EventType.ReverseRecordUpdateEvent.ToString()); });
                RegisterClassMap<ReverseRecordClaimEvent>(m => { m.SetDiscriminator(EventType.ReverseRecordClaimEvent.ToString()); });

                _initialized = true;
            }

            void RegisterClassWithHistoryMap<TClassWithHistory>(Action<BsonClassMap<TClassWithHistory>> classMapInitializer)
                where TClassWithHistory : IDocumentWithHistory
            {
                BsonClassMap.RegisterClassMap<TClassWithHistory>(
                    m =>
                    {
                        m.AutoMap();
                        classMapInitializer(m);

                        m.MapProperty(p => p.ValidUntilBlockLevel).SetDefaultValue(() => null).SetIgnoreIfNull(true);
                        m.MapProperty(p => p.ValidUntilTimestamp).SetDefaultValue(() => null).SetIgnoreIfNull(true);
                    }
                );
            }

            void RegisterClassMap<TClass>(Action<BsonClassMap<TClass>> classMapInitializer)
            {
                BsonClassMap.RegisterClassMap<TClass>(
                    m =>
                    {
                        m.AutoMap();
                        classMapInitializer(m);
                    }
                );
            }
        }

        private class AuctionIdGenerator : IIdGenerator
        {
            public object GenerateId(object container, object document)
            {
                var auction = (Auction) document;
                return Auction.GenerateId(auction.DomainName, auction.Block.Level);
            }

            public bool IsEmpty(object id)
            {
                return string.IsNullOrEmpty((string) id);
            }
        }
    }
}