using AspNetCoreRateLimit;
using GraphQL.Server;
using GraphQL.Server.Ui.Playground;
using GraphQL.Types;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using TezosDomains.Api.GraphQL;
using TezosDomains.Api.GraphQL.Filtering;
using TezosDomains.Api.GraphQL.Filtering.Generic;
using TezosDomains.Api.GraphQL.Ordering;
using TezosDomains.Api.GraphQL.Output;
using TezosDomains.Api.GraphQL.Output.Auctions;
using TezosDomains.Api.GraphQL.Output.Events;
using TezosDomains.Api.GraphQL.Output.Generic;
using TezosDomains.Api.GraphQL.Paging;
using TezosDomains.Api.GraphQL.RootQueries;
using TezosDomains.Api.GraphQL.Scalars;
using TezosDomains.Common;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Configurations;
using TezosDomains.Data.MongoDb.Query.Filtering;
using TezosDomains.Data.MongoDb.Query.Filtering.Auctions;
using TezosDomains.Data.MongoDb.Query.Filtering.Events;
using TezosDomains.Data.MongoDb.Query.Ordering;

namespace TezosDomains.Api
{
    public class Startup
    {
        private readonly IWebHostEnvironment _env;
        private const string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            _env = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddMongoDb(_env, Configuration.GetSection("TezosDomains:MongoDb").Get<MongoDbConfiguration>());
            services.AddHttpContextAccessor();

            AddGraphQLTypes(services);
            AddIpRateLimit(services);
            AddCors(services);

            services.AddGraphQL(ConfigureGraphQL).AddSystemTextJson();
            services.AddHealthChecks().AddVersion();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var defaultPath = "/playground";
            var graphQLOptions = new GraphQLPlaygroundOptions
            {
                Path = defaultPath,
                // See https://github.com/prisma-labs/graphql-playground#settings
                PlaygroundSettings = new Dictionary<string, object> { { "schema.polling.enable", false } }
            };

            app.UseCors(MyAllowSpecificOrigins);
            app.UseIpRateLimiting();
            app.UseSecurityHeaders(Configuration, contentSecurityPolicyExcludedPaths: new[] { graphQLOptions.Path.ToString() });
            app.UseGraphQLPlayground(graphQLOptions);
            app.UseGraphQL<ISchema>();
            app.UseHealth();

            //add default redirect to playground
            app.UseRewriter(
                new RewriteOptions()
                    .AddRedirect("^$", defaultPath)
            );
        }

        private void AddCors(IServiceCollection services)
            => services.AddCors(
                options =>
                {
                    options.AddPolicy(
                        name: MyAllowSpecificOrigins,
                        builder =>
                        {
                            var allowedAny = Configuration["TezosDomains:AllowedOrigins"] == "*"
                                ? builder.AllowAnyOrigin()
                                : builder.WithOrigins(Configuration["TezosDomains:AllowedOrigins"].Split(';'));
                            builder.WithHeaders(
                                HeaderNames.ContentType,
                                "X-CSRF-Token",
                                "X-Requested-With",
                                HeaderNames.Accept,
                                HeaderNames.AcceptEncoding,
                                HeaderNames.AcceptLanguage,
                                HeaderNames.ContentLength,
                                HeaderNames.Origin,
                                HeaderNames.Referer,
                                HeaderNames.UserAgent,
                                "apollographql-client-name",
                                "apollographql-client-version"
                            );
                            builder.WithMethods("POST", "OPTIONS");
                        }
                    );
                }
            );

        private void AddGraphQLTypes(IServiceCollection services)
        {
            global::GraphQL.Utilities.GraphTypeTypeRegistry.Register<DateTime, DateTimeGraphType>(); // Default one outputs only date -> overwrite

            services.AddSingleton<ISchema, TezosDomainsSchema>();
            services.AddSingleton<TezosRootQuery>();
            services.AddSingleton(typeof(EnumerationGraphType<>));
            services.AddSingleton<AddressScalarType>();
            services.AddSingleton<AddressPrefixGraphType>();
            services.AddSingleton<MutezScalarType>();
            services.AddSingleton<DataItemGraphType>();
            services.AddSingleton<NodeInterfaceType>();
            services.AddSingleton<TezosDocumentInterfaceType>();

            // Domain
            services.AddSingleton<IRootQueryProvider, RecordRootQueries<Domain, DomainGraphType, DomainsFilter>>();
            services.AddSingleton<DomainGraphType>();
            services.AddSingleton(new DocumentOrderFieldGraphType<Domain>(DomainsOrder.Fields));
            services.AddSingleton<InputObjectGraphType<DomainsFilter>, DomainsFilterGraphType>();

            // Reverse record
            services.AddSingleton<IRootQueryProvider, RecordRootQueries<ReverseRecord, ReverseRecordGraphType, ReverseRecordsFilter>>();
            services.AddSingleton<ReverseRecordGraphType>();
            services.AddSingleton(new DocumentOrderFieldGraphType<ReverseRecord>(ReverseRecordsOrder.Fields));
            services.AddSingleton<InputObjectGraphType<ReverseRecordsFilter>, ReverseRecordsFilterGraphType>();

            // Block
            services.AddSingleton<IRootQueryProvider, BlockRootQueries>();
            services.AddSingleton<BlockGraphType>();

            // Auctions
            services.AddSingleton<IRootQueryProvider, AuctionRootQueries>();
            services.AddSingleton<AuctionGraphType>();
            services.AddSingleton<BidGraphType>();
            services.AddSingleton(new DocumentOrderFieldGraphType<Auction>(AuctionsOrder.Fields));
            services.AddSingleton<InputObjectGraphType<AuctionStateFilter>, EnumFilterGraphType<AuctionState, AuctionStateFilter>>();
            services.AddSingleton<InputObjectGraphType<AuctionsFilter>, AuctionsFilterGraphType>();

            // Events
            services.AddSingleton<IRootQueryProvider, EventRootQueries>();
            services.AddSingleton<EventInterfaceType>();
            services.AddSingleton<AuctionBidEventGraphType>();
            services.AddSingleton<AuctionEndEventGraphType>();
            services.AddSingleton<AuctionParticipantGraphType>();
            services.AddSingleton<AuctionSettleEventGraphType>();
            services.AddSingleton<AuctionWithdrawEventGraphType>();
            services.AddSingleton<DomainBuyEventGraphType>();
            services.AddSingleton<DomainRenewEventGraphType>();
            services.AddSingleton<DomainUpdateEventGraphType>();
            services.AddSingleton<DomainCommitEventGraphType>();
            services.AddSingleton<DomainSetChildRecordEventGraphType>();
            services.AddSingleton<ReverseRecordClaimEventGraphType>();
            services.AddSingleton<ReverseRecordUpdateEventGraphType>();
            services.AddSingleton(new DocumentOrderFieldGraphType<Event>(EventsOrder.Fields));
            services.AddSingleton<InputObjectGraphType<EventsFilter>, EventsFilterGraphType>();

            // Bidder balances
            services.AddSingleton<IRootQueryProvider, BidderBalanceRootQueries>();
            services.AddSingleton<BidderBalancesGraphType>();
            services.AddSingleton<BalanceGraphType>();

            // Generic filters
            services.AddSingleton<StringEqualityFilterGraphType>();
            services.AddSingleton<StringFilterGraphType>();
            services.AddSingleton<NullableStringFilterGraphType>();
            services.AddSingleton<AddressFilterGraphType>();
            services.AddSingleton<NullableAddressFilterGraphType>();
            services.AddSingleton<IntFilterGraphType>();
            services.AddSingleton<MutezFilterGraphType>();
            services.AddSingleton<DateTimeFilterGraphType>();
            services.AddSingleton<NullableDateTimeFilterGraphType>();
            services.AddSingleton<StringArrayFilterGraphType>();
            services.AddSingleton<AssociatedBlockFilterGraphType>();
            services.AddSingleton<BlockHistoryFilterGraphType>();
            services.AddSingleton(typeof(EnumFilterGraphType<>));
            services.AddSingleton(typeof(EnumFilterGraphType<,>));

            // Generic paging
            services.AddSingleton(typeof(DocumentOrderGraphType<>));
            services.AddSingleton<IRelayConnectionService, RelayConnectionService>();
            services.AddSingleton<IPageSizeCalculator, PageSizeCalculator>();
            services.AddSingleton<IGraphQLPageSizeOptions>(p => p.GetRequiredService<IOptions<GraphQLPageSizeOptions>>().Value);
            services.AddOptions<GraphQLPageSizeOptions>()
                .Bind(Configuration.GetSection("GraphQLPageSize"))
                .ValidateDataAnnotations();
        }

        private void ConfigureGraphQL(GraphQLOptions options, IServiceProvider provider)
        {
            Configuration.Bind("GraphQL", options);

            var handler = ActivatorUtilities.CreateInstance<TezosErrorHandler>(provider);
            options.UnhandledExceptionDelegate = handler.Handle;
        }

        private void AddIpRateLimit(IServiceCollection services)
        {
            services.AddMemoryCache();
            services.Configure<IpRateLimitOptions>(Configuration.GetSection("IpRateLimiting"));
            services.Configure<IpRateLimitPolicies>(Configuration.GetSection("IpRateLimitPolicies"));
            services.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();
            services.AddSingleton<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>();
            services.AddSingleton<IRateLimitConfiguration, RateLimitConfiguration>();
        }
    }
}