﻿using GraphQL.Builders;
using GraphQL.Types;
using System.Collections.Generic;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Arguments.Generic
{
    public static class ArgumentBuilderExtensions
    {
        public static FieldBuilder<TSource, TReturn> Arguments<TSource, TReturn>(
            this FieldBuilder<TSource, TReturn> builder,
            IEnumerable<IGraphQLArgument>? arguments
        )
        {
            AddArguments(builder.FieldType, arguments);
            return builder;
        }

        public static ConnectionBuilder<TSource> Arguments<TSource>(
            this ConnectionBuilder<TSource> builder,
            IEnumerable<IGraphQLArgument>? arguments
        )
        {
            AddArguments(builder.FieldType, arguments);
            return builder;
        }

        private static void AddArguments(IFieldType fieldType, IEnumerable<IGraphQLArgument>? arguments)
        {
            foreach (var argument in arguments.NullToEmpty())
                fieldType.Arguments.Add(argument.ToGraphQL());
        }
    }
}