﻿using GraphQL;
using GraphQL.Types;
using System;

namespace TezosDomains.Api.GraphQL.Arguments.Generic
{
    public interface IGraphQLArgument
    {
        string Name { get; }
        string Description { get; }
        Type GraphType { get; }

        QueryArgument ToGraphQL();
    }

    public class GraphQLArgument<TValue, TGraph> : IGraphQLArgument
        where TGraph : GraphType
    {
        public string Name { get; }
        public string Description { get; }
        public Type GraphType => typeof(TGraph);
        public object? DefaultValue { get; }

        public GraphQLArgument(string name, string description, object? defaultValue = null)
        {
            Name = name;
            Description = description;
            DefaultValue = defaultValue;
        }

        public virtual TValue GetValue(IResolveFieldContext context)
            => context.GetArgument<TValue>(Name);

        public TValue GetParentValue(IResolveFieldContext context)
            => (TValue) context.UserContext[Name];

        public void SetValueForChildren(IResolveFieldContext context, TValue value)
            => context.UserContext[Name] = value;

        public QueryArgument ToGraphQL()
            => new QueryArgument<TGraph>
            {
                Name = Name,
                Description = Description,
                DefaultValue = DefaultValue
            };
    }
}