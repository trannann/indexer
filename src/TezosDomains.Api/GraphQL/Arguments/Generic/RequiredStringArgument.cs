﻿using GraphQL;
using GraphQL.Types;

namespace TezosDomains.Api.GraphQL.Arguments.Generic
{
    public class RequiredStringArgument : GraphQLArgument<string, NonNullGraphType<StringGraphType>>
    {
        public RequiredStringArgument(string name, string description) : base(name, description)
        {
        }

        public override string GetValue(IResolveFieldContext context)
        {
            var value = base.GetValue(context);
            return !string.IsNullOrWhiteSpace(value)
                ? value
                : throw new GraphQLInputException(Name, value, "a non-empty string");
        }
    }
}