﻿using GraphQL.Types;
using TezosDomains.Api.GraphQL.Arguments.Generic;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Api.GraphQL.Arguments
{
    public class ValidityArgument
    {
        public static readonly GraphQLArgument<RecordValidity, EnumerationGraphType<RecordValidity>> Instance = new(
            name: "validity",
            description: "Filters validity of the entity based on it's validity date-time."
        );
    }
}