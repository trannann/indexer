﻿using GraphQL;
using TezosDomains.Api.GraphQL.Arguments.Generic;
using TezosDomains.Api.GraphQL.Ordering;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Api.GraphQL.Arguments
{
    public static class OrderArgument
    {
        public const string Name = "order";
    }

    public class OrderArgument<TDocument> : GraphQLArgument<DocumentOrder<TDocument>, DocumentOrderGraphType<TDocument>>
    {
        public OrderArgument(DocumentOrder<TDocument> defaultOrder)
            : base(OrderArgument.Name, GetDescription(defaultOrder), defaultValue: defaultOrder)
        {
        }

        private static string GetDescription(DocumentOrder<TDocument> defaultOrder)
            => $"Ordering options. Default is `{{ {nameof(defaultOrder.Field).ToCamelCase()}: {defaultOrder.Field.DisplayName},"
               + $" {nameof(defaultOrder.Direction).ToCamelCase()}: {defaultOrder.Direction} }}`.";
    }
}