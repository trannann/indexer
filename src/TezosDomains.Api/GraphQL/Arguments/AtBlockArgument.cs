﻿using TezosDomains.Api.GraphQL.Arguments.Generic;
using TezosDomains.Api.GraphQL.Filtering.Generic;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Api.GraphQL.Arguments
{
    public static class AtBlockArgument
    {
        public static readonly GraphQLArgument<BlockHistoryFilter?, BlockHistoryFilterGraphType> Instance = new(
            name: "atBlock",
            description: "Filters entities by block history."
        );
    }
}