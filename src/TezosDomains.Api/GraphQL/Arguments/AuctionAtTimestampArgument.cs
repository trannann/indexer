﻿using GraphQL;
using System;

namespace TezosDomains.Api.GraphQL.Arguments
{
    public static class AuctionAtTimestampArgument
    {
        private const string Name = "auctionAtTimestamp";

        public static void SetAuctionAtTimestampForChildren(this IResolveFieldContext context, DateTime value)
            => context.UserContext[Name] = value;

        public static DateTime GetParentAuctionAtTimestamp(this IResolveFieldContext context)
            => (DateTime) context.UserContext[Name];
    }
}