﻿using TezosDomains.Api.GraphQL.Arguments;
using TezosDomains.Api.GraphQL.Filtering.Generic;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Filtering
{
    public sealed class DomainsFilterGraphType : BaseFilterGraphType<DomainsFilter>
    {
        public DomainsFilterGraphType()
        {
            Builder.NullableField(d => d.Name, "Filters domains by their name.", new StringFilterGraphType());
            Builder.NullableField(d => d.Address, "Filters domains by their address.", new NullableAddressFilterGraphType());
            Builder.NullableField(d => d.Owner, "Filters domains by their name.", new AddressFilterGraphType());
            Builder.NullableField(d => d.Level, "Filters domains by their level.", new IntFilterGraphType());
            Builder.NullableField(d => d.ExpiresAtUtc, "Filters domains by their validity date-time.", new NullableDateTimeFilterGraphType());
            Builder.NullableField(d => d.Ancestors, "Filters domains by exact match of one of their ancestors.", new StringArrayFilterGraphType());
            Builder.NullableEnumField(d => d.Validity, ValidityArgument.Instance.Description);
        }
    }
}