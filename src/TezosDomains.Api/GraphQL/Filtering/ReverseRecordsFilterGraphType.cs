﻿using TezosDomains.Api.GraphQL.Arguments;
using TezosDomains.Api.GraphQL.Filtering.Generic;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL.Filtering
{
    public sealed class ReverseRecordsFilterGraphType : BaseFilterGraphType<ReverseRecordsFilter>
    {
        public ReverseRecordsFilterGraphType()
        {
            Builder.NullableField(x => x.Address, "Filters reverse records by their address.", new AddressFilterGraphType());
            Builder.NullableField(x => x.Name, "Filters reverse records by their name.", new NullableStringFilterGraphType());
            Builder.NullableField(x => x.Owner, "Filters reverse records by their name.", new AddressFilterGraphType());
            Builder.NullableField(x => x.ExpiresAtUtc, "Filters reverse records by their validity date-time.", new NullableDateTimeFilterGraphType());
            Builder.NullableEnumField(x => x.Validity, ValidityArgument.Instance.Description);
        }
    }
}