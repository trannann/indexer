﻿using GraphQL.Types;
using System;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Api.GraphQL.Filtering.Generic
{
    public class EnumFilterGraphType<TEnum, TFilter> : InputGraphType<TFilter>
        where TEnum : Enum
        where TFilter : IEnumFilter<TEnum>
    {
        public EnumFilterGraphType()
            : base(name: $"{typeof(TEnum).Name}Filter", description: $"Filter for {typeof(TEnum).Name}.")
            => Builder.Field(
                f => f.In,
                typeof(ListGraphType<NonNullGraphType<EnumerationGraphType<TEnum>>>),
                "Specifies exact values to be matched."
            );
    }

    public sealed class EnumFilterGraphType<TEnum> : EnumFilterGraphType<TEnum, EnumFilter<TEnum>>
        where TEnum : Enum
    {
    }
}