﻿using GraphQL.Types;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Api.GraphQL.Filtering.Generic
{
    public abstract class EqualityFilterGraphType<TFilter, TFiltered, TScalar> : GenericFilterGraphType<TFilter>
        where TFilter : EqualityFilter<TFiltered>
        where TScalar : ScalarGraphType
    {
        protected EqualityFilterGraphType()
        {
            Builder.Field(x => x.EqualTo, typeof(TScalar), "Specifies exact value to match.");
            Builder.Field(x => x.NotEqualTo, typeof(TScalar), "Specifies exact value NOT to match.");

            var scalarListType = typeof(ListGraphType<NonNullGraphType<TScalar>>);
            Builder.Field(x => x.In, scalarListType, "Specifies list of exact values to match.");
            Builder.Field(x => x.NotIn, scalarListType, "Specifies list of exact values NOT to match.");
        }

        protected void IsNullField()
            => Builder.NullableField(x => x.IsNull, "Matches `null` value if `true` is specified. Matches not `null` values if `false` is specified.");
    }
}