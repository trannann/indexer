﻿using GraphQL.Types;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Api.GraphQL.Filtering.Generic
{
    public class StringEqualityFilterGraphType : EqualityFilterGraphType<StringFilter, string?, StringGraphType>
    {
    }

    public class StringFilterGraphType : StringEqualityFilterGraphType
    {
        public StringFilterGraphType()
        {
            Builder.NullableField(x => x.StartsWith, "Specifies prefix of the value to match.");
            Builder.NullableField(x => x.EndsWith, "Specifies suffix of the value to match.");
            Builder.NullableField(x => x.Like, "Specifies similar string to the value to match.");
        }
    }

    public sealed class NullableStringFilterGraphType : StringFilterGraphType
    {
        public NullableStringFilterGraphType()
            => IsNullField();
    }
}