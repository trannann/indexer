﻿using GraphQL.Types;
using System.Linq;
using TezosDomains.Api.GraphQL.Scalars;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;
using TezosDomains.Indexer.Tezos.Encoding;

namespace TezosDomains.Api.GraphQL.Filtering.Generic
{
    public class AddressFilterGraphType : EqualityFilterGraphType<StringFilter, string?, AddressScalarType>
    {
        public AddressFilterGraphType()
        {
            Builder.Field(x => x.StartsWith, typeof(AddressPrefixGraphType), "Specifies prefix of the address to match.");
        }
    }

    public sealed class NullableAddressFilterGraphType : AddressFilterGraphType
    {
        public NullableAddressFilterGraphType()
        {
            IsNullField();
        }
    }

    public sealed class AddressPrefixGraphType : EnumerationGraphType
    {
        public AddressPrefixGraphType()
        {
            Name = "AddressPrefix";
            Description = "Address prefix which specifies its kind.";

            foreach (var prefix in TezosEncoding.Address.Prefixes.Select(p => p.Base58))
                AddValue(prefix.ToUpper(), $"Prefix of {prefix} addresses.", prefix);
        }
    }
}