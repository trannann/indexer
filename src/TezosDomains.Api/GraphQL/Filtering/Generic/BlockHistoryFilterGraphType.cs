﻿using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Api.GraphQL.Filtering.Generic
{
    public class BlockHistoryFilterGraphType : GenericFilterGraphType<BlockHistoryFilter>
    {
        public BlockHistoryFilterGraphType()
        {
            Builder.NullableField(x => x.Level, "Specifies point in time (block level).");
            Builder.NullableField(x => x.Timestamp, "Specifies point in time (timestamp).");
        }
    }
}