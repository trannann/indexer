﻿using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Api.GraphQL.Filtering.Generic
{
    public sealed class StringArrayFilterGraphType : GenericFilterGraphType<StringArrayFilter>
    {
        public StringArrayFilterGraphType()
            => Builder.NullableField(x => x.Include, "Specifies value which should be included in the array.");
    }
}