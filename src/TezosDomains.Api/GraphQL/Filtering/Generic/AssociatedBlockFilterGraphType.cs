﻿using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Api.GraphQL.Filtering.Generic
{
    public sealed class AssociatedBlockFilterGraphType : GenericFilterGraphType<AssociatedBlockFilter>
    {
        public AssociatedBlockFilterGraphType()
        {
            Builder.NullableField(b => b.Hash, "Filters by block hash.", new StringEqualityFilterGraphType());
            Builder.NullableField(b => b.Level, "Filters by block level.", new IntFilterGraphType());
            Builder.NullableField(b => b.Timestamp, "Filters by block timestamp.", new DateTimeFilterGraphType());
        }
    }
}