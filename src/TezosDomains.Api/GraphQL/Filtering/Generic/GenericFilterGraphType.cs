﻿using TezosDomains.Api.GraphQL.Building;

namespace TezosDomains.Api.GraphQL.Filtering.Generic
{
    public abstract class GenericFilterGraphType<TFilter> : InputGraphType<TFilter>
        where TFilter : class
    {
        protected GenericFilterGraphType() : base(description: "Filter for particular property type of entity that it is associated with.")
        {
        }
    }
}