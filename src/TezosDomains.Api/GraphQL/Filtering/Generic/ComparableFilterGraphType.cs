﻿using GraphQL.Types;
using System;
using TezosDomains.Api.GraphQL.Scalars;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Api.GraphQL.Filtering.Generic
{
    public abstract class ComparableFilterGraphType<TFiltered, TScalar> : EqualityFilterGraphType<ComparableFilter<TFiltered>, TFiltered?, TScalar>
        where TFiltered : struct
        where TScalar : ScalarGraphType
    {
        protected ComparableFilterGraphType()
        {
            Builder.Field(x => x.LessThan, typeof(TScalar), "Matches values less than specified value.");
            Builder.Field(x => x.LessThanOrEqualTo, typeof(TScalar), "Matches values less than or equal to specified value.");
            Builder.Field(x => x.GreaterThan, typeof(TScalar), "Matches values greater than specified value.");
            Builder.Field(x => x.GreaterThanOrEqualTo, typeof(TScalar), "Matches values greater than or equal to specified value.");
        }
    }

    public sealed class IntFilterGraphType : ComparableFilterGraphType<int, IntGraphType>
    {
    }

    public sealed class MutezFilterGraphType : ComparableFilterGraphType<decimal, MutezScalarType>
    {
    }

    public class DateTimeFilterGraphType : ComparableFilterGraphType<DateTime, DateTimeGraphType>
    {
    }

    public sealed class NullableDateTimeFilterGraphType : DateTimeFilterGraphType
    {
        public NullableDateTimeFilterGraphType()
            => IsNullField();
    }
}