﻿using TezosDomains.Api.GraphQL.Filtering.Generic;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query.Filtering.Events;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Api.GraphQL.Filtering
{
    public sealed class EventsFilterGraphType : BaseFilterGraphType<EventsFilter>
    {
        public EventsFilterGraphType()
        {
            Builder.NullableField(e => e.Address, "Filters events by related address.", new AddressFilterGraphType());
            Builder.NullableField(e => e.DomainName, "Filters events by domain name.", new StringFilterGraphType());
            Builder.NullableField(e => e.Type, "Filters events by event type.", new EnumFilterGraphType<EventType, EnumAsStringFilter<EventType>>());
            Builder.NullableField(f => f.Block, "Filters by block in which the event was created.", new AssociatedBlockFilterGraphType());
        }
    }
}