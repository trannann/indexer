﻿using TezosDomains.Api.GraphQL.Filtering.Generic;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb.Query.Filtering.Auctions;

namespace TezosDomains.Api.GraphQL.Filtering
{
    public sealed class AuctionsFilterGraphType : BaseFilterGraphType<AuctionsFilter>
    {
        public AuctionsFilterGraphType()
        {
            Builder.NullableField(f => f.DomainName, "Filters auctions by their domain name.", new StringFilterGraphType());
            Builder.NullableField(x => x.State, "Filters auctions by their state.", new EnumFilterGraphType<AuctionState, AuctionStateFilter>());
            Builder.NullableField(x => x.HighestBidder, "Filters auctions by highest bidder address.", new AddressFilterGraphType());
            Builder.NullableField(x => x.Bidders, "Filters auctions by bidder addresses which placed at least one bid.", new StringArrayFilterGraphType());
            Builder.NullableField(x => x.EndsAtUtc, "Filters auctions by their end datetime.", new NullableDateTimeFilterGraphType());
            Builder.NullableField(x => x.BidCount, "Filters auctions by number of bids.", new IntFilterGraphType());
        }
    }
}