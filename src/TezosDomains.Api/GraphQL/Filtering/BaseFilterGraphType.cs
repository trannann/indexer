﻿using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Api.GraphQL.Filtering
{
    public abstract class BaseFilterGraphType<TFilter> : InputGraphType<TFilter>
        where TFilter : INestedFilter<TFilter>
    {
        protected BaseFilterGraphType() : base(description: "Filter for entities of particular type.")
        {
            var graphType = GetType().MakeNonNull().MakeList();
            Builder.Field(x => x.And, graphType, GetFieldDescription("AND"));
            Builder.Field(x => x.Or, graphType, GetFieldDescription("OR"));
        }

        private static string GetFieldDescription(string @operator)
            => $"Additional filters of the same type joined using {@operator} operator. It's joined using AND with other filter properties.";
    }
}