﻿using GraphQL.Types;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Api.GraphQL.Ordering
{
    public sealed class DocumentOrderGraphType<TDocument> : InputGraphType<DocumentOrder<TDocument>>
    {
        public DocumentOrderGraphType()
            : base(name: $"{typeof(TDocument).Name}Order", description: $"Ordering options for {typeof(TDocument).Name.ToHumanReadable().Pluralize()}.")
        {
            Builder.NullableEnumField(o => o.Direction, "The ordering direction.");
            Builder.Field(o => o.Field, typeof(NonNullGraphType<DocumentOrderFieldGraphType<TDocument>>), "The field to order by.");
        }
    }
}