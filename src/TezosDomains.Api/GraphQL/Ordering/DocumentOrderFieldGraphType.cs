﻿using System.Collections.Generic;
using System.Linq;
using GraphQL.Types;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Api.GraphQL.Ordering
{
    public sealed class DocumentOrderFieldGraphType<TDocument> : EnumerationGraphType
    {
        public DocumentOrderFieldGraphType(IEnumerable<DocumentOrderField<TDocument>> fields)
        {
            var documentName = (typeof(TDocument).GenericTypeArguments.FirstOrDefault() ?? typeof(TDocument)).Name;
            Name = $"{documentName}OrderField";
            Description = $"Properties by which {documentName}s can be ordered.";

            foreach (var field in fields)
                AddValue(field.DisplayName, field.Description, field);
        }
    }
}