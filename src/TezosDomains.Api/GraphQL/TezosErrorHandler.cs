﻿using GraphQL;
using GraphQL.Execution;
using Microsoft.Extensions.Logging;

namespace TezosDomains.Api.GraphQL
{
    public sealed class TezosErrorHandler
    {
        private readonly ILogger _logger;

        public TezosErrorHandler(ILogger<TezosErrorHandler> logger)
        {
            _logger = logger;
        }

        public void Handle(UnhandledExceptionContext context)
        {
            if (context.Exception is GraphQLInputException && context.FieldContext != null)
            {
                context.FieldContext.Errors.Add(new ExecutionError(context.Exception.Message));
                context.Exception = null;
            }
            else
            {
                var message = context.ErrorMessage ?? "Unhandled GraphQL error.";
                _logger.LogError(context.Exception, message);
            }
        }
    }
}