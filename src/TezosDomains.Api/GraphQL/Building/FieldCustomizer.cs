﻿using GraphQL.Builders;

namespace TezosDomains.Api.GraphQL.Building
{
    public interface IFieldCustomizer
    {
        IFieldCustomizer Name(string name);
        IFieldCustomizer Description(string description);
        IFieldCustomizer DeprecationReason(string deprecationReason);
    }

    public sealed class FieldCustomizer<TSource, TField> : IFieldCustomizer
    {
        private readonly FieldBuilder<TSource, TField> _builder;

        public FieldCustomizer(FieldBuilder<TSource, TField> builder)
            => _builder = builder;


        public IFieldCustomizer Name(string name)
        {
            _builder.Name(name);
            return this;
        }

        public IFieldCustomizer Description(string description)
        {
            _builder.Description(description);
            return this;
        }

        public IFieldCustomizer DeprecationReason(string deprecationReason)
        {
            _builder.DeprecationReason(deprecationReason);
            return this;
        }
    }

    public static class FieldCustomizerExtensions
    {
        public static IFieldCustomizer GetCustomizer<TSource, TField>(this FieldBuilder<TSource, TField> builder)
            => new FieldCustomizer<TSource, TField>(builder);
    }
}