﻿using GraphQL;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Arguments.Generic;

namespace TezosDomains.Api.GraphQL.Building
{
    public partial class GraphTypeBuilder<TSource>
    {
        // Dummy graphType so that all generics don't need to be specified.
        public IFieldCustomizer Field<TField, TGraph>(Expression<Func<TSource, TField>> expression, string description, TGraph? graphType = null)
            where TField : notnull
            where TGraph : ComplexGraphType<TField>
            => ExpressionField(expression, description, typeof(NonNullGraphType<TGraph>));

        // Dummy graphType so that all generics don't need to be specified.
        public IFieldCustomizer NullableField<TField, TGraph>(Expression<Func<TSource, TField?>> expression, string description, TGraph? graphType = null)
            where TGraph : ComplexGraphType<TField>
            => ExpressionField(expression, description, typeof(TGraph));

        public IFieldCustomizer Field<TField, TGraph>(
            string name,
            string description,
            Func<IResolveFieldContext<TSource>, Task<TField>> resolve,
            IEnumerable<IGraphQLArgument>? arguments = null
        )
            where TField : notnull
            where TGraph : ComplexGraphType<TField>
            => ResolvedField<NonNullGraphType<TGraph>>(name, description, resolve, arguments);

        public IFieldCustomizer NullableField<TField, TGraph>(
            string name,
            string description,
            Func<IResolveFieldContext<TSource>, Task<TField?>> resolve,
            IEnumerable<IGraphQLArgument>? arguments = null
        )
            where TGraph : ComplexGraphType<TField>
            => ResolvedField<TGraph>(name, description, resolve, arguments);
    }
}