﻿using GraphQL;
using GraphQL.Builders;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Arguments;
using TezosDomains.Api.GraphQL.Arguments.Generic;
using TezosDomains.Api.GraphQL.Output.Generic;
using TezosDomains.Api.GraphQL.Paging;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Building
{
    public partial class GraphTypeBuilder<TSource>
    {
        public ConnectionBuilder<TSource> Connection<TGraph>(string name)
            where TGraph : GraphType
        {
            var builder = _sourceGraphType.Connection<NonNullGraphType<TGraph>, NonNullEdgeType<TGraph>, NonNullConnectionType<TGraph>>();
            builder.FieldType.Type = builder.FieldType.Type.MakeNonNull();
            return builder.Name(name);
        }

        public void DocumentConnection<TDocument, TGraph, TFilter>(
            IRelayConnectionService connectionService,
            IQueryService<TDocument> queryService,
            DocumentOrder<TDocument> defaultOrder,
            Func<IResolveConnectionContext, TFilter, Task> prepareFilter,
            IEnumerable<IGraphQLArgument>? arguments = null
        )
            where TDocument : class, IDocument
            where TGraph : ComplexGraphType<TDocument>
            where TFilter : class, IDocumentFilter<TDocument>, new()
        {
            var documentsName = typeof(TDocument).Name.Pluralize().ToCamelCase();
            var whereArg = new GraphQLArgument<TFilter?, InputObjectGraphType<TFilter>>("where", description: "Complex object with filter conditions.");

            DocumentConnection<TDocument, TGraph>(
                connectionService,
                queryService,
                defaultOrder,
                name: documentsName,
                description: $"Finds all {documentsName.ToHumanReadable()} corresponding to specified filters.",
                arguments: arguments.NullToEmpty().Append(whereArg),
                createFilter: async context =>
                {
                    var filter = whereArg.GetValue(context) ?? new TFilter();
                    await prepareFilter(context, filter);
                    return filter;
                }
            );
        }

        public void DocumentConnection<TDocument, TGraph>(
            IRelayConnectionService connectionService,
            IQueryService<TDocument> queryService,
            DocumentOrder<TDocument> defaultOrder,
            string name,
            string description,
            Func<IResolveConnectionContext<TSource>, Task<IDocumentFilter<TDocument>>> createFilter,
            IEnumerable<IGraphQLArgument>? arguments = null
        )
            where TDocument : class, IDocument
            where TGraph : ComplexGraphType<TDocument>
        {
            var orderArg = new OrderArgument<TDocument>(defaultOrder);

            Connection<TGraph>(name)
                .Description(description)
                .Arguments(arguments.NullToEmpty().Append(orderArg))
                .Bidirectional()
                .ResolveAsync(
                    async context =>
                    {
                        var order = orderArg.GetValue(context);
                        var filter = await createFilter(context);

                        return await connectionService.ResolveAsync(context, filter, order, queryService);
                    }
                );
        }
    }
}