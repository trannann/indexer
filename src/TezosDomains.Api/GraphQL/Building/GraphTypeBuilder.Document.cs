﻿using GraphQL;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Arguments.Generic;
using TezosDomains.Api.GraphQL.Output;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Building
{
    public partial class GraphTypeBuilder<TSource>
    {
        public IFieldCustomizer DocumentByIdField<TDocument, TGraph>(
            IQueryService<TDocument> queryService,
            Func<IResolveFieldContext, string, IDocumentFilter<TDocument>> createFilter,
            IEnumerable<IGraphQLArgument>? arguments = null
        )
            where TDocument : class, IDocument
            where TGraph : ComplexGraphType<TDocument>
        {
            var documentName = typeof(TDocument).Name.ToCamelCase();
            var idFieldName = DocumentHelper.GetIdExpression<TDocument>().NameOf().ToCamelCase();
            var idArg = new RequiredStringArgument(idFieldName, description: $"Unique {idFieldName} of {documentName.ToHumanReadable()} to fetch.");

            return NullableField<TDocument, TGraph>(
                name: documentName,
                description: $"Finds a single {documentName.ToHumanReadable()} by it's unique {idFieldName} and specified filters.",
                arguments: arguments.NullToEmpty().Append(idArg),
                resolve: context =>
                {
                    var id = idArg.GetValue(context);
                    var filter = createFilter(context, id);
                    return queryService.FindSingleAsync(filter, context.CancellationToken);
                }
            );
        }

        public IFieldCustomizer ReverseRecordField(string name, string description, Func<IResolveFieldContext<TSource>, Task<ReverseRecord?>> resolve)
            => NullableField<ReverseRecord, ReverseRecordGraphType>(name, description, resolve);

        public IFieldCustomizer ReverseRecordFieldForAddress(Expression<Func<TSource, string?>> addressExpression, IQueryService<ReverseRecord> queryService)
        {
            var getAddress = addressExpression.Compile();
            var addressPropertyName = addressExpression.NameOf().ToCamelCase();

            return ReverseRecordField(
                name: $"{addressPropertyName}ReverseRecord",
                description: $"Reverse record corresponding to {addressPropertyName} address.",
                resolve: context =>
                {
                    var address = getAddress(context.Source);
                    return queryService.FindSingleByIdAndParentArguments(address, context);
                }
            );
        }
    }
}