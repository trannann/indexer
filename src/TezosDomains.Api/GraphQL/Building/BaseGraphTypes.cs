﻿using GraphQL.Types;

namespace TezosDomains.Api.GraphQL.Building
{
    /// <summary>Common base for output object GraphQL types.</summary>
    public abstract class OutputGraphType<TSource> : ObjectGraphType<TSource>
        where TSource : notnull
    {
        protected OutputGraphType(string description, string? name = null)
            => this.SetNameAndDescription(description, name);

        protected GraphTypeBuilder<TSource> Builder => this.GetBuilder();
    }

    /// <summary>Common base for input object GraphQL types.</summary>
    public abstract class InputGraphType<TSource> : InputObjectGraphType<TSource>
        where TSource : notnull
    {
        protected InputGraphType(string description, string? name = null)
            => this.SetNameAndDescription(description, name);

        protected GraphTypeBuilder<TSource> Builder => this.GetBuilder();
    }

    public static class BaseGraphTypeExtensions
    {
        public static void SetNameAndDescription(this GraphType graphType, string description, string? name = null)
        {
            graphType.Name = name ?? graphType.GetType().GetGraphName();
            graphType.Description = description;
        }
    }
}