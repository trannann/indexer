﻿using GraphQL;
using GraphQL.Types;
using System;
using System.Linq.Expressions;

namespace TezosDomains.Api.GraphQL.Building
{
    public partial class GraphTypeBuilder<TSource>
    {
        public IFieldCustomizer NullableEnumField<TEnum>(Expression<Func<TSource, TEnum>> expression, string description)
            where TEnum : Enum
            => Field(expression, typeof(EnumerationGraphType<TEnum>), description);

        public IFieldCustomizer EnumField<TEnum>(string name, string description, Func<IResolveFieldContext<TSource>, TEnum> resolve)
            where TEnum : Enum
            => ResolvedField<NonNullGraphType<EnumerationGraphType<TEnum>>>(name, description, context => resolve(context));
    }
}