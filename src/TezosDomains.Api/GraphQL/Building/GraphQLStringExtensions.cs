﻿using GraphQL.Utilities;

namespace TezosDomains.Api.GraphQL.Building
{
    public static class GraphQLStringExtensions
    {
        public static string ToHumanReadable(this string str)
            => StringUtils.ChangeCase(str, sep: " ", word => word.ToLower());

        public static string Pluralize(this string str)
            => str + (str.EndsWith('s') ? "es" : "s");
    }
}