﻿using GraphQL.Types;
using System;
using System.Linq.Expressions;
using TezosDomains.Api.GraphQL.Scalars;

namespace TezosDomains.Api.GraphQL.Building
{
    public partial class GraphTypeBuilder<TSource>
    {
        public IFieldCustomizer AddressField(Expression<Func<TSource, string>> expression, string description)
            => Field(expression, typeof(NonNullGraphType<AddressScalarType>), description);

        public IFieldCustomizer NullableAddressField(Expression<Func<TSource, string?>> expression, string description)
            => Field(expression, typeof(AddressScalarType), description);

        public IFieldCustomizer MutezField(Expression<Func<TSource, decimal>> expression, string description)
            => Field(expression, typeof(NonNullGraphType<MutezScalarType>), description);

        public IFieldCustomizer NullableMutezField(Expression<Func<TSource, decimal?>> expression, string description)
            => Field(expression, typeof(MutezScalarType), description);
    }
}