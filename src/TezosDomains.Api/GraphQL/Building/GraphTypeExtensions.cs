﻿using GraphQL.Types;
using System;

namespace TezosDomains.Api.GraphQL.Building
{
    public static class GraphTypeExtensions
    {
        public static Type MakeNonNull(this Type graphType)
            => typeof(NonNullGraphType<>).MakeGenericType(Guard(graphType));

        public static Type MakeList(this Type itemGraphType)
            => typeof(ListGraphType<>).MakeGenericType(Guard(itemGraphType));

        public static string GetGraphName(this Type graphType)
            => Guard(graphType).Name.Replace("GraphType", "").Replace("InterfaceType", "");

        private static Type Guard(Type graphType)
            => typeof(GraphType).IsAssignableFrom(graphType)
                ? graphType
                : throw new ArgumentException($"{graphType} isn't {typeof(GraphType)}.");
    }
}