﻿using GraphQL;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using TezosDomains.Api.GraphQL.Output.Generic;

namespace TezosDomains.Api.GraphQL.Building
{
    public partial class GraphTypeBuilder<TSource>
    {
        public IFieldCustomizer CollectionField<TItem, TItemGraph>(
            string name,
            string description,
            Func<IResolveFieldContext<TSource>, IEnumerable<TItem>> resolve
        )
            where TItemGraph : ObjectGraphType<TItem>
            => ResolvedField<NonNullGraphType<ListGraphType<NonNullGraphType<TItemGraph>>>>(name, description, resolve);

        public IFieldCustomizer CollectionField<TItem, TItemGraph>(Expression<Func<TSource, IEnumerable<TItem>>> expression, string description)
            where TItemGraph : ObjectGraphType<TItem>
        {
            var getProperty = expression.Compile();
            return CollectionField<TItem, TItemGraph>(
                name: expression.NameOf(),
                description,
                resolve: context => getProperty(context.Source)
            );
        }

        public IFieldCustomizer RawDataField(Expression<Func<TSource, IEnumerable<KeyValuePair<string, string>>>> expression, string description)
            => CollectionField<KeyValuePair<string, string>, DataItemGraphType>(expression, description);
    }
}