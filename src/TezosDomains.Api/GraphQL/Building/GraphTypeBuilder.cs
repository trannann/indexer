﻿using GraphQL;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using TezosDomains.Api.GraphQL.Arguments.Generic;

namespace TezosDomains.Api.GraphQL.Building
{
    public static class GraphTypeBuilder
    {
        public static GraphTypeBuilder<TSource> GetBuilder<TSource>(this ComplexGraphType<TSource> sourceGraphType)
            => new GraphTypeBuilder<TSource>(sourceGraphType);
    }

    public partial class GraphTypeBuilder<TSource>
    {
        private readonly ComplexGraphType<TSource> _sourceGraphType;

        public GraphTypeBuilder(ComplexGraphType<TSource> sourceGraphType)
            => _sourceGraphType = sourceGraphType;

        public IFieldCustomizer Field<TField>(Expression<Func<TSource, TField>> expression, Type graphType, string description)
            => ExpressionField(expression, description, graphType);

        public IFieldCustomizer Field<TField>(Expression<Func<TSource, TField>> expression, string description)
            where TField : notnull
            => ExpressionField(expression, description, nullable: false);

        public IFieldCustomizer NullableField<TField>(Expression<Func<TSource, TField>> expression, string description)
            => ExpressionField(expression, description, nullable: true);

        private IFieldCustomizer ExpressionField<TField>(
            Expression<Func<TSource, TField>> expression,
            string description,
            Type? graphType = null,
            bool nullable = false
        )
            => _sourceGraphType.Field(expression, nullable, graphType)
                .Description(description)
                .GetCustomizer();

        private IFieldCustomizer ResolvedField<TGraph>(
            string name,
            string description,
            Func<IResolveFieldContext<TSource>, object> resolve,
            IEnumerable<IGraphQLArgument>? arguments = null
        )
            where TGraph : GraphType
            => _sourceGraphType.Field<TGraph, object>(name)
                .Description(description)
                .Arguments(arguments)
                .Resolve(resolve)
                .GetCustomizer();
    }
}