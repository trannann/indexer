﻿using GraphQL.Builders;
using GraphQL.Types.Relay.DataObjects;
using System;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Arguments;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Api.GraphQL.Paging
{
    public interface IRelayConnectionService
    {
        Task<Connection<TDocument>> ResolveAsync<TDocument>(
            IResolveConnectionContext context,
            IDocumentFilter<TDocument> filter,
            DocumentOrder<TDocument> order,
            IQueryService<TDocument> queryService
        )
            where TDocument : class, IDocument;
    }

    public sealed class RelayConnectionService : IRelayConnectionService
    {
        private readonly IPageSizeCalculator _pageSizeCalculator;

        public RelayConnectionService(IPageSizeCalculator pageSizeCalculator)
        {
            _pageSizeCalculator = pageSizeCalculator;
        }

        public Task<Connection<TDocument>> ResolveAsync<TDocument>(
            IResolveConnectionContext context,
            IDocumentFilter<TDocument> filter,
            DocumentOrder<TDocument> order,
            IQueryService<TDocument> queryService
        )
            where TDocument : class, IDocument
        {
            ValidatePagingParameters(context);

            return context.Before == null && context.Last == null
                ? ResolveUnidirectionalAsync(context.After, context.First, argNames: ("after", "first"))
                : ResolveReversedAsync();

            async Task<Connection<TDocument>> ResolveReversedAsync()
            {
                order = new DocumentOrder<TDocument>(order.Field, order.Direction.Reverse());
                var connection = await ResolveUnidirectionalAsync(context.Before, context.Last, argNames: ("before", "last"));

                connection.Edges.Reverse();
                (connection.PageInfo.StartCursor, connection.PageInfo.EndCursor) = (connection.PageInfo.EndCursor, connection.PageInfo.StartCursor);
                (connection.PageInfo.HasPreviousPage, connection.PageInfo.HasNextPage) = (connection.PageInfo.HasNextPage, connection.PageInfo.HasPreviousPage);

                return connection;
            }

            async Task<Connection<TDocument>> ResolveUnidirectionalAsync(string? after, int? first, (string After, string First) argNames)
            {
                var afterIndex = after != null ? ParsePageIndex(after, argNames.After, order) : null;
                var requestedCount = _pageSizeCalculator.Calculate(first, argNames.First);
                var query = new DocumentQuery<TDocument>(filter, order, afterIndex, takeCount: requestedCount + 1);

                var documentsTask = queryService.FindAllAsync(query, context.CancellationToken);
                var totalCount = context.SubFields.ContainsKey("totalCount")
                    ? await queryService.CountAsync(query.Filter, context.CancellationToken)
                    : 0;
                var documents = await documentsTask; // Runs in parallel with totalCount task

                var hasNextPage = false;
                if (documents.Count > requestedCount)
                {
                    hasNextPage = true;
                    documents.RemoveAt(documents.Count - 1);
                }

                var edges = documents.ConvertAll(
                    document =>
                    {
                        var orderField = order.Field.DisplayName;
                        var orderValue = order.Field.PrimaryAccessor(document);
                        var secondaryValue = order.Field.SecondaryAccessor?.Invoke(document);
                        return new Edge<TDocument>
                        {
                            Cursor = CursorConverter.ToCursorId(new PagingIndex(orderField, orderValue, secondaryValue)),
                            Node = document
                        };
                    }
                );

                return new Connection<TDocument>
                {
                    Edges = edges,
                    PageInfo = new PageInfo
                    {
                        StartCursor = edges.FirstOrDefault()?.Cursor,
                        EndCursor = edges.LastOrDefault()?.Cursor,
                        HasPreviousPage = after != null,
                        HasNextPage = hasNextPage,
                    },
                    TotalCount = (int) totalCount,
                };
            }
        }

        private static PagingIndex ParsePageIndex<TDocument>(string cursorId, string argName, DocumentOrder<TDocument> order)
        {
            try
            {
                var index = CursorConverter.ToPagingIndex(cursorId);

                if (index.OrderFieldName != order.Field.DisplayName)
                    throw new GraphQLInputException(
                        $"its order '{index.OrderFieldName}' must be as same as '{order.Field.DisplayName}' which is specified in '{OrderArgument.Name}' argument (may be default)"
                    );

                if (index.OrderFieldValue == null && !order.Field.IsPrimaryNullable)
                    throw new NullReferenceException();

                if (index.SecondaryValue == null && order.Field.SecondaryExpression != null)
                    throw new NullReferenceException();

                return index;
            }
            catch (Exception ex)
            {
                var reason = (ex as GraphQLInputException)?.Message ?? "it can't be deserialized";
                throw new GraphQLInputException($"Invalid cursor '{cursorId}' specified in argument '{argName}' because {reason}. Just don't use it anymore.");
            }
        }

        private static void ValidatePagingParameters(IResolveConnectionContext context)
        {
            if (context.After != null && context.Before != null)
                throw new GraphQLInputException("Arguments 'after' and 'before' cannot be specified at the same time.");
            if (context.After != null && context.Last != null)
                throw new GraphQLInputException("If argument 'after' is specified then 'last' cannot be specified. Use 'first' instead.");
            if (context.Before != null && context.First != null)
                throw new GraphQLInputException("If argument 'before' is specified then 'first' cannot be specified. Use 'last' instead.");
        }
    }
}