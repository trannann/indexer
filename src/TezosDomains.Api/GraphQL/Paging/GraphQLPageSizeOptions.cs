﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TezosDomains.Api.GraphQL.Paging
{
    public interface IGraphQLPageSizeOptions
    {
        int Default { get; }
        int Max { get; }
    }

    public sealed class GraphQLPageSizeOptions : IGraphQLPageSizeOptions, IValidatableObject
    {
        public int Default { get; set; } = 10;
        public int Max { get; set; } = 50;

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Default <= 0)
                yield return new ValidationResult($"Default must be greater than 0 but it's {Default}.", new[] { nameof(Default) });

            if (Max < Default)
                yield return new ValidationResult($"Max must be greater than or equal to Default {Default} but it's {Max}.", new[] { nameof(Max) });
        }
    }
}