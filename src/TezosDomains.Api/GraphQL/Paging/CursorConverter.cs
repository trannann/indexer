﻿using System;
using System.Text;
using Newtonsoft.Json;
using TezosDomains.Data.MongoDb.Query.Abstract;

namespace TezosDomains.Api.GraphQL.Paging
{
    public static class CursorConverter
    {
        private static readonly JsonSerializerSettings JsonSettings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };

        public static string ToCursorId(PagingIndex index)
        {
            var dto = new Dto { F = index.OrderFieldName, V = index.OrderFieldValue, S = index.SecondaryValue };
            var json = JsonConvert.SerializeObject(dto, JsonSettings);

            return Convert.ToBase64String(Encoding.UTF8.GetBytes(json));
        }

        public static PagingIndex ToPagingIndex(string cursorId)
        {
            var json = Encoding.UTF8.GetString(Convert.FromBase64String(cursorId));
            var dto = JsonConvert.DeserializeObject<Dto>(json, JsonSettings);

            return new PagingIndex(orderFieldName: dto!.F!, orderFieldValue: dto.V, secondaryValue: dto.S);
        }

        // Makes cursors much shorter
        private sealed class Dto
        {
            public string? F { get; set; }
            public object? V { get; set; }
            public object? S { get; set; }
        }
    }
}