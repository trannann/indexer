﻿namespace TezosDomains.Api.GraphQL.Paging
{
    public interface IPageSizeCalculator
    {
        int Calculate(int? value, string argName);
    }

    public sealed class PageSizeCalculator : IPageSizeCalculator
    {
        private readonly IGraphQLPageSizeOptions _options;

        public PageSizeCalculator(IGraphQLPageSizeOptions options)
        {
            _options = options;
        }

        public int Calculate(int? value, string argName)
        {
            if (value == null)
                return _options.Default;
            if (value <= 0)
                throw new GraphQLInputException(argName, value, mustBe: "greater than 0");
            if (value > _options.Max)
                throw new GraphQLInputException(argName, value, mustBe: $"less than or equal to max {_options.Max}");

            return value.Value;
        }
    }
}