﻿using GraphQL;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Arguments;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Filtering;

namespace TezosDomains.Api.GraphQL
{
    public static class QueryServiceExtensions
    {
        public static Task<TRecord?> FindSingleByIdAndParentArguments<TRecord>(
            this IQueryService<TRecord> queryService,
            string? id,
            IResolveFieldContext context
        )
            where TRecord : class, IRecord
        {
            if (id == null)
                return Task.FromResult<TRecord?>(result: null);

            var filter = new SingleRecordFilter<TRecord>(
                id,
                validity: ValidityArgument.Instance.GetParentValue(context),
                atBlock: AtBlockArgument.Instance.GetParentValue(context)
            );
            return queryService.FindSingleAsync(filter, context.CancellationToken);
        }
    }
}