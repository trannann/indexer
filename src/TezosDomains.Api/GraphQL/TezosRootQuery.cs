﻿using System.Collections.Generic;
using TezosDomains.Api.GraphQL.Building;

namespace TezosDomains.Api.GraphQL
{
    public interface IRootQueryProvider
    {
        void RegisterQueries(GraphTypeBuilder<object> builder);
    }

    public class TezosRootQuery : OutputGraphType<object>
    {
        public TezosRootQuery(IEnumerable<IRootQueryProvider> queryProviders)
            : base(name: "Query", description: "Root node with entry queries.")
        {
            foreach (var provider in queryProviders)
                provider.RegisterQueries(Builder);
        }
    }
}