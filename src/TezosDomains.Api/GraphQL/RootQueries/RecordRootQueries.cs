﻿using GraphQL;
using GraphQL.Types;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Arguments;
using TezosDomains.Api.GraphQL.Arguments.Generic;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Paging;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Data.MongoDb.Query.Filtering;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;

namespace TezosDomains.Api.GraphQL.RootQueries
{
    public sealed class RecordRootQueries<TRecord, TGraph, TFilter> : IRootQueryProvider
        where TRecord : class, IRecord
        where TGraph : ObjectGraphType<TRecord>
        where TFilter : class, IRecordFilter<TRecord>, new()
    {
        private readonly IQueryService<TRecord> _queryService;
        private readonly IRelayConnectionService _connectionService;
        private readonly DocumentOrder<TRecord> _defaultOrder;

        public RecordRootQueries(IQueryService<TRecord> queryService, IRelayConnectionService connectionService, DocumentOrder<TRecord> defaultOrder)
        {
            _queryService = queryService;
            _connectionService = connectionService;
            _defaultOrder = defaultOrder;
        }

        public void RegisterQueries(GraphTypeBuilder<object> builder)
        {
            builder.DocumentByIdField<TRecord, TGraph>(
                _queryService,
                arguments: new IGraphQLArgument[] { AtBlockArgument.Instance, ValidityArgument.Instance },
                createFilter: (context, id) =>
                {
                    var filter = new SingleRecordFilter<TRecord>(
                        id,
                        validity: ValidityArgument.Instance.GetValue(context),
                        atBlock: AtBlockArgument.Instance.GetValue(context)
                    );

                    PassArgumentsForChildren(context, filter);
                    return filter;
                }
            );

            builder.DocumentConnection<TRecord, TGraph, TFilter>(
                _connectionService,
                _queryService,
                _defaultOrder,
                arguments: new[] { AtBlockArgument.Instance },
                prepareFilter: (context, filter) =>
                {
                    filter.AtBlock = AtBlockArgument.Instance.GetValue(context);

                    PassArgumentsForChildren(context, filter);
                    return Task.CompletedTask;
                }
            );
        }

        private static void PassArgumentsForChildren(IResolveFieldContext context, IRecordFilter<TRecord> filter)
        {
            AtBlockArgument.Instance.SetValueForChildren(context, filter.AtBlock);
            ValidityArgument.Instance.SetValueForChildren(context, filter.Validity);
        }
    }
}