﻿using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Arguments;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Output.Events;
using TezosDomains.Api.GraphQL.Paging;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Filtering.Events;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;
using TezosDomains.Data.MongoDb.Query.Ordering;

namespace TezosDomains.Api.GraphQL.RootQueries
{
    public sealed class EventRootQueries : IRootQueryProvider
    {
        private readonly IQueryService<Event> _queryService;
        private readonly IRelayConnectionService _connectionService;

        public EventRootQueries(IQueryService<Event> queryService, IRelayConnectionService connectionService)
        {
            _queryService = queryService;
            _connectionService = connectionService;
        }

        public void RegisterQueries(GraphTypeBuilder<object> builder)
            => builder.DocumentConnection<Event, EventInterfaceType, EventsFilter>(
                _connectionService,
                _queryService,
                EventsOrder.Default,
                prepareFilter: (context, filter) =>
                {
                    // Used to get nested reverse records as VALID and at the latest block.
                    AtBlockArgument.Instance.SetValueForChildren(context, value: null);
                    ValidityArgument.Instance.SetValueForChildren(context, value: RecordValidity.Valid);
                    return Task.CompletedTask;
                }
            );
    }
}