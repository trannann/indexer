﻿using GraphQL;
using System.Linq;
using TezosDomains.Api.GraphQL.Arguments;
using TezosDomains.Api.GraphQL.Arguments.Generic;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Output.Auctions;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.RootQueries
{
    public sealed class BidderBalanceRootQueries : IRootQueryProvider
    {
        private readonly IQueryService<BidderBalances> _bidderBalancesQueryService;

        public BidderBalanceRootQueries(IQueryService<BidderBalances> bidderBalancesQueryService)
            => _bidderBalancesQueryService = bidderBalancesQueryService;

        public void RegisterQueries(GraphTypeBuilder<object> builder)
        {
            var addressArg = new RequiredStringArgument("address", description: "The address for which current bidder balances should be obtained.");

            builder.Field<BidderBalanceStatus, BidderBalancesGraphType>(
                name: nameof(BidderBalances).ToCamelCase(),
                description: "Finds bidder balances for given address.",
                arguments: new IGraphQLArgument[] { addressArg, AtBlockArgument.Instance },
                resolve: async context =>
                {
                    var address = addressArg.GetValue(context);
                    var atBlock = AtBlockArgument.Instance.GetValue(context);
                    var filter = new SingleDocumentWithHistoryFilter<BidderBalances>(address, atBlock);

                    var bidder = await _bidderBalancesQueryService.FindSingleAsync(filter, context.CancellationToken);
                    return new BidderBalanceStatus(address, (bidder?.Balances).NullToEmpty().Select(b => new BidderBalance(b.Key, b.Value)).ToList());
                }
            );
        }
    }
}