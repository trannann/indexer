﻿using GraphQL;
using GraphQL.Types;
using System;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Arguments;
using TezosDomains.Api.GraphQL.Arguments.Generic;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Output.Auctions;
using TezosDomains.Api.GraphQL.Paging;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Filtering.Auctions;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;
using TezosDomains.Data.MongoDb.Query.Ordering;

namespace TezosDomains.Api.GraphQL.RootQueries
{
    public sealed class AuctionRootQueries : IRootQueryProvider
    {
        private readonly IQueryService<Auction> _auctionQueryService;
        private readonly IBlockQueryService _blockQueryService;
        private readonly IRelayConnectionService _connectionService;

        public AuctionRootQueries(IQueryService<Auction> auctionQueryService, IBlockQueryService blockQueryService, IRelayConnectionService connectionService)
        {
            _auctionQueryService = auctionQueryService;
            _blockQueryService = blockQueryService;
            _connectionService = connectionService;
        }

        public void RegisterQueries(GraphTypeBuilder<object> builder)
        {
            var domainNameArg = new RequiredStringArgument("domainName", "The domain name corresponding to the auction.");
            var startedAtLevelArg = new GraphQLArgument<int, NonNullGraphType<IntGraphType>>("startedAtLevel", "The auction's first bid block level.");

            builder.NullableField<Auction, AuctionGraphType>(
                name: "auction",
                description: "Finds a single auction by the domain's name and the first bid block level.",
                arguments: new IGraphQLArgument[] { domainNameArg, startedAtLevelArg, AtBlockArgument.Instance },
                resolve: async context =>
                {
                    var id = Auction.GenerateId(domainNameArg.GetValue(context), startedAtLevelArg.GetValue(context));
                    var filter = new SingleDocumentWithHistoryFilter<Auction>(id, atBlock: AtBlockArgument.Instance.GetValue(context));

                    await PassAuctionArgumentToChildren(context, filter);
                    return await _auctionQueryService.FindSingleAsync(filter, context.CancellationToken);
                }
            );

            builder.DocumentConnection<Auction, AuctionGraphType, AuctionsFilter>(
                _connectionService,
                _auctionQueryService,
                AuctionsOrder.Default,
                arguments: new[] { AtBlockArgument.Instance },
                prepareFilter: async (context, filter) =>
                {
                    filter.AtBlock = AtBlockArgument.Instance.GetValue(context);
                    await PassAuctionArgumentToChildren(context, filter);
                }
            );

            builder.NullableField<Auction, AuctionGraphType>(
                name: "currentAuction",
                description: "Finds a single auction by the domain's name in time.",
                arguments: new IGraphQLArgument[] { domainNameArg, AtBlockArgument.Instance },
                resolve: async context =>
                {
                    var filter = new CurrentAuctionFilter(domainNameArg.GetValue(context), atBlock: AtBlockArgument.Instance.GetValue(context));

                    await PassAuctionArgumentToChildren(context, filter);
                    return await _auctionQueryService.FindSingleAsync(filter, context.CancellationToken);
                }
            );
        }

        private async Task PassAuctionArgumentToChildren(IResolveFieldContext context, IDocumentWithHistoryFilter<Auction> filter)
        {
            // Used to get nested reverse records as VALID and at the same block.
            AtBlockArgument.Instance.SetValueForChildren(context, filter.AtBlock);
            ValidityArgument.Instance.SetValueForChildren(context, value: RecordValidity.Valid);

            // Used to determine auction state.
            var atTimestamp = await ResolveAtTimestampAsync(context, filter.AtBlock);
            context.SetAuctionAtTimestampForChildren(atTimestamp);
        }

        private async Task<DateTime> ResolveAtTimestampAsync(IResolveFieldContext context, BlockHistoryFilter? atBlock)
        {
            if (atBlock?.Level != null)
            {
                var block = await _blockQueryService.FindAsync(atBlock.Level.Value, context.CancellationToken);
                return block?.Timestamp
                       ?? throw new GraphQLInputException($"block.level {atBlock.Level} is invalid. It is either too far in past or in future.");
            }

            return atBlock?.Timestamp ?? DateTime.UtcNow;
        }
    }
}