﻿using GraphQL.Types;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Arguments.Generic;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Output;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.RootQueries
{
    public sealed class BlockRootQueries : IRootQueryProvider
    {
        private readonly IBlockQueryService _blockQueryService;
        private int? _blockStartLevel;

        public BlockRootQueries(IBlockQueryService blockQueryService)
            => _blockQueryService = blockQueryService;

        public void RegisterQueries(GraphTypeBuilder<object> builder)
        {
            var hashArg = new GraphQLArgument<string?, StringGraphType>(name: "hash", description: "The hash of the block to fetch.");
            var levelArg = new GraphQLArgument<int?, IntGraphType>(name: "level", description: "The level of the block to fetch.");

            builder.NullableField<BlockSlim, BlockGraphType>(
                name: "block",
                description: $"Finds the latest indexed block (head) or a specific block by its {hashArg.Name}, {levelArg.Name} when supplied."
                             + " Note: Only blocks starting with the origination of the smart contracts are available.",
                arguments: new IGraphQLArgument[] { hashArg, levelArg },
                resolve: async context =>
                {
                    var hash = hashArg.GetValue(context)?.WhiteSpaceToNull();
                    var level = levelArg.GetValue(context);

                    var fullBlock = await ((level, hash) switch
                    {
                        (level: null, string validHash) => _blockQueryService.FindAsync(validHash, context.CancellationToken),
                        (int validLevel, var anyHash) => FindByLevel(validLevel, anyHash, levelArg.Name, context.CancellationToken),
                        _ => _blockQueryService.GetLatestOrNullAsync(context.CancellationToken),
                    });
                    return fullBlock?.ToSlim();
                }
            );
        }

        private async Task<Block?> FindByLevel(int level, string? hash, string levelArgName, CancellationToken cancellationToken)
        {
            _blockStartLevel ??= await _blockQueryService.GetStartLevelOrNullAsync(cancellationToken);
            if (level < _blockStartLevel)
            {
                var mustBe = $"greater than or equal to {_blockStartLevel} because older blocks are not indexed";
                throw new GraphQLInputException(levelArgName, level, mustBe);
            }

            var block = await _blockQueryService.FindAsync(level, cancellationToken);
            return hash == null || hash == block?.Hash ? block : null;
        }
    }
}