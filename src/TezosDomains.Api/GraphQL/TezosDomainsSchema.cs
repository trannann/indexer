﻿using GraphQL.Types;
using GraphQL.Utilities;
using System;
using TezosDomains.Api.GraphQL.Output.Events;

namespace TezosDomains.Api.GraphQL
{
    public class TezosDomainsSchema : Schema
    {
        public TezosDomainsSchema(IServiceProvider serviceProvider)
            : base(serviceProvider)
        {
            Query = serviceProvider.GetRequiredService<TezosRootQuery>();

            //events
            //the registration order is important - first register child type then parent
            RegisterType<AuctionBidEventGraphType>();
            RegisterType<AuctionSettleEventGraphType>();
            RegisterType<AuctionWithdrawEventGraphType>();
            RegisterType<AuctionEndEventGraphType>();
            RegisterType<DomainBuyEventGraphType>();
            RegisterType<DomainRenewEventGraphType>();
            RegisterType<DomainCommitEventGraphType>();
            RegisterType<DomainSetChildRecordEventGraphType>();
            RegisterType<DomainUpdateEventGraphType>();
            RegisterType<ReverseRecordUpdateEventGraphType>();
            RegisterType<ReverseRecordClaimEventGraphType>();
        }
    }
}