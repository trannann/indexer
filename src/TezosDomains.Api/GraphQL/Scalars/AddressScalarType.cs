﻿using GraphQL.Language.AST;
using GraphQL.Types;
using TezosDomains.Indexer.Tezos.Encoding;

namespace TezosDomains.Api.GraphQL.Scalars
{
    public sealed class AddressScalarType : ScalarGraphType
    {
        public AddressScalarType()
        {
            Name = "Address";
            Description = "Address identifier - " + TezosEncoding.Address.Description;
        }

        public override object? ParseLiteral(IValue value)
            => value is StringValue str ? ValidateAddress(str.Value) : null;

        public override object ParseValue(object value)
            => ValidateAddress(value);

        public override object Serialize(object value)
            => ValidateAddress(value);

        public static object ValidateAddress(object value)
        {
            TezosEncoding.Address.ConvertFromBase58(value?.ToString() ?? "");
            return value!;
        }
    }
}