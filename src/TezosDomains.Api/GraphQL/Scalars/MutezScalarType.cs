﻿using GraphQL.Language.AST;
using GraphQL.Types;
using System;
using System.Globalization;

namespace TezosDomains.Api.GraphQL.Scalars
{
    public sealed class MutezScalarType : ScalarGraphType
    {
        public MutezScalarType()
        {
            Name = "Mutez";
            Description = "Micro TEZ, the smallest monetary unit in Tezos.";
        }

        public override object? ParseLiteral(IValue value)
            => value is StringValue str ? ParseValue(str.Value) : null;

        public override object ParseValue(object value)
            => decimal.Parse((string) value);

        public override object Serialize(object rawValue)
        {
            var value = Math.Floor((decimal) rawValue); // Smaller than mutez is only for internal calculations -> floor
            return value.ToString(CultureInfo.InvariantCulture);
        }
    }
}