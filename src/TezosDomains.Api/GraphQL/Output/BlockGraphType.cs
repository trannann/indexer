﻿using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Data.Models;

namespace TezosDomains.Api.GraphQL.Output
{
    public sealed class BlockGraphType : OutputGraphType<BlockSlim>
    {
        public BlockGraphType() : base(description: "Block in the chain.")
        {
            Builder.Field(b => b.Hash, "Block hash.");
            Builder.Field(b => b.Level, "Block level.");
            Builder.Field(b => b.Timestamp, "Block timestamp.");
        }
    }
}