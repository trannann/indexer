﻿using TezosDomains.Api.GraphQL.Building;

namespace TezosDomains.Api.GraphQL.Output.Auctions
{
    public sealed record BidderBalance(
        string TldName,
        decimal Balance
    );

    public sealed class BalanceGraphType : OutputGraphType<BidderBalance>
    {
        public BalanceGraphType() : base(description: "A user balance for the specific TLD registrar.")
        {
            Builder.Field(i => i.TldName, "The tld registrar name.");
            Builder.MutezField(i => i.Balance, "The withdrawable balance.");
        }
    }
}