﻿using GraphQL.Types;
using System.Linq;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Arguments;
using TezosDomains.Api.GraphQL.Output.Generic;
using TezosDomains.Data.Models.Auctions;

namespace TezosDomains.Api.GraphQL.Output.Auctions
{
    public sealed class AuctionGraphType : DocumentGraphType<Auction>
    {
        public AuctionGraphType() : base(description: "Domain auction.")
        {
            Builder.Field(h => h.DomainName, "Domain name.");
            Builder.Field(h => h.EndsAtUtc, "Auction end datetime.");
            Builder.Field(h => h.OwnedUntilUtc, "DateTime until an auction winner can claim the domain.");
            Builder.Field(h => h.BidCount, "Number of bids for this auction.");
            Builder.Field<BidWithAuction, BidGraphType>(
                name: "highestBid",
                description: "The current highest bid.",
                resolve: context => Task.FromResult(new BidWithAuction(context.Source.Bids.First(), context.Source))
            );
            Builder.EnumField(
                name: "state",
                description: "Auction state.",
                resolve: context =>
                {
                    var at = context.GetParentAuctionAtTimestamp();
                    return context.Source.GetState(at);
                }
            );

            Builder.CollectionField<BidWithAuction, BidGraphType>(
                name: "bids",
                description: "Auction bids.",
                resolve: context => context.Source.Bids.Select(b => new BidWithAuction(b, context.Source))
            );
            Field<NonNullGraphType<IntGraphType>, int>("startedAtLevel")
                .Description("Block level of the auction's first bid")
                .Resolve(context => Auction.ParseStartedAtLevelFromId(context.Source.Id));
        }
    }
}