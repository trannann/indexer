﻿using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Output.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Auctions;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Output.Auctions
{
    public sealed record BidWithAuction(Bid Bid, Auction Auction);

    public sealed class BidGraphType : OutputGraphType<BidWithAuction>
    {
        public BidGraphType(IQueryService<ReverseRecord> reverseRecordQueryService) : base(description: "Auction bid.")
        {
            this.NodeInterfaceWithFields(
                documentType: typeof(Bid),
                getDocumentId: source =>
                {
                    var bidReverseIndex = source.Auction.Bids.Count - source.Auction.Bids.RequiredIndexOf(source.Bid);
                    return $"{source.Auction.Id}:{bidReverseIndex}";
                }
            );

            Builder.AddressField(h => h.Bid.Bidder, "Bidder address.");
            Builder.MutezField(t => t.Bid.Amount, "Bid amount.");
            Builder.Field(h => h.Bid.Block.Timestamp, "Bid timestamp.");
            Builder.ReverseRecordFieldForAddress(h => h.Bid.Bidder, reverseRecordQueryService);
        }
    }
}