﻿using System.Collections.Generic;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Output.Generic;
using TezosDomains.Data.Models.Auctions;

namespace TezosDomains.Api.GraphQL.Output.Auctions
{
    public sealed record BidderBalanceStatus(
        string Address,
        IReadOnlyList<BidderBalance> Balances
    );

    public sealed class BidderBalancesGraphType : OutputGraphType<BidderBalanceStatus>
    {
        public BidderBalancesGraphType() : base(description: "Bidder balances from unused auctions bids.")
        {
            this.NodeInterfaceWithFields(
                documentType: typeof(BidderBalances),
                getDocumentId: b => b.Address
            );
            Builder.AddressField(x => x.Address, "Reverse record address.");
            Builder.CollectionField<BidderBalance, BalanceGraphType>(x => x.Balances, "User's withdrawable balances by tld registrars.");
        }
    }
}