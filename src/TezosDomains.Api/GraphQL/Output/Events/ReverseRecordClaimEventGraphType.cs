﻿using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query;

namespace TezosDomains.Api.GraphQL.Output.Events
{
    public class ReverseRecordClaimEventGraphType : EventGraphType<ReverseRecordClaimEvent>
    {
        public ReverseRecordClaimEventGraphType(IQueryService<ReverseRecord> reverseRecordQueryService) : base(reverseRecordQueryService)
        {
            Builder.OperationGroupHashField();
            Builder.NullableField(b => b.Name, "The domain name.");
            Builder.AddressField(b => b.ReverseRecordOwnerAddress, "The owner for the bought domain.");
        }
    }
}