﻿using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query;

namespace TezosDomains.Api.GraphQL.Output.Events
{
    public class DomainBuyEventGraphType : EventGraphType<DomainBuyEvent>
    {
        public DomainBuyEventGraphType(IQueryService<ReverseRecord> reverseRecordQueryService) : base(reverseRecordQueryService)
        {
            Builder.OperationGroupHashField();
            Builder.Field(b => b.DomainName, "The domain name.");
            Builder.AddressField(b => b.DomainOwnerAddress, "The owner for the bought domain.");
            Builder.NullableAddressField(b => b.DomainForwardRecordAddress, "The forward record for the bought domain.");
            Builder.RawDataField(b => b.Data, "The data for the bought domain.");
            Builder.Field(b => b.DurationInDays, "The duration of the bought domain.");
            Builder.MutezField(b => b.Price, "The price payed for the domain.");
        }
    }
}