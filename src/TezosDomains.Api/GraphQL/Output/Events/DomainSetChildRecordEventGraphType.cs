﻿using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query;

namespace TezosDomains.Api.GraphQL.Output.Events
{
    public class DomainSetChildRecordEventGraphType : EventGraphType<DomainSetChildRecordEvent>
    {
        public DomainSetChildRecordEventGraphType(IQueryService<ReverseRecord> reverseRecordQueryService) : base(reverseRecordQueryService)
        {
            Builder.OperationGroupHashField();
            Builder.Field(b => b.DomainName, "The domain name.");
            Builder.Field(b => b.IsNewRecord, "When True, new subdomain is created. When False, subdomain was updated by owner of parent domain");
            Builder.AddressField(b => b.DomainOwnerAddress, "The owner for the bought domain.");
            Builder.NullableAddressField(b => b.DomainForwardRecordAddress, "The forward record for the bought domain.");
            Builder.RawDataField(b => b.Data, "The data for the bought domain.");
        }
    }
}