﻿using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Output.Generic;
using TezosDomains.Data.Models.Events;

namespace TezosDomains.Api.GraphQL.Output.Events
{
    public sealed record AuctionParticipant(string Address, AuctionEndEvent AuctionEndEvent);

    public sealed class AuctionParticipantGraphType : OutputGraphType<AuctionParticipant>
    {
        public AuctionParticipantGraphType() : base(description: "An auction participant.")
        {
            this.NodeInterfaceWithFields(
                documentType: typeof(AuctionParticipant),
                getDocumentId: source => $"{source.AuctionEndEvent.Id}:{source.Address}"
            );
            Builder.AddressField(p => p.Address, "Participant's address");
        }
    }
}