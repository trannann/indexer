﻿using GraphQL.Types;
using System;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Api.GraphQL.Output.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Utils;

namespace TezosDomains.Api.GraphQL.Output.Events
{
    public sealed class EventInterfaceType : InterfaceGraphType<Event>
    {
        public EventInterfaceType()
        {
            this.SetNameAndDescription(description: "An action in Tezos Domains describing an interaction with a domain, a reverse record or an auction.");
            this.NodeInterfaceFields(default!);
            this.TezosDocumentInterfaceFields();
            this.EventInterfaceFields(default!, default!);
        }
    }

    public abstract class EventGraphType<TEvent> : DocumentGraphType<TEvent>
        where TEvent : Event
    {
        protected EventGraphType(IQueryService<ReverseRecord> reverseRecordQueryService)
            : base(description: GetEventType().GetDescription())
        {
            Interface<EventInterfaceType>();
            this.EventInterfaceFields(GetEventType(), reverseRecordQueryService);
            this.TezosDocumentInterfaceWithFields();
        }

        private static EventType GetEventType()
        {
            var rawValue = typeof(EventType).GetField(typeof(TEvent).Name)?.GetValue(null);
            return rawValue as EventType?
                   ?? throw new Exception($"Type {typeof(TEvent)} doesn't correspond to any {typeof(EventType)}.");
        }
    }

    public static class EventGraphTypeExtensions
    {
        public static void EventInterfaceFields<TEvent>(
            this ComplexGraphType<TEvent> eventGraphType,
            EventType eventType,
            IQueryService<ReverseRecord> reverseRecordQueryService
        )
            where TEvent : Event
        {
            var builder = eventGraphType.GetBuilder();
            builder.EnumField(name: "Type", description: "Event type.", resolve: _ => eventType);
            builder.AddressField(b => b.SourceAddress, "An address which triggered the event.");
            builder.ReverseRecordFieldForAddress(a => a.SourceAddress, reverseRecordQueryService);
        }

        public static void OperationGroupHashField<TEvent>(this GraphTypeBuilder<TEvent> builder)
            where TEvent : Event, IHasOperationGroupHash
            => builder.Field(e => e.OperationGroupHash, "Operation group hash containing the change.");
    }
}