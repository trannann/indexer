﻿using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query;

namespace TezosDomains.Api.GraphQL.Output.Events
{
    public class AuctionBidEventGraphType : EventGraphType<AuctionBidEvent>
    {
        public AuctionBidEventGraphType(IQueryService<ReverseRecord> reverseRecordQueryService) : base(reverseRecordQueryService)
        {
            Builder.OperationGroupHashField();
            Builder.Field(b => b.DomainName, "The domain name.");
            Builder.MutezField(b => b.BidAmount, "The bid amount.");
            Builder.MutezField(b => b.TransactionAmount, "The amount spent in this transaction.");
            Builder.NullableMutezField(b => b.PreviousBidAmount, "The last bid amount before this bid.");
            Builder.NullableAddressField(b => b.PreviousBidderAddress, "The last bidder address before this bid.");
            Builder.ReverseRecordFieldForAddress(a => a.PreviousBidderAddress, reverseRecordQueryService);
        }
    }
}