﻿using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query;

namespace TezosDomains.Api.GraphQL.Output.Events
{
    public class AuctionWithdrawEventGraphType : EventGraphType<AuctionWithdrawEvent>
    {
        public AuctionWithdrawEventGraphType(IQueryService<ReverseRecord> reverseRecordQueryService) : base(reverseRecordQueryService)
        {
            Builder.OperationGroupHashField();
            Builder.Field(b => b.TldName, "The tld domain name.");
            Builder.MutezField(b => b.WithdrawnAmount, "The withdrawn amount.");
        }
    }
}