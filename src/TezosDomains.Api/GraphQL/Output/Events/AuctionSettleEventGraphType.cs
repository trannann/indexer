﻿using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query;

namespace TezosDomains.Api.GraphQL.Output.Events
{
    public class AuctionSettleEventGraphType : EventGraphType<AuctionSettleEvent>
    {
        public AuctionSettleEventGraphType(IQueryService<ReverseRecord> reverseRecordQueryService) : base(reverseRecordQueryService)
        {
            Builder.OperationGroupHashField();
            Builder.Field(b => b.DomainName, "The domain name.");
            Builder.AddressField(b => b.DomainOwnerAddress, "The owner for the won domain.");
            Builder.NullableAddressField(b => b.DomainForwardRecordAddress, "The forward record for the won domain.");
            Builder.RawDataField(b => b.Data, "The data for the won domain.");
            Builder.MutezField(b => b.WinningBid, "The price payed for the won domain.");
            Builder.Field(
                b => b.RegistrationDurationInDays,
                "The period in days during which the auction winner owns this domain. This includes the period after the auction ended but was not settled yet."
            );
        }
    }
}