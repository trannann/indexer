﻿using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query;

namespace TezosDomains.Api.GraphQL.Output.Events
{
    public class DomainRenewEventGraphType : EventGraphType<DomainRenewEvent>
    {
        public DomainRenewEventGraphType(IQueryService<ReverseRecord> reverseRecordQueryService) : base(reverseRecordQueryService)
        {
            Builder.OperationGroupHashField();
            Builder.Field(b => b.DomainName, "The domain name.");
            Builder.Field(b => b.DurationInDays, "The duration of the renewed domain.");
            Builder.MutezField(b => b.Price, "The price payed for the renewal.");
        }
    }
}