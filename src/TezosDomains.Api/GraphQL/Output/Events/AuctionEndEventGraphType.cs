﻿using System.Linq;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query;

namespace TezosDomains.Api.GraphQL.Output.Events
{
    public sealed class AuctionEndEventGraphType : EventGraphType<AuctionEndEvent>
    {
        public AuctionEndEventGraphType(IQueryService<ReverseRecord> reverseRecordQueryService)
            : base(reverseRecordQueryService)
        {
            Builder.Field(b => b.DomainName, "The domain name.");
            Builder.MutezField(b => b.WinningBid, "The winning bid amount.");
            Builder.CollectionField<AuctionParticipant, AuctionParticipantGraphType>(
                nameof(AuctionEndEvent.Participants),
                "The auction bidders' addresses.",
                context => context.Source.Participants.Select(p => new AuctionParticipant(p, context.Source))
            );
        }
    }
}