﻿using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query;

namespace TezosDomains.Api.GraphQL.Output.Events
{
    public class DomainUpdateEventGraphType : EventGraphType<DomainUpdateEvent>
    {
        public DomainUpdateEventGraphType(IQueryService<ReverseRecord> reverseRecordQueryService) : base(reverseRecordQueryService)
        {
            Builder.OperationGroupHashField();
            Builder.Field(b => b.DomainName, "The domain name.");
            Builder.AddressField(b => b.DomainOwnerAddress, "The owner for the bought domain.");
            Builder.NullableAddressField(b => b.DomainForwardRecordAddress, "The forward record for the bought domain.");
            Builder.RawDataField(b => b.Data, "The data for the bought domain.");
        }
    }
}