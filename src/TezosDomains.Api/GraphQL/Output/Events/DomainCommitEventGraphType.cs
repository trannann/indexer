﻿using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Data.MongoDb.Query;

namespace TezosDomains.Api.GraphQL.Output.Events
{
    public class DomainCommitEventGraphType : EventGraphType<DomainCommitEvent>
    {
        public DomainCommitEventGraphType(IQueryService<ReverseRecord> reverseRecordQueryService) : base(reverseRecordQueryService)
        {
            Builder.OperationGroupHashField();
            Builder.Field(b => b.CommitmentHash, "Commitment hash which allows for the domain to be bought.");
        }
    }
}