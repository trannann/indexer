﻿using System.Collections.Generic;
using TezosDomains.Api.GraphQL.Building;

namespace TezosDomains.Api.GraphQL.Output.Generic
{
    public sealed class DataItemGraphType : OutputGraphType<KeyValuePair<string, string>>
    {
        public DataItemGraphType() : base(description: "An item of arbitrary data associated with the record.")
        {
            Builder.Field(i => i.Key, "The unique key of the data item.");
            Builder.Field(i => i.Value, "The raw value of the data item.").Name("RawValue");
        }
    }
}