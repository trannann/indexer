﻿using GraphQL;
using GraphQL.Types;
using System;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Helpers;

namespace TezosDomains.Api.GraphQL.Output.Generic
{
    public sealed class NodeInterfaceType : InterfaceGraphType
    {
        public NodeInterfaceType()
        {
            this.SetNameAndDescription(description: "Common interface with globally unique ID.");
            this.NodeInterfaceFields(null!);
        }
    }

    public static class NodeInterfaceTypeExtensions
    {
        public static void NodeInterfaceFields<TSource>(
            this ComplexGraphType<TSource> thisGraphType,
            Func<IResolveFieldContext<TSource>, string> resolve
        )
            => thisGraphType.Field<NonNullGraphType<IdGraphType>>(
                name: "id",
                description: "Unique global ID of the object.",
                resolve: resolve
            );

        public static void NodeInterfaceWithFields<TDocument>(this ObjectGraphType<TDocument> thisGraphType)
            where TDocument : IDocument
            => thisGraphType.NodeInterfaceWithFields(typeof(TDocument), d => d.GetId());

        public static void NodeInterfaceWithFields<TSource>(
            this ObjectGraphType<TSource> thisGraphType,
            Type documentType,
            Func<TSource, string> getDocumentId
        )
        {
            thisGraphType.Interface<NodeInterfaceType>();
            thisGraphType.NodeInterfaceFields(
                resolve: context =>
                {
                    var documentId = getDocumentId(context.Source);
                    return $"{documentType.Name}:{documentId}";
                }
            );
        }
    }
}