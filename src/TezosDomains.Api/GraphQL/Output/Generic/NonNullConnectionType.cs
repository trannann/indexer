﻿using GraphQL.Types;
using GraphQL.Types.Relay;
using TezosDomains.Api.GraphQL.Building;

namespace TezosDomains.Api.GraphQL.Output.Generic
{
    /// <summary>
    /// Refactor once this is fixed https://github.com/graphql-dotnet/graphql-dotnet/issues/1790
    /// </summary>
    public sealed class NonNullConnectionType<TNodeType> : ConnectionType<NonNullGraphType<TNodeType>, NonNullEdgeType<TNodeType>>
        where TNodeType : GraphType
    {
        public NonNullConnectionType()
        {
            var graphName = typeof(TNodeType).GetGraphName();
            Name = $"{graphName}Connection";
            Description = $"A connection from an object to a list of objects of type `{graphName}`.";

            // In fact we don't return nulls despite Relay specs https://relay.dev/graphql/connections.htm
            GetField("edges").Type = typeof(NonNullGraphType<ListGraphType<NonNullGraphType<NonNullEdgeType<TNodeType>>>>);
            GetField("items").Type = typeof(NonNullGraphType<ListGraphType<NonNullGraphType<TNodeType>>>);
            GetField("totalCount").Type = typeof(NonNullGraphType<IntGraphType>);
        }
    }

    public sealed class NonNullEdgeType<TNodeType> : EdgeType<NonNullGraphType<TNodeType>>
        where TNodeType : GraphType
    {
        public NonNullEdgeType()
        {
            var graphName = typeof(TNodeType).GetGraphName();
            Name = $"{graphName}Edge";
            Description = $"An edge in a connection from an object to another object of type `{graphName}`.";
        }
    }
}