﻿using GraphQL.Types;
using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Data.Models;

namespace TezosDomains.Api.GraphQL.Output.Generic
{
    public class TezosDocumentInterfaceType : InterfaceGraphType<TezosDocument>
    {
        public TezosDocumentInterfaceType()
        {
            this.SetNameAndDescription(description: "An action in Tezos Domains describing an interaction with a domain or an auction.");
            this.TezosDocumentInterfaceFields();
        }
    }

    public static class TezosDocumentInterfaceTypeExtensions
    {
        public static void TezosDocumentInterfaceFields<TDocument>(this ComplexGraphType<TDocument> sourceGraphType)
            where TDocument : TezosDocument
            => sourceGraphType.GetBuilder().Field<BlockSlim, BlockGraphType>(t => t.Block, "Block in which last change happened.");

        public static void TezosDocumentInterfaceWithFields<TDocument>(this ObjectGraphType<TDocument> thisGraphType)
            where TDocument : TezosDocument
        {
            thisGraphType.Interface<TezosDocumentInterfaceType>();
            thisGraphType.TezosDocumentInterfaceFields();
        }
    }
}