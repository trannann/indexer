﻿using TezosDomains.Api.GraphQL.Building;
using TezosDomains.Data.Models;

namespace TezosDomains.Api.GraphQL.Output.Generic
{
    public abstract class DocumentGraphType<TDocument> : OutputGraphType<TDocument>
        where TDocument : class, IDocument
    {
        protected DocumentGraphType(string description) : base(description)
        {
            IsTypeOf = obj => obj is TDocument;
            this.NodeInterfaceWithFields();
        }
    }
}