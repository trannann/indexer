﻿using GraphQL.Types;
using System.Threading.Tasks;
using TezosDomains.Api.GraphQL.Arguments;
using TezosDomains.Api.GraphQL.Arguments.Generic;
using TezosDomains.Api.GraphQL.Output.Generic;
using TezosDomains.Api.GraphQL.Paging;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Query.Abstract;
using TezosDomains.Data.MongoDb.Query.Filtering;
using TezosDomains.Data.MongoDb.Query.Filtering.Generic;
using TezosDomains.Data.MongoDb.Query.Ordering;

namespace TezosDomains.Api.GraphQL.Output
{
    public sealed class DomainGraphType : DocumentGraphType<Domain>
    {
        public DomainGraphType(
            IQueryService<Domain> domainQueryService,
            IQueryService<ReverseRecord> reverseRecordQueryService,
            IRelayConnectionService connectionService
        ) : base(description: "Domain - forward record.")
        {
            Builder.Field(h => h.Name, "Domain name.");
            Builder.NullableAddressField(h => h.Address, "Domain address.");
            Builder.AddressField(h => h.Owner, "Owner address.");
            Builder.NullableField(h => h.ExpiresAtUtc, "Domain validity.");
            Builder.Field(h => h.Level, "Domain level which is number of segments in its name.");
            Builder.RawDataField(h => h.Data, "Data associated with this domain.");

            var onlyDirectChildrenArg = new GraphQLArgument<bool?, BooleanGraphType>(
                name: "onlyDirectChildren",
                description: "If true then only direct subdomains with level +1 are returned. Otherwise all subdomains are returned. Default is `false`."
            );
            Builder.DocumentConnection<Domain, DomainGraphType>(
                connectionService,
                domainQueryService,
                DomainsOrder.Default,
                name: "subdomains",
                description: "Subdomains of the domain e.g. foo.bar.tez is subdomain of bar.tez.",
                arguments: new[] { onlyDirectChildrenArg },
                createFilter: context => Task.FromResult<IDocumentFilter<Domain>>(
                    new DomainsFilter
                    {
                        Ancestors = new StringArrayFilter { Include = context.Source.Name },
                        Level = onlyDirectChildrenArg.GetValue(context) == true
                            ? new ComparableFilter<int> { EqualTo = context.Source.Level + 1 }
                            : null,
                        Validity = ValidityArgument.Instance.GetParentValue(context),
                        AtBlock = AtBlockArgument.Instance.GetParentValue(context),
                    }
                )
            );

            // Different from addressReverseRecord below b/c it's null if name doesn't correspond.
            Builder.ReverseRecordField(
                name: "reverseRecord",
                description: "Reverse record corresponding to both the address and the name of this domain.",
                resolve: async context =>
                {
                    var record = await reverseRecordQueryService.FindSingleByIdAndParentArguments(context.Source.Address, context);
                    return record?.Name == context.Source.Name ? record : null;
                }
            );

            Builder.ReverseRecordFieldForAddress(d => d.Address, reverseRecordQueryService);
            Builder.ReverseRecordFieldForAddress(d => d.Owner, reverseRecordQueryService);
        }
    }
}