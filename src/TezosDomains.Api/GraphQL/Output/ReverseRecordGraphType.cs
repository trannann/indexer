﻿using TezosDomains.Api.GraphQL.Output.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.MongoDb.Query;

namespace TezosDomains.Api.GraphQL.Output
{
    public sealed class ReverseRecordGraphType : DocumentGraphType<ReverseRecord>
    {
        public ReverseRecordGraphType(IQueryService<Domain> domainQueryService, IQueryService<ReverseRecord> reverseRecordQueryService)
            : base(description: "Reverse record.")
        {
            Builder.AddressField(r => r.Address, "Reverse record address.");
            Builder.NullableField(r => r.Name, "Name of corresponding domain.").DeprecationReason("domain { name } should be used instead");
            Builder.AddressField(r => r.Owner, "Reverse record owner.");
            Builder.NullableField(r => r.ExpiresAtUtc, "Reverse record validity.");
            Builder.ReverseRecordFieldForAddress(r => r.Owner, reverseRecordQueryService);
            Builder.NullableField<Domain, DomainGraphType>(
                name: "domain",
                description: "Domain corresponding to the name field.",
                resolve: context => domainQueryService.FindSingleByIdAndParentArguments(context.Source.Name, context)
            );
        }
    }
}