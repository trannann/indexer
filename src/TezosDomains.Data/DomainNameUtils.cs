﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TezosDomains.Data
{
    public static class DomainNameUtils
    {
        private const string LabelSeparator = ".";

        public static string GetTopLevelDomain(string domain)
        {
            var (label, _, ancestors, _, _) = Parse(domain);
            return ancestors.LastOrDefault() ?? label;
        }

        public static (string Label, string? Parent, string[] Ancestors, string NameReverse, int Level) Parse(string name)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentException("Value cannot be null or empty.", nameof(name));

            var ancestors = new List<string>();
            var labels = name.Split(LabelSeparator);

            ValidateLabels(labels);

            var label = labels[0];
            labels.Reverse()
                .Aggregate(
                    (parent, currentLabel) =>
                    {
                        ancestors.Add(parent);
                        return $"{currentLabel}{LabelSeparator}{parent}";
                    }
                );

            var nameReverse = string.Join(LabelSeparator, labels.Reverse());
            return (label, ancestors.LastOrDefault(), ancestors.ToArray(), nameReverse, labels.Length);
        }

        private static void ValidateLabels(string[] labels)
        {
            if (labels.Length == 0)
                throw new ArgumentException($"Name must contain at least one label");

            if (labels.Any(string.IsNullOrEmpty))
                throw new ArgumentException($"Name must only contain non-empty labels");
        }
    }
}