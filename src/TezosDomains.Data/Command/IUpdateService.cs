﻿using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Updates;

namespace TezosDomains.Data.Command
{
    public interface IUpdateService
    {
        Task StoreUpdatesAsync(Block block, IBlockUpdates updates, Block? previousBlock, CancellationToken cancellationToken);
        Task RollbackAsync(int level, CancellationToken cancellationToken);
    }
}