﻿using System.Threading;
using System.Threading.Tasks;

namespace TezosDomains.Data.Command
{
    public interface IIndexingCommand
    {
        Task ExecuteAsync(CancellationToken ct);
    }
}