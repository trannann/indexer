﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Updates;

namespace TezosDomains.Data.Command
{
    public class WriteBlockCommand : IIndexingCommand
    {
        private readonly IUpdateService _updateService;
        private readonly Block _block;
        private readonly IBlockUpdates _updates;
        private readonly Block? _previousBlock;
        private readonly ILogger _logger;

        public WriteBlockCommand(IUpdateService updateService, ILogger logger, Block block, IBlockUpdates updates, Block? previousBlock)
        {
            _updateService = updateService;
            _block = block;
            _updates = updates;
            _previousBlock = previousBlock;
            _logger = logger;
        }

        public async Task ExecuteAsync(CancellationToken ct)
        {
            _logger.LogDebug(@"Block {blockHash} storing in db STARTED ... ", _block.Hash);
            await _updateService.StoreUpdatesAsync(_block, _updates, _previousBlock, ct);
            _logger.LogInformation(@"Block {blockHash} storing in db FINISHED.", _block.Hash);
        }
    }
}