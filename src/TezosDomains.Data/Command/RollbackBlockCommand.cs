﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace TezosDomains.Data.Command
{
    public class RollbackBlockCommand : IIndexingCommand
    {
        private readonly IUpdateService _updateService;
        private readonly int _level;
        private readonly ILogger _logger;

        public RollbackBlockCommand(IUpdateService updateService, ILogger logger, int level)
        {
            _updateService = updateService;
            _logger = logger;
            _level = level;
        }

        public async Task ExecuteAsync(CancellationToken ct)
        {
            _logger.LogInformation(@"Rollback in db STARTED ... {level}", _level);
            await _updateService.RollbackAsync(_level, ct);
            _logger.LogInformation(@"Rollback in db FINISHED ... {level}", _level);
        }
    }
}