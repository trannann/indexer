﻿using System;

namespace TezosDomains.Data.Models
{
    public class BlockSlim
    {
        public BlockSlim(int level, string hash, DateTime timestamp)
        {
            Level = level;
            Hash = hash;
            Timestamp = timestamp;
        }

        public string Hash { get; }
        public int Level { get; }
        public DateTime Timestamp { get; }
    }
}