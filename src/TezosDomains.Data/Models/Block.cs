﻿using System;

namespace TezosDomains.Data.Models
{
    // intentionally not inherited from BlockSlim - we only want to store Block in Block collection
    public class Block
    {
        public Block(
            int level,
            string hash,
            DateTime timestamp,
            string predecessor
        )
        {
            Predecessor = predecessor;
            Level = level;
            Hash = hash;
            Timestamp = timestamp;
        }

        public string Predecessor { get; }

        public string Hash { get; }
        public int Level { get; }
        public DateTime Timestamp { get; }


        public BlockSlim ToSlim() => new BlockSlim(Level, Hash, Timestamp);
    }
}