﻿using System;

namespace TezosDomains.Data.Models
{
    public interface IRecord : IDocumentWithHistory
    {
        DateTime? ExpiresAtUtc { get; }
        string? ValidityKey { get; }
    }
}