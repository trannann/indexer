﻿using System;
using System.Collections.Generic;

namespace TezosDomains.Data.Models
{
    public sealed class Domain : IRecord
    {
        public Domain(
            string name,
            string? address,
            string owner,
            IReadOnlyDictionary<string, string> data,
            string label,
            IReadOnlyList<string> ancestors,
            string? parent,
            DateTime? expiresAtUtc,
            BlockSlim block,
            string? validityKey,
            int level,
            string nameReverse,
            int? validUntilBlockLevel = null,
            DateTime? validUntilTimestamp = null
        )
        {
            Name = name;
            Address = address;
            Owner = owner;
            ExpiresAtUtc = expiresAtUtc;
            Label = label;
            Ancestors = ancestors;
            Parent = parent;
            Block = block;
            ValidityKey = validityKey;
            Level = level;
            NameReverse = nameReverse;
            ValidUntilBlockLevel = validUntilBlockLevel;
            ValidUntilTimestamp = validUntilTimestamp;
            Data = data;
        }

        public string Name { get; }
        public string NameReverse { get; }
        public string Label { get; }
        public int Level { get; }
        public string? Parent { get; }
        public string Owner { get; }
        public string? Address { get; }
        public string? ValidityKey { get; }
        public DateTime? ExpiresAtUtc { get; }
        public IReadOnlyList<string> Ancestors { get; }
        public int? ValidUntilBlockLevel { get; }
        public DateTime? ValidUntilTimestamp { get; }
        public BlockSlim Block { get; }
        public IReadOnlyDictionary<string, string> Data { get; }
    }
}