﻿using System;

namespace TezosDomains.Data.Models
{
    public interface IDocumentWithHistory : IDocumentWithBlock
    {
        /// <summary>
        /// EXCLUSIVE - Valid until this block level
        /// </summary>
        int? ValidUntilBlockLevel { get; }

        /// <summary>
        /// EXCLUSIVE - Valid until this block timestamp
        /// </summary>
        DateTime? ValidUntilTimestamp { get; }
    }
}