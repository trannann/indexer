﻿using System;

namespace TezosDomains.Data.Models
{
    public sealed class ReverseRecord : IRecord
    {
        public ReverseRecord(
            string address,
            BlockSlim block,
            string? name,
            string owner,
            DateTime? expiresAtUtc,
            string? validityKey,
            int? validUntilBlockLevel = null,
            DateTime? validUntilTimestamp = null
        )
        {
            Address = address;
            Name = name;
            Owner = owner;
            Block = block;
            ExpiresAtUtc = expiresAtUtc;
            ValidityKey = validityKey;
            ValidUntilBlockLevel = validUntilBlockLevel;
            ValidUntilTimestamp = validUntilTimestamp;
        }

        public string Address { get; }
        public BlockSlim Block { get; }
        public string? Name { get; }
        public string Owner { get; }
        public DateTime? ExpiresAtUtc { get; }
        public string? ValidityKey { get; }
        public int? ValidUntilBlockLevel { get; }
        public DateTime? ValidUntilTimestamp { get; }
    }
}