﻿namespace TezosDomains.Data.Models
{
    public interface IDocumentWithBlock : IDocument
    {
        BlockSlim Block { get; }
    }
}