﻿namespace TezosDomains.Data.Models
{
    public class TezosDocument : IDocumentWithBlock
    {
        public TezosDocument(BlockSlim block, string id)
        {
            Id = id;
            Block = block;
        }

        public string Id { get; }
        public BlockSlim Block { get; }
    }
}