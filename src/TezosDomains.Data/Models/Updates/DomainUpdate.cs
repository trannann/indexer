﻿using System.Collections.Generic;

namespace TezosDomains.Data.Models.Updates
{
    public sealed class DomainUpdate
    {
        public DomainUpdate(string name, string? address, string owner, string? validityKey, IReadOnlyDictionary<string, string> data)
        {
            Name = name;
            Address = address;
            Owner = owner;
            ValidityKey = validityKey;
            Data = data;
        }

        public string Name { get; }
        public string Owner { get; }
        public string? Address { get; }
        public string? ValidityKey { get; }
        public IReadOnlyDictionary<string, string> Data { get; }
    }
}