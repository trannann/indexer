﻿namespace TezosDomains.Data.Models.Updates
{
    public sealed class BidderBalanceWithdrawUpdate
    {
        public BidderBalanceWithdrawUpdate(string destinationAddress, string tldName, decimal amount)
        {
            DestinationAddress = destinationAddress;
            Amount = amount;
            TldName = tldName;
        }

        public string DestinationAddress { get; }
        public decimal Amount { get; }
        public string TldName { get; }
    }
}