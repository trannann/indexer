﻿namespace TezosDomains.Data.Models.Updates
{
    public sealed class ReverseRecordUpdate
    {
        public ReverseRecordUpdate(string address, string owner, string? name)
        {
            Address = address;
            Name = name;
            Owner = owner;
        }

        public string Address { get; }
        public string? Name { get; }
        public string Owner { get; }
    }
}