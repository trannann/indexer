﻿using System.Collections.Generic;
using TezosDomains.Data.Models.Events;

namespace TezosDomains.Data.Models.Updates
{
    public interface IBlockUpdates
    {
        IReadOnlyList<DomainUpdate> DomainUpdates { get; }
        IReadOnlyList<ValidityUpdate> ValidityUpdates { get; }
        IReadOnlyList<ReverseRecordUpdate> ReverseRecordUpdates { get; }
        IReadOnlyList<AuctionBidUpdate> BidUpdates { get; }
        IReadOnlyList<string> AuctionSettledUpdates { get; }
        IReadOnlyList<Event> Events { get; }
        IReadOnlyList<BidderBalanceUpdate> BidderBalancesUpdates { get; }
    }
}