﻿using System.Collections.Generic;
using TezosDomains.Data.Models.Events;

namespace TezosDomains.Data.Models.Updates
{
    public class BlockUpdates : IBlockUpdates
    {
        public List<DomainUpdate> DomainUpdates { get; } = new List<DomainUpdate>();
        public List<ValidityUpdate> ValidityUpdates { get; } = new List<ValidityUpdate>();
        public List<ReverseRecordUpdate> ReverseRecordUpdates { get; } = new List<ReverseRecordUpdate>();
        public List<AuctionBidUpdate> AuctionBidUpdates { get; } = new List<AuctionBidUpdate>();
        public List<string> AuctionSettledUpdates { get; } = new List<string>();
        public List<Event> Events { get; } = new List<Event>();
        public List<BidderBalanceUpdate> BidderBalancesUpdates { get; } = new List<BidderBalanceUpdate>();

        IReadOnlyList<DomainUpdate> IBlockUpdates.DomainUpdates => DomainUpdates;
        IReadOnlyList<ValidityUpdate> IBlockUpdates.ValidityUpdates => ValidityUpdates;
        IReadOnlyList<ReverseRecordUpdate> IBlockUpdates.ReverseRecordUpdates => ReverseRecordUpdates;
        IReadOnlyList<AuctionBidUpdate> IBlockUpdates.BidUpdates => AuctionBidUpdates;
        IReadOnlyList<string> IBlockUpdates.AuctionSettledUpdates => AuctionSettledUpdates;
        IReadOnlyList<Event> IBlockUpdates.Events => Events;
        IReadOnlyList<BidderBalanceUpdate> IBlockUpdates.BidderBalancesUpdates => BidderBalancesUpdates;

        public BlockUpdates Merge(IBlockUpdates other)
        {
            DomainUpdates.AddRange(other.DomainUpdates);
            ValidityUpdates.AddRange(other.ValidityUpdates);
            ReverseRecordUpdates.AddRange(other.ReverseRecordUpdates);
            AuctionBidUpdates.AddRange(other.BidUpdates);
            AuctionSettledUpdates.AddRange(other.AuctionSettledUpdates);
            Events.AddRange(other.Events);
            BidderBalancesUpdates.AddRange(other.BidderBalancesUpdates);
            return this;
        }
    }
}