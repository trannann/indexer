﻿using System;

namespace TezosDomains.Data.Models.Updates
{
    public class AuctionBidUpdate
    {
        public AuctionBidUpdate(string domainName, string bidder, decimal amount, DateTime auctionEndInUtc, DateTime ownedUntilUtc)
        {
            Amount = amount;
            DomainName = domainName;
            Bidder = bidder;
            AuctionEndInUtc = auctionEndInUtc;
            OwnedUntilUtc = ownedUntilUtc;
        }

        public decimal Amount { get; }
        public string DomainName { get; }
        public string Bidder { get; }
        public DateTime AuctionEndInUtc { get; }
        public DateTime OwnedUntilUtc { get; }
    }
}