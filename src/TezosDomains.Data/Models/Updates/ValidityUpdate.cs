﻿using System;

namespace TezosDomains.Data.Models.Updates
{
    public class ValidityUpdate
    {
        public ValidityUpdate(string validityKey, DateTime? expiresAtUtc)
        {
            ValidityKey = validityKey;
            ExpiresAtUtc = expiresAtUtc;
        }

        public string ValidityKey { get; }
        public DateTime? ExpiresAtUtc { get; }
    }
}