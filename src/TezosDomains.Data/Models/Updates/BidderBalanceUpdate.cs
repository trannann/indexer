﻿namespace TezosDomains.Data.Models.Updates
{
    public sealed class BidderBalanceUpdate
    {
        public BidderBalanceUpdate(string address, string tldName, decimal? balance)
        {
            Address = address;
            Balance = balance;
            TldName = tldName;
        }

        public string Address { get; }
        public string TldName { get; }
        public decimal? Balance { get; }
    }
}