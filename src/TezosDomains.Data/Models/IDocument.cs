﻿namespace TezosDomains.Data.Models
{
    /// <summary>
    /// Marker interface for MongoDB documents.
    /// </summary>
    public interface IDocument
    {
    }
}