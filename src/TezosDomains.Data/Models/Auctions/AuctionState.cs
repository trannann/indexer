﻿namespace TezosDomains.Data.Models.Auctions
{
    public enum AuctionState
    {
        NotStarted,
        InProgress,
        CanBeSettled,
        Settled,
        SettlementExpired
    }
}