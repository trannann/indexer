﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TezosDomains.Data.Models.Auctions
{
    public sealed class Auction : IDocumentWithHistory
    {
        public Auction(
            string id,
            string domainName,
            IReadOnlyList<Bid> bids,
            DateTime endsAtUtc,
            BlockSlim block,
            int? validUntilBlockLevel,
            DateTime? validUntilTimestamp,
            DateTime ownedUntilUtc,
            bool isSettled,
            DateTime firstBidAtUtc,
            int bidCount
        )
        {
            Id = id;
            DomainName = domainName;
            Bids = bids;
            EndsAtUtc = endsAtUtc;
            Block = block;
            ValidUntilBlockLevel = validUntilBlockLevel;
            ValidUntilTimestamp = validUntilTimestamp;
            OwnedUntilUtc = ownedUntilUtc;
            IsSettled = isSettled;
            FirstBidAtUtc = firstBidAtUtc;
            BidCount = bidCount;
        }

        public string Id { get; }
        public string DomainName { get; }
        public IReadOnlyList<Bid> Bids { get; }
        public DateTime FirstBidAtUtc { get; }
        public DateTime EndsAtUtc { get; }
        public DateTime OwnedUntilUtc { get; }

        public BlockSlim Block { get; }
        public int? ValidUntilBlockLevel { get; }
        public DateTime? ValidUntilTimestamp { get; }
        public bool IsSettled { get; }
        public int BidCount { get; }

        public static string GenerateId(string domainName, int startedAtLevel) => $"{domainName}:{startedAtLevel}";

        public static int ParseStartedAtLevelFromId(string id)
        {
            var segments = id.Split(":");
            if (segments.Length < 2 || !int.TryParse(segments.Last(), out var startedAtLevel))
                throw new ArgumentException($"Id '{id}' is not in correct format {{domainName}}:{{startedAtLevel}}");

            return startedAtLevel;
        }

        public AuctionState GetState(DateTime atUtc)
        {
            if (atUtc < FirstBidAtUtc)
                return AuctionState.NotStarted;

            if (IsSettled)
                return AuctionState.Settled;

            if (atUtc < EndsAtUtc)
                return AuctionState.InProgress;

            if (atUtc < OwnedUntilUtc)
                return AuctionState.CanBeSettled;

            return AuctionState.SettlementExpired;
        }
    }
}