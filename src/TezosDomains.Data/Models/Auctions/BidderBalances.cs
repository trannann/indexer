﻿using System;
using System.Collections.Generic;

namespace TezosDomains.Data.Models.Auctions
{
    public sealed class BidderBalances : IDocumentWithHistory
    {
        public BidderBalances(string address, Dictionary<string, decimal> balances, BlockSlim block, int? validUntilBlockLevel, DateTime? validUntilTimestamp)
        {
            Address = address;
            Balances = balances;
            Block = block;
            ValidUntilBlockLevel = validUntilBlockLevel;
            ValidUntilTimestamp = validUntilTimestamp;
        }

        public string Address { get; }
        public Dictionary<string, decimal> Balances { get; }
        public BlockSlim Block { get; }
        public int? ValidUntilBlockLevel { get; }
        public DateTime? ValidUntilTimestamp { get; }
    }
}