﻿namespace TezosDomains.Data.Models.Auctions
{
    public class Bid : IDocumentWithBlock
    {
        public Bid(BlockSlim block, string bidder, decimal amount)
        {
            Bidder = bidder;
            Amount = amount;
            Block = block;
        }

        public string Bidder { get; }
        public decimal Amount { get; }
        public BlockSlim Block { get; }
    }
}