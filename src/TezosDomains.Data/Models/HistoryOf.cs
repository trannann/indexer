﻿using System.Collections.Generic;

namespace TezosDomains.Data.Models
{
    public class HistoryOf<TDocument> : IDocument
    {
        public HistoryOf(IReadOnlyList<TDocument> history)
        {
            History = history;
        }

        public IReadOnlyList<TDocument> History { get; }
    }
}