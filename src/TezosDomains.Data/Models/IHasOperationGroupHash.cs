﻿namespace TezosDomains.Data.Models
{
    public interface IHasOperationGroupHash
    {
        string OperationGroupHash { get; }
    }
}