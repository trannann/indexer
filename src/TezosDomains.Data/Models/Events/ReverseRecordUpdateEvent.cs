﻿namespace TezosDomains.Data.Models.Events
{
    public class ReverseRecordUpdateEvent : ReverseRecordClaimEvent
    {
        public ReverseRecordUpdateEvent(
            string sourceAddress,
            string? name,
            string reverseRecordOwnerAddress,
            string reverseRecordAddress,
            BlockSlim block,
            string operationGroupHash,
            string id
        ) : base(sourceAddress, name, reverseRecordOwnerAddress, block, operationGroupHash, id)
        {
            ReverseRecordAddress = reverseRecordAddress;
        }

        public string ReverseRecordAddress { get; }
    }
}