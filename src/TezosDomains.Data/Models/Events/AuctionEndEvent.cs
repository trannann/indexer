﻿using System.Collections.Generic;

namespace TezosDomains.Data.Models.Events
{
    public class AuctionEndEvent : Event
    {
        public AuctionEndEvent(
            string domainName,
            string sourceAddress, //winnerAddress
            decimal winningBid,
            IReadOnlyList<string> participants,
            BlockSlim block,
            string id
        ) : base(
            block,
            id,
            sourceAddress
        )
        {
            WinningBid = winningBid;
            Participants = participants;
            DomainName = domainName;
        }

        public decimal WinningBid { get; }
        public string DomainName { get; }
        public IReadOnlyList<string> Participants { get; }
    }
}