﻿using System.Collections.Generic;

namespace TezosDomains.Data.Models.Events
{
    public class DomainSetChildRecordEvent : DomainUpdateEvent
    {
        public DomainSetChildRecordEvent(
            string sourceAddress,
            string domainName,
            string domainOwnerAddress,
            string? domainForwardRecordAddress,
            IReadOnlyDictionary<string, string> data,
            BlockSlim block,
            string operationGroupHash,
            string id,
            bool isNewRecord
        ) : base(
            sourceAddress,
            domainName,
            domainOwnerAddress,
            domainForwardRecordAddress,
            data,
            block,
            operationGroupHash,
            id
        )
        {
            IsNewRecord = isNewRecord;
        }

        public bool IsNewRecord { get; }

        public DomainSetChildRecordEvent Clone(bool isNewRecord) => new DomainSetChildRecordEvent(
            SourceAddress,
            DomainName,
            DomainOwnerAddress,
            DomainForwardRecordAddress,
            Data,
            Block,
            OperationGroupHash,
            Id,
            isNewRecord
        );
    }
}