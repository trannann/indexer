﻿namespace TezosDomains.Data.Models.Events
{
    public class ReverseRecordClaimEvent : Event, IHasOperationGroupHash
    {
        public ReverseRecordClaimEvent(
            string sourceAddress,
            string? name,
            string reverseRecordOwnerAddress,
            BlockSlim block,
            string operationGroupHash,
            string id
        ) : base(
            block,
            id,
            sourceAddress
        )
        {
            Name = name;
            ReverseRecordOwnerAddress = reverseRecordOwnerAddress;
            OperationGroupHash = operationGroupHash;
        }

        public string ReverseRecordOwnerAddress { get; }
        public string? Name { get; }
        public string OperationGroupHash { get; }
    }
}