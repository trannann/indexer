﻿using System.Collections.Generic;

namespace TezosDomains.Data.Models.Events
{
    public class DomainUpdateEvent : Event, IHasOperationGroupHash
    {
        public DomainUpdateEvent(
            string sourceAddress,
            string domainName,
            string domainOwnerAddress,
            string? domainForwardRecordAddress,
            IReadOnlyDictionary<string, string> data,
            BlockSlim block,
            string operationGroupHash,
            string id
        ) : base(
            block,
            id,
            sourceAddress
        )
        {
            DomainName = domainName;
            DomainForwardRecordAddress = domainForwardRecordAddress;
            Data = data;
            DomainOwnerAddress = domainOwnerAddress;
            OperationGroupHash = operationGroupHash;
        }

        public string DomainOwnerAddress { get; }
        public string? DomainForwardRecordAddress { get; }
        public IReadOnlyDictionary<string, string> Data { get; }
        public string DomainName { get; }
        public string OperationGroupHash { get; }
    }
}