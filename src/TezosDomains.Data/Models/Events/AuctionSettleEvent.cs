﻿using System.Collections.Generic;

namespace TezosDomains.Data.Models.Events
{
    public class AuctionSettleEvent : Event, IHasOperationGroupHash
    {
        public AuctionSettleEvent(
            string sourceAddress,
            string domainName,
            string domainOwnerAddress,
            string? domainForwardRecordAddress,
            IReadOnlyDictionary<string, string> data,
            BlockSlim block,
            string operationGroupHash,
            string id,
            int registrationDurationInDays,
            decimal winningBid
        ) : base(
            block,
            id,
            sourceAddress
        )
        {
            DomainName = domainName;
            DomainForwardRecordAddress = domainForwardRecordAddress;
            DomainOwnerAddress = domainOwnerAddress;
            Data = data;
            RegistrationDurationInDays = registrationDurationInDays;
            WinningBid = winningBid;
            OperationGroupHash = operationGroupHash;
        }

        public string? DomainForwardRecordAddress { get; }
        public string DomainOwnerAddress { get; }
        public IReadOnlyDictionary<string, string> Data { get; }
        public string DomainName { get; }
        public int RegistrationDurationInDays { get; }
        public decimal WinningBid { get; }
        public string OperationGroupHash { get; }

        public AuctionSettleEvent Clone(int registrationDurationInDays, decimal winningPrice) => new(
            SourceAddress,
            DomainName,
            DomainOwnerAddress,
            DomainForwardRecordAddress,
            Data,
            Block,
            OperationGroupHash,
            Id,
            registrationDurationInDays,
            winningPrice
        );
    }
}