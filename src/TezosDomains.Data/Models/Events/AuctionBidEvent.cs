﻿namespace TezosDomains.Data.Models.Events
{
    public class AuctionBidEvent : Event, IHasOperationGroupHash
    {
        public AuctionBidEvent(
            string sourceAddress,
            string domainName,
            decimal bidAmount,
            decimal transactionAmount,
            BlockSlim block,
            string operationGroupHash,
            string id,
            string? previousBidderAddress,
            decimal? previousBidAmount
        ) : base(
            block,
            id,
            sourceAddress
        )
        {
            BidAmount = bidAmount;
            TransactionAmount = transactionAmount;
            PreviousBidderAddress = previousBidderAddress;
            PreviousBidAmount = previousBidAmount;
            DomainName = domainName;
            OperationGroupHash = operationGroupHash;
        }

        public decimal BidAmount { get; }
        public decimal? PreviousBidAmount { get; }
        public string? PreviousBidderAddress { get; }
        public decimal TransactionAmount { get; }
        public string DomainName { get; }
        public string OperationGroupHash { get; }

        public AuctionBidEvent Clone(string previousBidderAddress, decimal previousBidAmount) => new AuctionBidEvent(
            SourceAddress,
            DomainName,
            BidAmount,
            TransactionAmount,
            Block,
            OperationGroupHash,
            Id,
            previousBidderAddress,
            previousBidAmount
        );
    }
}