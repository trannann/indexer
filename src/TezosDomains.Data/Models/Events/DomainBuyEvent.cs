﻿using System.Collections.Generic;

namespace TezosDomains.Data.Models.Events
{
    public class DomainBuyEvent : Event, IHasOperationGroupHash
    {
        public DomainBuyEvent(
            string sourceAddress,
            string domainName,
            decimal price,
            int durationInDays,
            string domainOwnerAddress,
            string? domainForwardRecordAddress,
            IReadOnlyDictionary<string, string> data,
            BlockSlim block,
            string operationGroupHash,
            string id
        ) : base(
            block,
            id,
            sourceAddress
        )
        {
            DomainName = domainName;
            DurationInDays = durationInDays;
            Price = price;
            DomainForwardRecordAddress = domainForwardRecordAddress;
            Data = data;
            DomainOwnerAddress = domainOwnerAddress;
            OperationGroupHash = operationGroupHash;
        }

        public int DurationInDays { get; }
        public decimal Price { get; }
        public string DomainOwnerAddress { get; }
        public string? DomainForwardRecordAddress { get; }
        public IReadOnlyDictionary<string, string> Data { get; }
        public string DomainName { get; }
        public string OperationGroupHash { get; }
    }
}