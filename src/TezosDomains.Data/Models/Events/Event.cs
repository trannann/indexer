﻿using System;

namespace TezosDomains.Data.Models.Events
{
    public abstract class Event : TezosDocument
    {
        public static string GenerateId() => Guid.NewGuid().ToString("N");

        protected Event(BlockSlim block, string id, string sourceAddress) : base(block, id)
            => SourceAddress = sourceAddress;

        public string SourceAddress { get; }
    }
}