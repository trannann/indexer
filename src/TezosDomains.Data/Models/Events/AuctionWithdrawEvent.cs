﻿namespace TezosDomains.Data.Models.Events
{
    public class AuctionWithdrawEvent : Event, IHasOperationGroupHash
    {
        public AuctionWithdrawEvent(
            string sourceAddress,
            string tldName,
            decimal withdrawnAmount,
            BlockSlim block,
            string operationGroupHash,
            string id
        ) : base(
            block,
            id,
            sourceAddress
        )
        {
            WithdrawnAmount = withdrawnAmount;
            TldName = tldName;
            OperationGroupHash = operationGroupHash;
        }

        public decimal WithdrawnAmount { get; }
        public string TldName { get; }
        public string OperationGroupHash { get; }
    }
}