﻿using System.ComponentModel;

namespace TezosDomains.Data.Models.Events
{
    [Description("Event types.")]
    public enum EventType
    {
        [Description("An event triggered by user's bid to an auction.")]
        AuctionBidEvent,

        [Description("An event triggered by block which has a timestamp greater or equal to an auction's end.")]
        AuctionEndEvent,

        [Description("An event triggered by user's auction settlement action.")]
        AuctionSettleEvent,

        [Description("An event triggered by user's withdrawal action.")]
        AuctionWithdrawEvent,

        [Description("An event triggered by a user buying a domain.")]
        DomainBuyEvent,

        [Description("An event triggered by a user renewing a domain.")]
        DomainRenewEvent,

        [Description("An event triggered by a user updating domain's data.")]
        DomainUpdateEvent,

        [Description("An event triggered by a user creating a subdomain.")]
        DomainSetChildRecordEvent,

        [Description("An event triggered by a user's commit transaction during domain buying process.")]
        DomainCommitEvent,

        [Description("An event triggered by a user claiming a reverse record.")]
        ReverseRecordClaimEvent,

        [Description("An event triggered by a user updating a reverse record.")]
        ReverseRecordUpdateEvent
    }
}