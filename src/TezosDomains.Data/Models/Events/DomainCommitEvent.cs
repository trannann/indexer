﻿namespace TezosDomains.Data.Models.Events
{
    public class DomainCommitEvent : Event, IHasOperationGroupHash
    {
        public DomainCommitEvent(
            string sourceAddress,
            BlockSlim block,
            string operationGroupHash,
            string id,
            string commitmentHash
        ) : base(
            block,
            id,
            sourceAddress
        )
        {
            CommitmentHash = commitmentHash;
            OperationGroupHash = operationGroupHash;
        }

        public string CommitmentHash { get; }
        public string OperationGroupHash { get; }
    }
}