﻿namespace TezosDomains.Data.Models.Events
{
    public class DomainRenewEvent : Event, IHasOperationGroupHash
    {
        public DomainRenewEvent(
            string sourceAddress,
            string domainName,
            decimal price,
            int durationInDays,
            BlockSlim block,
            string operationGroupHash,
            string id
        ) : base(
            block,
            id,
            sourceAddress
        )
        {
            DomainName = domainName;
            DurationInDays = durationInDays;
            Price = price;
            OperationGroupHash = operationGroupHash;
        }

        public int DurationInDays { get; }
        public decimal Price { get; }
        public string DomainName { get; }
        public string OperationGroupHash { get; }
    }
}