﻿using System;

namespace TezosDomains.Indexer.Tezos.Models.Block
{
    public class TezosBlockHeader
    {
        public int Level { get; set; }
        public int Proto { get; set; }
        public string? Predecessor { get; set; }
        public DateTime Timestamp { get; set; }
    }
}