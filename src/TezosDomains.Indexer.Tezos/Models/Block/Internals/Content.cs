﻿namespace TezosDomains.Indexer.Tezos.Models.Block.Internals
{
    public class Content
    {
        public string? Kind { get; set; }
        public string? Source { get; set; }
        public string? Destination { get; set; }
        public string? Amount { get; set; }
        public OperationParameters? Parameters { get; set; }
    }
}