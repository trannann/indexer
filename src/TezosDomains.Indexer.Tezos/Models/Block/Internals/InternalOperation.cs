﻿namespace TezosDomains.Indexer.Tezos.Models.Block.Internals
{
    public class InternalOperation : Content
    {
        public Result? Result { get; set; }
    }
}