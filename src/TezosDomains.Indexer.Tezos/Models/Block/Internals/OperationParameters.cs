﻿using System.Text.Json;

namespace TezosDomains.Indexer.Tezos.Models.Block.Internals
{
    public class OperationParameters
    {
        public string? Entrypoint { get; set; }
        public JsonElement Value { get; set; }
    }
}