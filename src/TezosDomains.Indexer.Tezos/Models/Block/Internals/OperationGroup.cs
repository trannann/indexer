﻿using System.Text.Json.Serialization;

namespace TezosDomains.Indexer.Tezos.Models.Block.Internals
{
    public class OperationGroup
    {
        public string? Hash { get; set; }

        [JsonPropertyName("contents")]
        public Operation[] Operations { get; set; } = new Operation[0];
    }
}