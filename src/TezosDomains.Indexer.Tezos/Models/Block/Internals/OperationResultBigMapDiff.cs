﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace TezosDomains.Indexer.Tezos.Models.Block.Internals
{
    public class OperationResultBigMapDiff
    {
        [JsonPropertyName("big_map")]
        public string? BigMap { get; set; }

        public string? Action { get; set; }
        public JsonElement Key { get; set; }
        public JsonElement? Value { get; set; }
    }
}