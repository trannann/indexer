﻿using System.Text.Json.Serialization;

namespace TezosDomains.Indexer.Tezos.Models.Block.Internals
{
    public class Result
    {
        public string? Status { get; set; }

        [JsonPropertyName("big_map_diff")]
        public OperationResultBigMapDiff[] BigMapDiffs { get; set; } = new OperationResultBigMapDiff[0];
    }
}