﻿using System.Text.Json.Serialization;
using TezosDomains.Indexer.Tezos.Models.Block.Internals;
using TezosDomains.Indexer.Tezos.Serializers;

namespace TezosDomains.Indexer.Tezos.Models.Block
{
    public class TezosBlock
    {
        public string? Hash { get; set; }
        public TezosBlockHeader Header { get; set; } = new TezosBlockHeader();

        [JsonPropertyName("operations"), JsonConverter(typeof(OperationGroupConverter))]
        public OperationGroup[] OperationGroups { get; set; } = new OperationGroup[0];
    }
}