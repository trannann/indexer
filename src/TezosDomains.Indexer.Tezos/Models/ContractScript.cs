﻿using System.Text.Json;

namespace TezosDomains.Indexer.Tezos.Models
{
    public class ContractScript
    {
        public ContractScript(string address, JsonElement code, JsonElement storage)
        {
            Address = address;
            Code = code;
            Storage = storage;
        }

        public string Address { get; }
        public JsonElement Code { get; }
        public JsonElement Storage { get; }

        public static ContractScript Create(string address, JsonElement contract)
        {
            var scriptElement = contract.GetProperty("script");
            return new ContractScript(address, scriptElement.GetProperty("code"), scriptElement.GetProperty("storage"));
        }
    }
}