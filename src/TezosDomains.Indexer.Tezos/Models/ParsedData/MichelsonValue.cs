﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using TezosDomains.Indexer.Tezos.Encoding;

namespace TezosDomains.Indexer.Tezos.Models.ParsedData
{
    public interface IMichelsonValue
    {
        string GetValueAsString();
        string? GetValueAsNullableString();
        string GetValueAsAddress();
        string? GetValueAsNullableAddress();
        DateTime GetValueAsDateTime();
        int GetValueAsInt();
        decimal GetValueAsMutezDecimal();
        string GetValueAsHexBytesString();
        Dictionary<string, string> GetValueAsMap();
        string GetValueAsBigMapId();
        JsonElement Definition { get; }
    }

    public class MichelsonValue : IMichelsonValue
    {
        public MichelsonValue(JsonElement definition, JsonElement value)
        {
            Definition = definition;
            Value = value;
        }

        public JsonElement Definition { get; }
        public JsonElement Value { get; }

        public string GetValueAsString()
        {
            return ParseString(Definition, Value);
        }

        public string? GetValueAsNullableString()
        {
            var nullable = ParseNullable(Definition, Value);
            if (nullable.HasValue)
                return ParseString(nullable.Value.InnerDefinition, nullable.Value.InnerValue);

            return null;
        }

        public string GetValueAsBigMapId()
        {
            var id = Michelson.IsPrim(Definition, Michelson.Const.Prim.BigMap) && Value.TryGetProperty("int", out var @int)
                ? @int.GetString()
                : null;

            return !string.IsNullOrWhiteSpace(id)
                ? id
                : throw new InvalidOperationException(GenerateInvalidOperationMessage(Definition, Value));
        }

        public string GetValueAsAddress()
        {
            return ParseAddress(Definition, Value);
        }

        public string? GetValueAsNullableAddress()
        {
            var nullable = ParseNullable(Definition, Value);
            if (nullable.HasValue)
                return ParseAddress(nullable.Value.InnerDefinition, nullable.Value.InnerValue);

            return null;
        }

        public DateTime GetValueAsDateTime() => ParseDateTime(Definition, Value);

        public int GetValueAsInt() => ParseInt(Definition, Value);

        public decimal GetValueAsMutezDecimal() => ParseMutezDecimal(Definition, Value);

        public string GetValueAsHexBytesString() => ParseHexBytesString(Definition, Value);


        private decimal ParseMutezDecimal(JsonElement definition, JsonElement value)
        {
            if (Michelson.IsPrim(definition, Michelson.Const.Prim.Money.Mutez)
                && Michelson.TryGetValue(value, Michelson.Const.Type.Int, out var intString))
                return decimal.Parse(intString);

            throw new InvalidOperationException(GenerateInvalidOperationMessage(Definition, Value));
        }

        private int ParseInt(JsonElement definition, JsonElement value)
        {
            if ((Michelson.IsPrim(definition, Michelson.Const.Prim.Int) || Michelson.IsPrim(definition, Michelson.Const.Prim.Nat))
                && Michelson.TryGetValue(value, Michelson.Const.Type.Int, out var intString))
                return int.Parse(intString);

            throw new InvalidOperationException(GenerateInvalidOperationMessage(Definition, Value));
        }

        private DateTime ParseDateTime(JsonElement definition, JsonElement value)
        {
            if (Michelson.IsPrim(definition, Michelson.Const.Prim.Timestamp) && Michelson.TryGetValue(value, Michelson.Const.Type.Int, out var intString))
            {
                return DateTimeOffset.FromUnixTimeSeconds(long.Parse(intString)).UtcDateTime;
            }

            throw new InvalidOperationException(GenerateInvalidOperationMessage(definition, value));
        }

        public Dictionary<string, string> GetValueAsMap()
        {
            if (Michelson.IsPrim(Definition, Michelson.Const.Prim.Map) && Value.ValueKind == JsonValueKind.Array)
            {
                return Value.EnumerateArray()
                    .Select(
                        v =>
                        {
                            if (!Michelson.IsPrim(v, Michelson.Const.Prim.Elt))
                                throw new InvalidOperationException(GenerateInvalidOperationMessage(Definition, Value));

                            return (Key: ParseString(Michelson.GetArg(Definition, 0), Michelson.GetArg(v, 0)),
                                Value: ParseHexBytesString(Michelson.GetArg(Definition, 1), Michelson.GetArg(v, 1)));
                        }
                    )
                    .ToDictionary(v => v.Key, v => v.Value);
            }

            throw new InvalidOperationException(GenerateInvalidOperationMessage(Definition, Value));
        }


        private static string GenerateInvalidOperationMessage(JsonElement definition, JsonElement value)
        {
            return $"Data doesn't correspond to requested value type. {Environment.NewLine}"
                   + $"(Definition:{definition},{Environment.NewLine}"
                   + $"Value:{value})";
        }

        private static string ParseString(JsonElement definition, JsonElement value)
        {
            if (Michelson.IsPrim(definition, Michelson.Const.Prim.Bytes) && Michelson.TryGetValue(value, Michelson.Const.Type.Bytes, out var hexBytesString))
                return TezosEncoding.String.ConvertFromHex(hexBytesString);

            if (Michelson.IsPrim(definition, Michelson.Const.Prim.String) && Michelson.TryGetValue(value, Michelson.Const.Type.String, out var @string))
                return @string;

            throw new InvalidOperationException(
                GenerateInvalidOperationMessage(definition, value)
            );
        }

        private static string ParseHexBytesString(JsonElement definition, JsonElement value)
        {
            if (Michelson.IsPrim(definition, Michelson.Const.Prim.Bytes) && Michelson.TryGetValue(value, Michelson.Const.Type.Bytes, out var hexBytesString))
                return hexBytesString;

            throw new InvalidOperationException(
                GenerateInvalidOperationMessage(definition, value)
            );
        }

        private static (JsonElement InnerDefinition, JsonElement InnerValue)? ParseNullable(JsonElement definition, JsonElement value)
        {
            if (Michelson.IsPrim(definition, Michelson.Const.Prim.Option))
            {
                if (Michelson.IsPrim(value, Michelson.Const.Prim.Some))
                    return (Michelson.GetArg(definition, 0), Michelson.GetArg(value, 0));

                if (Michelson.IsPrim(value, Michelson.Const.Prim.None))
                    return null;
            }

            throw new InvalidOperationException(
                GenerateInvalidOperationMessage(definition, value)
            );
        }

        private string ParseAddress(JsonElement definition, JsonElement value)
        {
            if (Michelson.IsPrim(definition, Michelson.Const.Prim.Address))
            {
                if (Michelson.TryGetValue(value, Michelson.Const.Type.Bytes, out var hexBytesString))
                    return TezosEncoding.Address.ConvertFromHex(hexBytesString);
                if (Michelson.TryGetValue(value, Michelson.Const.Type.String, out var @string))
                    return @string;
            }

            throw new InvalidOperationException(
                GenerateInvalidOperationMessage(definition, value)
            );
        }
    }
}