﻿using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Indexer.Tezos.Clients;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.Indexer.Tezos.Models.Block;

namespace TezosDomains.Indexer.Tezos.Implementations
{
    public class BlockReader : IBlockReader
    {
        private readonly IBlockMonitor _blockMonitor;
        private readonly ITezosClient _tezosClient;
        private readonly ILogger _logger;
        private readonly TezosContractConfiguration _configuration;
        private volatile int _currentLevel;
        private volatile int _lastKnownHead;

        public BlockReader(IBlockMonitor blockMonitor, ITezosClient tezosClient, ILogger<BlockReader> logger, TezosContractConfiguration configuration)
        {
            _blockMonitor = blockMonitor ?? throw new ArgumentNullException(nameof(blockMonitor));
            _tezosClient = tezosClient ?? throw new ArgumentNullException(nameof(tezosClient));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configuration = configuration;
        }

        private async Task<TezosBlock> AdvanceAsync(CancellationToken cancellationToken)
        {
            var newLevel = _currentLevel + 1;
            _logger.LogDebug("Advancing level to {newLevel}", newLevel);
            var block = await _tezosClient.GetBlockAsync(newLevel, cancellationToken);

            // only advance when successful
            _currentLevel = newLevel;
            return block;
        }

        public async Task<TezosBlock> NextBlockAsync(CancellationToken cancellationToken)
        {
            if (_currentLevel <= 0)
            {
                throw new InvalidOperationException("BlockReader set to an invalid level");
            }

            if (!_blockMonitor.IsOpen && _currentLevel == _lastKnownHead)
            {
                var head = await _tezosClient.GetHeadAsync(cancellationToken);
                _lastKnownHead = head.Level;
                _logger.LogDebug("Fetched new head {newLevel}", _lastKnownHead);
            }

            if (_currentLevel >= _lastKnownHead)
            {
                // current level == current head -> use monitor to fetch the new head
                TezosBlockHeader newHead;
                do
                {
                    newHead = await _blockMonitor.WaitForNextAsync(cancellationToken);
                } while (newHead.Level <= _lastKnownHead && !cancellationToken.IsCancellationRequested);

                _lastKnownHead = newHead.Level;
            }

            return await AdvanceAsync(cancellationToken);
        }

        public void Reset(int? currentBlock)
        {
            var block = currentBlock ?? _configuration.StartFromLevel - 1;
            _currentLevel = block;
            _lastKnownHead = block;
        }
    }
}