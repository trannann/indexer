﻿using Microsoft.Extensions.Logging;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Indexer.Tezos.Clients;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.Indexer.Tezos.Layouts;
using TezosDomains.Indexer.Tezos.Models;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Indexer.Tezos.Models.Block.Internals;
using TezosDomains.Indexer.Tezos.Models.ParsedData;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Tezos.Implementations
{
    internal class TezosBlockTraverser : ITezosBlockTraverser
    {
        private readonly IDictionary<string, Contract> _subscribedContracts = new ConcurrentDictionary<string, Contract>();

        private readonly ITezosClient _tezosClient;
        private readonly ILogger _logger;
        private readonly ILayoutFactory _layoutFactory;

        public TezosBlockTraverser(ITezosClient tezosClient, ILogger<TezosBlockTraverser> logger, ILayoutFactory layoutFactory)
        {
            _tezosClient = tezosClient;
            _logger = logger;
            _layoutFactory = layoutFactory;
        }

        public async Task InitializeAsync(IReadOnlyDictionary<string, ContractSubscriptionConfiguration[]> subscriptions, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Loading contract definitions ... STARTED");
            var contractScriptCache = new Dictionary<string, ContractScript>();

            foreach (var contractAddress in subscriptions.Keys)
            foreach (var subscription in subscriptions[contractAddress])
            {
                var contract = await LoadContractDefinition(
                    contractAddress,
                    subscription.ResolveThroughProxy,
                    contractScriptCache,
                    cancellationToken
                );
                _subscribedContracts[contract.Address] = contract;
            }

            _logger.LogInformation("Loading contract definitions ... FINISHED");
        }

        private async Task<Contract> LoadContractDefinition(
            string address,
            bool resolveThroughProxy,
            Dictionary<string, ContractScript> contractScriptCache,
            CancellationToken cancellationToken
        )
        {
            _logger.LogInformation("Loading contract definition {contract} ... STARTED", address);

            // load the initial contract
            var initialContractScript = await LoadContractScript(address, contractScriptCache, cancellationToken);
            var storageProperties = _layoutFactory.CreateStorageLayout(initialContractScript).Apply(initialContractScript.Storage);

            // find the destination address and load the destination contract if needed
            var destinationContractScript = initialContractScript;
            if (storageProperties.ContainsKey(Michelson.Const.Proxy.DestinationContractAddress))
            {
                var destinationContractAddress = storageProperties[Michelson.Const.Proxy.DestinationContractAddress].GetValueAsAddress();
                destinationContractScript = await LoadContractScript(destinationContractAddress, contractScriptCache, cancellationToken);
            }

            // choose the actual contract script
            var contractScript = resolveThroughProxy
                ? destinationContractScript
                : initialContractScript;

            // create contract instance
            var destinationStorageProperties = _layoutFactory.CreateStorageLayout(destinationContractScript).Apply(destinationContractScript.Storage);
            var entrypointLayouts = _layoutFactory.CreateEntrypointLayouts(contractScript.Address, contractScript);
            var bigMapLayouts = _layoutFactory.CreateBigMapLayouts(contractScript);
            var contract = new Contract(bigMapLayouts, entrypointLayouts, destinationStorageProperties, contractScript.Address, address);

            _logger.LogInformation("Loading contract definition {contract} ... FINISHED", address);
            return contract;
        }

        private async Task<ContractScript> LoadContractScript(
            string address,
            Dictionary<string, ContractScript> contractScriptCache,
            CancellationToken cancellationToken
        )
        {
            var contractScript = contractScriptCache.ContainsKey(address)
                ? contractScriptCache[address]
                : contractScriptCache[address] = await _tezosClient.GetContractScriptAsync(address, cancellationToken);
            return contractScript;
        }

        public void TraverseAndNotifySubscribers(TezosBlock block, IReadOnlyDictionary<string, IContractSubscriber[]> subscribers)
        {
            foreach (var operationGroup in block.OperationGroups)
            {
                var operationGroupHash = operationGroup.Hash.GuardNotNull();
                foreach (var operation in operationGroup.Operations)
                {
                    if (operation.Kind != Michelson.Const.Operation.Kind.Transaction
                        || operation.Metadata.GuardNotNull().OperationResult.GuardNotNull().Status != Michelson.Const.OperationResultStatus.Applied)
                        continue;

                    NotifySubscribers(subscribers, block, operation, operationGroupHash, operation.Metadata.GuardNotNull().OperationResult.GuardNotNull());

                    foreach (var internalOperation in operation.Metadata.GuardNotNull().InternalOperations)
                    {
                        if (internalOperation.Kind != Michelson.Const.Operation.Kind.Transaction
                            || internalOperation.Result.GuardNotNull().Status != Michelson.Const.OperationResultStatus.Applied)
                            continue;

                        NotifySubscribers(subscribers, block, internalOperation, operationGroupHash, internalOperation.Result.GuardNotNull());
                    }
                }
            }
        }

        private void NotifySubscribers(
            IReadOnlyDictionary<string, IContractSubscriber[]> subscribers,
            TezosBlock block,
            Content operation,
            string operationGroupHash,
            Result result
        )
        {
            if (_subscribedContracts.TryGetValue(operation.Destination.GuardNotNull(), out var contract))
            {
                var entrypointName = operation.Parameters.GuardNotNull().Entrypoint.GuardNotNull();
                var entrypointLayout = contract.EntrypointLayouts[entrypointName];

                foreach (var subscriber in subscribers[contract.InitialAddress])
                    subscriber.OnEntrypoint(
                        block,
                        operationGroupHash,
                        entrypointName,
                        operation.Source.GuardNotNull(),
                        decimal.Parse(operation.Amount.GuardNotNull()),
                        entrypointLayout.Apply(operation.Parameters.GuardNotNull().Value),
                        contract.DestinationStorageProperties
                    );

                foreach (var bigMapDiff in result.BigMapDiffs)
                {
                    if (bigMapDiff.Action == Michelson.Const.BigMapDiffAction.Update)
                    {
                        var bigMapLayout = contract.BigMapLayouts[bigMapDiff.BigMap.GuardNotNull()];
                        var (key, value) = bigMapLayout.Apply(bigMapDiff);
                        foreach (var subscriber in subscribers[contract.InitialAddress])
                            subscriber.OnBigMapDiff(block, operationGroupHash, bigMapLayout.Name, key, value, contract.DestinationStorageProperties);
                    }
                }
            }

            if (_subscribedContracts.TryGetValue(operation.Source.GuardNotNull(), out contract))
            {
                foreach (var subscriber in subscribers[contract.InitialAddress])
                    subscriber.OnOutgoingTransaction(
                        block,
                        operationGroupHash,
                        operation.Destination.GuardNotNull(),
                        decimal.Parse(operation.Amount.GuardNotNull()),
                        contract.DestinationStorageProperties
                    );
            }
        }

        private class Contract
        {
            public Contract(
                IReadOnlyDictionary<string, BigMapLayout> bigMapLayouts,
                IReadOnlyDictionary<string, RecordLayout> entrypointLayouts,
                IReadOnlyDictionary<string, IMichelsonValue> destinationStorageProperties,
                string address,
                string initialAddress
            )
            {
                BigMapLayouts = bigMapLayouts;
                EntrypointLayouts = entrypointLayouts;
                DestinationStorageProperties = destinationStorageProperties;
                Address = address;
                InitialAddress = initialAddress;
            }

            public IReadOnlyDictionary<string, BigMapLayout> BigMapLayouts { get; }
            public IReadOnlyDictionary<string, RecordLayout> EntrypointLayouts { get; }
            public IReadOnlyDictionary<string, IMichelsonValue> DestinationStorageProperties { get; }
            public string Address { get; }
            public string InitialAddress { get; }
        }
    }
}