﻿using NokitaKaze.Base58Check;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TezosDomains.Indexer.Tezos.Encoding
{
    public static class TezosEncoding
    {
        private static string ToHexString(Span<byte> inArray) => Convert.ToHexString(inArray).ToLowerInvariant();
        private static byte[] FromHexString(string hexString) => Convert.FromHexString(hexString);


        public static class String
        {
            public static string ConvertFromHex(string hexEncodedName) => System.Text.Encoding.UTF8.GetString(FromHexString(hexEncodedName));
        }

        public static class Address
        {
            public static readonly IReadOnlyList<(string Base58, string Hex, string HexPadding, byte[] Bytes)> Prefixes = new[]
            {
                ("tz1", "0000", "", new byte[] { 6, 161, 159 }),
                ("tz2", "0001", "", new byte[] { 6, 161, 161 }),
                ("tz3", "0002", "", new byte[] { 6, 161, 164 }),
                ("KT1", "01", "00", new byte[] { 2, 90, 121 }),
            };

            private const int HexLength = 44;
            private const int SignificantByteLength = 20; // (HexLength - prefix.Hex.Length - prefix.HexPadding.Length) / 2
            public const string Description = "Base58Check-encoded string of length 36 prefixed with tz1 (ed25519), tz2 (secp256k1), tz3 (p256) or KT1.";

            public static string ConvertFromBase58(string base58EncodedAddress)
            {
                if (base58EncodedAddress is null)
                    throw new ArgumentNullException(nameof(base58EncodedAddress));

                try
                {
                    var prefix = Prefixes.FirstOrDefault(p => base58EncodedAddress.StartsWith(p.Base58));
                    if (prefix.Base58 == null)
                        throw new NotSupportedException($"Prefix isn't supported. Supported ones: {string.Join(", ", Prefixes.Select(p => p.Base58))}");

                    var bytes = Base58CheckEncoding.Decode(base58EncodedAddress);
                    var hexValue = ToHexString(bytes.AsSpan().Slice(prefix.Bytes.Length, SignificantByteLength));
                    return prefix.Hex + hexValue + prefix.HexPadding;
                }
                catch (Exception ex)
                {
                    throw new ArgumentException($"Invalid address '{base58EncodedAddress}'. It must be {Description}", nameof(base58EncodedAddress), ex);
                }
            }

            public static string ConvertFromHex(string hexEncodedAddress)
            {
                if (hexEncodedAddress is null)
                    throw new ArgumentNullException(nameof(hexEncodedAddress));

                try
                {
                    if (hexEncodedAddress.Length != HexLength)
                        throw new Exception($"Address must have {HexLength} characters.");

                    var prefix = Prefixes.FirstOrDefault(p => hexEncodedAddress.StartsWith(p.Hex));
                    if (prefix.Base58 == null)
                        throw new NotSupportedException($"Prefix isn't supported. Supported ones: {string.Join(", ", Prefixes.Select(p => p.Hex))}");

                    var hexValue = hexEncodedAddress[prefix.Hex.Length..^prefix.HexPadding.Length];
                    var bytes = prefix.Bytes.Concat(FromHexString(hexValue)).ToArray();
                    return Base58CheckEncoding.Encode(bytes);
                }
                catch (Exception ex)
                {
                    throw new ArgumentException($"Invalid hex-encoded address '{hexEncodedAddress}'.", nameof(hexEncodedAddress), ex);
                }
            }
        }
    }
}