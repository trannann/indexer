﻿using Microsoft.Extensions.DependencyInjection;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.Indexer.Tezos.Implementations;
using TezosDomains.Indexer.Tezos.Layouts;
using TezosDomains.Indexer.Tezos.Services;

namespace TezosDomains.Indexer.Tezos
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddTezos(
            this IServiceCollection services,
            TezosNodeConfiguration tezosNodeConfiguration,
            TezosContractConfiguration tezosContractConfiguration
        )
        {
            services
                .AddHealthChecks()
                .AddCheck<TezosNodeHealthCheck>("TezosNode");

            return services
                    .AddSingleton(tezosNodeConfiguration)
                    .AddSingleton(tezosContractConfiguration)
                    .AddSingleton<ITezosBlockTraverser, TezosBlockTraverser>()
                    .AddSingleton<IBlockReader, BlockReader>()
                    .AddSingleton<ILayoutFactory, LayoutFactory>()
                ;
        }
    }
}