﻿using System;
using System.Linq;
using System.Text.Json;

namespace TezosDomains.Indexer.Tezos
{
    internal static class Michelson
    {
        internal static class Const
        {
            internal const string Args = "args";
            internal const string Annotation = "annots";

            internal static class Proxy
            {
                public const string DestinationContractAddress = "contract";
            }

            internal static class Operation
            {
                internal static class Kind
                {
                    public const string Transaction = "transaction";
                }
            }

            internal static class OperationResultStatus
            {
                public const string Applied = "applied";
            }

            internal static class BigMapDiffAction
            {
                public const string Update = "update";
            }

            internal static class Type
            {
                public const string Int = "int";
                public const string Bytes = "bytes";
                public const string String = "string";
            }

            internal static class Prim
            {
                public const string Map = "map";
                public const string BigMap = "big_map";
                public const string Elt = "Elt";
                public const string Int = "int";
                public const string Nat = "nat";
                public const string Timestamp = "timestamp";
                public const string Bytes = "bytes";
                public const string String = "string";
                public const string Option = "option";
                public const string Some = "Some";
                public const string None = "None";
                public const string Address = "address";
                public const string Pair = "pair";
                public const string Parameter = "parameter";
                public const string Storage = "storage";
                public const string Or = "or";

                internal static class Money
                {
                    public const string Mutez = "mutez";
                }
            }
        }

        public static string? GetAnnotationOrNull(JsonElement element)
        {
            if (element.TryGetProperty(Const.Annotation, out var annotationElement))
            {
                return annotationElement.EnumerateArray()
                    .Select(n => n.GetString() ?? "")
                    .Where(s => s.StartsWith("%"))
                    .Select(s => s[1..])
                    .FirstOrDefault();
            }

            return null;
        }

        public static bool IsPrim(JsonElement element, string prim) =>
            element.ValueKind == JsonValueKind.Object
            && element.TryGetProperty("prim", out var elementPrim)
            && StringEquals(elementPrim, prim);

        public static JsonElement GetArg(JsonElement element, int index) => element.GetProperty(Const.Args)[index];
        public static JsonElement.ArrayEnumerator EnumerateArgs(JsonElement element) => element.GetProperty(Const.Args).EnumerateArray();

        private static bool StringEquals(JsonElement element, string expected)
            => string.Equals(element.GetString(), expected, StringComparison.InvariantCultureIgnoreCase);

        public static bool TryGetValue(JsonElement jsonElement, string type, out string value)
        {
            value = jsonElement.TryGetProperty(type, out var property)
                ? property.GetString() ?? ""
                : "";

            return !string.IsNullOrWhiteSpace(value);
        }
    }
}