﻿using System.Collections.Generic;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Tezos
{
    public interface IContractSubscriber
    {
        void OnEntrypoint(
            TezosBlock block,
            string operationGroupHash,
            string name,
            string source,
            decimal amount,
            IReadOnlyDictionary<string, IMichelsonValue> propertiesByName,
            IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName
        );

        void OnBigMapDiff(
            TezosBlock block,
            string operationGroupHash,
            string name,
            IMichelsonValue keyProperty,
            IReadOnlyDictionary<string, IMichelsonValue> valueProperty,
            IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName
        );

        void OnOutgoingTransaction(
            TezosBlock block,
            string operationGroupHash,
            string destination,
            decimal amount,
            IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName
        );
    }
}