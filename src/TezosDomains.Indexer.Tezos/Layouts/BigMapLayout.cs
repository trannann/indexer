﻿using System.Collections.Generic;
using System.Text.Json;
using TezosDomains.Indexer.Tezos.Models.Block.Internals;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Tezos.Layouts
{
    internal class BigMapLayout
    {
        public string Name { get; }
        public JsonElement KeyDefinition { get; }
        public RecordLayout ValueLayout { get; }

        public BigMapLayout(string name, JsonElement keyDefinition, RecordLayout valueLayout)
        {
            Name = name;
            KeyDefinition = keyDefinition;
            ValueLayout = valueLayout;
        }

        public (MichelsonValue Key, IReadOnlyDictionary<string, IMichelsonValue> Value) Apply(OperationResultBigMapDiff bigMapDiff)
        {
            var valueProperties = bigMapDiff.Value.HasValue
                ? ValueLayout.Apply(bigMapDiff.Value.Value)
                : new Dictionary<string, IMichelsonValue>();
            var key = new MichelsonValue(KeyDefinition, bigMapDiff.Key);
            return (key, valueProperties);
        }
    }
}