﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using TezosDomains.Indexer.Tezos.Models;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Tezos.Layouts
{
    internal class LayoutFactory : ILayoutFactory
    {
        public RecordLayout CreateStorageLayout(ContractScript contractScript)
        {
            var typeProperty = contractScript.Code.EnumerateArray()
                .Single(el => Michelson.IsPrim(el, Michelson.Const.Prim.Storage));
            var storageType = Michelson.GetArg(typeProperty, 0);

            return new RecordLayout(storageType);
        }

        public IReadOnlyDictionary<string, BigMapLayout> CreateBigMapLayouts(ContractScript contractScript)
        {
            var bigMapLayouts = new Dictionary<string, BigMapLayout>();

            foreach (var (_, prop) in CreateStorageLayout(contractScript).Apply(contractScript.Storage))
            {
                if (Michelson.IsPrim(prop.Definition, Michelson.Const.Prim.BigMap))
                {
                    var key = Michelson.GetArg(prop.Definition, 0);
                    var value = Michelson.GetArg(prop.Definition, 1);
                    var bigMapName = Michelson.GetAnnotationOrNull(prop.Definition).GuardNotNull();
                    bigMapLayouts[prop.GetValueAsBigMapId()] = new BigMapLayout(bigMapName, key, new RecordLayout(value));
                }
            }

            return bigMapLayouts;
        }

        public IReadOnlyDictionary<string, RecordLayout> CreateEntrypointLayouts(string address, ContractScript contractScript)
        {
            var parameterProperty = contractScript.Code.EnumerateArray()
                .Single(el => Michelson.IsPrim(el, Michelson.Const.Prim.Parameter));
            var parameterType = Michelson.GetArg(parameterProperty, 0);
            var entrypointLayouts = EnumerateEntrypoints(parameterType)
                .Select(
                    element => (
                        Name: Michelson.GetAnnotationOrNull(element) ?? throw new InvalidOperationException($"null entrypoint at {address}"),
                        Entrypoint: element)
                )
                .ToDictionary(
                    element => element.Name,
                    el => new RecordLayout(el.Entrypoint)
                );
            return entrypointLayouts;
        }

        private static IEnumerable<JsonElement> EnumerateEntrypoints(JsonElement element)
        {
            if (Michelson.IsPrim(element, Michelson.Const.Prim.Or))
            {
                foreach (var arg in Michelson.EnumerateArgs(element))
                foreach (var child in EnumerateEntrypoints(arg))
                {
                    yield return child;
                }
            }
            else
            {
                yield return element;
            }
        }
    }
}