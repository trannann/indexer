﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using TezosDomains.Indexer.Tezos.Clients;

namespace TezosDomains.Indexer.Tezos.Services
{
    public class TezosNodeHealthCheck : IHealthCheck
    {
        private readonly ITezosClient _tezosClient;

        public TezosNodeHealthCheck(ITezosClient tezosClient)
        {
            _tezosClient = tezosClient;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            try
            {
                var version = await _tezosClient.GetNodeVersionAsync(cancellationToken);
                return HealthCheckResult.Healthy($"Tezos Node connection is working. Tezos Node version: {version.Version}");
            }
            catch (Exception e)
            {
                return HealthCheckResult.Unhealthy("Tezos Node connection unhealthy", e);
            }
        }
    }
}