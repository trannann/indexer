﻿using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.Indexer.Tezos.Models;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Indexer.Tezos.Serializers;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Tezos.Clients
{
    public class TezosClient : ITezosClient
    {
        private readonly HttpClient _client;
        private readonly ILogger _logger;

        public TezosClient(HttpClient httpClient, TezosNodeConfiguration configuration, ILogger<TezosClient> logger)
        {
            _logger = logger;
            _client = httpClient;
            _client.BaseAddress = new Uri(configuration.ConnectionString.GuardNotNull());
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<ContractScript> GetContractScriptAsync(string contract, CancellationToken cancellationToken)
        {
            _logger.LogDebug("Getting contract {contract}", contract);
            var result = await GetResponse<JsonElement>($"/chains/main/blocks/head/context/contracts/{contract}", cancellationToken);
            return ContractScript.Create(contract, result);
        }

        private async Task<T> GetResponse<T>(string requestUri, CancellationToken cancellationToken)
            where T : notnull
        {
            var response = await _client.GetAsync(requestUri, cancellationToken);

            var statusCodeInt = (int) response.StatusCode;
            if (statusCodeInt < 200 || statusCodeInt >= 300)
            {
                var contentStr = await TryReadAsStringAsync(response.Content);
                throw new Exception($"Response code {statusCodeInt} {response.StatusCode} from {requestUri} doesn't indicate success. Details: {contentStr}");
            }

            await using var stream = await response.Content.ReadAsStreamAsync(cancellationToken);
            var result = await TezosNodeDeserializer.DeserializeAsync<T>(stream, cancellationToken);
            return result.GuardNotNull($"Null returned from: {requestUri}");
        }

        public Task<NodeVersion> GetNodeVersionAsync(CancellationToken cancellationToken)
        {
            _logger.LogDebug("Getting version...");
            return GetResponse<NodeVersion>("/version", cancellationToken);
        }

        public Task<TezosBlock> GetBlockAsync(int level, CancellationToken cancellationToken)
        {
            _logger.LogDebug($"Getting block {level}");
            return GetResponse<TezosBlock>($"/chains/main/blocks/{level}", cancellationToken);
        }

        public Task<TezosBlockHeader> GetHeadAsync(CancellationToken cancellationToken)
        {
            _logger.LogDebug("Getting the current head");
            return GetResponse<TezosBlockHeader>("/chains/main/blocks/head/header", cancellationToken);
        }

        private static async Task<string> TryReadAsStringAsync(HttpContent content)
        {
            try
            {
                return await content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}