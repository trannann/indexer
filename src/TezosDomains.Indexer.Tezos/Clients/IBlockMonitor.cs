﻿using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Indexer.Tezos.Models.Block;

namespace TezosDomains.Indexer.Tezos.Clients
{
    public interface IBlockMonitor
    {
        Task<TezosBlockHeader> WaitForNextAsync(CancellationToken cancellationToken);
        bool IsOpen { get; }
    }
}