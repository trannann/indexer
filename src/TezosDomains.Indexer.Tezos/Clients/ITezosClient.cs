﻿using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Indexer.Tezos.Models;
using TezosDomains.Indexer.Tezos.Models.Block;

namespace TezosDomains.Indexer.Tezos.Clients
{
    public interface ITezosClient
    {
        Task<TezosBlock> GetBlockAsync(int level, CancellationToken cancellationToken);
        Task<TezosBlockHeader> GetHeadAsync(CancellationToken cancellationToken);
        Task<ContractScript> GetContractScriptAsync(string contract, CancellationToken cancellationToken);
        Task<NodeVersion> GetNodeVersionAsync(CancellationToken cancellationToken);
    }
}