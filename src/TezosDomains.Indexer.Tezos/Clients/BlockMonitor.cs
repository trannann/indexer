﻿using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.Indexer.Tezos.Implementations;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Indexer.Tezos.Serializers;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Tezos.Clients
{
    public class BlockMonitor : IDisposable, IBlockMonitor
    {
        private const string TezosMonitorUriTemplate = "{0}/monitor/heads/main";

        private readonly HttpClient _client;
        private readonly string _tezosMonitorUri;
        private readonly ILogger _logger;
        private volatile StreamReader? _streamReader;
        private volatile HttpResponseMessage? _httpResponse;

        public BlockMonitor(HttpClient client, TezosNodeConfiguration configuration, ILogger<BlockReader> logger)
        {
            _client = client;
            _tezosMonitorUri = string.Format(TezosMonitorUriTemplate, configuration.ConnectionString.GuardNotNull());
            _client.BaseAddress = new Uri(configuration.ConnectionString.GuardNotNull());
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _logger = logger;
        }

        private async Task<StreamReader> GetMonitorStreamReader(CancellationToken cancellationToken)
        {
            if (_streamReader != null)
            {
                return _streamReader;
            }

            _logger.LogDebug($"Opening monitor stream at {_tezosMonitorUri}");
            var request = new HttpRequestMessage(HttpMethod.Get, _tezosMonitorUri);
            try
            {
                _httpResponse = await _client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead, cancellationToken);
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, $"Cannot connect to Tezos Node at {_tezosMonitorUri}");
                throw;
            }

            var stream = await _httpResponse.Content.ReadAsStreamAsync(cancellationToken);
            _streamReader = new StreamReader(stream);
            return _streamReader;
        }

        private static async Task<string> ReadLineAsync(TextReader reader, CancellationToken cancellationToken)
        {
            var lineBuffer = new StringBuilder();
            var readBuffer = new char[1];

            while (await reader.ReadAsync(readBuffer, cancellationToken) > 0)
            {
                if (readBuffer[0] != '\r' && readBuffer[0] != '\n')
                    lineBuffer.Append(readBuffer[0]);
                else if (lineBuffer.Length > 0)
                    return lineBuffer.ToString();
            }

            throw new ApplicationException("HTTP stream ended.");
        }

        public async Task<TezosBlockHeader> WaitForNextAsync(CancellationToken cancellationToken)
        {
            var reader = await GetMonitorStreamReader(cancellationToken);
            var line = await ReadLineAsync(reader, cancellationToken);
            _logger.LogTrace(line);

            var header = TezosNodeDeserializer.Deserialize<TezosBlockHeader>(line).GuardNotNull();
            cancellationToken.ThrowIfCancellationRequested();
            return header;
        }

        public void Dispose()
        {
            _httpResponse?.Dispose();
            _httpResponse = null;
            _streamReader = null;
        }

        public bool IsOpen => _streamReader != null;
    }
}