﻿using System.IO;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Tezos.Serializers
{
    public static class TezosNodeDeserializer
    {
        private static readonly JsonSerializerOptions JsonSerializerOptions = new()
        {
            PropertyNamingPolicy = JsonSnakeCaseNamingPolicy.Instance,
            MaxDepth = int.MaxValue,
        };

        public static T? Deserialize<T>(string str)
            => JsonSerializer.Deserialize<T>(str, JsonSerializerOptions).GuardNotNull();

        public static ValueTask<T?> DeserializeAsync<T>(Stream str, CancellationToken cancellationToken)
            => JsonSerializer.DeserializeAsync<T>(str, JsonSerializerOptions, cancellationToken);
    }
}