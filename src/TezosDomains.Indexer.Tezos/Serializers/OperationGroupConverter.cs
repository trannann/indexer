﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using TezosDomains.Indexer.Tezos.Models.Block.Internals;

namespace TezosDomains.Indexer.Tezos.Serializers
{
    internal class OperationGroupConverter : JsonConverter<OperationGroup[]>
    {
        public override OperationGroup[] Read(
            ref Utf8JsonReader reader,
            Type typeToConvert,
            JsonSerializerOptions options
        )
        {
            //skip first three children arrays of operation array element 
            var childDepth = reader.CurrentDepth + 1;
            var childArrayCounts = 0;
            while (childArrayCounts < 4)
            {
                if (!reader.Read())
                    throw new JsonException();

                if (reader.TokenType == JsonTokenType.StartArray && childDepth == reader.CurrentDepth)
                {
                    childArrayCounts++;
                }
            }

            var operationGroups = JsonSerializer.Deserialize<OperationGroup[]>(ref reader, options);

            if (operationGroups == null || reader.TokenType != JsonTokenType.EndArray || !reader.Read()) //move reader behind EndArray token
                throw new JsonException();

            return operationGroups;
        }

        public override void Write(Utf8JsonWriter writer, OperationGroup[] value, JsonSerializerOptions options)
        {
            throw new InvalidOperationException();
        }
    }
}