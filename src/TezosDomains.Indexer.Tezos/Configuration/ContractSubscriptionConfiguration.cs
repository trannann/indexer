﻿namespace TezosDomains.Indexer.Tezos.Configuration
{
    public class ContractSubscriptionConfiguration
    {
        public string? SubscriberClassName { get; set; }
        public bool ResolveThroughProxy { get; set; }
    }
}