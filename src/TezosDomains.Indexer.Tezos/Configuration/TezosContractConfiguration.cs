﻿using System.Collections.Generic;

namespace TezosDomains.Indexer.Tezos.Configuration
{
    public class TezosContractConfiguration
    {
        public TezosContractConfiguration()
        {
            Subscriptions = new Dictionary<string, ContractSubscriptionConfiguration[]>();
        }

        public int StartFromLevel { get; set; }


        public Dictionary<string, ContractSubscriptionConfiguration[]> Subscriptions { get; set; }
    }
}