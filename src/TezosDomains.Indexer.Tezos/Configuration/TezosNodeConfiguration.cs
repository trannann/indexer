﻿namespace TezosDomains.Indexer.Tezos.Configuration
{
    public class TezosNodeConfiguration
    {
        public string? ConnectionString { get; set; }
    }
}