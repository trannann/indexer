﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.Indexer.Tezos.Models.Block;

namespace TezosDomains.Indexer.Tezos
{
    public interface ITezosBlockTraverser
    {
        Task InitializeAsync(IReadOnlyDictionary<string, ContractSubscriptionConfiguration[]> subscriptions, CancellationToken cancellationToken);
        void TraverseAndNotifySubscribers(TezosBlock block, IReadOnlyDictionary<string, IContractSubscriber[]> subscribers);
    }
}