﻿using TezosDomains.Data.Models.Updates;

namespace TezosDomains.Indexer.Processor
{
    public interface IBlockUpdatesCollector
    {
        IBlockUpdates Updates { get; }
    }
}