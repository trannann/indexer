﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Indexer.Mappers;
using TezosDomains.Indexer.Tezos;
using TezosDomains.Indexer.Tezos.Configuration;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Utils;

namespace TezosDomains.Indexer.Processor
{
    public class BlockProcessor
    {
        private readonly Dictionary<string, ContractSubscriptionConfiguration[]> _subscriptions;
        private readonly ITezosBlockTraverser _tezosBlockTraverser;
        private readonly IServiceProvider _serviceProvider;
        private bool _initialized;

        public BlockProcessor(
            TezosContractConfiguration configuration,
            ITezosBlockTraverser tezosBlockTraverser,
            IServiceProvider serviceProvider
        )
        {
            _tezosBlockTraverser = tezosBlockTraverser;
            _serviceProvider = serviceProvider;
            _subscriptions = configuration.Subscriptions;
            _initialized = false;
        }

        public async Task InitializeAsync(CancellationToken cancellationToken)
        {
            await _tezosBlockTraverser.InitializeAsync(_subscriptions, cancellationToken);

            _initialized = true;
        }

        public (Block block, BlockUpdates blockUpdates) Process(TezosBlock tezosBlock)
        {
            if (!_initialized)
                throw new InvalidOperationException($"BlockProcessor is not initialized. Call {nameof(InitializeAsync)} first.");

            var subscribers = _subscriptions.ToDictionary(
                s => s.Key,
                s => s.Value
                    .Select(
                        sub => (IContractSubscriber) _serviceProvider.GetRequiredService(Type.GetType(sub.SubscriberClassName.GuardNotNull()).GuardNotNull())
                    )
                    .ToArray()
            );
            _tezosBlockTraverser.TraverseAndNotifySubscribers(tezosBlock, subscribers);
            var blockUpdates = subscribers.Values
                .SelectMany(s => s)
                .Distinct()
                .Aggregate(new BlockUpdates(), (u, s) => u.Merge(((IBlockUpdatesCollector) s).Updates));


            var block = BlockMapper.Map(tezosBlock);

            return (block, blockUpdates);
        }
    }
}