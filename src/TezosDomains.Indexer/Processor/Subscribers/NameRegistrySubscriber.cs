﻿using System.Collections.Generic;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Processor.Subscribers
{
    public class NameRegistrySubscriber : BaseSubscriber
    {
        public override void OnBigMapDiff(
            TezosBlock block,
            string operationGroupHash,
            string name,
            IMichelsonValue keyProperty,
            IReadOnlyDictionary<string, IMichelsonValue> valueProperty,
            IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName
        )
        {
            switch (name)
            {
                case "records":
                    var du = new DomainUpdate(
                        keyProperty.GetValueAsString(),
                        valueProperty["address"].GetValueAsNullableAddress(),
                        valueProperty["owner"].GetValueAsAddress(),
                        valueProperty["expiry_key"].GetValueAsNullableString(),
                        valueProperty["data"].GetValueAsMap()
                    );
                    Updates.DomainUpdates.Add(du);
                    break;
                case "reverse_records":
                    var rru = new ReverseRecordUpdate(
                        keyProperty.GetValueAsAddress(),
                        valueProperty["owner"].GetValueAsAddress(),
                        valueProperty["name"].GetValueAsNullableString()
                    );
                    Updates.ReverseRecordUpdates.Add(rru);
                    break;
                case "expiry_map":
                    var vu = new ValidityUpdate(
                        keyProperty.GetValueAsString(),
                        valueProperty.GetValueOrDefault("__value__")?.GetValueAsDateTime()
                    );
                    Updates.ValidityUpdates.Add(vu);
                    break;
            }
        }
    }
}