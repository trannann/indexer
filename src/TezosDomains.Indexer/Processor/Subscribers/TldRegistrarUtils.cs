﻿using System.Collections.Generic;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Processor.Subscribers
{
    internal class TldRegistrarUtils
    {
        public static string CreateDomainName(string domainWithoutTld, IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName) =>
            $"{domainWithoutTld}.{storagePropertiesByName["tld"].GetValueAsString()}";


        public static string ExtractDomainName(
            IReadOnlyDictionary<string, IMichelsonValue> propertiesByName,
            IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName
        )
        {
            var tldName = ExtractTldName(storagePropertiesByName);
            return $"{propertiesByName["label"].GetValueAsString()}.{tldName}";
        }

        public static string ExtractTldName(IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName)
        {
            return storagePropertiesByName["tld"].GetValueAsString();
        }
    }
}