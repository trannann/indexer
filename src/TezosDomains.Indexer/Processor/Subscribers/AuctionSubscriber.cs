﻿using System.Collections.Generic;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Processor.Subscribers
{
    public class AuctionSubscriber : BaseSubscriber
    {
        public override void OnBigMapDiff(
            TezosBlock block,
            string operationGroupHash,
            string name,
            IMichelsonValue keyProperty,
            IReadOnlyDictionary<string, IMichelsonValue> valueProperty,
            IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName
        )
        {
            if (name != "auctions")
                return;

            var domainName = TldRegistrarUtils.CreateDomainName(keyProperty.GetValueAsString(), storagePropertiesByName);

            if (valueProperty.Count == 0)
            {
                // auction removed from the bigmap means it has been settled
                Updates.AuctionSettledUpdates.Add(domainName);
                return;
            }

            //auction bid update
            var endsAtUtc = valueProperty["ends_at"].GetValueAsDateTime();
            Updates.AuctionBidUpdates.Add(
                new AuctionBidUpdate(
                    domainName,
                    valueProperty["last_bidder"].GetValueAsAddress(),
                    valueProperty["last_bid"].GetValueAsMutezDecimal(),
                    endsAtUtc,
                    endsAtUtc.AddDays(valueProperty["ownership_period"].GetValueAsInt())
                )
            );
        }
    }
}