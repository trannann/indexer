﻿using System.Collections.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Processor.Subscribers.Events
{
    public class AuctionBidEntrypointSubscriber : EventEntrypointSubscriber<AuctionBidEvent>
    {
        protected override string EntrypointName => "bid";

        protected override AuctionBidEvent
            MapEvent(
                BlockSlim block,
                string operationGroupHash,
                string sourceAddress,
                decimal amount,
                IReadOnlyDictionary<string, IMichelsonValue> propertiesByName,
                IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName,
                string id
            )
        {
            var domainName = TldRegistrarUtils.ExtractDomainName(propertiesByName, storagePropertiesByName);
            return new AuctionBidEvent(
                sourceAddress,
                domainName,
                propertiesByName["bid"].GetValueAsMutezDecimal(),
                amount,
                block,
                operationGroupHash,
                id,
                default,
                default
            );
        }
    }
}