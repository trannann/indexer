﻿using System.Collections.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Processor.Subscribers.Events
{
    public class ReverseRecordClaimEntrypointSubscriber : EventEntrypointSubscriber<ReverseRecordClaimEvent>
    {
        protected override string EntrypointName => "claim_reverse_record";

        protected override ReverseRecordClaimEvent MapEvent(
            BlockSlim block,
            string operationGroupHash,
            string sourceAddress,
            decimal amount,
            IReadOnlyDictionary<string, IMichelsonValue> propertiesByName,
            IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName,
            string id
        )
        {
            return new ReverseRecordClaimEvent(
                sourceAddress,
                propertiesByName["name"].GetValueAsNullableString(),
                propertiesByName["owner"].GetValueAsAddress(),
                block,
                operationGroupHash,
                id
            );
        }
    }
}