﻿using System.Collections.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Processor.Subscribers.Events
{
    public class DomainSetChildRecordEntrypointSubscriber : EventEntrypointSubscriber<DomainSetChildRecordEvent>
    {
        protected override string EntrypointName => "set_child_record";

        protected override DomainSetChildRecordEvent MapEvent(
            BlockSlim block,
            string operationGroupHash,
            string sourceAddress,
            decimal amount,
            IReadOnlyDictionary<string, IMichelsonValue> propertiesByName,
            IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName,
            string id
        )
        {
            return new DomainSetChildRecordEvent(
                sourceAddress,
                $"{propertiesByName["label"].GetValueAsString()}.{propertiesByName["parent"].GetValueAsString()}",
                propertiesByName["owner"].GetValueAsAddress(),
                propertiesByName["address"].GetValueAsNullableAddress(),
                propertiesByName["data"].GetValueAsMap(),
                block,
                operationGroupHash,
                id,
                isNewRecord: true
            );
        }
    }
}