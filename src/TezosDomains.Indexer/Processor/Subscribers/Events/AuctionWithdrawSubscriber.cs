﻿using System.Collections.Generic;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Mappers;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Processor.Subscribers.Events
{
    /// <summary>
    /// AuctionWithdrawEvent is generated from outgoing transaction from TlDRegistrar contact
    /// It is the only outgoing transaction in TezosDomains contracts
    /// </summary>
    public class AuctionWithdrawSubscriber : BaseSubscriber
    {
        public override void OnOutgoingTransaction(
            TezosBlock block,
            string operationGroupHash,
            string destination,
            decimal amount,
            IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName
        )
        {
            Updates.Events.Add(
                new AuctionWithdrawEvent(
                    destination,
                    TldRegistrarUtils.ExtractTldName(storagePropertiesByName),
                    amount,
                    BlockMapper.MapSlim(block),
                    operationGroupHash,
                    Event.GenerateId()
                )
            );
        }
    }
}