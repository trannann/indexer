﻿using System.Collections.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Processor.Subscribers.Events
{
    public class AuctionSettleEntrypointSubscriber : EventEntrypointSubscriber<AuctionSettleEvent>
    {
        protected override string EntrypointName => "settle";

        protected override AuctionSettleEvent MapEvent(
            BlockSlim block,
            string operationGroupHash,
            string sourceAddress,
            decimal amount,
            IReadOnlyDictionary<string, IMichelsonValue> propertiesByName,
            IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName,
            string id
        )
        {
            var domainName = TldRegistrarUtils.ExtractDomainName(propertiesByName, storagePropertiesByName);
            return new AuctionSettleEvent(
                sourceAddress,
                domainName,
                propertiesByName["owner"].GetValueAsAddress(),
                propertiesByName["address"].GetValueAsNullableAddress(),
                propertiesByName["data"].GetValueAsMap(),
                block,
                operationGroupHash,
                id,
                registrationDurationInDays: default,
                winningBid: default
            );
        }
    }
}