﻿using System.Collections.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Processor.Subscribers.Events
{
    public class DomainCommitEntrypointSubscriber : EventEntrypointSubscriber<DomainCommitEvent>
    {
        protected override string EntrypointName => "commit";

        protected override DomainCommitEvent MapEvent(
            BlockSlim block,
            string operationGroupHash,
            string sourceAddress,
            decimal amount,
            IReadOnlyDictionary<string, IMichelsonValue> propertiesByName,
            IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName,
            string id
        )
        {
            return new DomainCommitEvent(
                sourceAddress,
                block,
                operationGroupHash,
                id,
                propertiesByName["commit"].GetValueAsHexBytesString()
            );
        }
    }
}