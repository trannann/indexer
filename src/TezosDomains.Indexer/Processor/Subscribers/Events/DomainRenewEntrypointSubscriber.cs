﻿using System.Collections.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Processor.Subscribers.Events
{
    public class DomainRenewEntrypointSubscriber : EventEntrypointSubscriber<DomainRenewEvent>
    {
        protected override string EntrypointName => "renew";

        protected override DomainRenewEvent MapEvent(
            BlockSlim block,
            string operationGroupHash,
            string sourceAddress,
            decimal amount,
            IReadOnlyDictionary<string, IMichelsonValue> propertiesByName,
            IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName,
            string id
        )
        {
            var domainName = TldRegistrarUtils.ExtractDomainName(propertiesByName, storagePropertiesByName);
            return new DomainRenewEvent(
                sourceAddress,
                domainName,
                amount,
                propertiesByName["duration"].GetValueAsInt(),
                block,
                operationGroupHash,
                id
            );
        }
    }
}