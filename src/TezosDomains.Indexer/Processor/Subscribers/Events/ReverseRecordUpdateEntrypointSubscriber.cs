﻿using System.Collections.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Processor.Subscribers.Events
{
    public class ReverseRecordUpdateEntrypointSubscriber : EventEntrypointSubscriber<ReverseRecordUpdateEvent>
    {
        protected override string EntrypointName => "update_reverse_record";

        protected override ReverseRecordUpdateEvent MapEvent(
            BlockSlim block,
            string operationGroupHash,
            string sourceAddress,
            decimal amount,
            IReadOnlyDictionary<string, IMichelsonValue> propertiesByName,
            IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName,
            string id
        )
        {
            return new ReverseRecordUpdateEvent(
                sourceAddress,
                propertiesByName["name"].GetValueAsNullableString(),
                propertiesByName["owner"].GetValueAsAddress(),
                propertiesByName["address"].GetValueAsAddress(),
                block,
                operationGroupHash,
                id
            );
        }
    }
}