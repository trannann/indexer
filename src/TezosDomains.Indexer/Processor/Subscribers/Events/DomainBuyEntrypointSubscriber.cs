﻿using System.Collections.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Processor.Subscribers.Events
{
    public class DomainBuyEntrypointSubscriber : EventEntrypointSubscriber<DomainBuyEvent>
    {
        protected override string EntrypointName => "buy";

        protected override DomainBuyEvent MapEvent(
            BlockSlim block,
            string operationGroupHash,
            string sourceAddress,
            decimal amount,
            IReadOnlyDictionary<string, IMichelsonValue> propertiesByName,
            IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName,
            string id
        )
        {
            var domainName = TldRegistrarUtils.ExtractDomainName(propertiesByName, storagePropertiesByName);
            return new DomainBuyEvent(
                sourceAddress,
                domainName,
                amount,
                propertiesByName["duration"].GetValueAsInt(),
                propertiesByName["owner"].GetValueAsAddress(),
                propertiesByName["address"].GetValueAsNullableAddress(),
                propertiesByName["data"].GetValueAsMap(),
                block,
                operationGroupHash,
                id
            );
        }
    }
}