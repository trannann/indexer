﻿using System.Collections.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Mappers;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Processor.Subscribers.Events
{
    public abstract class EventEntrypointSubscriber<TEvent> : BaseSubscriber
        where TEvent : Event
    {
        protected abstract string EntrypointName { get; }

        public sealed override void OnEntrypoint(
            TezosBlock block,
            string operationGroupHash,
            string name,
            string source,
            decimal amount,
            IReadOnlyDictionary<string, IMichelsonValue> propertiesByName,
            IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName
        )
        {
            if (EntrypointName != name)
                return;

            Updates.Events.Add(
                MapEvent(
                    BlockMapper.MapSlim(block),
                    operationGroupHash,
                    source,
                    amount,
                    propertiesByName,
                    storagePropertiesByName,
                    Event.GenerateId()
                )
            );
        }

        protected abstract TEvent MapEvent(
            BlockSlim block,
            string operationGroupHash,
            string sourceAddress,
            decimal amount,
            IReadOnlyDictionary<string, IMichelsonValue> propertiesByName,
            IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName,
            string id
        );
    }
}