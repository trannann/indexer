﻿using System.Collections.Generic;
using TezosDomains.Data.Models;
using TezosDomains.Data.Models.Events;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Processor.Subscribers.Events
{
    public class DomainUpdateEntrypointSubscriber : EventEntrypointSubscriber<DomainUpdateEvent>
    {
        protected override string EntrypointName => "update_record";

        protected override DomainUpdateEvent MapEvent(
            BlockSlim block,
            string operationGroupHash,
            string sourceAddress,
            decimal amount,
            IReadOnlyDictionary<string, IMichelsonValue> propertiesByName,
            IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName,
            string id
        )
        {
            return new DomainUpdateEvent(
                sourceAddress,
                propertiesByName["name"].GetValueAsString(),
                propertiesByName["owner"].GetValueAsAddress(),
                propertiesByName["address"].GetValueAsNullableAddress(),
                propertiesByName["data"].GetValueAsMap(),
                block,
                operationGroupHash,
                id
            );
        }
    }
}