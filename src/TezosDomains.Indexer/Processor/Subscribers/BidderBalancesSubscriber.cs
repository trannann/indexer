﻿using System.Collections.Generic;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Processor.Subscribers
{
    public class BidderBalancesSubscriber : BaseSubscriber
    {
        public override void OnBigMapDiff(
            TezosBlock block,
            string operationGroupHash,
            string name,
            IMichelsonValue keyProperty,
            IReadOnlyDictionary<string, IMichelsonValue> valueProperty,
            IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName
        )
        {
            if (name != "bidder_balances")
                return;

            Updates.BidderBalancesUpdates.Add(
                new BidderBalanceUpdate(
                    keyProperty.GetValueAsAddress(),
                    TldRegistrarUtils.ExtractTldName(storagePropertiesByName),
                    valueProperty.GetValueOrDefault("__value__")?.GetValueAsMutezDecimal()
                )
            );
        }
    }
}