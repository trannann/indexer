﻿using System.Collections.Generic;
using TezosDomains.Data.Models.Updates;
using TezosDomains.Indexer.Tezos;
using TezosDomains.Indexer.Tezos.Models.Block;
using TezosDomains.Indexer.Tezos.Models.ParsedData;

namespace TezosDomains.Indexer.Processor.Subscribers
{
    public abstract class BaseSubscriber : IContractSubscriber, IBlockUpdatesCollector
    {
        protected readonly BlockUpdates Updates = new BlockUpdates();

        public virtual void OnEntrypoint(
            TezosBlock block,
            string operationGroupHash,
            string name,
            string source,
            decimal amount,
            IReadOnlyDictionary<string, IMichelsonValue> propertiesByName,
            IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName
        )
        {
        }

        public virtual void OnBigMapDiff(
            TezosBlock block,
            string operationGroupHash,
            string name,
            IMichelsonValue keyProperty,
            IReadOnlyDictionary<string, IMichelsonValue> valueProperty,
            IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName
        )
        {
        }

        public virtual void OnOutgoingTransaction(
            TezosBlock block,
            string operationGroupHash,
            string destination,
            decimal amount,
            IReadOnlyDictionary<string, IMichelsonValue> storagePropertiesByName
        )
        {
        }

        IBlockUpdates IBlockUpdatesCollector.Updates => Updates;
    }
}