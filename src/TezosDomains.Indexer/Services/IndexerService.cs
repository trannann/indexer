﻿using System;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using TezosDomains.Data.Command;
using TezosDomains.Data.MongoDb.Query;
using TezosDomains.Data.MongoDb.Services;
using TezosDomains.Indexer.Processor;
using TezosDomains.Indexer.Tezos;

namespace TezosDomains.Indexer.Services
{
    public class IndexerService : BackgroundService
    {
        private static readonly TimeSpan CacheExpiration = TimeSpan.FromHours(1);

        private readonly ILogger _logger;
        private readonly IBlockQueryService _blockQueryService;
        private readonly IHostApplicationLifetime _appLifetime;
        private readonly IUpdateService _updateService;
        private readonly IndexerDistributedLockService _indexerDistributedLockService;
        private readonly IMemoryCache _blockCache;
        private readonly BlockProcessor _blockProcessor;
        private readonly IBlockReader _blockReader;

        public IndexerService(
            ILogger<IndexerService> logger,
            IBlockQueryService blockQueryService,
            IHostApplicationLifetime appLifetime,
            IUpdateService updateService,
            IndexerDistributedLockService indexerDistributedLockService,
            IMemoryCache blockCache,
            BlockProcessor blockProcessor,
            IBlockReader blockReader
        )
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _blockQueryService = blockQueryService ?? throw new ArgumentNullException(nameof(blockQueryService));
            _appLifetime = appLifetime ?? throw new ArgumentNullException(nameof(appLifetime));
            _updateService = updateService ?? throw new ArgumentNullException(nameof(updateService));
            _indexerDistributedLockService = indexerDistributedLockService ?? throw new ArgumentNullException(nameof(indexerDistributedLockService));
            _blockCache = blockCache ?? throw new ArgumentNullException(nameof(blockCache));
            _blockProcessor = blockProcessor;
            _blockReader = blockReader;
        }

        protected override async Task ExecuteAsync(CancellationToken token)
        {
            _logger.LogInformation("Indexer Service is starting.");

            try
            {
                var cts = CancellationTokenSource.CreateLinkedTokenSource(token);
                var cancellationToken = cts.Token;

                await _blockProcessor.InitializeAsync(cancellationToken);

                await _indexerDistributedLockService.AcquireAsync(cts);

                var lastIndexedLevel = await _blockQueryService.GetLatestLevelOrNullAsync(cancellationToken);
                _blockReader.Reset(lastIndexedLevel);

                var channel = Channel.CreateBounded<IIndexingCommand>(100);

                var producerTask = RunTaskWithCancellationOrError(
                    cts,
                    async (ct) =>
                    {
                        var block = await _blockReader.NextBlockAsync(ct);
                        var blockLevel = block.Header.Level;
                        var previousBlockLevel = blockLevel - 1;
                        var knownPredecessor = await _blockCache.GetOrCreateAsync(
                            previousBlockLevel,
                            async cacheEntry =>
                            {
                                cacheEntry.SlidingExpiration = CacheExpiration;
                                var previousBlock = await _blockQueryService.FindAsync(previousBlockLevel, ct);
                                return previousBlock;
                            }
                        );
                        if (knownPredecessor == null || block.Header.Predecessor == knownPredecessor.Hash)
                        {
                            var (blockData, updates) = _blockProcessor.Process(block);
                            await channel.Writer.WriteAsync(
                                new WriteBlockCommand(
                                    _updateService,
                                    _logger,
                                    blockData,
                                    updates,
                                    knownPredecessor
                                ),
                                ct
                            );
                            _logger.LogInformation(@"Block {blockHash} (level:{level}) added to processing queue.", block.Hash, blockLevel, block);
                            _blockCache.Set(blockLevel, blockData, CacheExpiration);
                        }
                        else
                        {
                            await channel.Writer.WriteAsync(new RollbackBlockCommand(_updateService, _logger, previousBlockLevel), ct);
                            _logger.LogWarning(
                                @"Rollback of {blockHash} (level:{level}) added to processing queue. Caused by {@block}",
                                knownPredecessor,
                                previousBlockLevel,
                                block
                            );
                            _blockReader.Reset(blockLevel - 2);
                        }
                    },
                    "producer"
                );

                var consumerTask = RunTaskWithCancellationOrError(
                    cts,
                    async (ct) =>
                    {
                        var command = await channel.Reader.ReadAsync(ct);
                        _indexerDistributedLockService.EnsureLockActive();
                        await command.ExecuteAsync(ct);
                    },
                    "consumer"
                );

                await Task.WhenAll(producerTask, consumerTask);
            }
            catch (Exception ex)
            {
                if (token.IsCancellationRequested)
                    _logger.LogDebug("Indexer stopping. Cancellation requested from ASP.NET Core.");
                else
                    _logger.LogCritical(ex, "Indexing failed. Tezos Monitor Service is stopping.");
            }
            finally
            {
                _appLifetime.StopApplication();
                _logger.LogInformation("Indexer Service stopped.");
            }
        }

        private Task RunTaskWithCancellationOrError(CancellationTokenSource cts, Func<CancellationToken, Task> function, string taskName)
        {
            return Task.Run(
                async () =>
                {
                    try
                    {
                        while (!cts.Token.IsCancellationRequested)
                            await function(cts.Token);
                    }
                    catch (Exception e)
                    {
                        if (cts.IsCancellationRequested)
                        {
                            _logger.LogDebug($"Task {taskName} cancelled");
                        }
                        else
                        {
                            _logger.LogError(e, $"Task {taskName} failed. Cancelling cancellationToken");
                            cts.Cancel();
                        }

                        throw;
                    }
                }
            );
        }
    }
}