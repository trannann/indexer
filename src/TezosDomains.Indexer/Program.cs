using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;
using TezosDomains.Common;

namespace TezosDomains.Indexer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            LoggerInit.CreateLogger("TezosDomains.Indexer");
            var logger = Log.ForContext<Program>();

            try
            {
                logger.Information("Starting up. Version: {version}", AppVersionInfo.Version);
                CreateHostBuilder(args).Build().Run();
                logger.Information("Shutting down");
            }
            catch (Exception ex)
            {
                logger.Fatal(ex, "Application start-up failed");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(params string[] args)
            => Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}