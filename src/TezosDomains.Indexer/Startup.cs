using System.Net.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using TezosDomains.Common;
using TezosDomains.Data.MongoDb;
using TezosDomains.Data.MongoDb.Configurations;
using TezosDomains.Indexer.Processor;
using TezosDomains.Indexer.Processor.Subscribers;
using TezosDomains.Indexer.Processor.Subscribers.Events;
using TezosDomains.Indexer.Services;
using TezosDomains.Indexer.Tezos;
using TezosDomains.Indexer.Tezos.Clients;
using TezosDomains.Indexer.Tezos.Configuration;

namespace TezosDomains.Indexer
{
    public class Startup
    {
        private readonly IWebHostEnvironment _env;

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            _env = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var tezosNodeConfiguration = Configuration.GetSection("TezosDomains:TezosNode").Get<TezosNodeConfiguration>();
            var tezosContractConfiguration = Configuration.GetSection("TezosDomains:TezosContract").Get<TezosContractConfiguration>();

            services.AddOptions();
            services.AddHttpClient<ITezosClient, TezosClient>();
            services.AddHttpClient<IBlockMonitor, BlockMonitor>()
                .ConfigurePrimaryHttpMessageHandler(
                    () =>
                        new HttpClientHandler { AllowAutoRedirect = false, MaxAutomaticRedirections = 20 }
                );
            services.AddControllers();
            services.AddMemoryCache();

            services.AddTezos(tezosNodeConfiguration, tezosContractConfiguration);
            services.AddMongoDb(_env, Configuration.GetSection("TezosDomains:MongoDb").Get<MongoDbConfiguration>());
            services.AddTransient<BidderBalancesSubscriber>();
            services.AddTransient<AuctionSubscriber>();
            services.AddTransient<NameRegistrySubscriber>();
            services.AddTransient<AuctionBidEntrypointSubscriber>();
            services.AddTransient<AuctionSettleEntrypointSubscriber>();
            services.AddTransient<AuctionWithdrawSubscriber>();
            services.AddTransient<DomainBuyEntrypointSubscriber>();
            services.AddTransient<DomainRenewEntrypointSubscriber>();
            services.AddTransient<DomainCommitEntrypointSubscriber>();
            services.AddTransient<DomainSetChildRecordEntrypointSubscriber>();
            services.AddTransient<DomainUpdateEntrypointSubscriber>();
            services.AddTransient<ReverseRecordUpdateEntrypointSubscriber>();
            services.AddTransient<ReverseRecordClaimEntrypointSubscriber>();
            services.AddSingleton<BlockProcessor>();

            services.AddHostedService<IndexerService>();

            services.AddHealthChecks().AddVersion();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, MongoDbInitializer dbInitializer)
        {
            dbInitializer.Initialize();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSerilogRequestLogging();
            app.UseSecurityHeaders(Configuration);
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseHealth();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}