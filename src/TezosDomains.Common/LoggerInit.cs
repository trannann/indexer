﻿using System;
using System.Diagnostics;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Exceptions;

namespace TezosDomains.Common
{
    public static class LoggerInit
    {
        public static void CreateLogger(string applicationName)
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("serilog.json")
                .AddJsonFile($"serilog.{environmentName}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

            var process = Process.GetCurrentProcess();
            var loggerConfig = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .Enrich.WithExceptionDetails()
                .Enrich.WithProperty("Commit", AppVersionInfo.ShortGitHash)
                .Enrich.WithProperty("Version", AppVersionInfo.Version)
                .Enrich.WithProperty("Application", applicationName)
                .Enrich.WithProperty("Machine", Environment.MachineName)
                .Enrich.WithProperty("Process", process.Id.ToString())
                .Enrich.WithEnvironment(environmentName)
                .Enrich.With(new HttpContextLogEnricher(new HttpContextAccessor()));

            Log.Logger = loggerConfig.CreateLogger();
        }
    }
}