﻿using Microsoft.AspNetCore.Http;
using Serilog.Core;
using Serilog.Events;
using System.Collections.Generic;
using System.Linq;

namespace TezosDomains.Common
{
    public sealed class HttpContextLogEnricher : ILogEventEnricher
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public HttpContextLogEnricher(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            var httpRequest = _httpContextAccessor.HttpContext?.Request;
            if (httpRequest == null) return;

            var values = new Dictionary<string, string?>
            {
                { "Method", httpRequest.Method },
                { "Scheme", httpRequest.Scheme },
                { "Host", httpRequest.Host.ToString() },
                { "Path", httpRequest.PathBase + httpRequest.Path },
                { "Query", httpRequest.QueryString.ToString() },
                { "ClientIp", ResolveClientIpAddress(httpRequest.HttpContext) },
            };

            logEvent.AddOrUpdateProperty(propertyFactory.CreateProperty("Http", values));
        }

        private static string? ResolveClientIpAddress(HttpContext httpContext)
        {
            var xForwardedFor = httpContext.Request.Headers["X-Forwarded-For"].ToString();
            var clientIp = xForwardedFor
                .Split(",")
                .Select(x => x.Trim())
                .FirstOrDefault(x => !string.IsNullOrWhiteSpace(x));

            return clientIp ?? httpContext.Connection.RemoteIpAddress?.ToString();
        }
    }
}