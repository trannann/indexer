﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace TezosDomains.Common
{
    public class VersionHealthCheck : IHealthCheck
    {
        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken)
        {
            return Task.FromResult(
                HealthCheckResult.Healthy(
                    data: new Dictionary<string, object>
                    {
                        ["version"] = AppVersionInfo.Version,
                        ["commit"] = AppVersionInfo.ShortGitHash
                    }
                )
            );
        }
    }
}