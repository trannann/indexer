﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace TezosDomains.Common
{
    public sealed class SecurityHeadersMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly string[] _contentSecurityPolicyExcludedPaths;

        public SecurityHeadersMiddleware(RequestDelegate next, string[] contentSecurityPolicyExcludedPaths)
        {
            _next = next;
            _contentSecurityPolicyExcludedPaths = contentSecurityPolicyExcludedPaths;
        }

        public Task InvokeAsync(HttpContext context)
        {
            // Tell browsers to connect using HTTPS, preload = not even try HTTP
            Set("Strict-Transport-Security", "max-age=63072000; includeSubDomains; preload");

            // Restrict assets except GraphQL playground
            if (!_contentSecurityPolicyExcludedPaths.Contains(context.Request.Path.ToString()))
                Set("Content-Security-Policy", "default-src 'none';");

            // Disable including in an iframe
            Set("X-Frame-Options", "SAMEORIGIN");

            // Only content-types explicitly declared by the app can be used -> disable client guessing
            Set("X-Content-Type-Options", "nosniff");

            // Don't send referrer to other sites
            Set("Referrer-Policy", "same-origin");

            // https://developer.mozilla.org/en-US/docs/Web/HTTP/Feature_Policy/Using_Feature_Policy#Enforcing_best_practices_for_good_user_experiences
            Set(
                "Permissions-Policy",
                "layout-animations 'none'; unoptimized-images 'none'; oversized-images 'none'; sync-script 'none'; sync-xhr 'none'; unsized-media 'none';"
            );

            void Set(string name, string value)
                => context.Response.Headers[name] = value;

            return _next.Invoke(context);
        }
    }
}