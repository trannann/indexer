﻿using System;
using System.Linq;
using System.Net.Mime;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Newtonsoft.Json;

namespace TezosDomains.Common
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseHealth(this IApplicationBuilder builder) => builder.UseHealthChecks(
            "/health",
            new HealthCheckOptions
            {
                ResponseWriter = async (context, report) =>
                {
                    var result = JsonConvert.SerializeObject(
                        new
                        {
                            status = report.Status.ToString(),
                            duration = report.TotalDuration,
                            entries = report.Entries.ToDictionary(
                                k => k.Key,
                                e => new
                                {
                                    description = e.Value.Description,
                                    duration = e.Value.Duration,
                                    status = Enum.GetName(typeof(HealthStatus), e.Value.Status),
                                    data = e.Value.Data,
                                    error = e.Value.Exception
                                }
                            )
                        },
                        Formatting.Indented,
                        new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }
                    );
                    context.Response.ContentType = MediaTypeNames.Application.Json;
                    await context.Response.WriteAsync(result);
                }
            }
        );
    }
}