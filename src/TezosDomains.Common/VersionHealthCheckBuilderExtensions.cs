﻿using Microsoft.Extensions.DependencyInjection;

namespace TezosDomains.Common
{
    public static class VersionHealthCheckBuilderExtensions
    {
        public static IHealthChecksBuilder AddVersion(this IHealthChecksBuilder builder)
            => builder.AddCheck<VersionHealthCheck>("Version");
    }
}