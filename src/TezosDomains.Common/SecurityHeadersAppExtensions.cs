﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;

namespace TezosDomains.Common
{
    public static class SecurityHeadersAppExtensions
    {
        public static void UseSecurityHeaders(this IApplicationBuilder app, IConfiguration config, params string[] contentSecurityPolicyExcludedPaths)
        {
            if (!config.GetSection("DisableSecurityHeaders").Get<bool>())
                app.UseMiddleware<SecurityHeadersMiddleware>((object) contentSecurityPolicyExcludedPaths);
        }
    }
}